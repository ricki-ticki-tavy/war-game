package config;

import api.core.database.service.PlayerWarriorService;
import api.core.database.service.UserService;
import org.mockito.Mockito;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.*;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.mail.internet.MimeMessage;

import static org.mockito.Mockito.when;

@Configuration
@ComponentScan(basePackages = {"core", "api", "web", "tests"})
@EnableTransactionManagement
@ActiveProfiles("test")
public class TestContextConfiguration {
  @Bean
  @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
  @Profile("mockDb")
  PlayerWarriorService getPlayerWarriorService(){
    return Mockito.mock(PlayerWarriorService.class);
  }

  @Bean
  @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
  @Profile("mockDb")
  UserService getUserService(){
    return Mockito.mock(UserService.class);
  }

  @Bean
  @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
  @Profile("mockMail")
  JavaMailSender getJavaMailSender(){
    JavaMailSender javaMailSender = Mockito.mock(JavaMailSender.class);
    when(javaMailSender.createMimeMessage()).thenReturn(Mockito.mock(MimeMessage.class));
    return javaMailSender;
  }

  @Bean
  @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
  @Profile("test")
  SimpMessagingTemplate getSimpMessagingTemplate(){
    SimpMessagingTemplate simpMessagingTemplate = Mockito.mock(SimpMessagingTemplate.class);
    return simpMessagingTemplate;
  }

}
