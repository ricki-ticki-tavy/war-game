package abstracts;

import api.core.Result;
import api.game.wraper.GameWrapper;
import core.entity.magic.SpellLighting;
import org.springframework.util.Assert;
import tests.core.entity.artifact.*;
import tests.core.entity.magic.*;
import tests.core.entity.warrior.*;
import tests.core.entity.weapons.*;

/**
 * Класс-предок базовой карты теста. Регистрирует тестовых воинов, магию, артефакты, оружие
 */
public abstract class AbstractRegistratorTest {
  public void initMap(GameWrapper gameWrapper) {
    // регистрируем классы воинов
    gameWrapper.getCore().registerWarriorBaseClass(TestSkeleton.class);
    gameWrapper.getCore().registerWarriorBaseClass(TestViking.class);
    gameWrapper.getCore().registerWarriorBaseClass(TestVityaz.class);
    gameWrapper.getCore().registerWarriorBaseClass(TestArmoredVityaz.class);
    gameWrapper.getCore().registerWarriorBaseClass(TestArmoredSkeleton.class);
    gameWrapper.getCore().registerWarriorBaseClass(TestUnluckyVityaz.class);
    gameWrapper.getCore().registerWarriorBaseClass(TestUnluckyVityaz.class);
    gameWrapper.getCore().registerWarriorBaseClass(TestVerwolfShaman.class);

    // регистрируем оружие
    gameWrapper.getCore().registerWeaponClass(TestBow.classInfo);
    gameWrapper.getCore().registerWeaponClass(TestShortSword.classInfo);
    gameWrapper.getCore().registerWeaponClass(TestFireBowOfRejuvenation.classInfo);
    gameWrapper.getCore().registerWeaponClass(TestFixedSword.classInfo);
    gameWrapper.getCore().registerWeaponClass(TestFixedBow.classInfo);
    gameWrapper.getCore().registerWeaponClass(TestSmallShield.classInfo);
    gameWrapper.getCore().registerWeaponClass(TestGreatSmallShield.classInfo);
    gameWrapper.getCore().registerWeaponClass(TestFixedBowOfPoison.classInfo);

    // регистрируем артефакт для воина
    gameWrapper.getCore().registerArtifactForWarriorClass(TestArtifactRainbowArrowForWarrior.CLASS_NAME, TestArtifactRainbowArrowForWarrior.class);
    gameWrapper.getCore().registerArtifactForWarriorClass(TestArtifactEmptyForWarrior.CLASS_NAME, TestArtifactEmptyForWarrior.class);

    // регистрируем артефакты для игрока
    gameWrapper.getCore().registerArtifactForPlayerClass(TestArtifactHealinFialForPlayer.classInfo);
    gameWrapper.getCore().registerArtifactForPlayerClass(TestArtifactHugeHummerForPlayer.classInfo);
    gameWrapper.getCore().registerArtifactForPlayerClass(TestArtifactCapitansSabreForPlayer.classInfo);

    // Регистрируем заклинание игрока
    gameWrapper.getCore().registerSpellForPlayerClass(SpellLighting.spellinfo.getName(), SpellLighting.spellinfo);
    gameWrapper.getCore().registerSpellForPlayerClass(TestSpellCheckNoParameters.spellinfo.getName(), TestSpellCheckNoParameters.spellinfo);
    gameWrapper.getCore().registerSpellForPlayerClass(TestSpellCheckParameters.spellinfo.getName(), TestSpellCheckParameters.spellinfo);
    gameWrapper.getCore().registerSpellForPlayerClass(TestSpellPoisonLightingForPlayer.spellinfo.getName(), TestSpellPoisonLightingForPlayer.spellinfo);
    gameWrapper.getCore().registerSpellForPlayerClass(TestSpellHealingForPlayer.spellinfo.getName(), TestSpellHealingForPlayer.spellinfo);

    // регистрируем не даваемое никому заклинание
    gameWrapper.getCore().registerSpellForWarriorClass(TestSpellNotRegisteredForWarrior.spellinfo.getName(), TestSpellNotRegisteredForWarrior.spellinfo);

  }

  protected String extractError(Result result) {
    return result.isFail() ? result.getError().getMessage() : "";
  }

  protected void assertSuccess(Result operatioinResult) {
    String msg = operatioinResult.isFail() ? operatioinResult.getError().getMessage() : "";
    Assert.isTrue(operatioinResult.isSuccess(), msg);
  }


}
