package abstracts;

import api.core.Context;
import api.core.Result;
import api.entity.warrior.Warrior;
import api.game.map.Player;
import api.game.map.metadata.GameRules;
import api.game.wraper.GameWrapper;
import api.geo.Coords;
import core.entity.map.GameRulesImpl;
import org.assertj.core.util.Arrays;
import tests.core.entity.warrior.TestSkeleton;
import tests.core.entity.warrior.TestViking;
import tests.core.entity.warrior.TestVityaz;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс авторизци двух пользователей, загрузка карты, создание по 2 невооруженных воина
 */
public abstract class AbstractMapTest extends AbstractRegistratorTest {

  protected String player1;
  protected String player2;
  protected String player3;
  protected String gameContext;
  protected String warrior1p1;
  protected String warrior2p1;
  protected String warrior1p2;
  protected String warrior2p2;
  protected String warrior1p3;
  protected String warrior2p3;

  private int playersCount = 0;
  private Coords warriorCoords[][] = new Coords[][]{
          new Coords[]{new Coords(200, 400), new Coords(800, 400)}
          , new Coords[]{new Coords(200, 600), new Coords(800, 600)}
          , new Coords[]{new Coords(50, 500), new Coords(600, 500)}};

  private String[] defaultWarriorClasses = new String[]{TestVityaz.classInfo.getName()
          , TestViking.classInfo.getName()
          , TestSkeleton.classInfo.getName()
          , TestSkeleton.classInfo.getName()
          , TestSkeleton.classInfo.getName()
          , TestSkeleton.classInfo.getName()};


  private String warriorNames[] = new String[]{"Гоша", "Кеша", "Каша"};

  private void innerInitMap(String mapName, String playerName, String... warriorClasses) {

  }

  private void initAnyMap(String mapName, GameWrapper gameWrapper, String playerNames[], String... warriorClasses_) {

    playersCount = 0;

    super.initMap(gameWrapper);

    List<String> additionalPlayers = new ArrayList();

    java.util.Arrays.stream(playerNames)
            .forEach(playerName -> {
              Result<Player> playerResult = gameWrapper.login(playerName, 0L);
              assertSuccess(playerResult);
            });

    GameRules rules = new GameRulesImpl(2, 2, 1, 50, 200, 2, 600, 30);
    Result<Context> contextResult = gameWrapper.createGame(playerNames[0], rules, mapName, "test-game2", false);
    assertSuccess(contextResult);
    gameContext = contextResult.getResult().getContextId();

    java.util.Arrays.stream(playerNames)
            .forEach(playerName -> {

              String[] warriorClasses = Arrays.isNullOrEmpty(warriorClasses_) || warriorClasses_.length < 6 ? new String[6] : warriorClasses_;

              if (Arrays.isNullOrEmpty(warriorClasses_) || warriorClasses_.length < 6) {
                int index = 0;
                if (!Arrays.isNullOrEmpty(warriorClasses_)) {
                  for (index = 0; index < warriorClasses_.length; index++){
                    warriorClasses[index] = warriorClasses_[index];
                  }
                }
                while (index < 6){
                  warriorClasses[index] = defaultWarriorClasses[index++];
                }
              }

              String playerId = playerName;

              if (playersCount > 0) {
                Result<Player> playerResult = gameWrapper.connectToGame(playerId, gameContext);
                assertSuccess(playerResult);
              }

              switch (playersCount) {
                case 0:
                  player1 = playerId;
                  break;
                case 1:
                  player2 = playerId;
                  break;
                case 2:
                  player3 = playerId;
                  break;
              }

              if (playersCount > 0) {
                additionalPlayers.add(playerId);
              }

              Result<Warrior> warriorResult = gameWrapper.createWarrior(gameContext, playerId, warriorClasses[playersCount * 2]
                      , warriorCoords[playersCount][0]);
              assertSuccess(warriorResult);
              warriorResult.getResult().setTitle(warriorNames[playersCount] + " 1");

              switch (playersCount) {
                case 0:
                  warrior1p1 = warriorResult.getResult().getId();
                  break;
                case 1:
                  warrior1p2 = warriorResult.getResult().getId();
                  break;
                case 2:
                  warrior1p3 = warriorResult.getResult().getId();
                  break;
              }

              warriorResult = gameWrapper.createWarrior(gameContext, playerId, warriorClasses[playersCount * 2 + 1]
                      , warriorCoords[playersCount][1]);
              assertSuccess(warriorResult);
              warriorResult.getResult().setTitle(warriorNames[playersCount] + " 2");

              switch (playersCount) {
                case 0:
                  warrior2p1 = warriorResult.getResult().getId();
                  break;
                case 1:
                  warrior2p2 = warriorResult.getResult().getId();
                  break;
                case 2:
                  warrior2p3 = warriorResult.getResult().getId();
                  break;
              }


              playersCount++;

            });

    additionalPlayers.stream()
            .forEach(playerId -> assertSuccess(gameWrapper.connectToGame(playerId, gameContext)));


  }

  public void initMapForTwoPlayers(GameWrapper gameWrapper, String player1Name, String player2Name, String... warriorClasses) {

    initAnyMap("testMap.xml", gameWrapper, new String[]{player1Name, player2Name}, warriorClasses);
  }

  public void initMapForThreePlayers(GameWrapper gameWrapper, String player1Name, String player2Name, String player3Name, String... warriorClasses) {
    initAnyMap("testLevel3.xml", gameWrapper, new String[]{player1Name, player2Name, player3Name}, warriorClasses);
  }
}
