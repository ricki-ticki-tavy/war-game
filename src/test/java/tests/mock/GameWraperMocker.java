package tests.mock;

import api.game.wraper.GameWrapper;
import core.system.error.GameErrors;
import org.mockito.Mockito;

import static core.system.ResultImpl.fail;
import static core.system.ResultImpl.success;
import static org.mockito.Mockito.when;

/**
 * Mock для lkz gameWrapper
 */
public class GameWraperMocker {
  public static GameWrapper getMockedGameWrapper() {
    GameWrapper mockedGameWrapper = Mockito.mock(GameWrapper.class);
    when(mockedGameWrapper.createTechnicalContext("admin")).thenReturn(success("1234-56789-1234-567890"));
    when(mockedGameWrapper.createTechnicalContext("")).thenReturn(fail(GameErrors.SYSTEM_NOT_AUTHORIZED.getError()));

    return mockedGameWrapper;
  }
}
