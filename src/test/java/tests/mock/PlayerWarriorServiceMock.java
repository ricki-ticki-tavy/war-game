package tests.mock;

import api.core.Result;
import api.core.database.service.PlayerWarriorService;
import api.entity.warrior.Warrior;
import api.game.map.Player;
import core.database.entity.warrior.PlayerWarrior;
import core.database.entity.warrior.WarriorEquipment;

import java.util.List;

/**
 * мок сервиса операций с воинами игрока
 */
public class PlayerWarriorServiceMock implements PlayerWarriorService {
  @Override
  public Result<List<PlayerWarrior>> findAllHeroes(String userName) {
    return null;
  }

  @Override
  public Result<PlayerWarrior> findHeroByName(String userName, String warriorName) {
    return null;
  }

  @Override
  public Result<WarriorEquipment> saveWarriorEquipment(Player player, Warrior warrior, String warriorEquipmentName) {
    return null;
  }

  @Override
  public Result<WarriorEquipment> renameWarriorEquipment(Player player, Warrior warrior, String oldEquipmentName, String newEquipmentName) {
    return null;
  }

  @Override
  public Result<WarriorEquipment> removeWarriorEquipment(Player player, Warrior warrior, String equipmentName) {
    return null;
  }
}
