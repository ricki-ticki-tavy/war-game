package tests.database;

import abstracts.AbstractMapTest;
import api.core.Context;
import api.core.Result;
import api.core.database.service.PlayerWarriorService;
import api.entity.artifct.Artifact;
import api.entity.warrior.Warrior;
import api.entity.weapon.Weapon;
import api.game.wraper.GameWrapper;
import api.geo.Coords;
import boot.config.DatabaseConfiguration;
import boot.config.security.SecurityConfig;
import config.TestContextConfiguration;
import core.database.entity.warrior.PlayerWarrior;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.util.Assert;
import tests.core.entity.artifact.TestArtifactEmptyForWarrior;
import tests.core.entity.artifact.TestArtifactRainbowArrowForWarrior;
import tests.core.entity.warrior.TestArmoredSkeleton;
import tests.core.entity.weapons.TestShortSword;

import static tests.Consts.USER_NAME;
import static tests.Consts.USER_PASSWORD;

/**
 * Проверка Пользователей и авторизации
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {AuthorityAndDatabaseTest.class})
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = {TestContextConfiguration.class, DatabaseConfiguration.class, SecurityConfig.class})
@TestPropertySource(locations = {"classpath:application.yml"})
@ActiveProfiles({"mockMail", "test"})
public class AuthorityAndDatabaseTest extends AbstractMapTest {
  public static final String USER_BAD_NAME = "admin12";
  public static final String USER_BAD_PASSWORD = "121212";

  @Autowired
  GameWrapper gameWrapper;

  @Autowired
  SessionFactory sessionFactory;

  @Autowired
  AuthenticationProvider authenticationProvider;

  @Autowired
  PlayerWarriorService playerWarriorService;

  private Context gameContext;


  public AuthorityAndDatabaseTest innerDoLoginTest() {
    try {
      authenticationProvider.authenticate(new UsernamePasswordAuthenticationToken(USER_BAD_NAME, USER_PASSWORD, null));
      Assert.isTrue(false, "Авторизация не верным пользователем прошла");
    } catch (Throwable th) {
      Assert.isTrue(th instanceof BadCredentialsException, "Авторизация администратором не прошла");
    }

    try {
      authenticationProvider.authenticate(new UsernamePasswordAuthenticationToken(USER_NAME, USER_BAD_PASSWORD, null));
      Assert.isTrue(false, "Авторизация не верным пользователем прошла");
    } catch (Throwable th) {
      Assert.isTrue(th instanceof BadCredentialsException, "Авторизация администратором не прошла");
    }

    try {
      authenticationProvider.authenticate(new UsernamePasswordAuthenticationToken(USER_NAME, USER_PASSWORD, null));
    } catch (Throwable th) {
      Assert.isTrue(false, "Авторизация администратором не прошла");
    }

    return this;
  }

  public AuthorityAndDatabaseTest innerDoMapTest() {
    initMap(gameWrapper);

    // Создадим технический контекст
    Result<Context> contextResult = gameWrapper.createTechnicalContext(USER_NAME);
    assertSuccess(contextResult);
    gameContext = contextResult.getResult();

    // Создадим воина на технической карте
    Result<Warrior> warriorResult = gameWrapper.createWarrior(
            gameContext.getContextId()
            , player1
            , TestArmoredSkeleton.classInfo.getName()
            , new Coords(200, 400));
    assertSuccess(warriorResult);
    warriorResult.getResult().setTitle("Гоша 1");
    warrior1p1 = warriorResult.getResult().getId();

    // Дадим воину 1 игрока 1 меч
    Result<Weapon> weaponResult = gameWrapper.giveWeaponToWarrior(gameContext.getContextId(), player1, warrior1p1, TestShortSword.classInfo.getName());
    assertSuccess(weaponResult);
    String sword1Warrior1p1 = weaponResult.getResult().getId();

    // Дадим воину 1 игрока 1 второй меч
    weaponResult = gameWrapper.giveWeaponToWarrior(gameContext.getContextId(), player1, warrior1p1, TestShortSword.classInfo.getName());
    assertSuccess(weaponResult);
    String sword2Warrior1p1 = weaponResult.getResult().getId();
    Warrior warrior1p1Impl = (Warrior) weaponResult.getResult().getOwner();
    // проверим, что меча 2
    Assert.isTrue(warrior1p1Impl.getWeapons().size() == 2, String.format("Воин 1 игрока 1 имеет не 2 меча (%s)", warrior1p1Impl.getWeapons().size()));

    // Дадим воину артефакты
    Result<Artifact<Warrior>> artifactResult = gameWrapper.giveArtifactToWarrior(gameContext.getContextId(), player1, warrior1p1, TestArtifactRainbowArrowForWarrior.CLASS_NAME);
    assertSuccess(artifactResult);

    // сохраним в базу
    warriorResult = gameWrapper.saveWarriorAmmunition(gameContext.getContextId(), USER_NAME, warrior1p1, "test1");
    assertSuccess(warriorResult);
    // проверим, что в базе есть эти записи
    Result<PlayerWarrior> playerWarriorResult = playerWarriorService.findHeroByName(player1, warrior1p1Impl.getTitle());
    assertSuccess(playerWarriorResult);
    Assert.isTrue(playerWarriorResult.getResult() != null, "Не найденазапись с воином");

    // Еще дадим воину артефакт
    artifactResult = gameWrapper.giveArtifactToWarrior(gameContext.getContextId(), player1, warrior1p1, TestArtifactEmptyForWarrior.CLASS_NAME);
    assertSuccess(artifactResult);

    // Еще раз сохраним в базу
    warriorResult = gameWrapper.saveWarriorAmmunition(gameContext.getContextId(), USER_NAME, warrior1p1, "test1");
    assertSuccess(warriorResult);
    // проверим, что в базе есть эти записи
    playerWarriorResult = playerWarriorService.findHeroByName(player1, warrior1p1Impl.getTitle());
    assertSuccess(playerWarriorResult);
    Assert.isTrue(playerWarriorResult.getResult() != null, "Не найденазапись с воином");
    // проверим, что там именно то, что надо
    // TODO тут выйдет далее тест ни о чем. так как преобращовать в JSON и обратно и сверить объект - смысла нет
//    Assert.isTrue(playerWarriorResult.getResult().getWarriorEquipments()
//                    .toArray(new WarriorEquipment[]{})[0].getSerializedJson()
//                    .equals("{\"weapons\":[{\"id\":\"WepShSwd_7fc078e8-f0ca-4b63-af7b-06c3e4f76247\",\"title\":\"Тестовый короткий меч\",\"abilities\":[]},{\"id\":\"WepShSwd_cfa76147-0a4f-47d6-b9f8-14fcc9f2c840\",\"title\":\"Тестовый короткий меч\",\"abilities\":[]}],\"warriorBaseClass\":\"Тестовый бронированный скелет\",\"artifacts\":[{\"id\":\"Art_Emp_024ad8f8-e7bd-46eb-ac81-9f5d0ff42f77\",\"title\":\"Сачок\",\"description\":\"Сачок\",\"abilities\":[]},{\"id\":\"Art_WRA_ee1cee12-6fe6-430a-8204-cff1b4197477\",\"title\":\"Золотая стрела удачи 2 ур\",\"description\":\"Золотая стрела удачи 2 ур\",\"abilities\":[{\"title\":\"Удачливый стрелок\",\"description\":\"Удача в стрельбе\"}]}],\"title\":\"Гоша 1\"}")
//            , "В базе неверно записаны данные аммуниции");


    // Пробуем logout
    assertSuccess(gameWrapper.logout(player1));

    return this;
  }

  public AuthorityAndDatabaseTest init() {
    player1 = USER_NAME;
    return this;
  }

  @Test
  public void doTest() {

    init()
            .innerDoLoginTest()
            .innerDoMapTest();
  }
}
