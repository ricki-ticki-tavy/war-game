package tests.core.entity.magic;

import api.entity.magic.SpellInfo;
import api.entity.magic.SpellParameter;
import api.enums.SignOfInfluenceEnum;
import api.enums.SpellParameterEnum;
import api.game.map.Player;
import api.geo.Coords;
import core.entity.magic.base.AbstractSpellImpl;
import core.entity.magic.base.SpellInfoImpl;
import core.entity.magic.base.SpellParameterImpl;
import javafx.util.Pair;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Тестовое заклинание теста параметров
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TestSpellCheckParameters extends AbstractSpellImpl<Player> {

  public static final SpellInfo spellinfo = new SpellInfoImpl(
          "Тест мультипараметров"
          , "Проверяет заполнение многих параметров параметров"
          , SignOfInfluenceEnum.NEGATIVE
          , 10
          , TestSpellCheckParameters.class
  , Arrays.stream(new SpellParameter[]{
          SpellParameterImpl.spellParameter(
                  "Выберете координату"
                  , SpellParameterEnum.MAP_POINT
                  , null)

          , SpellParameterImpl.spellParameter(
          "координату в пайоне центра"
          , SpellParameterEnum.MAP_POINT
          , new Pair<>(new Coords(500,500), 100))

          , SpellParameterImpl.spellParameter(
          "число"
          , SpellParameterEnum.INTEGER
          , 0
          , new Pair(1, 100))

          ,SpellParameterImpl.spellParameter(
          "Выберете координату"
          , SpellParameterEnum.ALLIED_WARRIOR
          , new Pair(new Coords(200, 350), 100))

          , SpellParameterImpl.spellParameter(
          "Выберете координату"
          , SpellParameterEnum.ENEMY_WARRIOR
          , new Pair(new Coords(800, 550), 100))

          , SpellParameterImpl.spellParameter(
          "Игрок"
          , SpellParameterEnum.PLAYER
          , null)
  }).collect(Collectors.toList()));

  @Autowired
  BeanFactory beanFactory;

  public TestSpellCheckParameters(Player owner) {
    super(owner, spellinfo.getSpellSign(), spellinfo.getMannaCost(),"SpellLgt_", spellinfo.getName(), spellinfo.getDescription());
    this.requiredParams.putAll(spellinfo.getRequiredAdditionalParams());

  }
}
