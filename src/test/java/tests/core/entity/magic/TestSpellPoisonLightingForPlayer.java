package tests.core.entity.magic;

import api.entity.magic.SpellInfo;
import api.entity.magic.SpellParameter;
import api.enums.SignOfInfluenceEnum;
import api.enums.SpellParameterEnum;
import api.game.map.Player;
import core.entity.ability.lighting.AbilityLightingForSpell;
import core.entity.ability.poison.AbilityPoisonArrowForSpell;
import core.entity.magic.base.AbstractSpellImpl;
import core.entity.magic.base.SpellInfoImpl;
import core.entity.magic.base.SpellParameterImpl;
import javafx.util.Pair;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Заклинание молнии и 3-х ходовой ядовитой стрелы
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TestSpellPoisonLightingForPlayer extends AbstractSpellImpl<Player> {

  public static final SpellInfo spellinfo = new SpellInfoImpl(
          "Ядовитая молния"
          , "Наносит уром молнией и, в течение 3-х ходов, ядом"
          , SignOfInfluenceEnum.NEGATIVE
          , 10
          , TestSpellPoisonLightingForPlayer.class
  , Arrays.stream(new SpellParameter[]{
          SpellParameterImpl.spellParameter(
                  "Выберете вражеского воина"
                  , SpellParameterEnum.ENEMY_WARRIOR
                  , null)
  }).collect(Collectors.toList()));

  @Autowired
  BeanFactory beanFactory;

  @PostConstruct
  public void init() {
    this.abilities.put(AbilityLightingForSpell.UNIT_NAME, beanFactory.getBean(AbilityLightingForSpell.class, this, 10, 800));
    this.abilities.put(AbilityPoisonArrowForSpell.UNIT_NAME, beanFactory.getBean(AbilityPoisonArrowForSpell.class, this, 3, 10, 200));
  }

  public TestSpellPoisonLightingForPlayer(Player owner) {
    super(owner, spellinfo.getSpellSign(), spellinfo.getMannaCost(), "SpellLgt_", spellinfo.getName(), spellinfo.getDescription());
    this.requiredParams.putAll(spellinfo.getRequiredAdditionalParams());
  }
}
