package tests.core.entity.magic;

import api.entity.magic.SpellInfo;
import api.entity.magic.SpellParameter;
import api.entity.warrior.Warrior;
import api.enums.SignOfInfluenceEnum;
import api.enums.TargetTypeEnum;
import api.game.map.Player;
import api.geo.Coords;
import core.entity.ability.healing.AbilityWarriorsHealthRejuvenationForSpell;
import core.entity.ability.lighting.AbilityLightingForSpell;
import core.entity.magic.base.AbstractSpellImpl;
import core.entity.magic.base.SpellInfoImpl;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;

/**
 * Тестовое заклинание без параметров для игрока
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TestSpellCheckNoParameters extends AbstractSpellImpl<Player> {

  public static final SpellInfo spellinfo = new SpellInfoImpl(
          "Тест массовое лечение"
          , "Лечит всех дружественных воинов"
          , SignOfInfluenceEnum.POSITIVE
          , 10
          , TestSpellCheckNoParameters.class
  , null);

  @Autowired
  BeanFactory beanFactory;

  @PostConstruct
  public void init() {
    this.abilities.put(AbilityLightingForSpell.UNIT_NAME, beanFactory.getBean(AbilityWarriorsHealthRejuvenationForSpell.class, this, 10, 300));
  }

  public TestSpellCheckNoParameters(Player owner) {
    super(owner, spellinfo.getSpellSign(), spellinfo.getMannaCost(), "SpellHelgAll_", spellinfo.getName(), spellinfo.getDescription());
  }

  @Override
  protected List<Warrior> buildWarriorsListForApplay(Map<Integer, SpellParameter> parameters) {
    return getContext().getLevelMap().getWarriors(new Coords(300, 500), 300, TargetTypeEnum.ALLIED_WARRIOR, getOwner());
  }
}
