package tests.core.entity.magic;

import api.entity.magic.SpellInfo;
import api.entity.magic.SpellParameter;
import api.enums.SignOfInfluenceEnum;
import api.enums.SpellParameterEnum;
import api.game.map.Player;
import core.entity.ability.healing.AbilityLongTimeHealingForSpell;
import core.entity.magic.base.AbstractSpellImpl;
import core.entity.magic.base.SpellInfoImpl;
import core.entity.magic.base.SpellParameterImpl;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Заклинание 2-х ходовового лечения
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TestSpellHealingForPlayer extends AbstractSpellImpl<Player> {

  public static final SpellInfo spellinfo = new SpellInfoImpl(
          "Прилив жизни"
          , "Лечит в течение 2-х ходов"
          , SignOfInfluenceEnum.POSITIVE
          , 10
          , TestSpellHealingForPlayer.class
  , Arrays.stream(new SpellParameter[]{
          SpellParameterImpl.spellParameter(
                  "Выберете своего воина"
                  , SpellParameterEnum.ALLIED_WARRIOR
                  , null)
  }).collect(Collectors.toList()));

  @Autowired
  BeanFactory beanFactory;

  @PostConstruct
  public void init() {
    this.abilities.put(AbilityLongTimeHealingForSpell.UNIT_NAME, beanFactory.getBean(AbilityLongTimeHealingForSpell.class, this, 2, 10, 30));
  }

  public TestSpellHealingForPlayer(Player owner) {
    super(owner, spellinfo.getSpellSign(), spellinfo.getMannaCost(), "SpellHealing_", spellinfo.getName(), spellinfo.getDescription());
    this.requiredParams.putAll(spellinfo.getRequiredAdditionalParams());
  }
}
