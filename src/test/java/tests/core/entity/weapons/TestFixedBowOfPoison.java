package tests.core.entity.weapons;

import api.entity.ability.Ability;
import api.entity.weapon.WeaponClassInfo;
import api.enums.WeaponClassEnum;
import core.entity.ability.poison.AbilityPoisonArrowForWeapon;
import core.entity.weapon.AbstractWeaponImpl;
import core.entity.weapon.base.WeaponClassInfoImpl;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.UUID;

/**
 * ДЛЯ ЬЕСТОВ
 * Лук с ядовитыми стрелами, отравляющими на несколько ходов
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TestFixedBowOfPoison extends AbstractWeaponImpl {

  public static final WeaponClassInfo classInfo = new WeaponClassInfoImpl("Простой фиксированный лук с ядом"
          , "Острие простого фиксированного ядовитого лука"
          , "Простой фиксированный лук с ядом. Двуручное оружие. При ближнем бое работает как кинжал"
          , TestFixedBowOfPoison.class
          , 30
          , 10);

  private final String OUID = "WepFixPoisBow_" + UUID.randomUUID().toString();

  @Autowired
  BeanFactory beanFactory;

  @PostConstruct
  private void initAbilities(){
    Ability poisonArrowAbility = beanFactory.getBean(AbilityPoisonArrowForWeapon.class, this, 3, 10, 200, -1, -1);
    this.abilities.put(AbilityPoisonArrowForWeapon.UNIT_NAME, poisonArrowAbility);
  }

  public TestFixedBowOfPoison() {
    super();
    this.id = OUID;
    this.title = classInfo.getName();
    this.description = classInfo.getDescription();
    this.meleeMinDamage = 600;
    this.meleeMaxDamage = 600;
    this.rangedMinDamage = 900;
    this.rangedMaxDamage = 900;
    this.meleeAttackCost = 100;
    this.rangedAttackCost = 120;
    this.unrejectable = false;
    this.useCountPerRound = -1;
    this.totalRangedUseCount = -1;
    this.dealsRangedDamage = true;
    this.dealsMeleeDamage = true;
    this.minRangedAttackRange = 2;
    this.maxRangedAttackRange = 50;
    this.meleeAttackRange = 2;
    this.fadeRangeStart = 49;
    this.fadeDamagePercentPerLength = 0;
    this.neededHandsCountToTakeWeapon = 2;
    this.secondWeaponName = classInfo.getSecondName();
  }

  @Override
  public WeaponClassEnum getWeaponClass() {
    return WeaponClassEnum.RANGED;
  }

  @Override
  public WeaponClassInfo getWeaponClassInfo() {
    return classInfo;
  }

}
