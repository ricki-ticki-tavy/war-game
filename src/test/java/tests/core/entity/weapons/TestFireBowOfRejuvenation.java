package tests.core.entity.weapons;

import api.entity.ability.Ability;
import api.entity.weapon.WeaponClassInfo;
import api.enums.WeaponClassEnum;
import core.entity.ability.fire.AbilityFireArrowForWeapon;
import core.entity.ability.healing.AbilityWarriorsHealthRejuvenationForWeapon;
import core.entity.weapon.AbstractWeaponImpl;
import core.entity.weapon.base.WeaponClassInfoImpl;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.UUID;

/**
 * Тестовый лук с огненными стрелами и восстановлением жизни
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TestFireBowOfRejuvenation extends AbstractWeaponImpl {

  public static final WeaponClassInfo classInfo = new WeaponClassInfoImpl("Тестовый Огненный лук"
          , "Тестовое острие огненного лука"
          , "Огненный лук. Двуручное оружие. При ближнем бое работает как кинжал"
          , TestFireBowOfRejuvenation.class
          , 30
          , 10);

  @Autowired
  BeanFactory beanFactory;

  private final String OUID = "WepFireBow_" + UUID.randomUUID().toString();

  @PostConstruct
  private void initAbilities() {
    Ability fireArrowAbility = beanFactory.getBean(AbilityFireArrowForWeapon.class, this, 1, -1, -1);
    this.abilities.put("Огненная стрела", fireArrowAbility);
    Ability rejuvenationAbility = beanFactory.getBean(AbilityWarriorsHealthRejuvenationForWeapon.class, this, 1, -1, -1);
    this.abilities.put(rejuvenationAbility.getTitle(), rejuvenationAbility);
  }

  public TestFireBowOfRejuvenation() {
    super();
    this.id = OUID;
    this.title = classInfo.getName();
    this.description = classInfo.getDescription();
    this.meleeMinDamage = 100;
    this.meleeMaxDamage = 300;
    this.rangedMinDamage = 100;
    this.rangedMaxDamage = 700;
    this.meleeAttackCost = 40;
    this.rangedAttackCost = 120;
    this.unrejectable = false;
    this.useCountPerRound = -1;
    this.totalRangedUseCount = -1;
    this.dealsRangedDamage = true;
    this.dealsMeleeDamage = true;
    this.minRangedAttackRange = 2;
    this.maxRangedAttackRange = 40;
    this.meleeAttackRange = 2;
    this.fadeRangeStart = 20;
    this.fadeDamagePercentPerLength = 3;
    this.neededHandsCountToTakeWeapon = 2;
    this.secondWeaponName = classInfo.getSecondName();

  }

  @Override
  public WeaponClassEnum getWeaponClass() {
    return WeaponClassEnum.SMALL_RANGED;
  }

  @Override
  public WeaponClassInfo getWeaponClassInfo() {
    return classInfo;
  }

}
