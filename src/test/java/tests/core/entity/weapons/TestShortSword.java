package tests.core.entity.weapons;

import api.entity.weapon.WeaponClassInfo;
import api.enums.WeaponClassEnum;
import core.entity.weapon.AbstractWeaponImpl;
import core.entity.weapon.base.WeaponClassInfoImpl;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * Короткий меч
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TestShortSword extends AbstractWeaponImpl {

  public static final WeaponClassInfo classInfo = new WeaponClassInfoImpl("Тестовый короткий меч"
          , null
          , "Короткий меч. Ближний бой "
          , TestShortSword.class
          , 7
          , 0);

  private final String OUID = "WepShSwd_" + UUID.randomUUID().toString();

  public TestShortSword(){
    super();
    this.id = OUID;
    this.title = classInfo.getName();
    this.description = classInfo.getDescription();
    this.meleeMinDamage = 100;
    this.meleeMaxDamage = 400;
    this.meleeAttackCost = 30;
    this.unrejectable = false;
    this.useCountPerRound = -1;
    this.totalRangedUseCount = -1;
    this.dealsRangedDamage = false;
    this.dealsMeleeDamage = true;
    this.meleeAttackRange = 2;
    this.neededHandsCountToTakeWeapon = 1;
  }

  @Override
  public WeaponClassEnum getWeaponClass() {
    return WeaponClassEnum.SHORT_MELEE;
  }

  @Override
  public WeaponClassInfo getWeaponClassInfo() {
    return classInfo;
  }

}
