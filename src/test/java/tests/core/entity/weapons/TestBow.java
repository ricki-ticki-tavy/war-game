package tests.core.entity.weapons;

import api.entity.weapon.WeaponClassInfo;
import api.enums.WeaponClassEnum;
import core.entity.weapon.AbstractWeaponImpl;
import core.entity.weapon.base.WeaponClassInfoImpl;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * Обычный меч
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TestBow extends AbstractWeaponImpl {

  public static final WeaponClassInfo classInfo = new WeaponClassInfoImpl("Тестовый простой лук"
          , "Тестовое острие лука"
          , "Простой лук. Двуручное оружие. При ближнем бое работает как кинжал"
          , TestBow.class
          , 10
          , 0);

  private final String OUID = "WepBow_" + UUID.randomUUID().toString();

  public TestBow() {
    super();
    this.id = OUID;
    this.title = classInfo.getName();
    this.description = classInfo.getDescription();
    this.meleeMinDamage = 100;
    this.meleeMaxDamage = 200;
    this.rangedMinDamage = 100;
    this.rangedMaxDamage = 800;
    this.meleeAttackCost = 40;
    this.rangedAttackCost = 120;
    this.unrejectable = false;
    this.useCountPerRound = -1;
    this.totalRangedUseCount = -1;
    this.dealsRangedDamage = true;
    this.dealsMeleeDamage = true;
    this.minRangedAttackRange = 2;
    this.maxRangedAttackRange = 40;
    this.meleeAttackRange = 2;
    this.fadeRangeStart = 20;
    this.fadeDamagePercentPerLength = 3;
    this.neededHandsCountToTakeWeapon = 2;
    this.secondWeaponName = classInfo.getSecondName();
  }

  @Override
  public WeaponClassEnum getWeaponClass() {
    return WeaponClassEnum.SMALL_RANGED;
  }

  @Override
  public WeaponClassInfo getWeaponClassInfo() {
    return classInfo;
  }

}
