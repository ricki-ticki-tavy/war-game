package tests.core.entity.weapons;

import api.entity.weapon.WeaponClassInfo;
import api.enums.WeaponClassEnum;
import core.entity.weapon.AbstractWeaponImpl;
import core.entity.weapon.base.WeaponClassInfoImpl;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * Обычный меч с фиксированным уроном
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TestFixedSword extends AbstractWeaponImpl {

  public static final WeaponClassInfo classInfo = new WeaponClassInfoImpl("Тестовый фиксированный меч"
          , null
          , "Простой меч. Ближний бой "
          , TestFixedSword.class
          , 7
          , 0);

  private final String OUID = "WepSwd_" + UUID.randomUUID().toString();

  public TestFixedSword() {
    super();
    this.id = OUID;
    this.title = classInfo.getName();
    this.description = classInfo.getDescription();
    this.meleeMinDamage = 500;
    this.meleeMaxDamage = 500;
    this.meleeAttackCost = 40;
    this.unrejectable = false;
    this.useCountPerRound = -1;
    this.totalRangedUseCount = -1;
    this.dealsRangedDamage = false;
    this.dealsMeleeDamage = true;
    this.meleeAttackRange = 2;
    this.neededHandsCountToTakeWeapon = 1;
  }

  @Override
  public WeaponClassEnum getWeaponClass() {
    return WeaponClassEnum.MELEE;
  }

  @Override
  public WeaponClassInfo getWeaponClassInfo() {
    return classInfo;
  }

}
