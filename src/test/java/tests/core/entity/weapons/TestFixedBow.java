package tests.core.entity.weapons;

import api.entity.weapon.WeaponClassInfo;
import api.enums.WeaponClassEnum;
import core.entity.weapon.AbstractWeaponImpl;
import core.entity.weapon.base.WeaponClassInfoImpl;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * Обычный меч
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TestFixedBow extends AbstractWeaponImpl {

  public static final WeaponClassInfo classInfo = new WeaponClassInfoImpl("Простой фиксированный лук"
          , "Острие простого фиксированного лука"
          , "Простой фиксированный лук. Двуручное оружие. При ближнем бое работает как кинжал"
          , TestFixedBow.class
          , 20
          , 10);

  private final String OUID = "WepBow_" + UUID.randomUUID().toString();

  public TestFixedBow() {
    super();
    this.id = OUID;
    this.title = classInfo.getName();
    this.description = classInfo.getDescription();
    this.meleeMinDamage = 600;
    this.meleeMaxDamage = 600;
    this.rangedMinDamage = 900;
    this.rangedMaxDamage = 900;
    this.meleeAttackCost = 100;
    this.rangedAttackCost = 120;
    this.unrejectable = false;
    this.useCountPerRound = -1;
    this.totalRangedUseCount = -1;
    this.dealsRangedDamage = true;
    this.dealsMeleeDamage = true;
    this.minRangedAttackRange = 2;
    this.maxRangedAttackRange = 50;
    this.meleeAttackRange = 2;
    this.fadeRangeStart = 49;
    this.fadeDamagePercentPerLength = 0;
    this.neededHandsCountToTakeWeapon = 2;
    this.secondWeaponName = classInfo.getSecondName();
  }

  @Override
  public WeaponClassEnum getWeaponClass() {
    return WeaponClassEnum.RANGED;
  }

  @Override
  public WeaponClassInfo getWeaponClassInfo() {
    return classInfo;
  }

}
