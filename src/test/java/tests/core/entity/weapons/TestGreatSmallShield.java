package tests.core.entity.weapons;

import api.entity.weapon.WeaponClassInfo;
import api.enums.ArmorClassEnum;
import api.enums.WeaponClassEnum;
import core.entity.weapon.AbstractWeaponImpl;
import core.entity.weapon.base.WeaponClassInfoImpl;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * маленький щит для тестов, отражающий некоторое оружие на 100 %
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TestGreatSmallShield extends AbstractWeaponImpl {

  public static final WeaponClassInfo classInfo = new WeaponClassInfoImpl("маленький Великий круглый щит"
          , null
          , "Маленький Великий круглый щит"
          , TestGreatSmallShield.class
          , 10
          , 0);

  private final String OUID = "WepShield_" + UUID.randomUUID().toString();

  public TestGreatSmallShield() {
    super();
    this.id = OUID;
    this.title = classInfo.getName();
    this.description = classInfo.getDescription();
    this.unrejectable = false;
    this.useCountPerRound = -1;
    this.totalRangedUseCount = -1;
    this.dealsRangedDamage = true;
    this.dealsMeleeDamage = true;
    this.meleeAttackRange = 1;
    this.dealsRangedDamage = false;
    this.dealsMeleeDamage = true;
    this.neededHandsCountToTakeWeapon = 1;
    this.armorClass = ArmorClassEnum.ARMOR_1;
    this.defenceCost = 20;
  }

  @Override
  public WeaponClassEnum getWeaponClass() {
    return WeaponClassEnum.SMALL_SHIELD;
  }

  @Override
  public int getDefenceProbability(WeaponClassEnum weaponClass) {
    switch (weaponClass) {
      case SMALL_SHIELD:
        return 45;
      case SHORT_MELEE:
      case MELEE:
        return 100;
      case LONG_MELEE:
      case LARGE_SHIELD:
        return 100;
      case HAVY_MELEE:
        return 22;
      default:
        return 0;
    }
  }

  @Override
  public WeaponClassInfo getWeaponClassInfo() {
    return classInfo;
  }

}
