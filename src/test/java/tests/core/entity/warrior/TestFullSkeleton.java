package tests.core.entity.warrior;

import api.core.Result;
import api.entity.ability.Influencer;
import api.entity.ability.Modifier;
import api.entity.warrior.WarriorBaseClassInfo;
import api.enums.ArmorClassEnum;
import api.enums.WeaponClassEnum;
import api.game.action.InfluenceResult;
import core.entity.warrior.base.AbstractWarriorBaseClass;
import core.entity.warrior.base.WarriorBaseClassInfoImpl;
import core.entity.warrior.base.WarriorSBaseAttributesImpl;
import core.entity.weapon.ShortSword;
import core.entity.weapon.Sword;
import core.system.ResultImpl;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TestFullSkeleton extends AbstractWarriorBaseClass {

  public static WarriorBaseClassInfo classInfo = new WarriorBaseClassInfoImpl(
          "Тестовый реальный скелет"
          , "Воин-скелет с легкой броней"
          , TestFullSkeleton.class
          , Collections.EMPTY_LIST
          , Collections.EMPTY_LIST
          , 30
          , 0
          , null);

  public TestFullSkeleton() {
    super(classInfo.getName(), classInfo.getDescription());
    setWarriorSBaseAttributes(new WarriorSBaseAttributesImpl(2000, 0, 1, 240, 2)
            .setMaxHealth(2000)
            .setMaxManna(0)
            .setMaxActionPoints(240)
            .setMaxAbilityActionPoints(1)
            .setArmorClass(ArmorClassEnum.ARMOR_1)
            .setDeltaCostMove(0)
            .setMaxDefenseActionPoints(60)
            .setSummonable(true));

    setSupportedWeaponClasses(Stream.of(
            ShortSword.class
            , Sword.class
    ).collect(Collectors.toList()));
  }
  //===================================================================================================

  @Override
  public Result<InfluenceResult> innerWarriorUnderAttack(InfluenceResult attackResult) {
    return ResultImpl.success(attackResult);
  }
  //===================================================================================================

  private void processModifierOnDefence(Influencer influencer, InfluenceResult attackResult) {
    Modifier modifier = influencer.getModifier();
    switch (modifier.getModifierClass()) {
      case WEAPON:
        // физиеский урон. проверим класс оружия;
        if (attackResult.getAttackerWeapon().getWeaponClass().equals(WeaponClassEnum.SMALL_MELEE)
                || attackResult.getAttackerWeapon().getWeaponClass().equals(WeaponClassEnum.SMALL_RANGED)) {
          // урон 1/3
          modifier.setLastCalculatedValue(modifier.getLastCalculatedValue() / 3);
        } else if (attackResult.getAttackerWeapon().getWeaponClass().equals(WeaponClassEnum.SHORT_MELEE)
                || attackResult.getAttackerWeapon().getWeaponClass().equals(WeaponClassEnum.RANGED)) {
          // урон 2/3
          modifier.setLastCalculatedValue(modifier.getLastCalculatedValue() * 2 / 3);
        } else if (attackResult.getAttackerWeapon().getWeaponClass().equals(WeaponClassEnum.HAVY_MELEE)) {
          // урон 1.5
          modifier.setLastCalculatedValue(modifier.getLastCalculatedValue() * 3 / 2);
        }
        break;
      // полная защита от яда и магии
      case COLD:
      case POISON:
        modifier.setLastCalculatedValue(0);
        break;
    }

    // Соберем дочерние влияния
    influencer.getChildren().stream()
            .forEach(subInfluencer -> processModifierOnDefence(subInfluencer, attackResult));
  }
  //===================================================================================================

  @Override
  public Result<InfluenceResult> defenceWarrior(InfluenceResult attackResult) {
    attackResult.getInfluencers().stream()
            .forEach(influencer -> processModifierOnDefence(influencer, attackResult));
    return super.defenceWarrior(attackResult);
  }
  //===================================================================================================

  @Override
  protected WarriorBaseClassInfo getInfo() {
    return classInfo;
  }
  //===================================================================================================

  @Override
  public WarriorBaseClassInfo getWarriorBaseClassInfo() {
    return classInfo;
  }
  //===================================================================================================
}
