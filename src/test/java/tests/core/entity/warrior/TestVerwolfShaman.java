package tests.core.entity.warrior;

import api.core.Result;
import api.entity.warrior.WarriorBaseClassInfo;
import api.enums.ArmorClassEnum;
import api.game.action.InfluenceResult;
import core.entity.warrior.base.AbstractWarriorBaseClass;
import core.entity.warrior.base.WarriorBaseClassInfoImpl;
import core.entity.warrior.base.WarriorSBaseAttributesImpl;
import core.entity.weapon.ShortSword;
import core.entity.weapon.Sword;
import core.system.ResultImpl;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import tests.core.entity.magic.TestSpellAbilityPointsForWarrior;
import tests.core.entity.magic.TestSpellPoisonLightingForWarrior;

import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Тестовый базовый класс шаман-оборотень
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TestVerwolfShaman extends AbstractWarriorBaseClass {

  public static WarriorBaseClassInfo classInfo = new WarriorBaseClassInfoImpl(
          "Тестовый шаман-оборотень"
          , "Воин-маг. Оборотень"
          , TestVerwolfShaman.class
          , Stream.of(

          TestSpellPoisonLightingForWarrior.spellinfo
          , TestSpellAbilityPointsForWarrior.spellinfo)

          .collect(
                  Collectors.toList()
          )
          , Collections.EMPTY_LIST
          , 30
          , 0
          , null);

  public TestVerwolfShaman() {
    super(classInfo.getName(), classInfo.getDescription());
    setWarriorSBaseAttributes(new WarriorSBaseAttributesImpl(2000, 0, 1, 240, 2)
            .setMaxHealth(2000)
            .setMaxManna(50)
            .setManna(50)
            .setMannaRejuvenation(3)
            .setMaxActionPoints(240)
            .setMaxAbilityActionPoints(4)
            .setArmorClass(ArmorClassEnum.ARMOR_0)
            .setDeltaCostMove(-1)
            .setMaxDefenseActionPoints(60)
            .setSummonable(false)
    );

    setSupportedWeaponClasses(Stream.of(
            ShortSword.class
            , Sword.class
    ).collect(Collectors.toList()));
  }
  //===================================================================================================

  @Override
  public Result<InfluenceResult> innerWarriorUnderAttack(InfluenceResult attackResult) {
    return ResultImpl.success(attackResult);
  }
  //===================================================================================================

  @Override
  protected WarriorBaseClassInfo getInfo() {
    return classInfo;
  }
  //===================================================================================================

  @Override
  public WarriorBaseClassInfo getWarriorBaseClassInfo() {
    return classInfo;
  }
  //===================================================================================================

}
