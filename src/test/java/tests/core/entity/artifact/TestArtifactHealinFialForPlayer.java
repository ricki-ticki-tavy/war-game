package tests.core.entity.artifact;

import api.entity.ability.Ability;
import api.entity.artifct.ArtifactClassInfo;
import api.enums.OwnerTypeEnum;
import api.game.map.Player;
import core.entity.ability.healing.AbilityWarriorsHealthRejuvenationForArtifact;
import core.entity.artifact.base.AbstractArtifactImpl;
import core.entity.artifact.base.ArtifactClassInfoImpl;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Артефакт лечения воинов. Для игрока
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TestArtifactHealinFialForPlayer extends AbstractArtifactImpl<Player>{
  public static ArtifactClassInfo classInfo = new ArtifactClassInfoImpl(
          "Фиал жизни"
          , "Восстанавливает каждому воину 1 ед. жизни каждый ход"
          , TestArtifactHealinFialForPlayer.class
          , 0
          , 0
          , null);

  private int level;

  @Autowired
  BeanFactory beanFactory;

  @PostConstruct
  public void initAbilities(){
    Ability luckForRangedAttackForWarrior = beanFactory.getBean(AbilityWarriorsHealthRejuvenationForArtifact.class, this, level, -1, -1);
    this.abilities.put(luckForRangedAttackForWarrior.getTitle(), luckForRangedAttackForWarrior);

  }

  public TestArtifactHealinFialForPlayer(Player owner){
    super(owner
            , OwnerTypeEnum.PLAYER
            , "Art_WRA"
            , classInfo.getName()
            , classInfo.getName());
    level = 1;
  }
}
