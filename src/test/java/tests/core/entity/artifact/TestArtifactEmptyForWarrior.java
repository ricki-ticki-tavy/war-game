package tests.core.entity.artifact;

import api.entity.warrior.Warrior;
import api.enums.OwnerTypeEnum;
import core.entity.artifact.base.AbstractArtifactImpl;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Артефакт не делающий ничего
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TestArtifactEmptyForWarrior extends AbstractArtifactImpl<Warrior>{
  public static final String CLASS_NAME = "Сачок";
  private int level;

  @Autowired
  BeanFactory beanFactory;

  @PostConstruct
  public void initAbilities(){
  }

  public TestArtifactEmptyForWarrior(Warrior owner){
    super(owner
            , OwnerTypeEnum.WARRIOR
            , "Art_Emp"
            , CLASS_NAME
            , CLASS_NAME);
    level = 20;
  }
}
