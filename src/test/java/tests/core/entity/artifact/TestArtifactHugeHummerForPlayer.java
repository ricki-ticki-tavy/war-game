package tests.core.entity.artifact;

import api.entity.artifct.ArtifactClassInfo;
import api.enums.OwnerTypeEnum;
import api.game.map.Player;
import core.entity.artifact.base.AbstractArtifactImpl;
import core.entity.artifact.base.ArtifactClassInfoImpl;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Артефакт повышения урона . Для игрока
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TestArtifactHugeHummerForPlayer extends AbstractArtifactImpl<Player>{
  public static ArtifactClassInfo classInfo = new ArtifactClassInfoImpl(
          "Молот гиганта"
          , ""
          , TestArtifactHugeHummerForPlayer.class
          , 0
          , 0
          , null);

  private int level;

  @Autowired
  BeanFactory beanFactory;

  @PostConstruct
  public void initAbilities(){
//    Ability luckForRangedAttackForWarrior = beanFactory.getBean(AbilityWarriorsHealthRejuvenationForArtifact.class, this, level, -1, -1);
//    this.abilities.put(luckForRangedAttackForWarrior.getTitle(), luckForRangedAttackForWarrior);

  }

  public TestArtifactHugeHummerForPlayer(Player owner){
    super(owner
            , OwnerTypeEnum.PLAYER
            , "Art_WRA"
            , classInfo.getName()
            , classInfo.getName());
    level = 0;
  }
}
