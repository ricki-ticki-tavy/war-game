package tests.core;

import abstracts.AbstractMapTest;
import api.core.Result;
import api.entity.warrior.Warrior;
import api.game.action.InfluenceResult;
import api.game.wraper.GameWrapper;
import api.geo.Coords;
import config.TestContextConfiguration;
import core.system.error.GameErrors;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.util.Assert;
import tests.core.entity.magic.TestSpellAbilityPointsForWarrior;
import tests.core.entity.magic.TestSpellNotRegisteredForWarrior;
import tests.core.entity.magic.TestSpellPoisonLightingForWarrior;
import tests.core.entity.warrior.TestArmoredVityaz;
import tests.core.entity.warrior.TestUnluckyVityaz;
import tests.core.entity.warrior.TestVerwolfShaman;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static core.system.ResultImpl.success;

/**
 * Проверка заклинаний воина
 * <p>
 * Проверка заклинания, требующего очки способностей, проверка заклинания, требующего только магию
 * <p>
 * Пробуем применить до начала игры, потом не в свой ход, потом заклинание, которого нет у воина,
 * <p>
 * Пробуем переместить воина после успешного заклинания, пробуем переместиться, и применить заклинание, которое требует
 * <p>
 * больше, чем есть очков действия
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SpellWarriorsTest.class})
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = TestContextConfiguration.class)
@ActiveProfiles({"mockDb", "test"})
public class SpellWarriorsTest extends AbstractMapTest {

  @Autowired
  GameWrapper gameWrapper;

  public SpellWarriorsTest setGameWrapper(GameWrapper gameWrapper) {
    this.gameWrapper = gameWrapper;
    return this;
  }

  public void innerDoTest() {
    initMapForTwoPlayers(gameWrapper, "SpellWarriorTest_user1", "SpellWarriorTest_User2",
            new String[]{TestVerwolfShaman.classInfo.getName()
                    , TestUnluckyVityaz.classInfo.getName()
                    , TestArmoredVityaz.classInfo.getName()
                    , TestVerwolfShaman.classInfo.getName()});

    // ПРобуем наложить заклинание которого не существует
    Map<Integer, String> spellParams = new HashMap<>(1);
    spellParams.put(0, warrior1p2);
    Result<List<Result<InfluenceResult>>> castResult = gameWrapper.castSpellByWarrior(gameContext, player1, warrior1p1
            , "blabla", spellParams);
    Assert.isTrue(castResult.isFail(GameErrors.SPELL_FOR_WARRIOR_NOT_FOUND_BY_NAME), "Заклинание с несуществующим названием наложено доначала игры");

    // ПРобуем наложить заклинание до начала игры. Должен быть отказ
    spellParams = new HashMap<>(1);
    spellParams.put(0, warrior1p2);
    castResult = gameWrapper.castSpellByWarrior(gameContext, player1, warrior1p1
            , TestSpellPoisonLightingForWarrior.spellinfo.getName(), spellParams);
    Assert.isTrue(castResult.isFail(GameErrors.CONTEXT_GAME_NOT_STARTED), "Заклинание наложено до начала игры");

    // Игроки готовы
    assertSuccess(gameWrapper.playerReadyToPlay(player1, true));
    assertSuccess(gameWrapper.playerReadyToPlay(player2, true));

    // если первым ходитигрок 2, то передаем ход игроку 1
    gameWrapper.getPlayerOwnsTheRound(gameContext)
            .peak(player -> {
              if (player.getId().equals(player2)) {
                assertSuccess(gameWrapper.nextTurn(gameContext, player2));
              }
            });

    // ПРобуем наложить заклинание не в свой ход. Должен быть отказ
    spellParams.put(0, warrior1p1);
    castResult = gameWrapper.castSpellByWarrior(gameContext, player2, warrior2p2, TestSpellPoisonLightingForWarrior.spellinfo.getName(), spellParams);
    Assert.isTrue(castResult.isFail(GameErrors.PLAYER_IS_NOT_OWENER_OF_THIS_ROUND), "Заклинание наложено не в свой ход");

    // ПРобуем наложить заклинание чужим воином. Должен быть отказ
    spellParams.put(0, warrior2p2);
    castResult = gameWrapper.castSpellByWarrior(gameContext, player1, warrior2p2, TestSpellPoisonLightingForWarrior.spellinfo.getName(), spellParams);
    Assert.isTrue(castResult.isFail(GameErrors.WARRIOR_NOT_FOUND_AT_PLAYER_BY_NAME), "Заклинание наложено чужим воином");

    // ПРобуем наложить заклинание c неверным кодом воина. Должен быть отказ
    spellParams.put(0, warrior1p2 + "9");
    castResult = gameWrapper.castSpellByWarrior(gameContext, player1, warrior1p1, TestSpellPoisonLightingForWarrior.spellinfo.getName(), spellParams);
    Assert.isTrue(castResult.isFail(GameErrors.WARRIOR_NOT_FOUND_ON_THE_MAP), "Заклинание наложено на не существующего юнита");

    // ПРобуем наложить заклинание c недоступным воину заклинанием. Должен быть отказ
    spellParams.put(0, warrior1p2);
    castResult = gameWrapper.castSpellByWarrior(gameContext, player1, warrior1p1, TestSpellNotRegisteredForWarrior.spellinfo.getName(), spellParams);
    Assert.isTrue(castResult.isFail(GameErrors.SPELL_FOR_WARRIOR_NOT_FOUND_BY_NAME_FOR_CLASS), "Заклинание не поддерживаемым классом воина заклинанием");

    // выполним перемещение вторым воином, потом пробуем наложитт заклинание первым. Так как нельзя пользоваться,по
    // условиям данной карты, более чем одним юнитом за ход - должен быть отказ
    Result<Warrior> warriorResult = gameWrapper.moveWarriorTo(gameContext, player1, warrior2p1, new Coords(820, 400));
    assertSuccess(warriorResult);
    // ПРобуем наложить заклинание . Должен быть отказ
    spellParams.put(0, warrior1p2);
    castResult = gameWrapper.castSpellByWarrior(gameContext, player1, warrior1p1, TestSpellPoisonLightingForWarrior.spellinfo.getName(), spellParams);
    Assert.isTrue(castResult.isFail(GameErrors.PLAYER_UNIT_MOVES_ON_THIS_TURN_ARE_EXCEEDED), "Заклинание наложено на не существующего юнита");

    // делаем откат движения первого воина
    assertSuccess(gameWrapper.rollbackMove(gameContext, player1, warrior2p1));

    // ПРобуем наложить заклинание на воина за границей двльности. Должен быть отказ
    spellParams.put(0, warrior2p2);
    castResult = gameWrapper.castSpellByWarrior(gameContext, player1, warrior1p1, TestSpellPoisonLightingForWarrior.spellinfo.getName(), spellParams);
    Assert.isTrue(castResult.isFail(GameErrors.SPELL_TARGET_IS_OUT_OF_RANGE), "Заклинание наложено на не существующего юнита");

    // получим запас магии воина
    int mnB = (Integer) gameWrapper.getCore().findUserByName(player1)
            .map(player -> player.findWarriorById(warrior1p1))
            .map(warrior -> success(warrior.getAttributes().getManna()))
            .getResult();

    // пробуем наложить заклинание со всеми правильно заданными параметрами. Должен быть успех
    spellParams.put(0, warrior1p2);
    castResult = gameWrapper.castSpellByWarrior(gameContext, player1, warrior1p1, TestSpellPoisonLightingForWarrior.spellinfo.getName(), spellParams);
    assertSuccess(castResult);

    // Проверим, что очки магии списались правильно
    int mnA = (Integer) gameWrapper.getCore().findUserByName(player1)
            .map(player -> player.findWarriorById(warrior1p1))
            .map(warrior -> success(warrior.getAttributes().getManna()))
            .getResult();
    Assert.isTrue(mnB - mnA == TestSpellPoisonLightingForWarrior.spellinfo.getMannaCost(), "Неверно списаны очки магии");
    mnB = mnA;

    // Проверим, что очки действия списались верно
    Assert.isTrue((Integer) gameWrapper.getCore().findUserByName(player1)
            .map(player -> player.findWarriorById(warrior1p1))
            .map(warrior -> success(warrior.getAttributes().getActionPoints()))
            .getResult() == 240 - TestSpellPoisonLightingForWarrior.spellinfo.getActionPointsCost(), "Не верно списались очки действия за заклинание");

    // Получим здоровье цели и убедимся, что его менее 2000
    int hpA = (Integer) gameWrapper.getCore().findUserByName(player2)
            .map(player -> player.findWarriorById(warrior1p2))
            .map(warrior -> success(warrior.getAttributes().getHealth()))
            .getResult();
    Assert.isTrue(hpA < 2000, "Воин врага не получил урон от заклинания");
    // подлечим подранка
    gameWrapper.getCore().findUserByName(player2)
            .map(player -> player.findWarriorById(warrior1p2))
            .peak(warrior -> warrior.getAttributes().setHealth(2000));

    // переходы ходом опять до воина 1
    assertSuccess(gameWrapper.nextTurn(gameContext, player1));
    assertSuccess(gameWrapper.nextTurn(gameContext, player2));

    // проверим восстановление очков магии
    mnA = (Integer) gameWrapper.getCore().findUserByName(player1)
            .map(player -> player.findWarriorById(warrior1p1))
            .map(warrior -> success(warrior.getAttributes().getManna()))
            .getResult();
    Assert.isTrue(mnA - mnB == gameWrapper.getCore().findUserByName(player1).getResult()
            .findWarriorById(warrior1p1).getResult()
            .getAttributes().getMannaRejuvenation(), "Неверно восстанавлваются очки магии");

    // проверим уменьшение здоровья вследствии отравления
    int hpB = (Integer) gameWrapper.getCore().findUserByName(player2)
            .map(player -> player.findWarriorById(warrior1p2))
            .map(warrior -> success(warrior.getAttributes().getHealth()))
            .getResult();
    Assert.isTrue(hpB > hpA, "Яд не сработал после первого круга");
    hpA = hpB;

    // переходы ходом опять до воина 1
    assertSuccess(gameWrapper.nextTurn(gameContext, player1));
    assertSuccess(gameWrapper.nextTurn(gameContext, player2));

    hpA = (Integer) gameWrapper.getCore().findUserByName(player2)
            .map(player -> player.findWarriorById(warrior1p2))
            .map(warrior -> success(warrior.getAttributes().getHealth()))
            .getResult();
    Assert.isTrue(hpB > hpA, "Яд не сработал после второго круга");

    hpB = hpA;
    // переходы ходом опять до воина 1
    assertSuccess(gameWrapper.nextTurn(gameContext, player1));
    assertSuccess(gameWrapper.nextTurn(gameContext, player2));

    hpA = (Integer) gameWrapper.getCore().findUserByName(player2)
            .map(player -> player.findWarriorById(warrior1p2))
            .map(warrior -> success(warrior.getAttributes().getHealth()))
            .getResult();
    Assert.isTrue(hpB == hpA, "Яд нанес урон после третьего круга");

    // теперь пробуем выполнить перемещение на расстояние большее, чем на 120 единиц, а потом применить
    // заклинание, которое требует более 120 единиц действия
    warriorResult = gameWrapper.moveWarriorTo(gameContext, player1, warrior1p1, new Coords(450, 400));
    assertSuccess(warriorResult);
    Assert.isTrue(warriorResult.getResult().getTreatedActionPointsForMove() == 125, "Не верно рассчитана стоимость перемещения (deltaCostMove)");

    // применим заклинание. Должен быть отказ
    spellParams.put(0, warrior1p2);
    castResult = gameWrapper.castSpellByWarrior(gameContext, player1, warrior1p1, TestSpellPoisonLightingForWarrior.spellinfo.getName(), spellParams);
    Assert.isTrue(castResult.isFail(GameErrors.SPELL_FOR_WARRIOR_THERE_IS_NOT_ENOUGH_ACTION_POINTS), "Заклинание наложено при нехватке очковдействия");

    // Откатим перемещение
    assertSuccess(gameWrapper.rollbackMove(gameContext, player1, warrior1p1));

    // Пробуем заклинание, требующее очков способности
    int apB = (Integer) gameWrapper.getCore().findUserByName(player1)
            .map(player -> player.findWarriorById(warrior1p1))
            .map(warrior -> success(warrior.getAttributes().getAbilityActionPoints()))
            .getResult();
    // кастаем
    castResult = gameWrapper.castSpellByWarrior(gameContext, player1, warrior1p1, TestSpellAbilityPointsForWarrior.spellinfo.getName(), spellParams);
    assertSuccess(castResult);

    // проверим как списались очки способностей
    int apA = (Integer) gameWrapper.getCore().findUserByName(player1)
            .map(player -> player.findWarriorById(warrior1p1))
            .map(warrior -> success(warrior.getAttributes().getAbilityActionPoints()))
            .getResult();
    Assert.isTrue(apB - apA == TestSpellAbilityPointsForWarrior.spellinfo.getAbilityActionPointsCost(), "Не списались очки способностей");
    Assert.isTrue(!gameWrapper.getCore().findUserByName(player1).getResult()
            .findWarriorById(warrior1p1).getResult()
            .getAttributes().isAbilityActionPointsRestored(), "Способность считается доступной");

    apB = apA;
    // переходы ходом опять до воина 1
    assertSuccess(gameWrapper.nextTurn(gameContext, player1));
    assertSuccess(gameWrapper.nextTurn(gameContext, player2));

    // проверим восстановление очков способностей
    apA = (Integer) gameWrapper.getCore().findUserByName(player1)
            .map(player -> player.findWarriorById(warrior1p1))
            .map(warrior -> success(warrior.getAttributes().getAbilityActionPoints()))
            .getResult();
    Assert.isTrue(apA - apB == 1, "Не верно восстановились или не восстановились очки способностей");
    Assert.isTrue(!gameWrapper.getCore().findUserByName(player1).getResult()
            .findWarriorById(warrior1p1).getResult()
            .getAttributes().isAbilityActionPointsRestored(), "Способность считается доступной 2");

    // Пробуем применить заклинание с невосстановившейся способностью. Должен быть отказ
    castResult = gameWrapper.castSpellByWarrior(gameContext, player1, warrior1p1, TestSpellAbilityPointsForWarrior.spellinfo.getName(), spellParams);
    Assert.isTrue(castResult.isFail(GameErrors.SPELL_FOR_WARRIOR_CAN_NOT_CAST_NOT_ENOUGH_ABILITY_POINTS), "Заклинание наложено при нехватке очеов способностей");

    // Применим заклинание, апотомпопробуем выполнить ход другим воином.
    spellParams.put(0, warrior1p2);
    castResult = gameWrapper.castSpellByWarrior(gameContext, player1, warrior1p1, TestSpellPoisonLightingForWarrior.spellinfo.getName(), spellParams);
    assertSuccess(castResult);
    // ереместим воина 2 игрока 1. Должен быть отказ
    warriorResult = gameWrapper.moveWarriorTo(gameContext, player1, warrior2p1, new Coords(800, 420));
    Assert.isTrue(warriorResult.isFail(GameErrors.PLAYER_UNIT_MOVES_ON_THIS_TURN_ARE_EXCEEDED), "Заклинание наложено при нехватке очеов способностей");


    // на этом все. Игра кончилась.
    assertSuccess(gameWrapper.getCore().removeGameContext(gameContext));
  }

  @Test
  public void doTest() {
    innerDoTest();
  }
}