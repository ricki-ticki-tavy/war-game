package tests.core;

import abstracts.AbstractMapTest;
import api.core.Context;
import api.core.Result;
import api.entity.warrior.Warrior;
import api.entity.weapon.Weapon;
import api.game.action.InfluenceResult;
import api.game.wraper.GameWrapper;
import api.geo.Coords;
import config.TestContextConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.util.Assert;
import tests.core.entity.weapons.TestFixedBowOfPoison;

import static core.system.error.GameErrors.WARRIOR_NOT_FOUND_AT_PLAYER_BY_NAME;

/**
 * Проверка подписывания и отписывания от событий.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {WeaponsWithLongEnfluencersTest.class})
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = TestContextConfiguration.class)
@ActiveProfiles({"mockDb", "test"})
public class WeaponsWithLongEnfluencersTest extends AbstractMapTest {

  @Autowired
  GameWrapper gameWrapper;

  public WeaponsWithLongEnfluencersTest setGameWrapper(GameWrapper gameWrapper) {
    this.gameWrapper = gameWrapper;
    return this;
  }

  public void innerDoTest() {
    initMapForTwoPlayers(gameWrapper, "WeaponsWLETest_user1", "WeaponsWLETest_User2");
    Context context = gameWrapper.getCore().findGameContextByUID(gameContext).getResult();


    // Дадим воину 1 игрока 1 лук с ядом
    Result<Weapon> weaponResult = gameWrapper.giveWeaponToWarrior(gameContext, player1, warrior1p1, TestFixedBowOfPoison.classInfo.getName());
    assertSuccess(weaponResult);
    String bowWarrior1p1 = weaponResult.getResult().getId();

    // подвинем воина 1 игрока 1 ближе к середине
    Result<Warrior> warriorResult = gameWrapper.moveWarriorTo(gameContext, player1, warrior1p1, new Coords(400, 400));
    assertSuccess(warriorResult);
    Warrior warriorImpl1p1 = warriorResult.getResult();

    Warrior warriorImpl1p2 = gameWrapper.getCore()
            .findGameContextByUID(gameContext).getResult()
            .findUserByName(player2).getResult()
            .findWarriorById(warrior1p2).getResult();

    // Игрок 1 готов
    assertSuccess(gameWrapper.playerReadyToPlay(player1, true));
    // Игрок 2 готов
    assertSuccess(gameWrapper.playerReadyToPlay(player2, true));

    // если первым ходитигрок 2, то передаем ход игроку 1
    gameWrapper.getPlayerOwnsTheRound(gameContext)
            .peak(player -> {
              if (player.getId().equals(player2)) {
                assertSuccess(gameWrapper.nextTurn(gameContext, player2));
              }
            });

    // Пробуем атаковать воином 1 игрока 1             воина 1 игрока 2. Долдно быть отравление ядом
    Result<InfluenceResult> attackResult = gameWrapper.attackWarrior(gameContext, player1, warrior1p1, warrior1p2, bowWarrior1p1);
    assertSuccess(attackResult);

    int hp = warriorImpl1p2.getAttributes().getHealth();
    // переходы ходом опять до воина 1
    assertSuccess(gameWrapper.nextTurn(gameContext, player1));
    // Проверим, что здоровье уменьшилось
    Assert.isTrue(hp > warriorImpl1p2.getAttributes().getHealth(), "Ядовитая стрела не дала эффекта в ход 1");
    hp = warriorImpl1p2.getAttributes().getHealth();
    // переход хода к игроку 1
    assertSuccess(gameWrapper.nextTurn(gameContext, player2));
    // Проверим, что здоровье не уменьшилось
    Assert.isTrue(hp == warriorImpl1p2.getAttributes().getHealth(), "Ядовитая стрела дала эффект не в свой ход (1)");

    hp = warriorImpl1p2.getAttributes().getHealth();
    // переходы ходом опять до воина 1
    assertSuccess(gameWrapper.nextTurn(gameContext, player1));
    // Проверим, что здоровье уменьшилось
    Assert.isTrue(hp > warriorImpl1p2.getAttributes().getHealth(), "Ядовитая стрела не дала эффекта в ход 2");
    hp = warriorImpl1p2.getAttributes().getHealth();
    // переход хода к игроку 1
    assertSuccess(gameWrapper.nextTurn(gameContext, player2));
    // Проверим, что здоровье не уменьшилось
    Assert.isTrue(hp == warriorImpl1p2.getAttributes().getHealth(), "Ядовитая стрела дала эффект не в свой ход (2)");

    hp = warriorImpl1p2.getAttributes().getHealth();
    // переходы ходом опять до воина 1
    assertSuccess(gameWrapper.nextTurn(gameContext, player1));
    // Проверим, что здоровье НЕ уменьшилось так как два хода действия стрелы кончились
    Assert.isTrue(hp == warriorImpl1p2.getAttributes().getHealth(), "Ядовитая стрела не дала эффекта в ход 2");
    hp = warriorImpl1p2.getAttributes().getHealth();
    // переход хода к игроку 1
    assertSuccess(gameWrapper.nextTurn(gameContext, player2));
    // Проверим, что здоровье не уменьшилось
    Assert.isTrue(hp == warriorImpl1p2.getAttributes().getHealth(), "Ядовитая стрела дала эффект не в свой ход (2)");


    // переходы ходом опять до воина 1
    assertSuccess(gameWrapper.nextTurn(gameContext, player1));
    assertSuccess(gameWrapper.nextTurn(gameContext, player2));

    // Опять атакуем воином 1 игрока 1             воина 1 игрока 2. Должно быть отравление ядом
    assertSuccess(gameWrapper.attackWarrior(gameContext, player1, warrior1p1, warrior1p2, bowWarrior1p1));

    // теперь оставим 2 ед. жизни, чтобы в следующий переход хода воин умер от яда
    warriorImpl1p2.getAttributes().setHealth(2);

    // переходы ходом опять до воина 1
    assertSuccess(gameWrapper.nextTurn(gameContext, player1));
    assertSuccess(gameWrapper.nextTurn(gameContext, player2));

    // воин должен был умереть. Он не должен быть найден у игрока
    warriorResult = gameWrapper.getCore()
            .findGameContextByUID(gameContext).getResult()
            .findUserByName(player2).getResult()
            .findWarriorById(warrior1p2);
    Assert.isTrue(warriorResult.isFail(WARRIOR_NOT_FOUND_AT_PLAYER_BY_NAME), "Воин не умер");


    // переходы ходом опять до воина 1
    assertSuccess(gameWrapper.nextTurn(gameContext, player1));
    assertSuccess(gameWrapper.nextTurn(gameContext, player2));

    // на этом все. Игра кончилась.

    assertSuccess(gameWrapper.getCore().removeGameContext(gameContext));
  }

  @Test
  public void doTest() {
    innerDoTest();
  }
}