package tests.core;

import abstracts.AbstractMapTest;
import api.core.Context;
import api.core.Result;
import api.entity.artifct.Artifact;
import api.entity.warrior.Warrior;
import api.game.map.Player;
import api.game.wraper.GameWrapper;
import config.TestContextConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.util.Assert;
import tests.core.entity.artifact.TestArtifactCapitansSabreForPlayer;
import tests.core.entity.artifact.TestArtifactHealinFialForPlayer;
import tests.core.entity.artifact.TestArtifactHugeHummerForPlayer;
import tests.core.entity.artifact.TestArtifactRainbowArrowForWarrior;

import static core.system.error.GameErrors.*;

/**
 * Проверка Работы артефактов
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ArtifactTest.class})
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = {TestContextConfiguration.class})
@ActiveProfiles({"mockDb", "test"})
public class ArtifactTest extends AbstractMapTest {

  @Autowired
  GameWrapper gameWrapper;

  public ArtifactTest setGameWrapper(GameWrapper gameWrapper) {
    this.gameWrapper = gameWrapper;
    return this;
  }

  public void innerDoTest() {
    initMapForTwoPlayers(gameWrapper, "ArtifactTest_user1", "ArtifactTest_User2");
    Context context = gameWrapper.getCore().findGameContextByUID(gameContext).getResult();

    Warrior warriorImpl1p1 = gameWrapper.getCore().findGameContextByUID(gameContext).getResult()
            .findUserByName(player1).getResult()
            .findWarriorById(warrior1p1).getResult();

    Assert.isTrue(warriorImpl1p1.getAttributes().getLuckRangeAtack() == 16, "способность воина удачи не подействовала");

    // выдать артефакт удачи при стрельбе +20%. Должно все получиться
    Result<Artifact<Warrior>> artifactForWarriorResult = gameWrapper.giveArtifactToWarrior(gameContext, player1, warrior1p1, TestArtifactRainbowArrowForWarrior.CLASS_NAME);
    assertSuccess(artifactForWarriorResult);
    Assert.isTrue(warriorImpl1p1.getArtifacts().getResult().size() == 1, "У воина нет добавленного артефакта");
    Assert.isTrue(warriorImpl1p1.getAttributes().getLuckRangeAtack() == 36, "Добавленный артефакт не подействовал");
    String artifact1w1p1 = artifactForWarriorResult.getResult().getId();

    // выбросим артефакт. Удача должна вернуться на прежние базовые + способность воина
    assertSuccess(artifactForWarriorResult = gameWrapper.dropArtifactByWarrior(gameContext, player1, warrior1p1, artifact1w1p1));
    Assert.isTrue(warriorImpl1p1.getArtifacts().getResult().size() == 0, "У воина не удалился артефакт");
    Assert.isTrue(warriorImpl1p1.getAttributes().getLuckRangeAtack() == 16, "Удаленный артефакт не откатил свое влияние");

    // Заново выдать артефакт удачи при стрельбе +20%. Должно все получиться
    artifactForWarriorResult = gameWrapper.giveArtifactToWarrior(gameContext, player1, warrior1p1, TestArtifactRainbowArrowForWarrior.CLASS_NAME);
    assertSuccess(artifactForWarriorResult);
    Assert.isTrue(warriorImpl1p1.getAttributes().getLuckRangeAtack() == 36, "Добавленный артефакт не подействовал");
    artifact1w1p1 = artifactForWarriorResult.getResult().getId();

    // выдать артефакт удачи при стрельбе +20% ЕЩЕ раз тому же воину. Так как повтор, то должен быть отказ
    artifactForWarriorResult = gameWrapper.giveArtifactToWarrior(gameContext, player1, warrior1p1, TestArtifactRainbowArrowForWarrior.CLASS_NAME);
    Assert.isTrue(artifactForWarriorResult.isFail(ARTIFACT_WARRIOR_ALREADY_HAS_IT), "Удалось дать воину 2 одинаковых артефакта");

    // выдать игроку 1 артефакт. Должно выйти
    Result<Artifact<Player>> artifactResult = gameWrapper.giveArtifactToPlayer(gameContext, player1, TestArtifactCapitansSabreForPlayer.classInfo.getName());
    assertSuccess(artifactResult);
    String artifact1Id = artifactResult.getResult().getId();

    // выдадим второйартефакт игроку 1. Должно получиться
    artifactResult = gameWrapper.giveArtifactToPlayer(gameContext, player1, TestArtifactHealinFialForPlayer.classInfo.getName());
    assertSuccess(artifactResult);
    String artifact2Id = artifactResult.getResult().getId();

    // удалим первый артефакт 1 у игрока 1
    assertSuccess(gameWrapper.dropArtifactByPlayer(gameContext, player1, artifact1Id));

    // удалим первый артефакт 2 у игрока 1
    assertSuccess(gameWrapper.dropArtifactByPlayer(gameContext, player1, artifact2Id));

    // Игрок 1 готов
    assertSuccess(gameWrapper.playerReadyToPlay(player1, true));

    // Игрок 2 готов
    assertSuccess(gameWrapper.playerReadyToPlay(player2, true));

    // проверим, что артефакт еще работает. должно быть 6 базовых, 10 - способность персонажа и 20 - артефакт
    Assert.isTrue(warriorImpl1p1.getAttributes().getLuckRangeAtack() == 36, "Добавленный артефакт после старта игры перестал действовать");

    // если первым ходитигрок 2, то передаем ход игроку 1
    gameWrapper.getPlayerOwnsTheRound(gameContext)
            .peak(player -> {
              if (player.getId().equals(player2)) {
                assertSuccess(gameWrapper.nextTurn(gameContext, player2));
              }
            });

    // переходы хода к игроку 2
    assertSuccess(gameWrapper.nextTurn(gameContext, player1));
    // проверим, что артефакт еще работает. см расчет выше
    Assert.isTrue(warriorImpl1p1.getAttributes().getLuckRangeAtack() == 36, "Добавленный артефакт после перехода хода перестал действовать");
    // переходы хода к игроку 1
    assertSuccess(gameWrapper.nextTurn(gameContext, player2));

    // проверим, что артефакт еще работает. см расчет выше
    Assert.isTrue(warriorImpl1p1.getAttributes().getLuckRangeAtack() == 36, "Добавленный артефакт после перехода хода перестал действовать");

    // дадим игроку 1 артефакт лечения воинов
    Result<Artifact<Player>> artifactForPlayerResult = gameWrapper.giveArtifactToPlayer(gameContext, player1, TestArtifactHealinFialForPlayer.classInfo.getName());
    assertSuccess(artifactForPlayerResult);
    String artifact1p1 = artifactForPlayerResult.getResult().getId();

    // Еще раз дадим игроку 1 артефакт лечения воинов. Не должно выйти так как можно только 1 артефакт за ход добавлять
    artifactForPlayerResult = gameWrapper.giveArtifactToPlayer(gameContext, player1, TestArtifactHealinFialForPlayer.classInfo.getName());
    Assert.isTrue(artifactForPlayerResult.isFail(ARTIFACT_CAN_NOT_TAKE_AT_THIS_TURN), "Игрок 1 смог взять второй артифакт за один ход");

    warriorImpl1p1.getAttributes().setHealth(1000);

    // переходы хода к игроку 2
    assertSuccess(gameWrapper.nextTurn(gameContext, player1));

    // здоровье первому воину игрока 1 не должно добавиться
    Assert.isTrue(warriorImpl1p1.getAttributes().getHealth() == 1000, "Артифакт выполнил лечение не в тот момент");

    // переходы хода к игроку 1
    assertSuccess(gameWrapper.nextTurn(gameContext, player2));

    // Теперь здоровье первому воину игрока 1 должно добавиться так как этот артифакт работает раз в круг
    // то есть когда игрок получает ход
    Assert.isTrue(warriorImpl1p1.getAttributes().getHealth() == 1010, "Артифакт не выполнил лечение. Не сработал");

    // И снова дадим игроку 1 артефакт лечения воинов. Не должно выйти так как можно только 1 артефакт такой уже есть
    artifactForPlayerResult = gameWrapper.giveArtifactToPlayer(gameContext, player1, TestArtifactHealinFialForPlayer.classInfo.getName());
    Assert.isTrue(artifactForPlayerResult.isFail(ARTIFACT_PLAYER_ALREADY_HAS_IT), "Игрок 1 смог взять два одинаковых артефакта");

    // даем другой артефакт. Должно получиться так как стартовый набор позволяет иметь 2 артефакта
    artifactForPlayerResult = gameWrapper.giveArtifactToPlayer(gameContext, player1, TestArtifactHugeHummerForPlayer.classInfo.getName());
    assertSuccess(artifactForPlayerResult);
    String artifact2p1 = artifactForPlayerResult.getResult().getId();
    Assert.isTrue(gameWrapper.getCore().findUserByName(player1).getResult().getArtifacts().getResult().size() == 2, "У игрока не 2 артифакта,как должно быть");

    // переходы хода к игроку 2
    assertSuccess(gameWrapper.nextTurn(gameContext, player1));
    // переходы хода к игроку 1
    assertSuccess(gameWrapper.nextTurn(gameContext, player2));

    // даем третий артефакт. Должен быть отказ так как стартовый набор позволяет иметь 2 артефакта
    artifactForPlayerResult = gameWrapper.giveArtifactToPlayer(gameContext, player1, TestArtifactHugeHummerForPlayer.classInfo.getName());
    Assert.isTrue(artifactForPlayerResult.isFail(ARTIFACT_ARTIFACT_QUANTITY_HAS_BEEN_EXCEEDED), "Игрок 1 смог взять третий артефакт");

    // на этом переходе должен расшириться димит на артефакты
    // переходы хода к игроку 2
    assertSuccess(gameWrapper.nextTurn(gameContext, player1));
    // переходы хода к игроку 1
    assertSuccess(gameWrapper.nextTurn(gameContext, player2));

    // даем третий артефакт. Должен быть успех так как лимит расширен до 3-х артефактов
    assertSuccess(gameWrapper.giveArtifactToPlayer(gameContext, player1, TestArtifactCapitansSabreForPlayer.classInfo.getName()));

    // Пробуем выбросить первый артефакт. Должна быть ошибка так как в этом ходу был взят другой артефакт
    artifactForPlayerResult = gameWrapper.dropArtifactByPlayer(gameContext, player1, artifact1p1);
    Assert.isTrue(artifactForPlayerResult.isFail(ARTIFACT_CAN_NOT_DROP_AT_THIS_TURN), "Игрок 1 смог бросить артефакт в тот же ход в который взял другой");

    // переходы хода к игроку 2
    assertSuccess(gameWrapper.nextTurn(gameContext, player1));
    // переходы хода к игроку 1
    assertSuccess(gameWrapper.nextTurn(gameContext, player2));

    // выбрасываем артефакт, которого нет у игрока
    artifactForPlayerResult = gameWrapper.dropArtifactByPlayer(gameContext, player1, "1212");
    Assert.isTrue(artifactForPlayerResult.isFail(ARTIFACT_NOT_FOUND_BY_PLAYER), "Игрок 1 смог бросить артефакт которого у него нет");

    // выбрасываем первый артефакт. Должно получиться
    artifactForPlayerResult = gameWrapper.dropArtifactByPlayer(gameContext, player1, artifact1p1);
    assertSuccess(artifactForPlayerResult);
    Assert.isTrue(gameWrapper.getCore().findUserByName(player1).getResult().getArtifacts().getResult().size() == 2, "артифакт не удалился");

    // выбрасываем второй артефакт. Не должно выйти так как более одного действия с артефактом за ход
    artifactForPlayerResult = gameWrapper.dropArtifactByPlayer(gameContext, player1, artifact2p1);
    Assert.isTrue(artifactForPlayerResult.isFail(ARTIFACT_CAN_NOT_DROP_AT_THIS_TURN), "Игрок 1 смог бросить артефакт в тот же ход в который выкинул другой");


    assertSuccess(gameWrapper.getCore().removeGameContext(gameContext));
  }

  @Test
  public void doTest() {
    innerDoTest();
  }
}
