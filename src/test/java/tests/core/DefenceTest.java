package tests.core;

import abstracts.AbstractMapTest;
import api.core.Context;
import api.core.Result;
import api.entity.warrior.Warrior;
import api.entity.weapon.Weapon;
import api.game.action.InfluenceResult;
import api.game.wraper.GameWrapper;
import api.geo.Coords;
import config.TestContextConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.util.Assert;
import tests.core.entity.warrior.TestArmoredSkeleton;
import tests.core.entity.warrior.TestArmoredVityaz;
import tests.core.entity.warrior.TestUnluckyVityaz;
import tests.core.entity.weapons.TestFixedBow;
import tests.core.entity.weapons.TestFixedSword;
import tests.core.entity.weapons.TestGreatSmallShield;

import static core.system.error.GameErrors.WARRIOR_ATTACK_TARGET_IS_OUT_OF_RANGE;

/**
 * Проверка защиты воинов
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {DefenceTest.class})
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = TestContextConfiguration.class)
@ActiveProfiles({"mockDb", "test"})
public class DefenceTest extends AbstractMapTest {

  @Autowired
  GameWrapper gameWrapper;

  public DefenceTest setGameWrapper(GameWrapper gameWrapper) {
    this.gameWrapper = gameWrapper;
    return this;
  }

  public void innerDoTest() {
    initMapForTwoPlayers(gameWrapper, "DefenceTest_user1", "DefenceTest_User2",
            new String[]{TestUnluckyVityaz.classInfo.getName()
                    , TestUnluckyVityaz.classInfo.getName()
                    , TestArmoredVityaz.classInfo.getName()
                    , TestArmoredSkeleton.classInfo.getName()});
    Context context = gameWrapper.getCore().findGameContextByUID(gameContext).getResult();


    // Дадим воину 1 игрока 1 фиксированный лук
    Result<Weapon> weaponResult = gameWrapper.giveWeaponToWarrior(gameContext, player1, warrior1p1, TestFixedBow.classInfo.getName());
    assertSuccess(weaponResult);
    String bowWarrior1p1 = weaponResult.getResult().getId();

    // Дадим воину 1 игрока 2 щит и меч на 500 фиксированных HP
    assertSuccess(gameWrapper.giveWeaponToWarrior(gameContext, player2, warrior1p2, TestGreatSmallShield.classInfo.getName()));
    weaponResult = gameWrapper.giveWeaponToWarrior(gameContext, player2, warrior1p2, TestFixedSword.classInfo.getName());
    assertSuccess(weaponResult);
    String weaponFixedSword_w1p2 = weaponResult.getResult().getId();

    // Дадим воину 2 игрока 2 щит
    assertSuccess(gameWrapper.giveWeaponToWarrior(gameContext, player2, warrior2p2, TestGreatSmallShield.classInfo.getName()));

    // Дадим воину 2 игрока 1 щит и меч
//    assertSuccess(gameWrapper.giveWeaponToWarrior(gameContext, player1, warrior2p1, TestSmallShield.CLASS_NAME));
    weaponResult = gameWrapper.giveWeaponToWarrior(gameContext, player1, warrior2p1, TestFixedSword.classInfo.getName());
    assertSuccess(weaponResult);
    String weaponFixedSword = weaponResult.getResult().getId();

    // поставим воина 2 игрока 1 и воина 1 игрока 2 на расстояние 1 см друг от друга
    Result<Warrior> warriorResult = gameWrapper.moveWarriorTo(gameContext, player1, warrior2p1, new Coords(800, 480));
    assertSuccess(warriorResult);
    Assert.isTrue(warriorResult.getResult().getCoords().equals(800, 480), "Неверные координаты воина 2 игрока 1");
    warriorResult = gameWrapper.moveWarriorTo(gameContext, player2, warrior1p2, new Coords(800, 520));
    assertSuccess(warriorResult);
    Assert.isTrue(warriorResult.getResult().getCoords().equals(800, 520), "Неверные координаты воина 2 игрока 2");

    // Игроки готовы
    assertSuccess(gameWrapper.playerReadyToPlay(player1, true));
    assertSuccess(gameWrapper.playerReadyToPlay(player2, true));

    // если первым ходитигрок 2, то передаем ход игроку 1
    gameWrapper.getPlayerOwnsTheRound(gameContext)
            .peak(player -> {
              if (player.getId().equals(player2)) {
                assertSuccess(gameWrapper.nextTurn(gameContext, player2));
              }
            });


    // Пробуем атаковать воином 1 игрока 1 воина 1 игрока 2. Это НЕ должно выйти так как велика дистанция
    Result<InfluenceResult> attackResult = gameWrapper.attackWarrior(gameContext, player1, warrior1p1, warrior1p2, bowWarrior1p1);
    Assert.isTrue(attackResult.isFail(WARRIOR_ATTACK_TARGET_IS_OUT_OF_RANGE), "Атака при превышении дальности стрельбы удалась");

    // Подвинем воина 1 игрока 1 поближе
    assertSuccess(gameWrapper.moveWarriorTo(gameContext, player1, warrior1p1, new Coords(350, 400)));

    // переходы ходом опять до воина 1
    assertSuccess(gameWrapper.nextTurn(gameContext, player1));
    assertSuccess(gameWrapper.nextTurn(gameContext, player2));

    // Пробуем атаковать воином 1 игрока 1 воина 1 игрока 2. Это должно выйти. Щит сработать не должен. Урон должен составить урон лука - броня
    // = 900 - 49%(3 класс брони) = 459. Останется здоровья 2000 - 459 = 1541
    attackResult = gameWrapper.attackWarrior(gameContext, player1, warrior1p1, warrior1p2, bowWarrior1p1);
    assertSuccess(attackResult);
    Assert.isTrue(attackResult.getResult().getTarget().getAttributes().getHealth() == 1541, "Неверно нанесен урон с учетом брони"
            + attackResult.getResult().getTarget().getAttributes().getHealth());

    // подлечим воина 1 игрока 2
    gameWrapper.getCore().findUserByName(player2).getResult().findWarriorById(warrior1p2).getResult().getAttributes().setHealth(2000);

    // Пробуем атаковать воином 1 игрока 1 воина 1 игрока 2. Это должно выйти. Щит сработать не должен. Урон должен составить урон лука - броня
    // = 900 - 49%(3 класс брони) = 459. Останется здоровья 2000 - 459 = 1541
    attackResult = gameWrapper.attackWarrior(gameContext, player1, warrior1p1, warrior1p2, bowWarrior1p1);
    assertSuccess(attackResult);
    Assert.isTrue(attackResult.getResult().getTarget().getAttributes().getHealth() == 1541, "Неверно нанесен урон с учетом брони и щита"
            + attackResult.getResult().getTarget().getAttributes().getHealth());

    // подлечим воина 2 игрока 2
    gameWrapper.getCore().findUserByName(player2).getResult().findWarriorById(warrior1p2).getResult().getAttributes().setHealth(2000);

    // Атакуем воина 1 игрока 2 мечником, воином 2 игрока 1. Должно выйти. Удар должен быть отбит. в ответ нападающий должен получить
    // урон от меча = 500 * 0.6 - 36% (2 класс брони) = 192
    attackResult = gameWrapper.attackWarrior(gameContext, player1, warrior2p1, warrior1p2, weaponFixedSword);
    assertSuccess(attackResult);
    Assert.isTrue(attackResult.getResult().getTarget().getAttributes().getHealth() == 2000, "(1) Удар не заблокирован");
    Assert.isTrue(attackResult.getResult().getInfluencers().get(0).isSuccessful() == false
    && attackResult.getResult().getInfluencers().get(0).getModifier().isBlocked(), "удар неверно отбит. Нет признаков.");
    // проверяем контрудар
    Assert.isTrue(attackResult.getResult().getContrInfluence() != null, "Ответный удар отсутствует");
    // проверяем нападающего
    Assert.isTrue(gameWrapper.getCore().findUserByName(player1).getResult().findWarriorById(warrior2p1).getResult().getAttributes().getHealth() == 2000 -192, "Ответный удар рассчитан не верно (1)");

    // Повторно атакуем воина 1 игрока 2 мечником, воином 2 игрока 1. Должно выйти. Удар должен быть отбит. в ответ
    // Удара быть не должно так как в этом ходе воин уже отвечал на удар
    attackResult = gameWrapper.attackWarrior(gameContext, player1, warrior2p1, warrior1p2, weaponFixedSword);
    assertSuccess(attackResult);
    Assert.isTrue(attackResult.getResult().getTarget().getAttributes().getHealth() == 2000, "(2) Удар не заблокирован");
    Assert.isTrue(attackResult.getResult().getInfluencers().get(0).isSuccessful() == false
            && attackResult.getResult().getInfluencers().get(0).getModifier().isBlocked(), "удар неверно отбит. Нет признаков.");
    // проверяем контрудар
    Assert.isTrue(attackResult.getResult().getContrInfluence() == null, "Ответный удар был повторно за один ход");

    // переходы ходом опять до воина 1
    assertSuccess(gameWrapper.nextTurn(gameContext, player1));
    assertSuccess(gameWrapper.nextTurn(gameContext, player2));

    // И в третий раз атакуем воина 1 игрока 2 мечником, воином 2 игрока 1. Должно выйти. Удар должен быть отбит. Так
    // как прошел полный круг, то контратака стала возможной опять и в ответ нападающий должен получить
    // урон от меча = 500 * 0.6 - 36% (2 класс брони) = 192
    attackResult = gameWrapper.attackWarrior(gameContext, player1, warrior2p1, warrior1p2, weaponFixedSword);
    assertSuccess(attackResult);
    Assert.isTrue(attackResult.getResult().getTarget().getAttributes().getHealth() == 2000, "(3) Удар не заблокирован");
    Assert.isTrue(attackResult.getResult().getInfluencers().get(0).isSuccessful() == false
            && attackResult.getResult().getInfluencers().get(0).getModifier().isBlocked(), "удар неверно отбит. Нет признаков.");
    // проверяем контрудар
    Assert.isTrue(attackResult.getResult().getContrInfluence() != null, "Ответный удар отсутствует");
    // проверяем нападающего
    Assert.isTrue(gameWrapper.getCore().findUserByName(player1).getResult()
            .findWarriorById(warrior2p1).getResult().getAttributes().getHealth() == 2000 -192 - 192, "Ответный удар рассчитан не верно (2)");


    // на этом все. Игра кончилась.

    assertSuccess(gameWrapper.getCore().removeGameContext(gameContext));
  }

  @Test
  public void doTest() {
    innerDoTest();
  }
}