package tests.core;

import api.enums.ArmorClassEnum;
import config.TestContextConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.util.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {UtilsTest.class})
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = TestContextConfiguration.class)
@ActiveProfiles({"mockDb", "test"})
public class UtilsTest {

  @Test
  public void testRestrictionPerimeter() {
    ArmorClassEnum armor = ArmorClassEnum.ARMOR_0;
    Assert.isTrue(ArmorClassEnum.ARMOR_0.add(2).equals(ArmorClassEnum.ARMOR_2), "численное сложение брони не работает");
    Assert.isTrue(ArmorClassEnum.ARMOR_3.add(3).equals(ArmorClassEnum.ARMOR_5), "переполнение при численном сложении брони не работает");
    Assert.isTrue(ArmorClassEnum.ARMOR_0.add(ArmorClassEnum.ARMOR_1).equals(ArmorClassEnum.ARMOR_1), "enum сложение брони не работает");
    Assert.isTrue(ArmorClassEnum.ARMOR_4.add(ArmorClassEnum.ARMOR_2).equals(ArmorClassEnum.ARMOR_5), "переполнение при enum сложении брони не работает");


  }
}
