package tests.core;


import abstracts.AbstractMapTest;
import api.core.Context;
import api.core.Result;
import api.entity.weapon.Weapon;
import api.enums.EventType;
import api.game.map.LevelMap;
import api.game.map.Player;
import api.game.wraper.GameWrapper;
import config.TestContextConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.util.Assert;
import tests.core.entity.warrior.TestUnluckyVityaz;
import tests.core.entity.weapons.TestFixedBow;
import tests.core.entity.weapons.TestFixedSword;
import tests.core.entity.weapons.TestGreatSmallShield;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import static core.system.ResultImpl.success;
import static core.system.error.GameErrors.USER_CONNECT_GAME_CLOSED_FOR_NEW_USERS;

/**
 * Побега с поля боя, проигрыша или победы, а так же попытка присоедининия других или проиграших игроков обратно к игре
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {DefenceTest.class})
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = TestContextConfiguration.class)
@ActiveProfiles({"mockDb", "test"})
public class RestWinLooseAndDecampingTest extends AbstractMapTest {

  public static final String PLAYER1_NAME = "RWLADT_User1";
  public static final String PLAYER2_NAME = "RWLADT_User2";
  public static final String PLAYER3_NAME = "RWLADT_User3";

  @Autowired
  GameWrapper gameWrapper;

  public RestWinLooseAndDecampingTest setGameWrapper(GameWrapper gameWrapper) {
    this.gameWrapper = gameWrapper;
    return this;
  }

  public void innerDoTest() {
    initMapForTwoPlayers(gameWrapper, PLAYER1_NAME, PLAYER2_NAME,
            new String[]{TestUnluckyVityaz.classInfo.getName()
                    , TestUnluckyVityaz.classInfo.getName()});

    Context context = gameWrapper.getCore().findGameContextByUID(gameContext).getResult();


    // Дадим воину 1 игрока 1 фиксированный лук
    Result<Weapon> weaponResult = gameWrapper.giveWeaponToWarrior(gameContext, player1, warrior1p1, TestFixedBow.classInfo.getName());
    assertSuccess(weaponResult);
    String bowWarrior1p1 = weaponResult.getResult().getId();

    // Дадим воину 1 игрока 2 щит и меч на 500 фиксированных HP
    assertSuccess(gameWrapper.giveWeaponToWarrior(gameContext, player2, warrior1p2, TestGreatSmallShield.classInfo.getName()));
    weaponResult = gameWrapper.giveWeaponToWarrior(gameContext, player2, warrior1p2, TestFixedSword.classInfo.getName());
    assertSuccess(weaponResult);
    String weaponFixedSword_w1p2 = weaponResult.getResult().getId();

    // Дадим воину 2 игрока 2 щит
    assertSuccess(gameWrapper.giveWeaponToWarrior(gameContext, player2, warrior2p2, TestGreatSmallShield.classInfo.getName()));

    // Дадим воину 2 игрока 1 щит и меч
//    assertSuccess(gameWrapper.giveWeaponToWarrior(gameContext, player1, warrior2p1, TestSmallShield.CLASS_NAME));
    weaponResult = gameWrapper.giveWeaponToWarrior(gameContext, player1, warrior2p1, TestFixedSword.classInfo.getName());
    assertSuccess(weaponResult);
    String weaponFixedSword = weaponResult.getResult().getId();

    // Игроки готовы
    assertSuccess(gameWrapper.playerReadyToPlay(player1, true));
    assertSuccess(gameWrapper.playerReadyToPlay(player2, true));

    // если первым ходитигрок 2, то передаем ход игроку 1
    gameWrapper.getPlayerOwnsTheRound(gameContext)
            .peak(player -> {
              if (player.getId().equals(player2)) {
                assertSuccess(gameWrapper.nextTurn(gameContext, player2));
              }
            });


    // Проверка бегства игроком 1
    // попишемся на события
    AtomicInteger eventCounter = new AtomicInteger(0);
    AtomicInteger playerDecampedIndex = new AtomicInteger(-1);

    AtomicInteger playerWinsIndex = new AtomicInteger(-1);
    AtomicReference<Player> winnerPlayer = new AtomicReference();

    AtomicInteger contextRemovedIndex = new AtomicInteger(-1);
    AtomicInteger nextTurnIndex = new AtomicInteger(-1);
    AtomicReference<Player> decampedPlayer = new AtomicReference();
    AtomicReference<Player> nextTurnPlayer = new AtomicReference();
    AtomicReference<Map<Integer, Player>> disconnectedPlayer = new AtomicReference(new HashMap<>());

    // Событие на побег
    String subIdDecamp = gameWrapper.getCore().findGameContextByUID(gameContext).getResult()
            .subscribeEvent(event -> {
                      playerDecampedIndex.set(eventCounter.getAndIncrement());
                      decampedPlayer.set(event.getSource(Player.class));
                    }
                    , EventType.PLAYER_DECAMPED_FROM_ARENA);

    // Событие на выход из игры игрока
    String subIdDisconnected = gameWrapper.getCore().findGameContextByUID(gameContext).getResult()
            .subscribeEvent(event ->
                            disconnectedPlayer.get().put(eventCounter.getAndIncrement(), event.getSource(Player.class))
                    , EventType.PLAYER_DISCONNECTED);

    // Событие на победу игрока
    String subIdWins = gameWrapper.getCore().findGameContextByUID(gameContext).getResult()
            .subscribeEvent(event -> {
                      playerWinsIndex.set(eventCounter.getAndIncrement());
                      winnerPlayer.set(event.getSource(Player.class));
                    }
                    , EventType.PLAYER_WINS_THE_MATCH);

    // Событие удаление контекста
    String subIdContextClosed = gameWrapper.getCore().findGameContextByUID(gameContext).getResult()
            .subscribeEvent(event ->
                            contextRemovedIndex.set(eventCounter.getAndIncrement())
                    , EventType.GAME_CONTEXT_REMOVED);

    // Событие перехода хода
    String subIdNextTurn = gameWrapper.getCore().findGameContextByUID(gameContext).getResult()
            .subscribeEvent(event ->
                            nextTurnIndex.set(eventCounter.getAndIncrement())
                    , EventType.PLAYER_TAKES_TURN);


    LevelMap levelMap = gameWrapper.getCore().findGameContextByUID(gameContext)
            .map(context1 -> (Result<LevelMap>) success(context1.getLevelMap())).getResult();

    Result<Player> playerResult = gameWrapper.decampFromTheArena(gameContext, PLAYER1_NAME);
    assertSuccess(playerResult);

    // Проверим, что игроков не осталось так как игра закончилась
    Assert.isTrue(levelMap.getPlayers().size() == 0, "Кол-во игроков после побега не уменьшилось");

    // Проверим, что создателем игры стал игрок 2
    Assert.isTrue(levelMap.getGameProcessData().frozenListOfPlayers.get(levelMap.getGameProcessData().indexOfPlayerOwnsTheTurn.get()).getId().equals(PLAYER2_NAME)
            , "плеер 2 не стал владельцем контекста");

    // Проверим, что событие перехода хода  наступило первым и оно для игрока 2
    Assert.isTrue(nextTurnIndex.get() == 0, "Событие перехода хода было не первым " + nextTurnIndex.get());
    Assert.isTrue(decampedPlayer.get().getId().equals(PLAYER1_NAME), "Событие побега Пришло не от того игрока");


    // Проверим, что событие побега наступило вторым и оно для игрока 1
    Assert.isTrue(playerDecampedIndex.get() == 1, "Событие побега игрока было не вторым " + playerDecampedIndex.get());

    // Проверим, что событие Отключения  наступило сначал адля игрока 1, потом для игрока 2
    Assert.isTrue(disconnectedPlayer.get().get(2).getId().equals(PLAYER1_NAME), "Событие отключение  игрока 1 было не третьим");
    Assert.isTrue(playerWinsIndex.get() == 3, "Событие победы игрока 2 не стало 4-м");
    Assert.isTrue(winnerPlayer.get().getId().equals(PLAYER2_NAME), "Победа присвоена не второму игроку");
    Assert.isTrue(disconnectedPlayer.get().get(4).getId().equals(PLAYER2_NAME), "Событие отключение  игрока 2 было не пятым");

    // Проверим, что далее был удален контекст
    Assert.isTrue(contextRemovedIndex.get() == 5, "Удаление контекста не стало последним 6-м событием");

    // на этом все. Игра кончилась.
//    assertSuccess(gameWrapper.getCore().removeGameContext(gameContext));


    // теперь с тремя игроками
    initMapForThreePlayers(gameWrapper, PLAYER1_NAME, PLAYER2_NAME, PLAYER3_NAME);

    // Игроки готовы
    assertSuccess(gameWrapper.playerReadyToPlay(player1, true));
    assertSuccess(gameWrapper.playerReadyToPlay(player2, true));
    assertSuccess(gameWrapper.playerReadyToPlay(player3, true));

    // если первым ходитигрок 2 или 3, то передаем ход игроку 1
    while (!gameWrapper.getPlayerOwnsTheRound(gameContext).getResult().getId().equals(PLAYER1_NAME)){
      assertSuccess(gameWrapper.nextTurn(gameContext, gameWrapper.getPlayerOwnsTheRound(gameContext).getResult().getId()));
    }

    // Событие на побег
    gameWrapper.getCore().findGameContextByUID(gameContext).getResult()
            .subscribeEvent(event -> {
                      playerDecampedIndex.set(eventCounter.getAndIncrement());
                      decampedPlayer.set(event.getSource(Player.class));
                    }
                    , EventType.PLAYER_DECAMPED_FROM_ARENA);

    // Событие на выход из игры игрока
    gameWrapper.getCore().findGameContextByUID(gameContext).getResult()
            .subscribeEvent(event ->
                            disconnectedPlayer.get().put(eventCounter.getAndIncrement(), event.getSource(Player.class))
                    , EventType.PLAYER_DISCONNECTED);

    // Событие на победу игрока
    gameWrapper.getCore().findGameContextByUID(gameContext).getResult()
            .subscribeEvent(event -> {
                      playerWinsIndex.set(eventCounter.getAndIncrement());
                      winnerPlayer.set(event.getSource(Player.class));
                    }
                    , EventType.PLAYER_WINS_THE_MATCH);

    // Событие удаление контекста
    gameWrapper.getCore().findGameContextByUID(gameContext).getResult()
            .subscribeEvent(event ->
                            contextRemovedIndex.set(eventCounter.getAndIncrement())
                    , EventType.GAME_CONTEXT_REMOVED);

    // Событие перехода хода
    gameWrapper.getCore().findGameContextByUID(gameContext).getResult()
            .subscribeEvent(event ->
                            nextTurnIndex.set(eventCounter.getAndIncrement())
                    , EventType.PLAYER_TAKES_TURN);

    eventCounter.set(0);
    playerDecampedIndex.set(-1);
    playerWinsIndex.set(-1);
    winnerPlayer.set(null);
    contextRemovedIndex.set(-1);
    nextTurnIndex.set(-1);
    decampedPlayer.set(null);
    nextTurnPlayer.set(null);
    disconnectedPlayer.set(new HashMap<>());

    // побег первого игрока
    levelMap = gameWrapper.getCore().findGameContextByUID(gameContext)
            .map(context1 -> (Result<LevelMap>) success(context1.getLevelMap())).getResult();

    playerResult = gameWrapper.decampFromTheArena(gameContext, PLAYER1_NAME);
    assertSuccess(playerResult);

    // Проверим, что игроков осталось двое
    Assert.isTrue(levelMap.getPlayers().size() == 2, "Кол-во игроков после побега не уменьшилось");

    // Проверим, что создателем игры стал игрок 2
    Assert.isTrue(levelMap.getGameProcessData().frozenListOfPlayers.get(levelMap.getGameProcessData().indexOfPlayerOwnsTheTurn.get()).getId().equals(PLAYER2_NAME)
            , "плеер 2 не стал владельцем контекста");

    // Проверим, что событие перехода хода  наступило первым и оно для игрока 2
    Assert.isTrue(nextTurnIndex.get() == 0, "Событие перехода хода было не первым " + nextTurnIndex.get());
    Assert.isTrue(decampedPlayer.get().getId().equals(PLAYER1_NAME), "Событие побега Пришло не от того игрока");

    // Проверим, что событие побега наступило вторым и оно для игрока 1
    Assert.isTrue(playerDecampedIndex.get() == 1, "Событие побега игрока было не вторым " + playerDecampedIndex.get());

    // Проверим, что событие Отключения  наступило сначал адля игрока 1, потом для игрока 2
    Assert.isTrue(disconnectedPlayer.get().get(2).getId().equals(PLAYER1_NAME), "Событие отключение  игрока 1 было не третьим");
    Assert.isTrue(playerWinsIndex.get() == -1, "Событие победы гаступило, хотя в игре 2 игрока");

    // пробуем сделать опять вход сбежавши игроком
    playerResult = gameWrapper.connectToGame(PLAYER1_NAME, gameContext);
    Assert.isTrue(playerResult.isFail(USER_CONNECT_GAME_CLOSED_FOR_NEW_USERS), "Подключение к игре сбежавшим игроком удалось");

  }

  @Test
  public void doTest() {
    innerDoTest();
  }
}