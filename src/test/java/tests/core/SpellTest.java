package tests.core;

import abstracts.AbstractMapTest;
import api.core.Context;
import api.core.Result;
import api.game.action.InfluenceResult;
import api.game.wraper.GameWrapper;
import config.TestContextConfiguration;
import core.entity.magic.SpellLighting;
import core.system.error.GameErrors;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.util.Assert;
import tests.core.entity.magic.TestSpellCheckNoParameters;
import tests.core.entity.magic.TestSpellCheckParameters;
import tests.core.entity.magic.TestSpellHealingForPlayer;
import tests.core.entity.magic.TestSpellPoisonLightingForPlayer;
import tests.core.entity.warrior.TestArmoredSkeleton;
import tests.core.entity.warrior.TestArmoredVityaz;
import tests.core.entity.warrior.TestUnluckyVityaz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static core.system.ResultImpl.success;

/**
 * Проверка заклинаний
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SpellTest.class})
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = TestContextConfiguration.class)
@ActiveProfiles({"mockDb", "test"})
public class SpellTest extends AbstractMapTest {

  @Autowired
  GameWrapper gameWrapper;

  public SpellTest setGameWrapper(GameWrapper gameWrapper) {
    this.gameWrapper = gameWrapper;
    return this;
  }

  public void innerDoTest() {
    initMapForTwoPlayers(gameWrapper, "SpellTest_user1", "SpellTest_User2",
            new String[]{TestUnluckyVityaz.classInfo.getName()
                    , TestUnluckyVityaz.classInfo.getName()
                    , TestArmoredVityaz.classInfo.getName()
                    , TestArmoredSkeleton.classInfo.getName()});
    Context context = gameWrapper.getCore().findGameContextByUID(gameContext).getResult();

    // ПРобуем наложить заклинание до начала игры. Должен быть отказ
    Map<Integer, String> spellParams = new HashMap<>(1);
    spellParams.put(0, warrior1p2);
    Result<List<Result<InfluenceResult>>> castResult = gameWrapper.castSpellByPlayer(gameContext, player1, SpellLighting.spellinfo.getName(), spellParams);
    Assert.isTrue(castResult.isFail(GameErrors.CONTEXT_GAME_NOT_STARTED), "Заклинание наложено до начала игры");

    // Игроки готовы
    assertSuccess(gameWrapper.playerReadyToPlay(player1, true));
    assertSuccess(gameWrapper.playerReadyToPlay(player2, true));

    // если первым ходитигрок 2, то передаем ход игроку 1
    gameWrapper.getPlayerOwnsTheRound(gameContext)
            .peak(player -> {
              if (player.getId().equals(player2)) {
                assertSuccess(gameWrapper.nextTurn(gameContext, player2));
              }
            });

    // ПРобуем наложить заклинание не в свой ход. Должен быть отказ
    spellParams.clear();
    spellParams.put(0, warrior1p1);
    castResult = gameWrapper.castSpellByPlayer(gameContext, player2, SpellLighting.spellinfo.getName(), spellParams);
    Assert.isTrue(castResult.isFail(GameErrors.PLAYER_IS_NOT_OWENER_OF_THIS_ROUND), "Заклинание наложено не в свой ход");

    // ПРобуем наложить заклинание c неверным кодом воина. Должен быть отказ
    spellParams.clear();
    spellParams.put(0, warrior1p2 + "9");
    castResult = gameWrapper.castSpellByPlayer(gameContext, player1, SpellLighting.spellinfo.getName(), spellParams);
    Assert.isTrue(castResult.isFail(GameErrors.WARRIOR_NOT_FOUND_ON_THE_MAP), "Заклинание наложено на не существующего юнита");

    // пробуем наложить заклинание, требующее координаты на карте. параметрыы не все. Должен бытьотказ
    spellParams.clear();  // тут надо так как там мусор от предыдущего заклинания
    spellParams.put(5, player2);
    spellParams.put(0, "12,");
    castResult = gameWrapper.castSpellByPlayer(gameContext, player1, TestSpellCheckParameters.spellinfo.getName(), spellParams);
    Assert.isTrue(castResult.isFail(GameErrors.SYSTEM_BAD_PARAMETERS), "Заклинание наложено с не полным набором параметров");

    // пробуем наложить заклинание, требующее координаты на карте. Координаты нечитаемые. Должен бытьотказ
    spellParams.put(0, "12,");
    spellParams.put(1, "12,10");
    spellParams.put(2, "1");
    spellParams.put(3, warrior1p1);
    spellParams.put(4, warrior2p2);
    castResult = gameWrapper.castSpellByPlayer(gameContext, player1, TestSpellCheckParameters.spellinfo.getName(), spellParams);
    Assert.isTrue(castResult.isFail(GameErrors.SPELL_MAP_POINTS_UNPARSEABLE), "Заклинание наложено с не распарсенными координатами");

    // пробуем наложить заклинание, требующее координаты на карте. Координаты выходят за рамки
    // допустимого(параметр 2). Должен быть отказ
    spellParams.put(0, "12,10");
    spellParams.put(1, "399,500");
    castResult = gameWrapper.castSpellByPlayer(gameContext, player1, TestSpellCheckParameters.spellinfo.getName(), spellParams);
    Assert.isTrue(castResult.isFail(GameErrors.SPELL_MAP_POINTS_OUT_OF_RANGE), "Заклинание наложено на координаты, выходящие за допустимые");

    // пробуем наложить заклинание, требующее координаты на карте. Числовой параметр выходитза границы. Должен быть отказ
    spellParams.put(1, "400,500");
    spellParams.put(2, "-3");
    castResult = gameWrapper.castSpellByPlayer(gameContext, player1, TestSpellCheckParameters.spellinfo.getName(), spellParams);
    Assert.isTrue(castResult.isFail(GameErrors.SPELL_INTEGER_OUT_OF_RANGE), "заклинание наложено с числовым параметром, выходящим за границы");

    // Увеличим дальность до дружественного воина. Должен быть отказ
    spellParams.put(2, "1");
    spellParams.put(3, warrior2p1);
    castResult = gameWrapper.castSpellByPlayer(gameContext, player1, TestSpellCheckParameters.spellinfo.getName(), spellParams);
    Assert.isTrue(castResult.isFail(GameErrors.SPELL_TARGET_IS_OUT_OF_RANGE), "(1) Заклинание наложено на цель, с координатами, выходящие за допустимые");

    // Увеличим дальность до вражеского воина. Должен быть отказ
    spellParams.put(2, "1");
    spellParams.put(3, warrior1p1);
    spellParams.put(4, warrior1p2);
    castResult = gameWrapper.castSpellByPlayer(gameContext, player1, TestSpellCheckParameters.spellinfo.getName(), spellParams);
    Assert.isTrue(castResult.isFail(GameErrors.SPELL_TARGET_IS_OUT_OF_RANGE), "(2) Заклинание наложено на цель, с координатами, выходящие за допустимые");

    // вместо дружественного воина подсунем в параметр врага. Должен быть отказ
    spellParams.put(2, "1");
    spellParams.put(3, warrior1p2);
    castResult = gameWrapper.castSpellByPlayer(gameContext, player1, TestSpellCheckParameters.spellinfo.getName(), spellParams);
    Assert.isTrue(castResult.isFail(GameErrors.SPELL_TARGET_IS_NOT_ALLIED), "Заклинание наложено вражеского воина, вместо дружественного");

    // вместо вражеского воина подсунем в параметр дружественного. Должен быть отказ
    spellParams.put(2, "1");
    spellParams.put(3, warrior1p1);
    spellParams.put(4, warrior2p1);
    castResult = gameWrapper.castSpellByPlayer(gameContext, player1, TestSpellCheckParameters.spellinfo.getName(), spellParams);
    Assert.isTrue(castResult.isFail(GameErrors.SPELL_TARGET_IS_ALLIED), "Заклинание наложено на вражеского воина, вместо дружественного");

    // пробуем наложить заклинание, требующее координаты на карте. Все параметры в норме кроме смешанности целей.
    // получаем отказ, но это уже не страшно
    spellParams.put(4, warrior2p2);
    spellParams.put(2, "1");
    castResult = gameWrapper.castSpellByPlayer(gameContext, player1, TestSpellCheckParameters.spellinfo.getName(), spellParams);
    Assert.isTrue(castResult.isFail(GameErrors.SPELL_MIXED_TARGET_ON_BASE_ALGORITHM), "Ошибка заклинания");

    // пробуем заклинание без параметров
    spellParams.clear();
    castResult = gameWrapper.castSpellByPlayer(gameContext, player1, TestSpellCheckNoParameters.spellinfo.getName(), spellParams);
    Assert.isTrue(castResult.isFail(GameErrors.SYSTEM_NOT_REALIZED), "Ошибка заклинания");

    // пробуем заклинание молнии
    spellParams.clear();
    spellParams.put(0, warrior2p2);
    castResult = gameWrapper.castSpellByPlayer(gameContext, player1, SpellLighting.spellinfo.getName(), spellParams);
    assertSuccess(castResult);

    // подлечим подранка
    gameWrapper.getCore().findUserByName(player2)
            .map(player -> player.findWarriorById(warrior2p2))
            .peak(warrior -> warrior.getAttributes().setHealth(2000));

    // Попробуем заклинание ядовитой молнии
    castResult = gameWrapper.castSpellByPlayer(gameContext, player1, TestSpellPoisonLightingForPlayer.spellinfo.getName(), spellParams);
    assertSuccess(castResult);

    int hpB = (Integer) gameWrapper.getCore().findUserByName(player2)
            .map(player -> player.findWarriorById(warrior2p2))
            .map(warrior -> success(warrior.getAttributes().getHealth()))
            .getResult();
    // переходы ходом опять до воина 1
    assertSuccess(gameWrapper.nextTurn(gameContext, player1));
    assertSuccess(gameWrapper.nextTurn(gameContext, player2));

    int hpA = (Integer) gameWrapper.getCore().findUserByName(player2)
            .map(player -> player.findWarriorById(warrior2p2))
            .map(warrior -> success(warrior.getAttributes().getHealth()))
            .getResult();
    Assert.isTrue(hpB > hpA, "Яд не сработал после первого круга");

    hpB = hpA;
    // переходы ходом опять до воина 1
    assertSuccess(gameWrapper.nextTurn(gameContext, player1));
    assertSuccess(gameWrapper.nextTurn(gameContext, player2));

    hpA = (Integer) gameWrapper.getCore().findUserByName(player2)
            .map(player -> player.findWarriorById(warrior2p2))
            .map(warrior -> success(warrior.getAttributes().getHealth()))
            .getResult();
    Assert.isTrue(hpB > hpA, "Яд не сработал после второго круга");

    hpB = hpA;
    // переходы ходом опять до воина 1
    assertSuccess(gameWrapper.nextTurn(gameContext, player1));
    assertSuccess(gameWrapper.nextTurn(gameContext, player2));

    hpA = (Integer) gameWrapper.getCore().findUserByName(player2)
            .map(player -> player.findWarriorById(warrior2p2))
            .map(warrior -> success(warrior.getAttributes().getHealth()))
            .getResult();
    Assert.isTrue(hpB == hpA, "Яд нанес урон после третьего круга");

    // теперь пробуем лечение. Для начала подпортим нашего вояку
    gameWrapper.getCore().findUserByName(player1)
            .map(player -> player.findWarriorById(warrior1p1))
            .peak(warrior -> warrior.getAttributes().setHealth(500));

    hpB = (Integer) gameWrapper.getCore().findUserByName(player1)
            .map(player -> player.findWarriorById(warrior1p1))
            .map(warrior -> success(warrior.getAttributes().getHealth()))
            .getResult();

    // наложим заклинание
    spellParams.clear();
    spellParams.put(0, warrior1p1);
    castResult = gameWrapper.castSpellByPlayer(gameContext, player1, TestSpellHealingForPlayer.spellinfo.getName(), spellParams);
    assertSuccess(castResult);

    // проверим уровень лечения
    hpA = (Integer) gameWrapper.getCore().findUserByName(player1)
            .map(player -> player.findWarriorById(warrior1p1))
            .map(warrior -> success(warrior.getAttributes().getHealth()))
            .getResult();

    Assert.isTrue((hpA  - hpB) >= 10 && (hpA  - hpB) <= 30, "Лечение не сработало после первого круга");
    hpB = hpA;

    // переходы ходом опять до воина 1
    assertSuccess(gameWrapper.nextTurn(gameContext, player1));
    assertSuccess(gameWrapper.nextTurn(gameContext, player2));

    // проверим уровень лечения
    hpA = (Integer) gameWrapper.getCore().findUserByName(player1)
            .map(player -> player.findWarriorById(warrior1p1))
            .map(warrior -> success(warrior.getAttributes().getHealth()))
            .getResult();

    Assert.isTrue((hpA  - hpB) >= 10 && (hpA  - hpB) <= 30, "Яд нанес урон после третьего круга");
    hpB = hpA;

    // переходы ходом опять до воина 1
    assertSuccess(gameWrapper.nextTurn(gameContext, player1));
    assertSuccess(gameWrapper.nextTurn(gameContext, player2));

    // проверим уровень лечения
    hpA = (Integer) gameWrapper.getCore().findUserByName(player1)
            .map(player -> player.findWarriorById(warrior1p1))
            .map(warrior -> success(warrior.getAttributes().getHealth()))
            .getResult();

    Assert.isTrue(hpA == hpB, "Лечение сработало после второго круга");

    // на этом все. Игра кончилась.
    assertSuccess(gameWrapper.getCore().removeGameContext(gameContext));
  }

  @Test
  public void doTest() {
    innerDoTest();
  }
}