package tests.rest;

import abstracts.AbstractRegistratorTest;
import api.game.map.metadata.xml.LevelMapMetaDataXml;
import api.game.wraper.GameWrapper;
import boot.config.DatabaseConfiguration;
import boot.config.security.SecurityConfig;
import com.google.gson.GsonBuilder;
import config.TestContextConfiguration;
import api.dto.artifact.ArtifactDto;
import api.dto.common.PlayerHeroDto;
import api.dto.spell.SpellDto;
import api.dto.warrior.WarriorDto;
import api.dto.weapon.WeaponDto;
import core.system.error.GameErrors;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import tests.Consts;
import web.controller.base.ResultResponse;
import web.controller.game.GameApiRestController;

import java.util.Arrays;
import java.util.List;

/**
 * тест REST
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = {TestContextConfiguration.class, DatabaseConfiguration.class, SecurityConfig.class})
@TestPropertySource(locations = {"classpath:application.yml"})
@WebMvcTest(controllers = GameApiRestController.class, secure = true)
@ActiveProfiles({"mockMail", "test"})
public class RestLoadArmyTest extends AbstractRegistratorTest {

  public static final String WARRIOR_NAME = "Каша";
  public static final String WARRIOR2_NAME = "Каша 2";
  public static final String WARRIOR_SAVE_NAME = WARRIOR_NAME + " вооружен";
  public static final String WARRIOR_SAVE_NAME_2 = WARRIOR_NAME + " вооружен 2";
  public static final String WARRIOR_NEW_SAVE_NAME = WARRIOR_SAVE_NAME + ". Новый";

  @Autowired
  AuthenticationProvider authenticationProvider;

  @Autowired
  GameWrapper gameWrapper;

  @Autowired
  GameApiRestController gameApiRest;

  private void doLogin() {
    try {
      SecurityContextHolder.getContext().setAuthentication(
              authenticationProvider.authenticate(
                      new UsernamePasswordAuthenticationToken(
                              Consts.USER_NAME
                              , Consts.USER_PASSWORD
                              , null)));
    } catch (Throwable th) {
      Assert.isTrue(false, "Авторизация администратором не прошла");
    }
  }
  //======================================================================================================

  private void doLogout() {
    SecurityContextHolder.clearContext();
  }
  //======================================================================================================

  public RestLoadArmyTest innerDoTest() {
    doLogin();

    // запросим список карт
    String restResult = gameApiRest.getMapsList();
    ResultResponse rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос списка карт не является объектом");
    Assert.isTrue(!rr.isFailed(), rr.getErrorMessage());
    LevelMapMetaDataXml maps[] = new GsonBuilder().create().fromJson(rr.getResult(), LevelMapMetaDataXml[].class);
    Assert.isTrue(maps.length == 2, "Неверное количество карт в доступных картах");
    Assert.isTrue(Arrays.stream(maps).anyMatch(levelMapMetaDataXml -> levelMapMetaDataXml.name.equals("2players") && levelMapMetaDataXml.description.equals("Карта для 2-х игроков"))
            , "Не верные данные доступных картах");

    // создадим технический контекст
    restResult = gameApiRest.createTechnicalContext();
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос временного контекста не является объектом");
    Assert.isTrue(!rr.isFailed(), rr.getErrorMessage());
    String contextId = rr.getResult();


    // запросим всех воинов игрока. с кривым контекстом
    restResult = gameApiRest.getPlayerHeroes(contextId + "1");
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос воинов игрока не является объектом 1");
    Assert.isTrue(rr.isFailed(), "Ответ на запрос воинов игрока при неверном контексте");

    // запросим всех воинов игрока. нормально
    restResult = gameApiRest.getPlayerHeroes(contextId);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос воинов игрока не является объектом 2");
    Assert.isTrue(!rr.isFailed(), rr.getErrorMessage());


    // запросим базовые классы воинов. с кривым контекстом
    restResult = gameApiRest.getBaseWarriorClasses(contextId + "1");
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос базовых классов воинов не является объектом 1");
    Assert.isTrue(rr.isFailed(), "Ответ на запрос базовых классов воинов при неверном контексте");

    // запросим базовые классы воинов
    restResult = gameApiRest.getBaseWarriorClasses(contextId);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос базовых классов воинов не является объектом");
    Assert.isTrue(!rr.isFailed(), rr.getErrorMessage());
    ClassesList classesList = new GsonBuilder().create().fromJson("{classesList:" + rr.getResult() + "}", ClassesList.class);
    Assert.isTrue(classesList != null, "Парсинг классов неверен");
    Assert.isTrue(classesList.classesList.size() >= 10, "Парсинг классов неверен. мало классов");


    // запросим базовые классы вооружения. с кривым контекстом
    restResult = gameApiRest.getWeaponClasses(contextId + "1");
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос базовых классов Оружия не является объектом 1");
    Assert.isTrue(rr.isFailed(), "Ответ на запрос базовых классов Оружия при неверном контексте");

    // запросим базовые классы вооружения
    restResult = gameApiRest.getWeaponClasses(contextId);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос базовых классов Оружия не является объектом");
    Assert.isTrue(!rr.isFailed(), rr.getErrorMessage());
    WeaponsList weaponsList = new GsonBuilder().create().fromJson("{weaponsList:" + rr.getResult() + "}", WeaponsList.class);
    Assert.isTrue(weaponsList != null, "Парсинг классов оружия неверен");
    Assert.isTrue(weaponsList.weaponsList.size() >= 11, "Парсинг классов оружия неверен. мало классов");


    // запросим заклинания игрока. с кривым контекстом
    restResult = gameApiRest.getPlayerSpells(contextId + "1");
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос заклинаний игрока не является объектом 1");
    Assert.isTrue(rr.isFailed(), "Ответ на запрос заклинаний игрока при неверном контексте");

    // запросим заклинания игрока
    restResult = gameApiRest.getPlayerSpells(contextId);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос заклинаний игрока не является объектом 2");
    Assert.isTrue(!rr.isFailed(), rr.getErrorMessage());
    PlayerSpellsList spellsList = new GsonBuilder().create().fromJson("{spellsList:" + rr.getResult() + "}", PlayerSpellsList.class);
    Assert.isTrue(spellsList != null, "Парсинг классов заклинаний игрока неверен");
    Assert.isTrue(spellsList.spellsList.size() >= 5, "Парсинг заклинаний неверен. мало классов");


    // запросим базовые артефакты игрока. с кривым контекстом
    restResult = gameApiRest.getPlayerArtifacts(contextId + "1");
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос артефактов игрока не является объектом 1");
    Assert.isTrue(rr.isFailed(), "Ответ на запрос артефактов игрока при неверном контексте");

    // запросим базовые артефакты игрока
    restResult = gameApiRest.getPlayerArtifacts(contextId);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос артефактов игрока не является объектом 2");
    Assert.isTrue(!rr.isFailed(), rr.getErrorMessage());
    ArtifactsList artifactsList = new GsonBuilder().create().fromJson("{artifactsList:" + rr.getResult() + "}", ArtifactsList.class);
    Assert.isTrue(artifactsList != null, "Парсинг артефактов игрока неверен");
    Assert.isTrue(artifactsList.artifactsList.size() >= 3, "Парсинг артефактов игрока неверен. мало классов");


    // запросим базовые артефакты для воина . с кривым контекстом
    restResult = gameApiRest.getWarriorArtifacts(contextId + "1");
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос артефактов для воина не является объектом 1");
    Assert.isTrue(rr.isFailed(), "Ответ на запрос артефактов игрока при неверном контексте");

    // запросим базовые артефакты для воинов
    restResult = gameApiRest.getWarriorArtifacts(contextId);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос артефактов для воина не является объектом");
    Assert.isTrue(!rr.isFailed(), rr.getErrorMessage());
    artifactsList = new GsonBuilder().create().fromJson("{artifactsList:" + rr.getResult() + "}", ArtifactsList.class);
    Assert.isTrue(artifactsList != null, "Парсинг артефактов для воина неверен");
    Assert.isTrue(artifactsList.artifactsList.size() >= 2, "Парсинг артефактов для воина неверен. мало классов");

    //запросим создание воина. Все хитрые комбинации выполняются в рамках других тестов. Тут простое создание через REST
    restResult = gameApiRest.createWarrior(contextId, classesList.classesList.get(0).getTitle(), WARRIOR_NAME, 1, 1);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос создания воина не является объектом");
    Assert.isTrue(!rr.isFailed(), rr.getErrorMessage());
    String warriorId = rr.getResult();

    //запросим создание воина 2.
    restResult = gameApiRest.createWarrior(contextId, classesList.classesList.get(0).getTitle(), WARRIOR2_NAME, 1, 1);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос создания воина не является объектом");
    Assert.isTrue(!rr.isFailed(), rr.getErrorMessage());
    String warrior2Id = rr.getResult();

    // выдадим оружие воину 2
    restResult = gameApiRest.giveWeaponToWarrior(contextId, warrior2Id, weaponsList.weaponsList.get(0).getTitle());
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос выдачи воину оружия не является объектом");
    Assert.isTrue(!rr.isFailed(), rr.getErrorMessage());

    // выдадим оружие воину 1
    restResult = gameApiRest.giveWeaponToWarrior(contextId, warriorId, weaponsList.weaponsList.get(0).getTitle());
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос выдачи воину оружия не является объектом");
    Assert.isTrue(!rr.isFailed(), rr.getErrorMessage());
    String weaponId = rr.getResult();

    // выдадим оружие 2 тому же воину
    restResult = gameApiRest.giveWeaponToWarrior(contextId, warriorId, weaponsList.weaponsList.get(0).getTitle());
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос выдачи воину оружия 2 не является объектом");
    Assert.isTrue(!rr.isFailed(), rr.getErrorMessage());
    String weaponId2 = rr.getResult();

    // сохраним вариант экипировки
    restResult = gameApiRest.saveWarriorAmmunition(contextId, warriorId, WARRIOR_SAVE_NAME);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос сохранения конфигурации воина не является объектом");
    Assert.isTrue(!rr.isFailed(), rr.getErrorMessage());
    String warriorId2 = rr.getResult();
    Assert.isTrue(warriorId.equals(warriorId2), "При сохранении конфигурации воина различаются оригинальный и сохраненный ID");

    // сохраним вариант экипировки, но с другим именем
    restResult = gameApiRest.saveWarriorAmmunition(contextId, warriorId, WARRIOR_SAVE_NAME_2);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос сохранения конфигурации воина не является объектом");
    Assert.isTrue(!rr.isFailed(), rr.getErrorMessage());
    warriorId2 = rr.getResult();
    Assert.isTrue(warriorId.equals(warriorId2), "При сохранении конфигурации воина различаются оригинальный и сохраненный ID");

    // Проверим, что сохранилась наша конфигурация
    restResult = gameApiRest.getPlayerHeroes(contextId);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос воинов игрока не является объектом 3");
    Assert.isTrue(!rr.isFailed(), rr.getErrorMessage());
    HeroesList heroesList = new GsonBuilder().create().fromJson("{heroesList:" + rr.getResult() + "}", HeroesList.class);
    Assert.isTrue(heroesList != null, "Парсинг сохранения воинов неверен");
    Assert.isTrue(heroesList.heroesList.stream().anyMatch(playerHeroJsonWrapper -> playerHeroJsonWrapper.getTitle().equals(WARRIOR_NAME)), "не найдено сохраненных героев");

    // найдем саму сохраненную конфигурацию
    restResult = gameApiRest.findHero(contextId, WARRIOR_NAME);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос сохраненного героя игрока не является объектом");
    Assert.isTrue(!rr.isFailed(), rr.getErrorMessage());
    PlayerHeroDto playerWarrior = new GsonBuilder().create().fromJson(rr.getResult(), PlayerHeroDto.class);
    Assert.isTrue(playerWarrior.getEquipments() != null
            && playerWarrior.getEquipments().stream().anyMatch(warriorEquipment -> warriorEquipment.getName().equals(WARRIOR_SAVE_NAME)), "аммуниция не сохранилась");

    // переименуем конфигурацию, с неверным именем
    restResult = gameApiRest.renameWarriorAmmunition(contextId, warriorId, WARRIOR_SAVE_NAME + "0", WARRIOR_NEW_SAVE_NAME);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос сохраненного героя игрока не является объектом");
    Assert.isTrue(rr.isFailed(), "Произошло переименование экипировки с неверным старым именем");

    // переименуем конфигурацию, с неверным кодом воина
    restResult = gameApiRest.renameWarriorAmmunition(contextId, warrior2Id, WARRIOR_SAVE_NAME, WARRIOR_NEW_SAVE_NAME);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос сохраненного героя игрока не является объектом");
    Assert.isTrue(rr.isFailed(), "Произошло переименование экипировки с кодом другого воина");

    // переименуем конфигурацию, с новым именем, которое уже есть
    restResult = gameApiRest.renameWarriorAmmunition(contextId, warriorId, WARRIOR_SAVE_NAME, WARRIOR_SAVE_NAME_2);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос сохраненного героя игрока не является объектом");
    Assert.isTrue(rr.isFailed(), "Произошло переименование экипировки с дублирующим названием");

    // переименуем конфигурацию, но все уже правильно
    restResult = gameApiRest.renameWarriorAmmunition(contextId, warriorId, WARRIOR_SAVE_NAME, WARRIOR_NEW_SAVE_NAME);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос сохраненного героя игрока не является объектом");
    Assert.isTrue(!rr.isFailed(), rr.getErrorMessage());
    restResult = gameApiRest.findHero(contextId, WARRIOR_NAME);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    playerWarrior = new GsonBuilder().create().fromJson(rr.getResult(), PlayerHeroDto.class);
    Assert.isTrue(playerWarrior.getEquipments() != null
            && playerWarrior.getEquipments().stream().anyMatch(warriorEquipment -> warriorEquipment.getName().equals(WARRIOR_NEW_SAVE_NAME)), "аммуниция не переименовалась");
    Assert.isTrue(playerWarrior.getEquipments() != null
            && !playerWarrior.getEquipments().stream().anyMatch(warriorEquipment -> warriorEquipment.getName().equals(WARRIOR_SAVE_NAME)), "аммуниция со старым именем тоже осталась");

    // запрос списка воинов на карте
    restResult = gameApiRest.getPlayerWarriors(contextId, "");
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос всех воинов всех героев не является объектом");
    Assert.isTrue(!rr.isFailed(), rr.getErrorMessage());
    WarriorDto warriorDtos[] = new GsonBuilder().create().fromJson(rr.getResult(), WarriorDto[].class);
    Assert.isTrue(warriorDtos.length == 2, "Не верный состав ответа всех воинов всех героев");
    // проверим расчет стоимости по предметам
    Assert.isTrue(Arrays.stream(warriorDtos)
            .filter(warriorDto -> warriorDto.getTitle().equals(WARRIOR_NAME))
            .anyMatch(warriorDto -> warriorDto.getGoldCost() == 58), "Не верный расчет стоимости воина " + WARRIOR_NAME);

    Assert.isTrue(Arrays.stream(warriorDtos)
            .filter(warriorDto -> warriorDto.getTitle().equals(WARRIOR2_NAME))
            .anyMatch(warriorDto -> warriorDto.getGoldCost() == 40), "Не верный расчет стоимости воина " + WARRIOR2_NAME);

    // Удаление оружия у воина
    restResult = gameApiRest.dropWeaponByWarrior(contextId, warriorId, weaponId);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос всех воинов всех героев не является объектом");
    Assert.isTrue(!rr.isFailed(), rr.getErrorMessage());
    restResult = gameApiRest.getPlayerWarriors(contextId, "");
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    warriorDtos = new GsonBuilder().create().fromJson(rr.getResult(), WarriorDto[].class);
    Assert.isTrue(Arrays.stream(warriorDtos).anyMatch(warriorDto -> warriorDto.getTitle().equals(WARRIOR_NAME) && warriorDto.getWeapons().size() == 1)
            , "Оружие не удалилось");

    // сохраним вариант экипировки, но с другим именем
    restResult = gameApiRest.saveWarriorAmmunition(contextId, warriorId, WARRIOR_NEW_SAVE_NAME);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос сохранения 2 конфигурации воина не является объектом");
    Assert.isTrue(!rr.isFailed(), rr.getErrorMessage());

    // Проверим что теперь в разных Экипировках разное кол-во оружия
    restResult = gameApiRest.findHero(contextId, WARRIOR_NAME);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    playerWarrior = new GsonBuilder().create().fromJson(rr.getResult(), PlayerHeroDto.class);
    Assert.isTrue(playerWarrior.getEquipments().stream()
            .map(warriorEquipmentDto -> warriorEquipmentDto.getSerializedJson().getWeapons().size())
            .reduce(0, (acc, cnt) -> acc += cnt) == 3, "Неверное суммарое кол-во оружия во всех экипировках");

    // удаление экипировки с неверным названием
    restResult = gameApiRest.removeWarriorAmmunition(contextId, warriorId, WARRIOR_SAVE_NAME);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос удадления экипировки с неверным названием не является объектом");
    Assert.isTrue(rr.isFailed(), "Экипировка с неверным названием удалилась");
    Assert.isTrue(rr.getErrorCode().equals(GameErrors.WARRIOR_HERO_EQUIPMENT_NOT_FOUND_BY_NAME.getErrorCode())
            , "неверный код ошибки на запрос удадления экипировки с неверным названием");

    // удаление экипировки с неверным воином
    restResult = gameApiRest.removeWarriorAmmunition(contextId, warriorId + "0", WARRIOR_NEW_SAVE_NAME);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос удадления экипировки с неверным воином не является объектом");
    Assert.isTrue(rr.isFailed(), "Экипировка с неверным воином удалилась");
    Assert.isTrue(rr.getErrorCode().equals(GameErrors.WARRIOR_NOT_FOUND_AT_PLAYER_BY_NAME.getErrorCode())
            , "неверный код ошибки на запрос удадления экипировки с неверным воином");

    // удаление экипировки правильное
    restResult = gameApiRest.removeWarriorAmmunition(contextId, warriorId, WARRIOR_NEW_SAVE_NAME);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос удадления экипировки не является объектом");
    Assert.isTrue(!rr.isFailed(), rr.getErrorMessage());
    restResult = gameApiRest.findHero(contextId, WARRIOR_NAME);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    playerWarrior = new GsonBuilder().create().fromJson(rr.getResult(), PlayerHeroDto.class);
    Assert.isTrue(playerWarrior.getEquipments().size() == 1
            && !playerWarrior.getEquipments().stream().anyMatch(warriorEquipment -> warriorEquipment.getName().equals(WARRIOR_NEW_SAVE_NAME))
            , "аммуниция не удалилась");

    // удалим последнюю экипировку. Вместе с ней должен удалиться и герой
    restResult = gameApiRest.removeWarriorAmmunition(contextId, warriorId, WARRIOR_SAVE_NAME_2);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос удадления экипировки не является объектом");
    Assert.isTrue(!rr.isFailed(), rr.getErrorMessage());
    restResult = gameApiRest.findHero(contextId, WARRIOR_NAME);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(StringUtils.isEmpty(rr.getResult()), "воин после удаления последней экипировки  не удалился");


    // Удаление воина
    restResult = gameApiRest.removeWarrior(contextId, warriorId);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос удаления воина не является объектом");
    Assert.isTrue(!rr.isFailed(), rr.getErrorMessage());
    restResult = gameApiRest.getPlayerWarriors(contextId, "");
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    warriorDtos = new GsonBuilder().create().fromJson(rr.getResult(), WarriorDto[].class);
    Assert.isTrue(warriorDtos.length == 1, "Воин не удалился");


    doLogout();
    return this;
  }

  public class HeroesList {
    public List<PlayerHeroDto> heroesList;
  }

  public class ClassesList {
    public List<PlayerHeroDto> classesList;
  }

  public class WeaponsList {
    public List<WeaponDto> weaponsList;
  }

  public class PlayerSpellsList {
    public List<SpellDto> spellsList;
  }

  public class ArtifactsList {
    public List<ArtifactDto> artifactsList;
  }

  @Test
  public void test() {
    initMap(gameWrapper);
    innerDoTest();
  }
}
