package web.controller.security;

import api.core.Result;
import api.core.database.service.UserService;
import core.database.entity.security.User;
import core.database.entity.security.UserAccountOperationRequest;
import core.security.CaptchaService;
import core.security.CaptchaStorageSingleton;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import web.controller.base.ResultResponse;

import javax.servlet.http.HttpSession;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Рест контроллер для регистрации новых пользователей
 */
@RestController
@RequestMapping({"/security"})
public class UserAccountRestController {
  public static final String REGISTER_USER_METHOD = "registerUser";
  public static final String CONFIRM_REGISTER_USER_METHOD = "confirmUser";
  public static final String RESTORE_ACCOUNT_ACCESS_METHOD = "restoreAccountAccess";
  public static final String CONFIRM_RESTORE_ACCOUNT_ACCESS_METHOD = "confirmRestoreAccountAccess";

  @Autowired
  UserService userService;

  @Autowired
  CaptchaService captchaService;

  @Autowired
  CaptchaStorageSingleton captchaStorageSingleton;

  /**
   * Запрос на регистрацию пользователя. При этом создается запись в специальной таблице, если все данные коректны,
   * и на почту высыоается письмо для подтверждения. В течение некоторого времени запись на регистрацию будет активна
   * и, перейдя по ссылке из письма запись пользователя перенесется в список реальных пользователей с правами игрока
   *
   * @param userName
   * @param password
   * @param retryPassword
   * @param eMail
   * @return
   */
  @RequestMapping(method = POST, path = {"/" + REGISTER_USER_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String registerUser(
          @RequestParam(value = "username") String userName
          , @RequestParam(value = "password") String password
          , @RequestParam(value = "retryPassword") String retryPassword
          , @RequestParam(value = "e-mail") String eMail
          , @RequestParam(value = "captcha") String captcha
          , HttpSession session
  ) {

    Result<UserAccountOperationRequest> registerResult = captchaService.checkCaptcha(session, captcha)
            .map(aBoolean -> userService.registerUser(userName, password, retryPassword, eMail));
    return registerResult.isFail()
            ? ResultResponse.failedAsJson(REGISTER_USER_METHOD, registerResult.getError())
            : ResultResponse.succeedAsJson(REGISTER_USER_METHOD, registerResult.getResult());
  }
  //==================================================================================================

  @RequestMapping(method = POST, path = {"/" + CONFIRM_REGISTER_USER_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String confirmRegisterUser(
          @RequestParam(value = "confirmCode") String confirmCode) {
    Result<User> confirmResult = userService.confirmRegisterUser(confirmCode);
    return confirmResult.isFail()
            ? ResultResponse.failedAsJson(REGISTER_USER_METHOD, confirmResult.getError())
            : ResultResponse.succeedAsJson(REGISTER_USER_METHOD, confirmResult.getResult().getName());
  }
  //==================================================================================================

  /**
   * Восстановления доступа к учетной записи. Первая фаза. Заполнены пользователь или почта
   *
   * @param userName
   * @param eMail
   * @return
   */
  @RequestMapping(method = POST, path = {"/" + RESTORE_ACCOUNT_ACCESS_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String restoreAccountAccess(
          @RequestParam(value = "userName", required = false) String userName
          , @RequestParam(value = "eMail", required = false) String eMail
          , @RequestParam(value = "captcha") String captcha
          , HttpSession session) {
    Result<UserAccountOperationRequest> confirmResult = captchaService.checkCaptcha(session, captcha)
            .map(aBoolean -> userService.restoreAccountAccess(userName, eMail));
    return confirmResult.isFail()
            ? ResultResponse.failedAsJson(RESTORE_ACCOUNT_ACCESS_METHOD, confirmResult.getError())
            : ResultResponse.succeedAsJson(RESTORE_ACCOUNT_ACCESS_METHOD, confirmResult.getResult().getConfirmCode());
  }
  //==================================================================================================

  /**
   * Восстановления доступа к учетной записи. задание нового пароля
   *
   * @param confirmCode
   * @param password
   * @param retypePassword
   * @return
   */
  @RequestMapping(method = POST, path = {"/" + CONFIRM_RESTORE_ACCOUNT_ACCESS_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String confirmRestoreAccountAccess(
          @RequestParam(value = "confirmCode") String confirmCode
          , @RequestParam(value = "password", required = false) String password
          , @RequestParam(value = "retypePassword", required = false) String retypePassword) {
    Result<User> confirmResult = userService.restoreAccountAccessSetNewPassword(confirmCode
            , password
            , retypePassword);
    return confirmResult.isFail()
            ? ResultResponse.failedAsJson(CONFIRM_RESTORE_ACCOUNT_ACCESS_METHOD, confirmResult.getError())
            : ResultResponse.succeedAsJson(CONFIRM_RESTORE_ACCOUNT_ACCESS_METHOD, confirmResult.getResult().getName());
  }
  //==================================================================================================

  @RequestMapping(method = GET, path = {"/captcha"}, produces = MediaType.IMAGE_JPEG_VALUE)
  public byte[] captcha(HttpSession session) {
    return captchaService.getImage(session);
  }

}