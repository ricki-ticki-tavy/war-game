package web.controller.index;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

@Controller
public class IndexController {

  @RequestMapping({"/", "/index"})
  public String home(Map<String, Object> model) {
    model.put("message", "HowToDoInJava Reader !!");
    return "index";
  }

}