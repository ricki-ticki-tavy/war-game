package web.controller.index;

import api.core.Core;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

@Controller
public class GamePageController {

  @Autowired
  Core core;

  @RequestMapping({"/army"})
  public String army(Map<String, Object> model) {
    return "army";
  }

  @RequestMapping({"/tavern"})
  public String tavern(Map<String, Object> model) {
    return "tavern";
  }

  @RequestMapping({"/arena"})
  public String arena(Map<String, Object> model) {
    model.put("core", core);
    return "arena";
  }

}