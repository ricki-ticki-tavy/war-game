package web.controller.game;
// TODO
// Тесты на dropWeaponByWarrior

import api.core.Context;
import api.core.Result;
import api.entity.artifct.Artifact;
import api.entity.warrior.Warrior;
import api.entity.weapon.Weapon;
import api.game.map.Player;
import api.game.map.metadata.GameRules;
import api.game.map.metadata.xml.LevelMapMetaDataXml;
import api.game.wraper.GameWrapper;
import api.geo.Coords;
import com.google.gson.GsonBuilder;
import api.dto.core.ContextDto;
import core.entity.map.GameRulesImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import web.controller.base.AbstractRestService;
import web.controller.base.ResultResponse;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Основной рест-сервер
 */
@RestController
@RequestMapping({"game/api"})
public class GameApiRestController extends AbstractRestService {
  public static final String CREATE_TECHNICAL_CONTEXT_METHOD = "createTechnicalContext";
  public static final String CREATE_CONTEXT_METHOD = "createGame";
  public static final String DECAMP_FROM_THE_ARENA_METHOD = "decampFromTheArena";
  public static final String CONNECT_TO_GAME_METHOD = "connectToGame";
  public static final String SET_GAME_VISIBILITY_METHOD = "setGameVisibility";
  public static final String GET_EXISTING_GAME_CONTEXT_METHOD = "getExistingGameContext";
  public static final String GET_GAME_FULL_DATA_METHOD = "getGameFullData";
  public static final String GET_ACTIVE_GAME_LIST_METHOD = "getActiveGameList";

  public static final String GET_MAPS_LIST_METHOD = "getMapsList";
  public static final String GET_ACTIVE_USERS_LIST_METHOD = "getActiveUsersList";

  public static final String END_TURN_METHOD = "endTurn";

  public static final String GIVE_ARTIFACT_TO_PLAYER_METHOD = "giveArtifactToPlayer";
  public static final String FIND_PLAYER_HERO_METHOD = "findPlayerHero";
  public static final String GET_PLAYER_HEROES_METHOD = "getPlayerHeroes";
  public static final String GET_BASE_WARRIOR_CLASSES_METHOD = "getBaseWarriorClasses";
  public static final String GET_WEAPON_CLASSES_METHOD = "getWeaponClasses";
  public static final String GET_PLAYER_SPELL_METHOD = "getPlayerSpells";
  public static final String GET_WARRIOR_SPELL_METHOD = "getWarriorSpells";
  public static final String GET_PLAYER_ARTIFACTS_METHOD = "getPlayerArtifacts";
  public static final String GET_WARRIOR_ARTIFACTS_METHOD = "getWarriorArtifacts";

  public static final String CREATE_WARRIOR_METHOD = "createWarrior";
  public static final String REMOVE_WARRIOR_METHOD = "removeWarrior";
  public static final String GIVE_WEAPON_TO_WARRIOR_METHOD = "giveWeaponToWarrior";
  public static final String DROP_WEAPON_BY_WARRIOR_METHOD = "dropWeaponByWarrior";
  public static final String MOVE_WARRIOR_METHOD = "moveWarriorTo";
  public static final String ROLLBACK_MOVE_METHOD = "rollbackMove";

  public static final String SAVE_WARRIOR_AMMUNITION_METHOD = "saveWarriorAmmunition";
  public static final String RENAME_WARRIOR_AMMUNITION_METHOD = "renameWarriorAmmunition";
  public static final String SET_READY_TO_PLAY_STATUS_METHOD = "playerReadyToPlay";
  public static final String REMOVE_WARRIOR_AMMUNITION_METHOD = "removeWarriorAmmunition";

  public static final String GET_PLAYER_WARRIORS_METHOD = "getPlayerWarriors";

  @Autowired
  GameWrapper gameWrapper;

  /**
   * Создание контекста игры.
   *
   * @return
   */
  @RequestMapping(method = GET, path = {"/" + CREATE_CONTEXT_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String createGame(
          @RequestParam(value = "maxStartCreaturePerPlayer") int maxStartCreaturePerPlayer
          , @RequestParam(value = "mapName") String mapName
          , @RequestParam(value = "gameName") String gameName
          , @RequestParam(value = "hidden", required = false, defaultValue = "false") boolean hidden
          , @RequestParam(value = "maxSummonedCreaturePerPlayer", required = false, defaultValue = "2") int maxSummonedCreaturePerPlayer
          , @RequestParam(value = "movesCountPerTurnForEachPlayer", required = false, defaultValue = "3") int movesCountPerTurnForEachPlayer
          , @RequestParam(value = "startMannaPoints", required = false, defaultValue = "50") int startMannaPoints
          , @RequestParam(value = "maxMannaPoints", required = false, defaultValue = "70") int maxMannaPoints
          , @RequestParam(value = "restorationMannaPointsPerTotalRound", required = false, defaultValue = "1") int restorationMannaPointsPerTotalRound
          , @RequestParam(value = "maxPlayerRoundTime", required = false, defaultValue = "6000") int maxPlayerRoundTime
          , @RequestParam(value = "warriorSize", required = false, defaultValue = "30") int warriorSize
  ) {
    Result<Context> contextResult = getLoggedinUserName()
            .map(userName -> {
              GameRules gameRules = new GameRulesImpl(maxStartCreaturePerPlayer, maxSummonedCreaturePerPlayer
                      , movesCountPerTurnForEachPlayer, startMannaPoints
                      , maxMannaPoints, restorationMannaPointsPerTotalRound
                      , maxPlayerRoundTime, warriorSize);
              return gameWrapper.createGame(userName, gameRules, mapName, gameName, hidden);

            });
    return contextResult.isFail()
            ? ResultResponse.failedAsJson(CREATE_CONTEXT_METHOD, contextResult.getError())
            : ResultResponse.succeedAsJson(CREATE_CONTEXT_METHOD, contextResult.getResult().getContextId());
  }
  //===================================================================================

  /**
   * Подключиться к игре
   *
   * @return
   */
  @RequestMapping(method = GET, path = {"/" + CONNECT_TO_GAME_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String connectToGame(
          @RequestParam(value = "contextId") String contextId
  ) {
    Result<Player> result = getLoggedinUserName()
            .map(userName -> gameWrapper.connectToGame(userName, contextId));
    return result.isFail()
            ? ResultResponse.failedAsJson(CONNECT_TO_GAME_METHOD, result.getError())
            : ResultResponse.succeedAsJson(CONNECT_TO_GAME_METHOD, new GsonBuilder().create().toJson(ContextDto.fromEntity(result.getResult().getContext(), true)));
  }
  //===================================================================================

  /**
   * Подключиться к игре
   *
   * @return
   */
  @RequestMapping(method = GET, path = {"/" + DECAMP_FROM_THE_ARENA_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String decampFromTheArena(
          @RequestParam(value = "contextId") String contextId
  ) {
    Result<Player> result = getLoggedinUserName()
            .map(userName -> gameWrapper.decampFromTheArena(contextId, userName));
    return result.isFail()
            ? ResultResponse.failedAsJson(DECAMP_FROM_THE_ARENA_METHOD, result.getError())
            : ResultResponse.succeedAsJson(DECAMP_FROM_THE_ARENA_METHOD, result.getResult().getId());
  }
  //===================================================================================

  /**
   * Выдать артефакт игроку
   *
   * @return
   */
  @RequestMapping(method = GET, path = {"/" + GIVE_ARTIFACT_TO_PLAYER_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String giveArtifactToPlayer(
          @RequestParam(value = "contextId") String contextId
          , @RequestParam(value = "artifactName") String artifactName
  ) {
    Result<Artifact<Player>> contextResult = getLoggedinUserName()
            .map(userName -> gameWrapper.giveArtifactToPlayer(contextId, userName, artifactName));
    return contextResult.isFail()
            ? ResultResponse.failedAsJson(GIVE_ARTIFACT_TO_PLAYER_METHOD, contextResult.getError())
            : ResultResponse.succeedAsJson(GIVE_ARTIFACT_TO_PLAYER_METHOD, contextResult.getResult().getId());
  }
  //===================================================================================

  /**
   * Задать готовность игрока к бою
   *
   * @return
   */
  @RequestMapping(method = GET, path = {"/" + SET_READY_TO_PLAY_STATUS_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String playerReadyToPlay(
          @RequestParam(value = "readyToPlay") Boolean readyToPlay) {
    Result<Player> result = getLoggedinUserName()
            .map(userName -> gameWrapper.playerReadyToPlay(userName, readyToPlay));
    return result.isFail()
            ? ResultResponse.failedAsJson(SET_READY_TO_PLAY_STATUS_METHOD, result.getError())
            : ResultResponse.succeedAsJson(SET_READY_TO_PLAY_STATUS_METHOD, result.getResult().isReadyToPlay());
  }
  //===================================================================================

  /**
   * Задать готовность игрока к бою
   *
   * @return
   */
  @RequestMapping(method = GET, path = {"/" + END_TURN_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String endTurn(
          @RequestParam(value = "contextId") String contextId) {
    Result<Player> result = getLoggedinUserName()
            .map(userName -> gameWrapper.nextTurn(contextId, userName));
    return result.isFail()
            ? ResultResponse.failedAsJson(END_TURN_METHOD, result.getError())
            : ResultResponse.succeedAsJson(END_TURN_METHOD, result.getResult().getId());
  }
  //===================================================================================

  /**
   * Запрос текущего, если есть, игрового контекста пользователя
   *
   * @return
   */
  @RequestMapping(method = GET, path = {"/" + GET_EXISTING_GAME_CONTEXT_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String getExistingGameContext() {
    Result<ContextDto> contextResult = getLoggedinUserName()
            .map(userName -> gameWrapper.getExistingGameContext(userName));
    return contextResult.isFail()
            ? ResultResponse.failedAsJson(GET_EXISTING_GAME_CONTEXT_METHOD, contextResult.getError())
            : ResultResponse.succeedAsJson(GET_EXISTING_GAME_CONTEXT_METHOD, new GsonBuilder().create().toJson(contextResult.getResult()));
  }
  //===================================================================================

  /**
   * Запрос текущего, если есть, игрового контекста пользователя
   *
   * @return
   */
  @RequestMapping(method = GET, path = {"/" + GET_GAME_FULL_DATA_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String getGameFullData() {
    Result<ContextDto> contextResult = getLoggedinUserName()
            .map(userName -> gameWrapper.getGameFullData(userName));
    return contextResult.isFail()
            ? ResultResponse.failedAsJson(GET_GAME_FULL_DATA_METHOD, contextResult.getError())
            : ResultResponse.succeedAsJson(GET_GAME_FULL_DATA_METHOD, new GsonBuilder().create().toJson(contextResult.getResult()));
  }
  //===================================================================================

  /**
   * Запрос списка активных игр. Идущих и ожидающих
   *
   * @return
   */
  @RequestMapping(method = GET, path = {"/" + GET_ACTIVE_GAME_LIST_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String getActiveGameList() {
    Result<List<ContextDto>> contextResult = getLoggedinUserName()
            .map(userName -> gameWrapper.getActiveGameList(userName));
    return contextResult.isFail()
            ? ResultResponse.failedAsJson(GET_ACTIVE_GAME_LIST_METHOD, contextResult.getError())
            : ResultResponse.succeedAsJson(GET_ACTIVE_GAME_LIST_METHOD, new GsonBuilder().create().toJson(contextResult.getResult()));
  }
  //===================================================================================

  /**
   * Установка нового значения видимости игры для пользователей
   *
   * @return
   */
  @RequestMapping(method = GET, path = {"/" + SET_GAME_VISIBILITY_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String setGameVisibleMode(
          @RequestParam(value = "contextId") String contextId
          , @RequestParam(value = "hidden") boolean hidden
  ) {
    Result<Context> contextResult = getLoggedinUserName()
            .map(userName -> gameWrapper.setGameVisibleMode(contextId, userName, hidden));
    return contextResult.isFail()
            ? ResultResponse.failedAsJson(SET_GAME_VISIBILITY_METHOD, contextResult.getError())
            : ResultResponse.succeedAsJson(SET_GAME_VISIBILITY_METHOD, contextResult.getResult().getContextId());
  }
  //===================================================================================

  /**
   * Создание технического контекста
   *
   * @return
   */
  @RequestMapping(method = GET, path = {"/" + CREATE_TECHNICAL_CONTEXT_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String createTechnicalContext() {
    Result<Context> contextResult = getLoggedinUserName()
            .map(userName -> gameWrapper.createTechnicalContext(userName));
    return contextResult.isFail()
            ? ResultResponse.failedAsJson(CREATE_TECHNICAL_CONTEXT_METHOD, contextResult.getError())
            : ResultResponse.succeedAsJson(CREATE_TECHNICAL_CONTEXT_METHOD, contextResult.getResult().getContextId());
  }
  //===================================================================================

  /**
   * Возвращает спсиок карт и все их параметры
   *
   * @return
   */
  @RequestMapping(method = GET, path = {"/" + GET_MAPS_LIST_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String getMapsList() {
    Result<List<LevelMapMetaDataXml>> mapsResult = getLoggedinUserName()
            .map(userName -> gameWrapper.getLevelMapsList(userName));
    return mapsResult.isFail()
            ? ResultResponse.failedAsJson(GET_MAPS_LIST_METHOD, mapsResult.getError())
            : ResultResponse.succeedAsJson(GET_MAPS_LIST_METHOD, new GsonBuilder().create().toJson(mapsResult.getResult()));
  }
  //===================================================================================

  /**
   * Получить список Активных пользователей чата
   *
   * @return
   */
  @RequestMapping(method = GET, path = {"/" + GET_ACTIVE_USERS_LIST_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String getActiveUsersList() {
    Result<List<String>> usersListResult = getLoggedinUserName()
            .map(userName -> gameWrapper.getActiveUsersList(userName));
    return usersListResult.isFail()
            ? ResultResponse.failedAsJson(GET_ACTIVE_USERS_LIST_METHOD, usersListResult.getError())
            : ResultResponse.succeedAsJson(GET_ACTIVE_USERS_LIST_METHOD, new GsonBuilder().create().toJson(usersListResult.getResult()));
  }
  //===================================================================================

  /**
   * Получить список сохраненных воинов
   *
   * @return
   */
  @RequestMapping(method = GET, path = {"/" + GET_PLAYER_HEROES_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String getPlayerHeroes(@RequestParam(value = "contextId") String contextId) {
    Result<String> warriorsListResult = getLoggedinUserName()
            .map(userName -> gameWrapper.getPayerHeroesAsJson(contextId, userName));
    return warriorsListResult.isFail()
            ? ResultResponse.failedAsJson(GET_PLAYER_HEROES_METHOD, warriorsListResult.getError())
            : ResultResponse.succeedAsJson(GET_PLAYER_HEROES_METHOD, warriorsListResult.getResult());
  }
  //===================================================================================

  /**
   * Получить героя и варианты его амуниции
   *
   * @return
   */
  @RequestMapping(method = GET, path = {"/" + FIND_PLAYER_HERO_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String findHero(@RequestParam(value = "contextId") String contextId
          , @RequestParam(value = "heroName") String heroName) {
    Result<String> warriorsResult = getLoggedinUserName()
            .map(userName -> gameWrapper.getPayerHeroAsJson(contextId, userName, heroName));
    return warriorsResult.isFail()
            ? ResultResponse.failedAsJson(FIND_PLAYER_HERO_METHOD, warriorsResult.getError())
            : ResultResponse.succeedAsJson(FIND_PLAYER_HERO_METHOD, warriorsResult.getResult());
  }
  //===================================================================================

  /**
   * Получить список базовых классов воинов
   *
   * @return
   */
  @RequestMapping(method = GET, path = {"/" + GET_BASE_WARRIOR_CLASSES_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String getBaseWarriorClasses(@RequestParam(value = "contextId") String contextId) {
    Result<String> warriorClassesResult = getLoggedinUserName()
            .map(userName -> gameWrapper.getWarriorBaseClassesAsJson(contextId));
    return warriorClassesResult.isFail()
            ? ResultResponse.failedAsJson(GET_BASE_WARRIOR_CLASSES_METHOD, warriorClassesResult.getError())
            : ResultResponse.succeedAsJson(GET_BASE_WARRIOR_CLASSES_METHOD, warriorClassesResult.getResult());
  }
  //===================================================================================

  @RequestMapping(method = GET, path = {"/" + GET_WEAPON_CLASSES_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String getWeaponClasses(@RequestParam(value = "contextId") String contextId) {
    Result<String> weaponsResult = getLoggedinUserName()
            .map(userName -> gameWrapper.getWeaponClassesAsJson(contextId));
    return weaponsResult.isFail()
            ? ResultResponse.failedAsJson(GET_WEAPON_CLASSES_METHOD, weaponsResult.getError())
            : ResultResponse.succeedAsJson(GET_WEAPON_CLASSES_METHOD, weaponsResult.getResult());
  }
  //===================================================================================

  @RequestMapping(method = GET, path = {"/" + GET_PLAYER_SPELL_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String getPlayerSpells(@RequestParam(value = "contextId") String contextId) {
    Result<String> playerSpellsResult = getLoggedinUserName()
            .map(userName -> gameWrapper.getPlayerSpellsAsJson(contextId));
    return playerSpellsResult.isFail()
            ? ResultResponse.failedAsJson(GET_PLAYER_SPELL_METHOD, playerSpellsResult.getError())
            : ResultResponse.succeedAsJson(GET_PLAYER_SPELL_METHOD, playerSpellsResult.getResult());
  }
  //===================================================================================

  @RequestMapping(method = GET, path = {"/" + GET_WARRIOR_SPELL_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String getWarriorSpells(@RequestParam(value = "contextId") String contextId) {
    Result<String> warriorSpellsResult = getLoggedinUserName()
            .map(userName -> gameWrapper.getWarriorSpellsAsJson(contextId));
    return warriorSpellsResult.isFail()
            ? ResultResponse.failedAsJson(GET_WARRIOR_SPELL_METHOD, warriorSpellsResult.getError())
            : ResultResponse.succeedAsJson(GET_WARRIOR_SPELL_METHOD, warriorSpellsResult.getResult());
  }
  //===================================================================================

  @RequestMapping(method = GET, path = {"/" + GET_PLAYER_ARTIFACTS_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String getPlayerArtifacts(@RequestParam(value = "contextId") String contextId) {
    Result<String> playerArtifactsResult = getLoggedinUserName()
            .map(userName -> gameWrapper.getPlayerArtifactsAsJson(contextId));
    return playerArtifactsResult.isFail()
            ? ResultResponse.failedAsJson(GET_PLAYER_ARTIFACTS_METHOD, playerArtifactsResult.getError())
            : ResultResponse.succeedAsJson(GET_PLAYER_ARTIFACTS_METHOD, playerArtifactsResult.getResult());
  }
  //===================================================================================

  @RequestMapping(method = GET, path = {"/" + GET_WARRIOR_ARTIFACTS_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String getWarriorArtifacts(@RequestParam(value = "contextId") String contextId) {
    Result<String> warriorArtifactsResult = getLoggedinUserName()
            .map(userName -> gameWrapper.getWarriorArtifactsAsJson(contextId));
    return warriorArtifactsResult.isFail()
            ? ResultResponse.failedAsJson(GET_WARRIOR_ARTIFACTS_METHOD, warriorArtifactsResult.getError())
            : ResultResponse.succeedAsJson(GET_WARRIOR_ARTIFACTS_METHOD, warriorArtifactsResult.getResult());
  }
  //===================================================================================

  /**
   * Создание воина на карте
   *
   * @param contextId
   * @param warriorBaseClassName
   * @param x
   * @param y
   * @return
   */
  @RequestMapping(method = GET, path = {"/" + CREATE_WARRIOR_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String createWarrior(@RequestParam(value = "contextId") String contextId
          , @RequestParam(value = "warriorBaseClassName") String warriorBaseClassName
          , @RequestParam(value = "heroName") String heroName
          , @RequestParam(value = "x") int x
          , @RequestParam(value = "y") int y) {
    Coords coords = new Coords(x, y);
    Result<Warrior> createWarriorResult = getLoggedinUserName()
            .map(userName -> gameWrapper.createWarrior(contextId, userName, warriorBaseClassName, coords))
            .peak(warrior -> warrior.setTitle(heroName));

    return createWarriorResult.isFail()
            ? ResultResponse.failedAsJson(CREATE_WARRIOR_METHOD, createWarriorResult.getError())
            : ResultResponse.succeedAsJson(CREATE_WARRIOR_METHOD, createWarriorResult.getResult().getId());
  }
  //============================================================================================================

  /**
   * Удаление воина с карте
   *
   * @param contextId
   * @param warriorId
   * @return
   */
  @RequestMapping(method = GET, path = {"/" + REMOVE_WARRIOR_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String removeWarrior(@RequestParam(value = "contextId") String contextId
          , @RequestParam(value = "warriorId") String warriorId) {
    Result<Warrior> removeWarriorResult = getLoggedinUserName()
            .map(userName -> gameWrapper.removeWarrior(contextId, userName, warriorId));

    return removeWarriorResult.isFail()
            ? ResultResponse.failedAsJson(REMOVE_WARRIOR_METHOD, removeWarriorResult.getError())
            : ResultResponse.succeedAsJson(REMOVE_WARRIOR_METHOD, removeWarriorResult.getResult().getId());
  }
  //============================================================================================================

  /**
   * Удаление воина с карте
   *
   * @param contextId
   * @param warriorId
   * @return
   */
  @RequestMapping(method = GET, path = {"/" + ROLLBACK_MOVE_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String rollbackMove(@RequestParam(value = "contextId") String contextId
          , @RequestParam(value = "warriorId") String warriorId) {
    Result<Warrior> removeWarriorResult = getLoggedinUserName()
            .map(userName -> gameWrapper.rollbackMove(contextId, userName, warriorId));

    return removeWarriorResult.isFail()
            ? ResultResponse.failedAsJson(ROLLBACK_MOVE_METHOD, removeWarriorResult.getError())
            : ResultResponse.succeedAsJson(ROLLBACK_MOVE_METHOD, removeWarriorResult.getResult().getCoords());
  }
  //============================================================================================================

  /**
   * Выдать воину оружие
   *
   * @param contextId
   * @param warriorId
   * @param weaponName
   * @return
   */
  @RequestMapping(method = GET, path = {"/" + GIVE_WEAPON_TO_WARRIOR_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String giveWeaponToWarrior(@RequestParam(value = "contextId") String contextId
          , @RequestParam(value = "warriorId") String warriorId
          , @RequestParam(value = "weaponName") String weaponName) {
    Result<Weapon> giveWeaponToWarriorResult = getLoggedinUserName()
            .map(userName -> gameWrapper.giveWeaponToWarrior(contextId, userName, warriorId, weaponName));

    return giveWeaponToWarriorResult.isFail()
            ? ResultResponse.failedAsJson(GIVE_WEAPON_TO_WARRIOR_METHOD, giveWeaponToWarriorResult.getError())
            : ResultResponse.succeedAsJson(GIVE_WEAPON_TO_WARRIOR_METHOD, giveWeaponToWarriorResult.getResult().getId());
  }
  //============================================================================================================

  /**
   * Забрать у  воина оружие
   *
   * @param contextId
   * @param warriorId
   * @param weaponId
   * @return
   */
  @RequestMapping(method = GET, path = {"/" + DROP_WEAPON_BY_WARRIOR_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String dropWeaponByWarrior(@RequestParam(value = "contextId") String contextId
          , @RequestParam(value = "warriorId") String warriorId
          , @RequestParam(value = "weaponId") String weaponId) {
    Result<Weapon> dropWeaponByWarriorResult = getLoggedinUserName()
            .map(userName -> gameWrapper.dropWeaponByWarrior(contextId, userName, warriorId, weaponId));

    return dropWeaponByWarriorResult.isFail()
            ? ResultResponse.failedAsJson(DROP_WEAPON_BY_WARRIOR_METHOD, dropWeaponByWarriorResult.getError())
            : ResultResponse.succeedAsJson(DROP_WEAPON_BY_WARRIOR_METHOD, dropWeaponByWarriorResult.getResult().getId());
  }
  //============================================================================================================

  /**
   * Переместить воина
   *
   * @param contextId
   * @param warriorId
   * @param x
   * @param y
   * @return
   */
  @RequestMapping(method = GET, path = {"/" + MOVE_WARRIOR_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String moveWarriorTo(@RequestParam(value = "contextId") String contextId
          , @RequestParam(value = "warriorId") String warriorId
          , @RequestParam(value = "x") int x
          , @RequestParam(value = "y") int y) {
    Result<Warrior> result = getLoggedinUserName()
            .map(userName -> gameWrapper.moveWarriorTo(contextId, userName, warriorId, new Coords(x, y)));

    return result.isFail()
            ? ResultResponse.failedAsJson(MOVE_WARRIOR_METHOD, result.getError())
            : ResultResponse.succeedAsJson(MOVE_WARRIOR_METHOD, new Coords(result.getResult().getCoords()));
  }
  //============================================================================================================


  //============================================================================================================

  /**
   * запросить подробную информацию о воинах одного игрока или всех сразу
   *
   * @param contextId
   * @param ownedByPlayerName
   * @return
   */
  @RequestMapping(method = GET, path = {"/" + GET_PLAYER_WARRIORS_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String getPlayerWarriors(@RequestParam(value = "contextId") String contextId
          , @RequestParam(value = "ownedByPlayerName") String ownedByPlayerName) {
    Result<String> getPlayerWarriorsResult = getLoggedinUserName()
            .map(userName -> gameWrapper.getPlayerWarriorsAsJson(contextId, userName, ownedByPlayerName));

    return getPlayerWarriorsResult.isFail()
            ? ResultResponse.failedAsJson(GET_PLAYER_WARRIORS_METHOD, getPlayerWarriorsResult.getError())
            : ResultResponse.succeedAsJson(GET_PLAYER_WARRIORS_METHOD, getPlayerWarriorsResult.getResult());
  }
  //============================================================================================================

  /**
   * Сохранить конфигурацию воина
   *
   * @param contextId
   * @param warriorId
   * @param ammunitionName
   * @return
   */
  @RequestMapping(method = GET, path = {"/" + SAVE_WARRIOR_AMMUNITION_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String saveWarriorAmmunition(@RequestParam(value = "contextId") String contextId
          , @RequestParam(value = "warriorId") String warriorId
          , @RequestParam(value = "ammunitionName") String ammunitionName) {
    Result<Warrior> createWarriorResult = getLoggedinUserName()
            .map(userName -> gameWrapper.saveWarriorAmmunition(contextId, userName, warriorId, ammunitionName));

    return createWarriorResult.isFail()
            ? ResultResponse.failedAsJson(SAVE_WARRIOR_AMMUNITION_METHOD, createWarriorResult.getError())
            : ResultResponse.succeedAsJson(SAVE_WARRIOR_AMMUNITION_METHOD, createWarriorResult.getResult().getId());
  }
  //============================================================================================================

  /**
   * Сохранить конфигурацию воина
   *
   * @param contextId
   * @param warriorId
   * @param oldAmmunitionName
   * @param newAmmunitionName
   * @return
   */
  @RequestMapping(method = GET, path = {"/" + RENAME_WARRIOR_AMMUNITION_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String renameWarriorAmmunition(@RequestParam(value = "contextId") String contextId
          , @RequestParam(value = "warriorId") String warriorId
          , @RequestParam(value = "oldAmmunitionName") String oldAmmunitionName
          , @RequestParam(value = "newAmmunitionName") String newAmmunitionName) {
    Result<Warrior> renameAmmunitionResult = getLoggedinUserName()
            .map(userName -> gameWrapper.renameWarriorEquipment(contextId, userName, warriorId, oldAmmunitionName, newAmmunitionName));

    return renameAmmunitionResult.isFail()
            ? ResultResponse.failedAsJson(RENAME_WARRIOR_AMMUNITION_METHOD, renameAmmunitionResult.getError())
            : ResultResponse.succeedAsJson(RENAME_WARRIOR_AMMUNITION_METHOD, renameAmmunitionResult.getResult().getId());
  }
  //============================================================================================================

  /**
   * Удалить экипировку воина. Если аммуниций у воина не остается, то он тоже удаляется
   *
   * @param contextId
   * @param warriorId
   * @param ammunitionName
   * @return
   */
  @RequestMapping(method = GET, path = {"/" + REMOVE_WARRIOR_AMMUNITION_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String removeWarriorAmmunition(@RequestParam(value = "contextId") String contextId
          , @RequestParam(value = "warriorId") String warriorId
          , @RequestParam(value = "ammunitionName") String ammunitionName) {
    Result<Warrior> renameAmmunitionResult = getLoggedinUserName()
            .map(userName -> gameWrapper.removeWarriorEquipment(contextId, userName, warriorId, ammunitionName));

    return renameAmmunitionResult.isFail()
            ? ResultResponse.failedAsJson(REMOVE_WARRIOR_AMMUNITION_METHOD, renameAmmunitionResult.getError())
            : ResultResponse.succeedAsJson(REMOVE_WARRIOR_AMMUNITION_METHOD, renameAmmunitionResult.getResult().getId());
  }
  //============================================================================================================

}
