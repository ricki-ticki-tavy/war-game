package web.socket.controller;

import api.core.Context;
import api.core.Core;
import api.game.map.Player;
import core.game.CoreImpl;
import core.system.event.EventImpl;
import core.system.log.messages.ContextEventMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import web.socket.message.ChatMessage;

import java.security.Principal;
import java.util.Arrays;

import static api.enums.EventType.PLAY_PLAYER_ENTERED_TO_MAP;
import static core.system.log.EventLogger.PLAYER_PARAM;

@Controller
@Profile("!test")
public class WebSocketsController {

  public static final String WHISPER_SUFFIX = "-whisper-";

  public static final String TAVERN_PUBLIC_MESSAGES_CHANNEL = "tavernChat";
  public static final String TAVERN_PRIVATE_MESSAGES_CHANNEL = TAVERN_PUBLIC_MESSAGES_CHANNEL + WHISPER_SUFFIX;

  public static final String CONTEXT_PUBLIC_MESSAGES_CHANNEL = "contextInfo";
  public static final String CONTEXT_PRIVATE_MESSAGES_CHANNEL = CONTEXT_PUBLIC_MESSAGES_CHANNEL + WHISPER_SUFFIX;

  public static final String ARENA_PUBLIC_MESSAGES_CHANNEL = "arenaChat";
  public static final String ARENA_PRIVATE_MESSAGES_CHANNEL = ARENA_PUBLIC_MESSAGES_CHANNEL + WHISPER_SUFFIX;

  public static final String TOPIC_PREFIX = "/topic/";

  @Autowired
  private SimpMessagingTemplate simpMessagingTemplate;

  @Autowired
  Core core;

  public void initTimeSend() {
    new Thread(() -> {
      while (!Thread.interrupted()) {
        try {
          Thread.sleep(500);
        } catch (Throwable th) {
        }

        simpMessagingTemplate.convertAndSend("/topic/tavernChat", new ChatMessage("system", "chat"));
      }
    }).start();
  }


  @MessageMapping("/tavern-hello")
  @SendTo(TOPIC_PREFIX + TAVERN_PUBLIC_MESSAGES_CHANNEL)
  public ChatMessage greeting(@Payload ChatMessage message
          , Principal user) throws Exception {
    Thread.sleep(300); // simulated delay
    ((CoreImpl) core).addActiveUser(user.getName(), TOPIC_PREFIX + TAVERN_PUBLIC_MESSAGES_CHANNEL);
    return new ChatMessage(user.getName(), "<span style=\"color: green\">В таверну заходит " + user.getName() + "</span> !")
            .setConnected(true);
  }

  @MessageMapping("/arena-hello")
  @SendTo(TOPIC_PREFIX + TAVERN_PUBLIC_MESSAGES_CHANNEL)
  public ChatMessage arenaGreeting(@Payload ChatMessage message
          , Principal user) throws Exception {

    Context context = core.findUserByName(user.getName()).getResult().getContext();

    String contextId = context.getContextId();

    ((CoreImpl) core).addActiveUser(user.getName(), TOPIC_PREFIX + ARENA_PUBLIC_MESSAGES_CHANNEL + contextId);

    simpMessagingTemplate.convertAndSend(TOPIC_PREFIX + TAVERN_PUBLIC_MESSAGES_CHANNEL
            , new ChatMessage(user.getName(), "<span style=\"color: green\">" + user.getName() + " Находится на арене. Можно отправлять ему личные сообщения</span>!")
                    .setConnected(true));

    simpMessagingTemplate.convertAndSend(TOPIC_PREFIX + ARENA_PUBLIC_MESSAGES_CHANNEL + contextId
            , new ChatMessage(user.getName(), "<span style=\"color: green\">На арене появляется " + user.getName() + "</span> !")
                    .setConnected(true));

    simpMessagingTemplate.convertAndSend(TOPIC_PREFIX + CONTEXT_PRIVATE_MESSAGES_CHANNEL + contextId
            , new ContextEventMessage(new EventImpl(context, null, PLAY_PLAYER_ENTERED_TO_MAP, null, null), null)
                    .addParameter(PLAYER_PARAM, user.getName()));
    return null;
  }

  @MessageMapping("/arena-say")
  @SendTo(TOPIC_PREFIX + ARENA_PUBLIC_MESSAGES_CHANNEL)
  public ChatMessage sayToAll(@Payload ChatMessage message
          , Principal user) throws Exception {
    // поищем список пользователей
    String messageText = message.getMessage().trim();
    if (messageText.startsWith("[") && messageText.contains("]:")) {
      // это сообщение адресовано определенным пользователям
      messageText = messageText.substring(1).trim();
      String usersList = messageText.substring(0, messageText.indexOf("]:"));
      final String textToSend = messageText.substring(messageText.indexOf("]:") + 2).trim().replace("<", "[").replace(">", "]");

      if (!StringUtils.isEmpty(textToSend)) {
        // отправить самому себе
        simpMessagingTemplate.convertAndSend(TOPIC_PREFIX + ARENA_PRIVATE_MESSAGES_CHANNEL
                        + core.findUserByName(user.getName()).getResult().getContext().getContextId()
                        + "-" + user.getName().toLowerCase()
                , new ChatMessage(user.getName(), textToSend).setWisp(true));
        // оправить остальным по списку
        String[] users = usersList.split(",");
        Arrays.stream(users)
                .filter(s -> !s.trim().toLowerCase().equals(user.getName().toLowerCase()))
                .forEach(userName -> {
                  simpMessagingTemplate.convertAndSend(((CoreImpl) core).getActiveUserChannel(userName) + WHISPER_SUFFIX
                                  + userName.trim().toLowerCase()
                          , new ChatMessage(user.getName(), textToSend).setWisp(true));
                });
      }

    } else {
      simpMessagingTemplate.convertAndSend(TOPIC_PREFIX + ARENA_PUBLIC_MESSAGES_CHANNEL
                      + core.findUserByName(user.getName()).getResult().getContext().getContextId()
              , new ChatMessage(user.getName(), message.getMessage().replace("<", "[").replace(">", "]")));

    }
    return null;
  }


  @SendTo(TOPIC_PREFIX + TAVERN_PUBLIC_MESSAGES_CHANNEL)
  @MessageMapping("/tavern-sayToAll")
  public ChatMessage arenaSay(@Payload ChatMessage message
          , Principal user) throws Exception {
    // поищем список пользователей
    String messageText = message.getMessage().trim();
    if (messageText.startsWith("[") && messageText.contains("]:")) {
      // это сообщение адресовано определенным пользователям
      messageText = messageText.substring(1).trim();
      String usersList = messageText.substring(0, messageText.indexOf("]:"));
      final String textToSend = messageText.substring(messageText.indexOf("]:") + 2).trim().replace("<", "[").replace(">", "]");

      if (!StringUtils.isEmpty(textToSend)) {
        // отправить самому себе
        simpMessagingTemplate.convertAndSend(TOPIC_PREFIX + TAVERN_PRIVATE_MESSAGES_CHANNEL + user.getName().toLowerCase()
                , new ChatMessage(user.getName(), textToSend).setWisp(true));
        // оправить остальным по списку
        String[] users = usersList.split(",");
        Arrays.stream(users)
                .filter(s -> !s.trim().toLowerCase().equals(user.getName().toLowerCase()))
                .forEach(userName -> {
                  simpMessagingTemplate.convertAndSend(((CoreImpl) core).getActiveUserChannel(userName) + WHISPER_SUFFIX + userName.trim().toLowerCase()
                          , new ChatMessage(user.getName(), textToSend).setWisp(true));
                });
      }

      return null;
    } else {
      return new ChatMessage(user.getName(), message.getMessage().replace("<", "[").replace(">", "]"));
    }
  }

}