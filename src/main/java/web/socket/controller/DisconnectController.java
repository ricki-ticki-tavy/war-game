package web.socket.controller;

import api.core.Core;
import core.game.CoreImpl;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import web.socket.message.ChatMessage;

import static web.socket.controller.WebSocketsController.ARENA_PUBLIC_MESSAGES_CHANNEL;
import static web.socket.controller.WebSocketsController.TAVERN_PUBLIC_MESSAGES_CHANNEL;
import static web.socket.controller.WebSocketsController.TOPIC_PREFIX;

/**
 * Контроллер отключения и подключения вебсокетов
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class DisconnectController {

  @Autowired
  BeanFactory beanFactory;

  @Autowired
  Core core;

  private SimpMessagingTemplate simpMessagingTemplate = null;

  private SimpMessagingTemplate getSimpMessagingTemplate() {
    if (simpMessagingTemplate == null) {
      synchronized (this) {
        if (simpMessagingTemplate == null) {
          simpMessagingTemplate = beanFactory.getBean(SimpMessagingTemplate.class);
        }
      }
    }

    return simpMessagingTemplate;
  }

  public void processDisconnectWebSocket(SessionDisconnectEvent event) {
    StompHeaderAccessor sha = StompHeaderAccessor.wrap(event.getMessage());
    UsernamePasswordAuthenticationToken token = (UsernamePasswordAuthenticationToken) sha.getHeader("simpUser");
    if (token != null) {

      String userChannel = ((CoreImpl) core).getActiveUserChannel(token.getName());

      // Есть имя оотключившегося пользователя
      ((CoreImpl) core).removeActiveUser(token.getName());

      if (userChannel.startsWith(TOPIC_PREFIX + ARENA_PUBLIC_MESSAGES_CHANNEL)) {
        // покидает арену

        getSimpMessagingTemplate().convertAndSend(TOPIC_PREFIX + TAVERN_PUBLIC_MESSAGES_CHANNEL
                , new ChatMessage(token.getName(), "<span style=\"color: orange\">"
                        + token.getName() + " покидает арену</span> !")
                        .setDisconnected(true));

        getSimpMessagingTemplate().convertAndSend(userChannel
                , new ChatMessage(token.getName(), "<span style=\"color: orange\">"
                        + token.getName() + " покидает арену</span> !")
                        .setDisconnected(true));
      } else {
        // Покидает таверну
        getSimpMessagingTemplate().convertAndSend(TOPIC_PREFIX + TAVERN_PUBLIC_MESSAGES_CHANNEL
                , new ChatMessage(token.getName(), "<span style=\"color: orange\">"
                        + token.getName() + " покидает таверну</span> !")
                        .setDisconnected(true));
      }
    }

  }
}
