package web.socket.message;

import java.util.Date;

public class ChatMessage {
  private String name;
  private Long dateTime = new Date().getTime();
  private String message;
  private boolean wisp = false;
  private Boolean connected = false;
  private Boolean disconnected = false;

  public ChatMessage() {
  }

  public ChatMessage(String name, String message) {
    this.name = name;
    this.message = message;
  }

  public String getName() {
    return name;
  }

  public ChatMessage setName(String name) {
    this.name = name;
    return this;
  }

  public Long getDateTime() {
    return dateTime;
  }

  public ChatMessage setDateTime(Long dateTime) {
    this.dateTime = dateTime;
    return this;
  }

  public String getMessage() {
    return message;
  }

  public ChatMessage setMessage(String message) {
    this.message = message;
    return this;
  }

  public boolean isWisp() {
    return wisp;
  }

  public ChatMessage setWisp(boolean wisp) {
    this.wisp = wisp;
    return this;
  }

  public Boolean getConnected() {
    return connected;
  }

  public ChatMessage setConnected(Boolean connected) {
    this.connected = connected;
    return this;
  }

  public Boolean getDisconnected() {
    return disconnected;
  }

  public ChatMessage setDisconnected(Boolean disconnected) {
    this.disconnected = disconnected;
    return this;
  }
}
