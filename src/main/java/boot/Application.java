package boot;

import boot.config.ApplicationConfigurator;
import boot.config.DatabaseConfiguration;
import boot.config.MvcConfiguration;
import boot.config.WebSocketConfig;
import boot.config.security.HttpRedirectConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.gson.GsonAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@ComponentScan(basePackages = {"core", "api", "web", "boot.config.security"})
//@Import({ApplicationConfigurator.class})
@EnableAutoConfiguration(exclude = {GsonAutoConfiguration.class
        , HibernateJpaAutoConfiguration.class})
@Import({ApplicationConfigurator.class, DatabaseConfiguration.class, MvcConfiguration.class, HttpRedirectConfig.class, WebSocketConfig.class})
public class Application {

  public static void main(String[] args) {
    ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);
//    context.getBean(DatabaseInitializer.class).initDatabase();
  }

}
