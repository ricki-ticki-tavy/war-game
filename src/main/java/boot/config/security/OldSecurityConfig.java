package boot.config.security;

import core.security.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.sql.DataSource;
import java.util.Map;

/**
 * Веб безопасность спринговая
 */
//@EnableWebSecurity
//@Controller
public class OldSecurityConfig extends WebSecurityConfigurerAdapter {

  // FOR Ajax  https://docs.spring.io/spring-security/site/docs/3.2.x/reference/htmlsingle/html5/#csrf-include-csrf-token-ajax

//  @Resource(name = "baseAuthenticationFilter")
//  UsernamePasswordAuthenticationFilter authenticationFilter;

  @Autowired
  DataSource dataSource;

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
            .authorizeRequests()
            .antMatchers("/css/**", "/forum/**", "/static/js/**", "/index.*").permitAll()
            .antMatchers("/game/**").hasRole("USER")
            .and()
            .formLogin()
            .loginPage("/login").loginProcessingUrl("/signin")
            .usernameParameter("username").passwordParameter("password")
            .failureUrl("/login-error?error=true")
            .and().logout().permitAll();
  }

  @Bean
  @Override
  public UserDetailsService userDetailsService(){
    return new UserDetailsServiceImpl();
  }

//  @Autowired
//  public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//    auth
//            .jdbcAuthentication().dataSource(dataSource).authoritiesByUsernameQuery("select * from ub");
//            .authenticationProvider(new CustomAuthenticationProvider());
//            .userDetailsService(new UserDetailsServiceImpl());
//            .inMemoryAuthentication()
//            .withUser("user").password("password").roles("USER");
//  }

  @RequestMapping(value = {"/login"}, method = RequestMethod.GET)
  public String processLoginForm(Map<String, Object> model) {
    return "security/login";
  }

//  @RequestMapping(value = "/login", method = RequestMethod.POST)
//  public String processLoginForm(HttpSession session, @ModelAttribute("user") User user,
//                                 BindingResult result, Model model, final RedirectAttributes redirectAttributes)
//  {
//    //what goes here?
//    return "secure/main";
//  }
//
//  @Override
//  public void configure(WebSecurity web) throws Exception {
//    web.ignoring().antMatchers("/css/*.css");
//    web.ignoring().antMatchers("/static/js/*.js");
//  }


}
