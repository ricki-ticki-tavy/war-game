package core.game.action;

import api.core.Event;
import api.entity.ability.Influencer;
import api.entity.magic.Spell;
import api.entity.warrior.Warrior;
import api.entity.weapon.Weapon;
import api.game.action.InfluenceResult;
import api.game.map.Player;

import java.util.ArrayList;
import java.util.List;

public class InfluenceResultImpl implements InfluenceResult {

  private Player actorPlayer;
  private Player targetPlayer;
  private Warrior actor;
  private Warrior target;
  private Weapon weapon;
  private Spell spell;
  private Event event;
  private int consumedActionPoints;
  private final List<Influencer> influencers = new ArrayList<>(20);
  private InfluenceResult contrInfluence;
  private InfluenceResult parentInfluence;

  public InfluenceResultImpl(Player actorPlayer, Warrior actor, Weapon weapon, Spell spell
          , Player targetPlayer, Warrior target, int consumedActionPoints){
    this.actorPlayer = actorPlayer;
    this.actor = actor;
    this.weapon = weapon;
    this.targetPlayer = targetPlayer;
    this.target = target;
    this.consumedActionPoints = consumedActionPoints;
    this.spell = spell;
  }
  //===================================================================================================

  public InfluenceResultImpl(Player actorPlayer, Warrior actor, Weapon weapon
          , Player targetPlayer, Warrior target, int consumedActionPoints, InfluenceResult parentInfluence){
    this(actorPlayer, actor, weapon, null, targetPlayer, target, consumedActionPoints);
    this.parentInfluence = parentInfluence;
    if (this.parentInfluence != null) {
      this.parentInfluence.setContrInfluence(this);
    }
  }
  //===================================================================================================

  public InfluenceResultImpl(Player actorPlayer, Warrior actor, Spell spell
          , Player targetPlayer, Warrior target, int consumedActionPoints){
    this(actorPlayer, actor, null, spell, targetPlayer, target, consumedActionPoints);
  }
  //===================================================================================================

  @Override
  public Warrior getActor() {
    return actor;
  }
  //===================================================================================================

  @Override
  public Weapon getAttackerWeapon() {
    return weapon;
  }
  //===================================================================================================

  @Override
  public Player getActorPlayer() {
    return actorPlayer;
  }
  //===================================================================================================

  @Override
  public Player getTargetPlayer() {
    return targetPlayer;
  }
  //===================================================================================================

  @Override
  public Warrior getTarget() {
    return target;
  }
  //===================================================================================================

  @Override
  public Event getEvent() {
    return event;
  }
  //===================================================================================================

  @Override
  public List<Influencer> getInfluencers() {
    return influencers;
  }
  //===================================================================================================

  @Override
  public InfluenceResult addInfluencer(Influencer influencer) {
    influencers.add(influencer);
    return this;
  }
  //===================================================================================================

  @Override
  public int getConsumedActionPoints() {
    return consumedActionPoints;
  }
  //===================================================================================================

  /**
   * создать структуру для создания воздействия на самого себя. То есть положительного(как правило) воздействия
   * @param target
   * @return
   */
  public static InfluenceResult forPositive(Warrior target){
    return new InfluenceResultImpl(target.getOwner(), target, null, target.getOwner(), target, 0);
  }
  //===================================================================================================

  @Override
  public InfluenceResult getContrInfluence() {
    return contrInfluence;
  }
  //===================================================================================================

  @Override
  public InfluenceResult setContrInfluence(InfluenceResult contrInfluence) {
    this.contrInfluence = contrInfluence;
    this.contrInfluence.setParentInfluence(this);
    return this;
  }
  //===================================================================================================

  @Override
  public InfluenceResult getParentInfluence() {
    return parentInfluence;
  }
  //===================================================================================================

  @Override
  public InfluenceResult setParentInfluence(InfluenceResult parentInfluence) {
    this.parentInfluence = parentInfluence;
    return this;
  }
  //===================================================================================================

  @Override
  public Spell getAttackerSpell() {
    return spell;
  }
  //===================================================================================================
}
