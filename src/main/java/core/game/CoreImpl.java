package core.game;

import api.core.*;
import api.entity.artifct.ArtifactClassInfo;
import api.entity.magic.SpellInfo;
import api.entity.artifct.Artifact;
import api.entity.warrior.Warrior;
import api.entity.warrior.WarriorBaseClass;
import api.entity.warrior.WarriorBaseClassInfo;
import api.entity.weapon.Weapon;
import api.entity.weapon.WeaponClassInfo;
import api.enums.EventType;
import api.game.map.Player;
import api.game.map.metadata.GameRules;
import api.game.map.metadata.xml.LevelMapMetaDataXml;
import api.dto.warrior.WarriorShortDto;
import core.entity.map.GameRulesImpl;
import core.entity.warrior.base.AbstractWarriorBaseClass;
import core.system.error.GameErrors;
import core.system.event.EventImpl;
import core.system.game.GameMetrics;
import core.system.log.EventLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static api.enums.EventType.GAME_CONTEXT_REMOVED;
import static api.enums.EventType.PLAYER_LOGGED_IN;
import static core.game.ContextImpl.NULL_GAME_CONTEXT;
import static core.system.ResultImpl.fail;
import static core.system.ResultImpl.success;
import static core.system.error.GameErrors.*;

/**
 * Игровой движок. Все операции выполняютсяв рамках динамического игрового контекста
 */
@Component
public class CoreImpl implements Core {

  private static int ZERO = 0;
  private static final String TECHNICAL_MAP_NAME = "technicalLevel.xml";

  private static final Logger logger = LoggerFactory.getLogger(CoreImpl.class);

  private final Map<String, WarriorBaseClassInfo> registeredWarriorBaseClasses = new ConcurrentHashMap<>(100);
  private final Map<String, WeaponClassInfo> registeredWeaponClasses = new ConcurrentHashMap<>(100);
  private final Map<String, Class<? extends Artifact>> registeredArtifactForWarriorClasses = new ConcurrentHashMap<>(100);
  private final Map<String, ArtifactClassInfo> registeredArtifactForPlayerClasses = new ConcurrentHashMap<>(100);
  private final Map<String, SpellInfo> registeredSpellForPlayerClasses = new ConcurrentHashMap<>(100);
  private final Map<String, SpellInfo> registeredSpellForWarriorClasses = new ConcurrentHashMap<>(100);

  /**
   * Активные пользователи. Те, что в чате
   */
  private final Map<String, String> activeUsersMap = new ConcurrentHashMap<>(100);

  private Map<String, Context> contextMap = new ConcurrentHashMap<>(10);
  private Map<String, Player> players = new ConcurrentHashMap<>(1000);
  private Random random = new Random(new Date().getTime());

  private final Map<String, LevelMapMetaDataXml> maps = new ConcurrentHashMap<>(10);

  /**
   * групировка по контекстам, далее внутри групировка по типам событий. Один и тот же потребитель может
   * быть в разных группах одновременно. Последний мэп - UID потребителя для возможности отписки даже лямды
   */
  private Map<Context, Map<EventType, Map<String, Consumer<Event>>>> eventConsumers =
          new ConcurrentHashMap<>(10);

  @Autowired
  BeanFactory beanFactory;

  @Autowired
  EventLogger gameEventLogger;

  @Autowired
  private GameMetrics gameMetrics;


  //===================================================================================================
  //===================================================================================================

  @Override
  public void registerWarriorBaseClass(Class<? extends WarriorBaseClass> warriorBaseClass) {
    AbstractWarriorBaseClass.registerWarriorBaseClass(this, warriorBaseClass);
  }
  //===================================================================================================

  @Override
  public void registerWeaponClass(WeaponClassInfo weaponClassInfo) {
    registeredWeaponClasses.put(weaponClassInfo.getName(), weaponClassInfo);
  }
  //===================================================================================================

  @Override
  public void registerArtifactForWarriorClass(String className, Class<? extends Artifact> artifactClass) {
    registeredArtifactForWarriorClasses.put(className, artifactClass);
  }
  //===================================================================================================

  @Override
  public void registerSpellForPlayerClass(String className, SpellInfo spellInfo) {
    registeredSpellForPlayerClasses.put(className, spellInfo);
  }
  //===================================================================================================

  @Override
  public void registerSpellForWarriorClass(String className, SpellInfo spellInfo) {
    registeredSpellForWarriorClasses.put(className, spellInfo);
  }
  //===================================================================================================

  @Override
  public void registerArtifactForPlayerClass(ArtifactClassInfo classInfo) {
    registeredArtifactForPlayerClasses.put(classInfo.getName(), classInfo);
  }
  //===================================================================================================

  /**
   * Предзагрузка всех игровых карт
   */
  private void loadMaps() {
    Arrays.stream(new File(this.getClass().getClassLoader().getResource("maps").getFile()).list())
            .forEach(fileName -> {
              try (InputStream is = this.getClass().getClassLoader().getResourceAsStream("maps/" + fileName)) {
                JAXBContext jaxbContext = JAXBContext.newInstance(LevelMapMetaDataXml.class);
                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                LevelMapMetaDataXml mapMetadata = (LevelMapMetaDataXml) jaxbUnmarshaller.unmarshal(is);
                mapMetadata.fileName = fileName;
                maps.put(fileName, mapMetadata);
              } catch (IOException ie) {

              } catch (JAXBException je) {

              }
            });
  }
  //===================================================================================================

  @Override
  public Result<List<LevelMapMetaDataXml>> getLevelMapsList() {
    return success(maps.values().stream()
            .filter(levelMapMetaDataXml -> !levelMapMetaDataXml.technical)
            .collect(Collectors.toList()));
  }
  //===================================================================================================

  @Override
  public Result<Context> createGameContext(Player gameCreator, GameRules gameRules
          , String mapFileName, String gameName, boolean hidden) {
    // Если у пользователя была привязка к другим контекстам - надо разорвать ее
    return ((Result<LevelMapMetaDataXml>) (maps.get(mapFileName) == null
            ? fail(GameErrors.MAP_IS_NOT_FOUND.getError(mapFileName))
            : success(maps.get(mapFileName))))
            .map(levelMapMetaDataXml ->
                    gameCreator.replaceContext(null).map(nullCtx ->
                            // Создадим контекст новой игры
                            beanFactory.getBean(Context.class, gameCreator, false)
                                    // Загрузим карту
                                    .loadMap(gameRules, levelMapMetaDataXml, gameName, hidden).map(createdCtx ->
                                    // сделаем вход пользователем в эту карту
                                    createdCtx.connectPlayer(gameCreator).map(connectedPlayer -> {
                                      contextMap.put(createdCtx.getContextId(), createdCtx);
                                      // подменим в ответе добавленного пользователя на контекст
                                      return success(createdCtx);
                                    })
                            )
                    )
            );

  }
  //===================================================================================================

  @Override
  public Result<Context> createTechnicalContext(Player ownerUser) {
    GameRules techGameRules = new GameRulesImpl(30, ZERO, 1, ZERO, ZERO, ZERO, ZERO, gameMetrics.getUnitSizeInPix());
    return ownerUser.replaceContext(null).map(nullCtx ->
            // Создадим контекст новой игры
            beanFactory.getBean(Context.class, ownerUser, true)
                    // Загрузим карту
                    .loadMap(techGameRules
                            , maps.get(TECHNICAL_MAP_NAME)
                            , "." + ownerUser.getId()
                            , true)
                    .map(createdCtx ->
                            // сделаем вход пользователем в эту карту
                            createdCtx.connectPlayer(ownerUser)
                                    .map(connectedPlayer -> {
                                      contextMap.put(createdCtx.getContextId(), createdCtx);
                                      // подменим в ответе добавленного пользователя на контекст
                                      return success(createdCtx);
                                    })));
  }
  //===================================================================================================

  @Override
  public Result<Context> findGameContextByUID(String contextId) {
    return Optional.ofNullable(contextMap.get(contextId))
            .map(foundContext -> success(foundContext))
            .orElse(fail(CONTEXT_NOT_FOUND_BY_ID.getError(contextId)));
  }
  //===================================================================================================

  @Override
  public Result<Context> removeGameContext(String contextId) {

    return findGameContextByUID(contextId)
            // переводим контекст в режим удаления
            .map(contextToDelete -> contextToDelete.initDelete())
            .map(foundContext -> {
              // отключим всех пользователей
              foundContext.getLevelMap().getPlayers()
                      .stream()
                      .forEach(player ->
                              foundContext.disconnectPlayer(player));
              // вычеркнуть контекст из списка
              contextMap.remove(foundContext.getContextId());
              // отправимсообщение об удалении контекста
              Result result = success(foundContext);
              foundContext.fireGameEvent(null, GAME_CONTEXT_REMOVED, new EventDataContainer(foundContext, result), null);
              // удалить все подписи данного контекста  на события
              eventConsumers.remove(foundContext);
              return result;
            });
  }
  //===================================================================================================

  @PostConstruct
  public void init() {
    // подпишем логер на все события. Пусть пишет по ним инфу
    subscribeEvent(null, this::eventLogger, EventType.values());

    // подгрузим инфу по картам
    loadMaps();
  }
  //===================================================================================================

  @Override
  public int getRandom(int min, int max) {
    synchronized (random) {
      return max == min
              ? max
              : random.nextInt(max - min + 1) + min;
    }
  }
  //===================================================================================================

  /**
   * Отправить сообщения в отпределенный контекст
   *
   * @param event
   */

  private void fireEventInContext(Context fireInContext, Event event) {
    Optional.ofNullable(fireInContext)
            .ifPresent(firedContext -> Optional.ofNullable(eventConsumers.get(firedContext))
                    .ifPresent(consumerMap -> Optional.ofNullable(consumerMap.get(event.getEventType()))
                            .ifPresent(uidToConsumerMap -> uidToConsumerMap.values().stream()
                                    .forEach(gameEventConsumer -> gameEventConsumer.accept(event)))));
  }
  //===================================================================================================

  @Override
  public Event fireEvent(Event event) {
    fireEventInContext(event.getSourceContext(), event);
    fireEventInContext(NULL_GAME_CONTEXT, event);
    return event;
  }
  //===================================================================================================

  @Override
  public String subscribeEvent(Context context, Consumer<Event> consumer, EventType... eventTypes) {
    if (context == null) {
      context = NULL_GAME_CONTEXT;
    }

    String consumerUID = UUID.randomUUID().toString();

    Map<EventType, Map<String, Consumer<Event>>> contextConsumers = eventConsumers
            .computeIfAbsent(context, keyContext -> new ConcurrentHashMap<>(EventType.values().length));

    Stream.of(eventTypes).forEach(eventType -> {
      contextConsumers
              .computeIfAbsent(eventType, keyEventType -> new ConcurrentHashMap<>(20))
              .put(consumerUID, consumer);
    });
    return consumerUID;
  }
  //===================================================================================================

  @Override
  public Result<Context> unsubscribeEvent(Context context, String consumerId, EventType... eventTypes) {
    Map<EventType, Map<String, Consumer<Event>>> contextConsumers = eventConsumers.get(context);
    if (contextConsumers != null) {
      if (eventTypes.length != 0) {
        Arrays.stream(eventTypes).forEach(eventType -> {
          Map<String, Consumer<Event>> consumersOfEvent = contextConsumers.get(eventType);
          if (consumersOfEvent != null) {
            consumersOfEvent.remove(consumerId);
          }
        });
      } else {
        // не указан тип события. Отписываемся везде
        contextConsumers.values().stream()
                .forEach(stringConsumerMap -> stringConsumerMap.remove(consumerId));
      }
    }
    return success(context);
  }
  //===================================================================================================

  private void eventLogger(Event event) {
    gameEventLogger.logGameEvent(event);
  }
  //===================================================================================================

  @Override
  public Result loginPlayer(String playerName, Long playerId) {
    Player player = players.computeIfAbsent(playerName, playerNameKey -> beanFactory.getBean(Player.class, playerNameKey, playerId));
    Result result = success(player);
    fireEvent(new EventImpl(null, null, PLAYER_LOGGED_IN, new EventDataContainer(player, result), null));
    return result;
  }
  //===================================================================================================

  @Override
  public Result logoutPlayer(String playerName) {
    Player player = players.get(playerName);
    if (player == null) {
      return fail(GameErrors.USER_NOT_LOGGED_IN.getError(playerName));
    } else {
      // плеер есть. надо закрыть все контексты, если таковые есть, и сделать выход
      if (player.getContext() != null) {
        // Есть контекст.надо его закрыть
        player.replaceContext(null);
      }
      players.remove(player);
    }
    return success(true);
  }
  //===================================================================================================

  @Override
  public Result<Player> findUserByName(String playerName) {
    return Optional.ofNullable(players.get(playerName))
            .map(player -> success(player))
            .orElse(fail(USER_NOT_LOGGED_IN.getError(playerName)));
  }
  //===================================================================================================

  @Override
  public List<Context> getContextList() {
    return new ArrayList(contextMap.values());
  }
  //===================================================================================================

  @Override
  public Result<List<String>> getBaseWarriorClasses() {
    return success(new ArrayList(registeredWarriorBaseClasses.keySet()));
  }
  //===================================================================================================

  @Override
  public Result<List<String>> getWeaponClasses() {
    return success(new ArrayList(registeredWeaponClasses.keySet()));
  }
  //===================================================================================================

  @Override
  public Result<WarriorBaseClassInfo> findWarriorBaseClassByName(String className) {
    return Optional.ofNullable(registeredWarriorBaseClasses.get(className))
            .map(info -> success(info))
            .orElse(fail(WARRIOR_BASE_CLASS_NOT_FOUND_BY_NAME.getError(className)));
  }
  //===================================================================================================

  @Override
  public Result<Class<? extends Weapon>> findWeaponClassByName(String weaponName) {
    return Optional.ofNullable(registeredWeaponClasses.get(weaponName))
            .map(weaponClassInfo -> success(weaponClassInfo.getWeaponClass()))
            .orElse(fail(WEAPON_BASE_CLASS_NOT_FOUND_BY_NAME.getError(weaponName)));
  }
  //===================================================================================================

  @Override
  public Result<WeaponClassInfo> findWeaponByName(String weaponName) {
    return Optional.ofNullable(registeredWeaponClasses.get(weaponName))
            .map(weaponClassInfo -> success(weaponClassInfo))
            .orElse(fail(WEAPON_BASE_CLASS_NOT_FOUND_BY_NAME.getError(weaponName)));
  }
  //===================================================================================================

  @Override
  public Result<Class<? extends Artifact<Warrior>>> findArtifactForWarrior(String artifactName) {
    return Optional.ofNullable(registeredArtifactForWarriorClasses.get(artifactName))
            .map(clazz -> success(clazz))
            .orElse(fail(ARTIFACT_BASE_CLASS_ARTIFACT_OF_WARRIOR_NOT_FOUND_BY_NAME.getError(artifactName)));
  }
  //===================================================================================================

  @Override
  public Result<Class<? extends Artifact<Player>>> findArtifactForPlayer(String artifactName) {
    return Optional.ofNullable(registeredArtifactForPlayerClasses.get(artifactName))
            .map(artifactClassInfo -> success(artifactClassInfo.getArtifactClass()))
            .orElse(fail(ARTIFACT_BASE_CLASS_ARTIFACT_OF_PLAYER_NOT_FOUND_BY_NAME.getError(artifactName)));
  }
  //===================================================================================================

  @Override
  public Result<List<Artifact>> getWarriorArtifacts() {
    return success(true)
            .mapSafe(t -> success(registeredArtifactForWarriorClasses.values().stream()
                    .map(aClass -> {
                      try {
                        return aClass.getConstructor(Warrior.class).newInstance(new Object[]{null});
                      } catch (Throwable th) {
                        throw new RuntimeException(th);
                      }
                    })
                    .collect(Collectors.toList()))
            );
  }
  //===================================================================================================

  @Override
  public Result<List<ArtifactClassInfo>> getPlayerArtifacts() {
    return success(true)
            .mapSafe(t -> success(registeredArtifactForPlayerClasses.values().stream()
                    .collect(Collectors.toList()))
            );
  }
  //===================================================================================================

  @Override
  public Result<SpellInfo> findSpellForPlayer(String spellName) {
    return Optional.ofNullable(registeredSpellForPlayerClasses.get(spellName))
            .map(clazz -> success(clazz))
            .orElse(fail(SPELL_FOR_PLAYER_NOT_FOUND_BY_NAME.getError(spellName)));
  }
  //===================================================================================================

  @Override
  public Result<List<SpellInfo>> getSpellsForPlayer() {
    return success(new ArrayList(registeredSpellForPlayerClasses.values()));
  }
  //===================================================================================================

  @Override
  public Result<List<SpellInfo>> getSpellsForWarrior() {
    return success(new ArrayList(registeredSpellForWarriorClasses.values()));
  }
  //===================================================================================================

  @Override
  public Result<SpellInfo> findSpellForWarrior(String spellName) {
    return Optional.ofNullable(registeredSpellForWarriorClasses.get(spellName))
            .map(clazz -> success(clazz))
            .orElse(fail(SPELL_FOR_WARRIOR_NOT_FOUND_BY_NAME.getError(spellName)));
  }
  //===================================================================================================

  /**
   * добавляет в список базовых классов информацию о новом базовом игровом классе воина
   *
   * @param baseClassInfo
   * @return
   */
  public Core innerRegisterWarriorBaseClass(WarriorBaseClassInfo baseClassInfo) {
    registeredWarriorBaseClasses.put(baseClassInfo.getName(), baseClassInfo);
    return this;
  }
  //===================================================================================================

  public BeanFactory getBeanFactory() {
    return beanFactory;
  }


  //===================================================================================================

  @Override
  public Result<Integer> calculatePrice(Weapon weapon) {
    return success(weapon.getWeaponClassInfo().getGoldCost());
  }
  //===================================================================================================

  @Override
  public Result<Integer> calculatePrice(Warrior warrior) {
    return success((int) Math.round(warrior.getWarriorBaseClassInfo().getGoldCost() + warrior.getWeapons().stream()
            .map(weapon -> calculatePrice(weapon).getResult())
            .reduce(0, (acc, cost) -> acc += cost)
            * (warrior.getWeapons().size() > 1
            ? Math.pow(gameMetrics.getWeaponGoldCostGain(), warrior.getWeapons().size() - 1)
            : 1)));
  }
  //===================================================================================================

  @Override
  public Result<Integer> calculatePrice(WarriorShortDto warriorShortDto) {
    return success((int) Math.round(warriorShortDto.getWeapons().stream()
            // TODO NPE возможно
            .map(weaponShortDto -> findWeaponByName(weaponShortDto.getTitle()).getResult().getGoldCost())
            .reduce(0, (acc, cost) -> acc += cost)
            * (warriorShortDto.getWeapons().size() > 1
            ? Math.pow(gameMetrics.getWeaponGoldCostGain(), warriorShortDto.getWeapons().size() - 1)
            : 1)
    ));
  }
  //===================================================================================================

  @Override
  public Result<Integer> calculatePrice(Player player) {
    int totalCost = player.getWarriors().stream()
            .map(warrior -> calculatePrice(warrior).getResult())
            .reduce(0, (acc, cost) -> acc += cost);

    return success((int) Math.round(totalCost * (player.getWarriors().size() > 1
            ? Math.pow(gameMetrics.getWarriorGoldCostGain(), player.getWarriors().size())
            : 1)));
  }
  //===================================================================================================

  @Override
  public List<String> getActiveUsersList() {
    return new ArrayList<>(activeUsersMap.keySet());
  }
  //===================================================================================================

  public Core addActiveUser(String userName, String params){
    activeUsersMap.put(userName, params);
    return this;
  }
  //===================================================================================================

  public Core removeActiveUser(String userName){
    activeUsersMap.remove(userName);
    return this;
  }
  //===================================================================================================

  public String getActiveUserChannel(String userName){
    return activeUsersMap.get(userName);
  }
  //===================================================================================================
}
