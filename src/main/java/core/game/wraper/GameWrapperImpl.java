package core.game.wraper;

import api.core.Context;
import api.core.Core;
import api.core.EventDataContainer;
import api.core.Result;
import api.dto.artifact.ArtifactDto;
import api.dto.common.HeroBaseClassDto;
import api.dto.common.PlayerHeroDto;
import api.dto.core.ContextDto;
import api.dto.spell.SpellDto;
import api.dto.weapon.WeaponDto;
import api.entity.ability.Influencer;
import api.entity.artifct.Artifact;
import api.entity.magic.Spell;
import api.entity.warrior.Warrior;
import api.entity.weapon.Weapon;
import api.enums.EventType;
import api.game.action.InfluenceResult;
import api.game.map.Player;
import api.game.map.metadata.GameRules;
import api.game.map.metadata.xml.LevelMapMetaDataXml;
import api.game.wraper.GameWrapper;
import api.geo.Coords;
import com.google.gson.GsonBuilder;
import core.entity.artifact.player.ArtifactCapitansSabreForPlayer;
import core.entity.artifact.player.ArtifactForeCloverForPlayer;
import core.entity.artifact.player.ArtifactHealingFialForPlayer;
import core.entity.map.GameRulesImpl;
import core.entity.warrior.Skeleton;
import core.entity.warrior.VerwolfShaman;
import core.entity.warrior.Viking;
import core.entity.warrior.Vityaz;
import core.entity.weapon.*;
import core.system.event.EventImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static core.system.ResultImpl.success;

@Component
public class GameWrapperImpl implements GameWrapper {

  private static final Logger logger = LoggerFactory.getLogger(GameWrapperImpl.class);

  @Autowired
  private Core core;

  @Autowired
  private BeanFactory beanFactory;

  //===================================================================================================

  @Override
  @PostConstruct
  public void init() {
    core.registerWarriorBaseClass(Skeleton.class);
    core.registerWarriorBaseClass(Viking.class);
    core.registerWarriorBaseClass(Vityaz.class);
    core.registerWarriorBaseClass(VerwolfShaman.class);

    core.registerWeaponClass(Bow.classInfo);
    core.registerWeaponClass(ShortSword.classInfo);
    core.registerWeaponClass(Sword.classInfo);
    core.registerWeaponClass(Axe.classInfo);

    core.registerWeaponClass(SmallShield.classInfo);
    core.registerWeaponClass(KiteShield.classInfo);

    core.registerArtifactForPlayerClass(ArtifactHealingFialForPlayer.classInfo);
    core.registerArtifactForPlayerClass(ArtifactCapitansSabreForPlayer.classInfo);
    core.registerArtifactForPlayerClass(ArtifactForeCloverForPlayer.classInfo);

//    core.registerWeaponClass(OldIronArmor.CLASS_NAME, OldIronArmor.class);

//    core.registerArtifactForWarriorClass(TestArtifactRainbowArrowForWarrior.CLASS_NAME, TestArtifactRainbowArrowForWarrior.class);
//
//    core.registerArtifactForPlayerClass(TestArtifactHealinFialForPlayer.CLASS_NAME, TestArtifactHealinFialForPlayer.class);
  }
  //===================================================================================================

  @Override
  public Result<Player> login(String userName, Long userId) {
    return core.loginPlayer(userName, userId)
            .logIfError(logger);
  }
  //===================================================================================================

  @Override
  public Result<Player> logout(String userName) {
    return core.logoutPlayer(userName)
            .logIfError(logger);
  }
  //===================================================================================================

  @Override
  public Result<List<LevelMapMetaDataXml>> getLevelMapsList(String userName) {
    return core.findUserByName(userName).mapSafe(player -> core.getLevelMapsList())
            .logIfError(logger);
  }
  //===================================================================================================

  @Override
  public Result<Context> createGame(String ownerUserName
          , GameRules gameRules
          , String mapName
          , String gameName
          , boolean hidden) {

    return core.findUserByName(ownerUserName)
            .mapFail(errorResult -> {
              core.fireEvent(new EventImpl(null, null
                      , EventType.GAME_CONTEXT_CREATE
                      , new EventDataContainer(errorResult
                      , new String[]{gameName, ownerUserName}), null));
              return errorResult
                      .logIfError(logger);
            })
            .mapSafe(foundUser -> core.createGameContext(foundUser, new GameRulesImpl(gameRules)
                    , mapName
                    , gameName, hidden))
            .logIfError(logger);
  }
  //===================================================================================================

  @Override
  public Result<Context> createTechnicalContext(String ownerUserName) {
    return core.findUserByName(ownerUserName)
            .mapFail(errorResult -> {
              core.fireEvent(new EventImpl(null, null
                      , EventType.GAME_CONTEXT_CREATE
                      , new EventDataContainer(errorResult
                      , new String[]{"." + ownerUserName, ownerUserName}), null));
              return errorResult
                      .logIfError(logger);
            })
            .mapSafe(foundUser -> core.createTechnicalContext(foundUser))
            .logIfError(logger);

  }
  //===================================================================================================

  @Override
  public Result<Player> connectToGame(String userName, String contextId) {
    return core.findUserByName(userName)
            .mapSafe(foundUser -> core.findGameContextByUID(contextId)
                    .map(foundContext -> foundContext.connectPlayer(foundUser))
                    .map(ctx -> (Result<Player>) success(foundUser)))
            .logIfError(logger);
  }
  //===================================================================================================

  @Override
  public Result getGamesList() {
    return success(core.getContextList());
  }
  //===================================================================================================

  @Override
  public Result<Warrior> createWarrior(String contextId, String userName, String className, Coords coords) {
    // ищем контекст
    return core.findGameContextByUID(contextId)
            .mapSafe(fineContext -> fineContext.createWarrior(userName, className, coords))
            .logIfError(logger);
  }
//===================================================================================================

  @Override
  public Result<InfluenceResult> attackWarrior(String contextId, String userName, String attackerWarriorId, String targetWarriorId, String weaponId) {
    return core.findGameContextByUID(contextId)
            .mapSafe(context -> context.attackWarrior(userName, attackerWarriorId, targetWarriorId, weaponId))
            .logIfError(logger);
  }
//===================================================================================================

  @Override
  public Result<Warrior> moveWarriorTo(String contextId, String userName, String warriorId, Coords coords) {
    return core.findGameContextByUID(contextId)
            .mapSafe(fineContext -> fineContext.moveWarriorTo(userName, warriorId, coords))
            .logIfError(logger);
  }
//===================================================================================================

  @Override
  public Result<Warrior> rollbackMove(String contextId, String userName, String warriorId) {
    return core.findGameContextByUID(contextId)
            .mapSafe(fineContext -> fineContext.rollbackMove(userName, warriorId))
            .logIfError(logger);
  }
//===================================================================================================

  @Override
  public Result<Coords> whatIfMoveWarriorTo(String contextId, String userName, String warriorId, Coords coords) {
    return core.findGameContextByUID(contextId)
            .mapSafe(fineContext -> fineContext.whatIfMoveWarriorTo(userName, warriorId, coords))
            .logIfError(logger);
  }
//===================================================================================================

  @Override
  public Result<Weapon> giveWeaponToWarrior(String contextId, String userName, String warriorId, String weaponClassName) {
    return core.findGameContextByUID(contextId)
            .mapSafe(fineContext -> core.findWeaponClassByName(weaponClassName)
                    .map(weponClass -> fineContext.giveWeaponToWarrior(userName, warriorId, weponClass)))
            .logIfError(logger);
  }
//===================================================================================================

  @Override
  public Result<Artifact<Warrior>> giveArtifactToWarrior(String contextId, String userName, String warriorId, String artifactName) {
    return core.findGameContextByUID(contextId)
            .mapSafe(fineContext -> core.findArtifactForWarrior(artifactName)
                    .map(artifactClass -> fineContext.giveArtifactToWarrior(userName, warriorId, artifactClass)))
            .logIfError(logger);
  }
//===================================================================================================

  @Override
  public Result<List<Result<InfluenceResult>>> castSpellByPlayer(String contextId, String userName, String spellName, Map<Integer, String> givenSpellParameters) {
    return core.findGameContextByUID(contextId)
            .mapSafe(fineContext -> core.findSpellForPlayer(spellName)
                    .map(spellInfo -> fineContext.castSpellByPlayer(userName, (Class<? extends Spell<Player>>) spellInfo.getSpellClass(), givenSpellParameters)))
            .logIfError(logger);
  }
//===================================================================================================

  @Override
  public Result<List<Result<InfluenceResult>>> castSpellByWarrior(String contextId, String userName, String warriorId, String spellName, Map<Integer, String> givenSpellParameters) {
    return core.findGameContextByUID(contextId)
            .mapSafe(fineContext -> core.findSpellForWarrior(spellName)
                    .map(spellInfo -> fineContext.castSpellByWarrior(userName, warriorId, spellName, givenSpellParameters)))
            .logIfError(logger);
  }
//===================================================================================================

  @Override
  public Result<Artifact<Player>> giveArtifactToPlayer(String contextId, String userName, String artifactName) {
    return core.findGameContextByUID(contextId)
            .mapSafe(fineContext -> core.findArtifactForPlayer(artifactName)
                    .map(artifactClass -> fineContext.giveArtifactToPlayer(userName, artifactClass)))
            .logIfError(logger);
  }
//===================================================================================================

  @Override
  public Result<Artifact<Player>> dropArtifactByPlayer(String contextId, String userName, String artifactInstanceId) {
    return core.findGameContextByUID(contextId)
            .mapSafe(fineContext -> fineContext.dropArtifactByPlayer(userName, artifactInstanceId))
            .logIfError(logger);
  }
//===================================================================================================

  @Override
  public Result<Artifact<Warrior>> dropArtifactByWarrior(String contextId, String userName, String warriorId, String artifactInstanceId) {
    return core.findGameContextByUID(contextId)
            .mapSafe(fineContext -> fineContext.dropArtifactByWarrior(userName, warriorId, artifactInstanceId))
            .logIfError(logger);
  }
//===================================================================================================

  @Override
  public Result<Weapon> dropWeaponByWarrior(String contextId, String userName, String warriorId, String weaponId) {
    return core.findGameContextByUID(contextId)
            .mapSafe(fineContext -> fineContext.dropWeaponByWarrior(userName, warriorId, weaponId))
            .logIfError(logger);
  }
//===================================================================================================

  @Override
  public Result<Warrior> findWarriorById(String contextId, String warriorId) {
    return core.findGameContextByUID(contextId)
            .mapSafe(context -> context.findWarriorById(warriorId))
            .logIfError(logger);
  }
//===================================================================================================

  @Override
  public Result<Warrior> ifWarriorCanActsAtThisTurn(String contextId, String userName, String warriorId) {
    return core.findGameContextByUID(contextId)
            .mapSafe(context -> context.findUserByName(userName)
                    .map(player -> context.ifWarriorCanActsAtThisTurn(userName, warriorId)))
            .logIfError(logger);
  }
//===================================================================================================

  @Override
  public Result<Weapon> findWeaponById(String contextId, String userName, String warriorId, String weaponId) {
    return core.findGameContextByUID(contextId)
            .mapSafe(context -> core.findUserByName(userName)
                    .map(player -> context.findWeaponById(userName, warriorId, weaponId)))
            .logIfError(logger);
  }
//===================================================================================================

  @Override
  public Result<Player> playerReadyToPlay(String userName, boolean readyToPlay) {
    return core.findUserByName(userName)
            .mapSafe(foundPlayer -> foundPlayer.findContext()
                    .map(foundContext -> foundContext.setPlayerReadyToGameState(foundPlayer, readyToPlay)))
            .logIfError(logger);
  }
  //===================================================================================================

  @Override
  public Result<Player> getPlayerOwnsTheRound(String contextId) {
    return core.findGameContextByUID(contextId)
            .mapSafe(fineContext -> fineContext.getPlayerOwnsThisTurn())
            .logIfError(logger);
  }
  //===================================================================================================

  @Override
  public Result<Warrior> removeWarrior(String contextId, String userName, String warriorId) {
    return core.findGameContextByUID(contextId)
            .mapSafe(fineContext -> fineContext.removeWarrior(userName, warriorId))
            .logIfError(logger);
  }
  //===================================================================================================

  @Override
  public Result<List<String>> getBaseWarriorClasses() {
    return core.getBaseWarriorClasses()
            .logIfError(logger);
  }
  //===================================================================================================

  @Override
  public Result<List<String>> getWeaponClasses() {
    return core.getWeaponClasses()
            .logIfError(logger);
  }
  //===================================================================================================

  @Override
  public Core getCore() {
    return core;
  }
  //===================================================================================================

  @Override
  public Result<Player> nextTurn(String contextId, String userName) {
    return core.findGameContextByUID(contextId)
            .mapSafe(fineContext -> fineContext.nextTurn(userName))
            .logIfError(logger);
  }
  //===================================================================================================

  @Override
  public Result<List<Influencer>> getWarriorSInfluencers(String contextId, String userName, String warriorId) {
    return core.findGameContextByUID(contextId)
            .mapSafe(context -> context.getWarriorSInfluencers(userName, warriorId))
            .logIfError(logger);
  }
  //===================================================================================================

  @Override
  public Result<Integer> getRoundsCount(String contextId) {
    return core.findGameContextByUID(contextId)
            .mapSafe(context -> context.getRoundsCount());
  }
  //===================================================================================================

  @Override
  public Result<Warrior> saveWarriorAmmunition(String contextId, String userName, String warriorId, String ammunitionName) {
    return core.findGameContextByUID(contextId)
            .mapSafe(context -> context.saveWarriorAmmunition(userName, warriorId, ammunitionName))
            .logIfError(logger);
  }
  //===================================================================================================

  @Override
  public Result<Warrior> renameWarriorEquipment(String contextId, String userName, String warriorId, String oldEquipmentName, String newEquipmentName) {
    return core.findGameContextByUID(contextId)
            .mapSafe(context -> context.renameWarriorEquipment(userName, warriorId, oldEquipmentName, newEquipmentName))
            .logIfError(logger);
  }
  //===================================================================================================

  @Override
  public Result<Warrior> removeWarriorEquipment(String contextId, String userName, String warriorId, String equipmentName) {
    return core.findGameContextByUID(contextId)
            .mapSafe(context -> context.removeWarriorEquipment(userName, warriorId, equipmentName))
            .logIfError(logger);
  }
  //===================================================================================================

  @Override
  public Result<String> getPayerHeroesAsJson(String contextId, String userName) {
    return core.findGameContextByUID(contextId)
            .mapSafe(context -> context.getPlayerHeroes(userName)
                    .map(playerWarriors -> (Result<String>) success(new GsonBuilder().create().toJson(
                            PlayerHeroDto.listFromEntity(playerWarriors, core))
                    ))
            )
            .logIfError(logger);
  }
  //===================================================================================================

  @Override
  public Result<String> getPayerHeroAsJson(String contextId, String userName, String heroName) {
    return core.findGameContextByUID(contextId)
            .mapSafe(context -> context.findPlayerHero(userName, heroName)
                    .map(playerWarrior -> playerWarrior == null ? success("")
                            : (Result<String>) success(new GsonBuilder().create().toJson(
                            PlayerHeroDto.fromEntity(playerWarrior, core))
                    ))
            )
            .logIfError(logger);
  }
  //===================================================================================================

  @Override
  public Result<String> getPlayerWarriorsAsJson(String contextId, String userName, String ownerUserName) {
    return core.findGameContextByUID(contextId)
            .mapSafe(context -> context.getPlayerWarriorsAsJson(userName, ownerUserName))
            .logIfError(logger);
  }
  //===================================================================================================

  @Override
  public Result<List<String>> getActiveUsersList(String userName) {
    return core.findUserByName(userName)
            .mapSafe(player -> (Result<List<String>>) success(core.getActiveUsersList()))
            .logIfError(logger);
  }
  //===================================================================================================

  @Override
  public Result<Context> setGameVisibleMode(String contextId, String userName, boolean hidden) {
    return core.findGameContextByUID(contextId)
            .mapSafe(context -> context.setGameVisibleMode(userName, hidden))
            .logIfError(logger);
  }
  //===================================================================================================

  @Override
  public Result<String> getWarriorBaseClassesAsJson(String contextId) {
    return core.findGameContextByUID(contextId)
            .mapSafe(context ->
                    core.getBaseWarriorClasses()
                            .map(baseClassNames -> success(
                                    baseClassNames.stream()
                                            .map(baseClassName -> HeroBaseClassDto.fromInfo(
                                                    core.findWarriorBaseClassByName(baseClassName).getResult()))
                                            .collect(Collectors.toList())
                                    )
                            )
                            .map(o -> (Result<String>) success(new GsonBuilder().create().toJson(o)))
            )
            .logIfError(logger);
  }
  //===================================================================================================

  @Override
  public Result<String> getWeaponClassesAsJson(String contextId) {
    return core.findGameContextByUID(contextId)
            .mapSafe(context ->
                    core.getWeaponClasses()
                            .map(weaponClasses -> success(weaponClasses.stream()
                                            .map(weaponClassName -> WeaponDto.fromEntity(
                                                    beanFactory.getBean(
                                                            core.findWeaponClassByName(weaponClassName).getResult())))
                                            .collect(Collectors.toList())
                                    )
                            )
                            .map(o -> (Result<String>) success(new GsonBuilder().create().toJson(o)))
            )
            .logIfError(logger);
  }
  //===================================================================================================

  @Override
  public Result<String> getPlayerSpellsAsJson(String contextId) {
    return core.findGameContextByUID(contextId)
            .mapSafe(context ->
                    core.getSpellsForPlayer()
                            .map(spellInfos -> success(SpellDto.listFromInfos(spellInfos)))
                            .map(o -> (Result<String>) success(new GsonBuilder().create().toJson(o))))
            .logIfError(logger);
  }
  //===================================================================================================

  @Override
  public Result<String> getWarriorSpellsAsJson(String contextId) {
    return core.findGameContextByUID(contextId)
            .mapSafe(context ->
                    core.getSpellsForWarrior()
                            .map(spellInfos -> success(SpellDto.listFromInfos(spellInfos)))
                            .map(o -> (Result<String>) success(new GsonBuilder().create().toJson(o))))
            .logIfError(logger);
  }
  //===================================================================================================

  @Override
  public Result<String> getWarriorArtifactsAsJson(String contextId) {
    return core.findGameContextByUID(contextId)
            .mapSafe(context ->
                    core.getWarriorArtifacts()
                            .map(artifacts -> success(ArtifactDto.listFromEntities(artifacts)))
                            .map(o -> (Result<String>) success(new GsonBuilder().create().toJson(o))))
            .logIfError(logger);
  }
  //===================================================================================================

  @Override
  public Result<String> getPlayerArtifactsAsJson(String contextId) {
    return core.findGameContextByUID(contextId)
            .mapSafe(context ->
                    core.getPlayerArtifacts()
                            .map(artifacts -> success(ArtifactDto.listFromEntitiesInfo(artifacts)))
                            .map(o -> (Result<String>) success(new GsonBuilder().create().toJson(o))))
            .logIfError(logger);
  }
  //===================================================================================================

  @Override
  public Result<ContextDto> getExistingGameContext(String userName) {
    return core.findUserByName(userName)
            .mapSafe(player -> (Result<ContextDto>) (player.getContext() == null
                    ? success(null)
                    : success(ContextDto.fromEntity(player.getContext(), true))))
            .logIfError(logger);

  }
  //===================================================================================================

  @Override
  public Result<ContextDto> getGameFullData(String userName) {
    return core.findUserByName(userName)
            .mapSafe(player -> (Result<ContextDto>) (player.getContext() == null
                    ? success(null)
                    : success(ContextDto.fromEntity(player.getContext(), false))))
            .logIfError(logger);
  }
  //===================================================================================================

  @Override
  public Result<List<ContextDto>> getActiveGameList(String userName) {
    return core.findUserByName(userName)
            .mapSafe(player -> (Result<List<ContextDto>>)
                    success(core.getContextList().stream()
                            .filter(context -> !context.isDeleting() && !context.isTechnical() && !context.isHidden())
                            .map(context -> ContextDto.fromEntity(context, true))
                            .collect(Collectors.toList())))
            .logIfError(logger);
  }
  //===================================================================================================

  @Override
  public Result<Player> decampFromTheArena(String contextId, String userName) {
    return core.findUserByName(userName)
            .mapSafe(player -> player.findContext()
                    .map(context -> context.decampFromTheArena(userName)))
            .logIfError(logger);
  }
  //===================================================================================================

}
