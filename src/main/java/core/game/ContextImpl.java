package core.game;

import api.core.*;
import api.core.database.service.PlayerWarriorService;
import api.entity.ability.Influencer;
import api.entity.magic.Spell;
import api.entity.artifct.Artifact;
import api.entity.warrior.Warrior;
import api.entity.weapon.Weapon;
import api.enums.EventType;
import api.game.action.InfluenceResult;
import api.game.map.LevelMap;
import api.game.map.Player;
import api.game.map.metadata.GameRules;
import api.game.map.metadata.xml.LevelMapMetaDataXml;
import api.geo.Coords;
import core.database.entity.warrior.PlayerWarrior;
import core.system.ResultImpl;
import core.system.error.GameError;
import core.system.error.GameErrors;
import core.system.event.EventImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static api.enums.EventType.*;
import static core.system.ResultImpl.fail;
import static core.system.ResultImpl.success;
import static core.system.error.GameErrors.*;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ContextImpl implements Context {

  public static Context NULL_GAME_CONTEXT = new ContextImpl(null, true);

  private static final Logger logger = LoggerFactory.getLogger(Context.class);
  private String contextId = UUID.randomUUID().toString();

  @Autowired
  private LevelMap levelMap;

  @Autowired
  private Core core;

  @Autowired
  BeanFactory beanFactory;

  @Autowired
  PlayerWarriorService playerWarriorService;

  private Player contextCreator;
  private GameRules gameRules;
  private String gameName;
  private boolean hidden;
  private boolean technical;
  private final AtomicBoolean gameRan = new AtomicBoolean(false);
  protected final AtomicBoolean deleting = new AtomicBoolean(false);

  //===================================================================================================
  //===================================================================================================

  public ContextImpl(Player creator, boolean technical) {
    this.contextCreator = creator;
    // признак технического контекста
    this.technical = technical;
  }
  //===================================================================================================

  @Override
  public boolean isTechnical() {
    return technical;
  }
  //===================================================================================================

  @Override
  public Result<List<String>> getFrozenListOfPlayers() {
    return gameRan.get()
            ? success(getLevelMap().getGameProcessData().frozenListOfPlayers.values().stream()
            .map(player -> player.getId()).collect(Collectors.toList()))
            : ResultImpl.fail(CONTEXT_GAME_NOT_STARTED.getError(getGameName(), getContextId()));
  }
  //===================================================================================================

  @Override
  public boolean isGameRan() {
    return gameRan.get();
  }
  //===================================================================================================

  @Override
  public boolean isDeleting() {
    return deleting.get();
  }
  //===================================================================================================

  @Override
  public Result<Context> initDelete() {
    return deleting.get()
            ? ResultImpl.fail(CONTEXT_DELETE_ALREADY_IN_PROGRESS.getError(getGameName(), getContextId()))
            : success(this).peak(context -> deleting.set(true));
  }
  //===================================================================================================

  @Override
  public String getGameName() {
    return gameName;
  }
  //===================================================================================================

  @Override
  public boolean isHidden() {
    return hidden;
  }
  //===================================================================================================

  @Override
  public String getContextId() {
    return contextId;
  }
  //===================================================================================================

  @Override
  public Core getCore() {
    return core;
  }
  //===================================================================================================

  @Override
  public void fireGameEvent(Event gameEvent) {
    core.fireEvent(gameEvent);
  }
  //===================================================================================================

  @Override
  public void fireGameEvent(Event causeEvent, EventType eventType, EventDataContainer source, Map<String, Object> params) {
    fireGameEvent(new EventImpl(this, causeEvent, eventType, source, params));
  }
  //===================================================================================================

  @Override
  public LevelMap getLevelMap() {
    return levelMap;
  }
  //===================================================================================================

  @Override
  public Result loadMap(GameRules gameRules, LevelMapMetaDataXml mapMetadata, String gameName, boolean hidden) {
    return ifGameRan(false).map(fineContext -> {
      Result result = null;
      this.gameName = gameName;
      this.hidden = hidden;

      levelMap.init(this, mapMetadata);

      this.gameRules = gameRules;
      result = success(this);

      fireGameEvent(null, GAME_CONTEXT_LOAD_MAP
              , new EventDataContainer(result, contextCreator, mapMetadata == null ? gameName : mapMetadata.name, hidden)
              , null);
      return result;
    });
  }
  //===================================================================================================

  @Override
  public Result connectPlayer(Player player) {
    if (gameRan.get() && levelMap.getPlayer(player.getId()) == null){
      return fail(USER_CONNECT_GAME_CLOSED_FOR_NEW_USERS.getError(gameName));
    } else if (levelMap != null) {
      return levelMap.connectPlayer(player);
    } else {
      return ResultImpl.fail(MAP_IS_NOT_LOADED.getError());
    }
  }
  //===================================================================================================

  @Override
  public Result disconnectPlayer(Player player) {
    return ifGameRan(false)
            .map(context -> {
              if (levelMap != null) {
                return levelMap.disconnectPlayer(player);
              } else {
                return ResultImpl.fail(MAP_IS_NOT_LOADED.getError());
              }
            });
  }
  //===================================================================================================

  @Override
  public Result<Warrior> findWarriorById(String warriorId) {
    return ifGameDeleting(false)
            .map(fineContext -> getLevelMap().findWarriorById(warriorId));
  }
  //===================================================================================================


  @Override
  public Result<Warrior> createWarrior(String userName, String warriorClassName, Coords coords) {
    return ifGameRan(false)
            .map(thisContext1 -> thisContext1.ifGameDeleting(false))
            .map(fineContext -> fineContext.findUserByName(userName))
            .map(player -> getLevelMap().createWarrior(player, warriorClassName, coords));
  }
  //===================================================================================================

  @Override
  public Result<InfluenceResult> attackWarrior(String userName, String attackerWarriorId, String targetWarriorId, String weaponId) {
    return ifGameDeleting(false)
            .map(fineContext -> fineContext.findUserByName(userName))
            .map(player -> getLevelMap().attackWarrior(player, attackerWarriorId, targetWarriorId, weaponId));
  }
  //===================================================================================================

  public Player getContextCreator() {
    return contextCreator;
  }
  //===================================================================================================

  public GameRules getGameRules() {
    return gameRules;
  }
  //===================================================================================================

  @Override
  public String subscribeEvent(Consumer<Event> consumer, EventType... eventTypes) {
    return core.subscribeEvent(this, consumer, eventTypes);
  }
  //===================================================================================================

  @Override
  public Result<Context> unsubscribeEvent(String consumerId, EventType... eventTypes) {
    return core.unsubscribeEvent(this, consumerId, eventTypes);
  }
  //===================================================================================================

  public Result<Context> ifGameRan(boolean state) {
    return isGameRan() == state ? success(this) : ResultImpl.fail(
            state
                    ? CONTEXT_GAME_NOT_STARTED.getError(getGameName(), getContextId())
                    : CONTEXT_IN_GAME_RAN_STATE.getError(getGameName(), getContextId()));
  }
  //===================================================================================================

  public Result<Context> ifGameDeleting(boolean state) {
    return isDeleting() == state ? success(this) : ResultImpl.fail(
            state
                    ? CONTEXT_IS_NOT_IN_DELETING_STATE.getError(getGameName(), getContextId())
                    : CONTEXT_IS_IN_DELETING_STATE.getError(getGameName(), getContextId()));
  }
  //===================================================================================================

  @Override
  public Result<Context> ifTechnicalStatusIs(boolean isTechnical) {
    return isTechnical() == isTechnical ? success(this) : ResultImpl.fail(
            isTechnical
                    ? CONTEXT_IS_NOT_TECHNICAL.getError(getGameName(), getContextId())
                    : CONTEXT_IS_TECHNICAL.getError(getGameName(), getContextId()));
  }
  //===================================================================================================

  @Override
  public Result<Player> setPlayerReadyToGameState(Player player, boolean readyToGame) {
    return ifGameRan(false)
            .map(fineContext -> fineContext.ifGameDeleting(false)
                    .map(context -> player.setReadyToPlay(readyToGame)));
  }
  //===================================================================================================

  @PostConstruct
  public void init() {
    // Подписаться на события изменения состава истатуса игроков
    subscribeEvent(this::checkForStartGame, PLAYER_CHANGED_ITS_READY_TO_PLAY_STATUS, PLAYER_CONNECTED
            , PLAYER_DISCONNECTED, PLAYER_RECONNECTED, GAME_CONTEXT_REMOVED);

    // подписаться на "умирание" юнитов
    subscribeEvent(this::checkPlayerLooseTheMatch, WARRIOR_WAS_DIE);

    // сообщение кинем, что контекст создался
    fireGameEvent(null, GAME_CONTEXT_CREATED, new EventDataContainer(success(this), getContextCreator()), null);
  }
  //===================================================================================================

  @Override
  public Result<Player> decampFromTheArena(String userName) {
    return findUserByName(userName)
            .map(player -> {
              levelMap.kickOffPlayer(player);
              player.replaceContextSilent(null);
              // если владелец контекста удаляемый игрок, то сделаем владельцем контекста первого из оставшихся игроков
              if (player == contextCreator) {
                contextCreator = levelMap.getPlayers().get(0);
              }

              fireGameEvent(null, PLAYER_DECAMPED_FROM_ARENA, new EventDataContainer(player), null);

              fireGameEvent(null, PLAYER_DISCONNECTED, new EventDataContainer(player, success(player)), null);

              // Проверим сколько игроков осталось.
              if (levelMap.getPlayers().size() == 1) {
                // остался победитель.
                gameRan.set(false);
                // отправим сообщение
                fireGameEvent(null, PLAYER_WINS_THE_MATCH, new EventDataContainer(levelMap.getPlayers().get(0)), null);

                // удалим игру
//                disconnectPlayer(levelMap.getPlayers().get(0));
                getCore().removeGameContext(getContextId());
              }

              return success(player);
            });
  }
  //===================================================================================================

  /**
   * Проверяет есть ли у игрока, потерявшего воина еще воины. Если нет, то выводит игрока из игры как проигравшего
   * Если после этого в игре остаетсятолько один игрок, то он считается победителем
   *
   * @param event
   */
  public void checkPlayerLooseTheMatch(Event event) {
    InfluenceResult result = event.getSource(InfluenceResult.class);
    // проверим, что есть еще юниты
    if (result.getTargetPlayer().getWarriors().size() == 0) {
      // воинов больше нету. Выведем его из игры
      // отправим сообщение
      fireGameEvent(event, PLAYER_LOOSE_THE_MATCH, new EventDataContainer(result.getTargetPlayer()), null);
      // удалим из контекста
      result.getTargetPlayer().replaceContextSilent(null);
      levelMap.kickOffPlayer(result.getTargetPlayer());
      // если владелец контекста удаляемый игрок, то сделаем владельцем контекста первого из оставшихся игроков
      if (result.getTargetPlayer() == contextCreator) {
        contextCreator = levelMap.getPlayers().get(0);
      }
      levelMap.kickOffPlayer(result.getTargetPlayer());

      // Проверим сколько игроков осталось.
      if (levelMap.getPlayers().size() == 1) {
        // остался победитель.
        gameRan.set(false);
        // отправим сообщение
        fireGameEvent(event, PLAYER_WINS_THE_MATCH, new EventDataContainer(levelMap.getPlayers().get(0)), null);
      }
    }
  }
  //===================================================================================================

  @Override
  public Result<Integer> getRoundsCount() {
    return ifGameDeleting(false)
            .map(context -> context.ifGameRan(true)
                    .map(fineContext -> success(levelMap.getRoundsCount())));
  }
  //===================================================================================================

  /**
   * Начать игру
   *
   * @return
   */
  private Result<Context> beginGame() {
    gameRan.set(true);
    getLevelMap().beginGame();
    Result result = success(this);
    fireGameEvent(null, GAME_CONTEXT_GAME_HAS_BEGAN, new EventDataContainer(this, result), null);
    return result;
  }
  //===================================================================================================

  private void checkForStartGame(Event event) {
    // без разницы какое событие произошло из касающихся смены состава и качества игроков - действия будут одинаковы
    boolean newState =
            getLevelMap().getPlayers().size() == getLevelMap().getMaxPlayerCount()
                    && getLevelMap().getPlayers().stream().filter(foundPlayer -> foundPlayer.isReadyToPlay())
                    .count() == getLevelMap().getMaxPlayerCount();

    // Если текущий статус игры отличен от нового, то будем его менять, если можно
    if (newState != isGameRan()) {
      (isGameRan()
              ? ResultImpl.fail(CONTEXT_IN_GAME_RAN_STATE.getError(getGameName(), getContextId()))
              : success(this))
              .map(fineContext -> ((ContextImpl) fineContext).beginGame())
              .doIfFail(error -> logger.error(((GameError) error).getMessage()));
    }
  }
  //===================================================================================================

  @Override
  public Result<Context> ifNewWarriorSCoordinatesAreAvailable(Warrior warrior, Coords newCoords) {
    return success(this);
  }
  //===================================================================================================

  @Override
  public Result<Warrior> ifWarriorCanActsAtThisTurn(String userName, String warriorId) {
    return ifGameDeleting(false)
            .map(fineContext -> fineContext.findUserByName(userName))
            .map(player -> getLevelMap().ifWarriorCanActsAtThisTurn(player, warriorId));
  }
  //===================================================================================================

  @Override
  public Result<Warrior> moveWarriorTo(String userName, String warriorId, Coords coords) {
    return ifGameDeleting(false)
            .map(fineContext -> fineContext.findUserByName(userName)
                    .map(player -> fineContext.getLevelMap().moveWarriorTo(player, warriorId, coords)));
  }
  //===================================================================================================

  @Override
  public Result<Warrior> rollbackMove(String userName, String warriorId) {
    return ifGameDeleting(false)
            .map(context -> findUserByName(userName))
            .map(player -> getLevelMap().rollbackMove(player, warriorId));
  }
  //===================================================================================================

  @Override
  public Result<Coords> whatIfMoveWarriorTo(String userName, String warriorId, Coords coords) {
    return ifGameDeleting(false)
            .map(fineContext -> fineContext.findUserByName(userName)
                    // если игра запущена, то двигать фигуры можно только в свой ход
                    .map(player -> !fineContext.isGameRan() || getPlayerOwnsThisTurn() == player
                            ? (Result<Coords>) getLevelMap().whatIfMoveWarriorTo(player, warriorId, coords)
                            : ResultImpl.fail(PLAYER_IS_NOT_OWENER_OF_THIS_ROUND.getError(userName, "перемещение юнита " + warriorId))));
  }
  //===================================================================================================

  @Override
  public Result<Player> findUserByName(String userName) {
    return Optional.ofNullable(getLevelMap().getPlayer(userName))
            .map(player -> success(player))
            .orElse(ResultImpl.fail(USER_NOT_CONNECTED_TO_THIS_GAME.getError(userName, getGameName(), getContextId())));
  }
  //===================================================================================================

  @Override
  public Result<Player> getPlayerOwnsThisTurn() {
    return ifGameDeleting(false)
            .map(context -> context.ifGameRan(true))
            .map(context -> getLevelMap().getPlayerOwnsThisTurn());
  }
  //===================================================================================================

  @Override
  public Result<Player> ifPlayerOwnsTheTurnEqualsTo(Player player, String... args) {
    return getLevelMap().ifPlayerOwnsThisTurn(player, args);
  }
  //===================================================================================================

  @Override
  public Result<Warrior> removeWarrior(String userName, String warriorId) {
    return ifGameDeleting(false)
            .map(context -> context.ifGameRan(false))
            .map(context -> context.findUserByName(userName)
                    .map(player -> context.getLevelMap().removeWarrior(player, warriorId)));
  }
  //===================================================================================================

  @Override
  public Result<Weapon> giveWeaponToWarrior(String userName, String warriorId, Class<? extends Weapon> weaponClass) {
    return ifGameDeleting(false)
            .map(fineContext -> findUserByName(userName))
            .map(player -> ifGameRan(false)
                    .map(context -> context.getLevelMap().giveWeaponToWarrior(player, warriorId, weaponClass)));
  }
  //===================================================================================================

  @Override
  public Result<Artifact<Warrior>> giveArtifactToWarrior(String userName, String warriorId, Class<? extends Artifact<Warrior>> artifactClass) {
    return ifGameDeleting(false)
            .map(fineContext -> findUserByName(userName))
            .map(player -> ifGameRan(false)
                    .map(context -> context.getLevelMap().giveArtifactToWarrior(player, warriorId, artifactClass)));
  }
  //===================================================================================================

  @Override
  public Result<List<Result<InfluenceResult>>> castSpellByPlayer(String userName, Class<? extends Spell<Player>> spellClass, Map<Integer, String> givenSpellParameters) {
    return ifGameDeleting(false)
            .map(fineContext -> findUserByName(userName)
                    .map(player -> fineContext.getLevelMap().castSpellByPlayer(player, spellClass, givenSpellParameters)));
  }
  //===================================================================================================

  @Override
  public Result<List<Result<InfluenceResult>>> castSpellByWarrior(String userName, String warriorId, String spellName, Map<Integer, String> givenSpellParameters) {
    return ifGameDeleting(false)
            .map(fineContext -> findUserByName(userName)
                    .map(player -> fineContext.getLevelMap().castSpellByWarrior(player, warriorId, spellName, givenSpellParameters)));
  }
  //===================================================================================================

  @Override
  public Result<Artifact<Player>> giveArtifactToPlayer(String userName, Class<? extends Artifact<Player>> artifactClass) {
    return ifGameDeleting(false)
            .map(fineContext -> findUserByName(userName)
                    .map(player -> fineContext.getLevelMap().giveArtifactToPlayer(player, artifactClass)));
  }
  //===================================================================================================

  @Override
  public Result<Artifact<Player>> dropArtifactByPlayer(String userName, String artifactInstanceId) {
    return ifGameDeleting(false)
            .map(fineContext -> findUserByName(userName)
                    .map(player -> fineContext.getLevelMap().dropArtifactByPlayer(player, artifactInstanceId)));
  }
  //===================================================================================================

  @Override
  public Result<Artifact<Warrior>> dropArtifactByWarrior(String userName, String warriorId, String artifactInstanceId) {
    return ifGameDeleting(false)
            .map(fineContext -> findUserByName(userName))
            .map(player -> ifGameRan(false)
                    .map(context -> context.getLevelMap().dropArtifactByWarrior(player, warriorId, artifactInstanceId)));
  }
  //===================================================================================================

  @Override
  public Result<Weapon> dropWeaponByWarrior(String userName, String warriorId, String weaponId) {
    return ifGameDeleting(false)
            .map(fineContext -> findUserByName(userName))
            .map(player -> ifGameRan(false)
                    .map(context -> context.getLevelMap().dropWeaponByWarrior(player, warriorId, weaponId)));
  }
  //===================================================================================================

  @Override
  public Result<Weapon> findWeaponById(String userName, String warriorId, String weaponId) {
    return ifGameDeleting(false)
            .map(context -> context.findUserByName(userName))
            .map(player -> getLevelMap().findWeaponById(player, warriorId, weaponId));
  }
  //===================================================================================================

  @Override
  public Result<Player> nextTurn(String userName) {
    return ifGameDeleting(false)
            .map(contextX -> contextX.ifGameRan(true))
            .map(fineContext -> fineContext.findUserByName(userName))
            .map(player -> getLevelMap().nextTurn(player));
  }
  //===================================================================================================

  @Override
  public Result<List<Influencer>> getWarriorSInfluencers(String userName, String warriorId) {
    return ifGameDeleting(false)
            .map(contextX -> findUserByName(userName))
            .map(player -> getLevelMap().getWarriorSInfluencers(player, warriorId));
  }
  //===================================================================================================

  @Override
  public int calcDistance(Coords from, Coords to) {
    return (int) Math.round(Math.sqrt((double) ((from.getX() - to.getX()) * (from.getX() - to.getX())
            + (from.getY() - to.getY()) * (from.getY() - to.getY()))));
  }
  //===================================================================================================

  @Override
  public int calcDistanceMm(Coords from, Coords to) {
    return (int) Math.round(Math.sqrt((double) ((from.getX() - to.getX()) * (from.getX() - to.getX())
            + (from.getY() - to.getY()) * (from.getY() - to.getY()))) / (double)levelMap.getMapMetadata().simpleUnitSize * 10.0D);
  }
  //===================================================================================================

  @Override
  public Result<Warrior> saveWarriorAmmunition(String userName, String warriorId, String ammunitionName) {
    return ifGameDeleting(false)
            .map(contextX -> ifTechnicalStatusIs(true))
            .map(fineContext -> ifGameRan(false))
            .map(context -> context.findUserByName(userName)
                    // найдем у игрока воина
                    .map(player -> player.findWarriorById(warriorId)
                            .map(warrior ->
                                    // теперь все есть. Сохраним
                                    playerWarriorService.saveWarriorEquipment(player, warrior, ammunitionName)

                                            .map(entityWarriorEquipment -> success(warrior))
                            )
                    )
            );
  }
  //===================================================================================================

  @Override
  public Result<Warrior> renameWarriorEquipment(String userName, String warriorId, String oldEquipmentName, String newEquipmentName) {
    return ifGameDeleting(false)
            .map(contextX -> ifTechnicalStatusIs(true))
            .map(fineContext -> ifGameRan(false))
            .map(context -> context.findUserByName(userName)
                    // найдем у игрока воина
                    .map(player -> player.findWarriorById(warriorId)
                            .map(warrior ->
                                    // теперь все есть. Сохраним
                                    playerWarriorService.renameWarriorEquipment(player, warrior, oldEquipmentName, newEquipmentName)
                                            .map(entityWarriorEquipment -> success(warrior))
                            )
                    )
            );
  }
  //===================================================================================================

  @Override
  public Result<Warrior> removeWarriorEquipment(String userName, String warriorId, String equipmentName) {
    return ifGameDeleting(false)
            .map(contextX -> ifTechnicalStatusIs(true))
            .map(fineContext -> ifGameRan(false))
            .map(context -> context.findUserByName(userName)
                    // найдем у игрока воина
                    .map(player -> player.findWarriorById(warriorId)
                            .map(warrior ->
                                    // теперь все есть. найдем экипировку и удалим
                                    playerWarriorService.removeWarriorEquipment(player, warrior, equipmentName)
                                            .map(entityWarriorEquipment -> success(warrior))
                            )
                    )
            );
  }
  //===================================================================================================

  @Override
  public Result<List<PlayerWarrior>> getPlayerHeroes(String userName) {
    return ifGameDeleting(false)
            .map(fineContext -> ifGameRan(false))
            .map(context -> context.findUserByName(userName)
                    // найдем у игрока воина
                    .map(player -> player.getAllHeroes()));
  }
  //===================================================================================================

  @Override
  public Result<String> getPlayerWarriorsAsJson(String userName, String ownerUserName) {
    return ifGameDeleting(false)
            .map(fineContext -> fineContext.findUserByName(userName)
                    // найдем у игрока воина
                    .map(player ->
                            // игрок есть, контекст тоже. Теперь соберем либо воинов одного игрока, либо всех воинов
                            fineContext.getLevelMap().getPlayerWarriorsAsJson(ownerUserName)
                    ));
  }
  //===================================================================================================

  @Override
  public Result<Context> setGameVisibleMode(String userName, boolean hidden) {
    return ifGameDeleting(false)
            // только игровой контекст
            .map(context -> (Result<Context>) (technical
                    ? fail(GameErrors.CONTEXT_IS_TECHNICAL.getError(getContextId()))
                    : success(context)))
            // пользователь
            .map(fineContext -> fineContext.findUserByName(userName)
                    // Игрок должен быть владельцем контекста
                    .map(player ->
                            // игрок есть, контекст тоже. Теперь соберем либо воинов одного игрока, либо всех воинов
                            !fineContext.getContextCreator().getId().equals(player.getId())
                                    // игрок '%s' не является создателем игры '%s'.
                                    ? fail(GameErrors.CONTEXT_IS_NOT_OWNED_BY_USER.getError(userName, fineContext.getGameName()))
                                    : innerSetGameVisibleMode(hidden)

                    ));
  }
  //===================================================================================================

  private Result<Context> innerSetGameVisibleMode(boolean hidden) {
    this.hidden = hidden;

    //  отправим сообщение
    fireGameEvent(new EventImpl(this, null, GAME_CONTEXT_VISIBILITY_CHANGED, new EventDataContainer(this), null));

    return success(this);
  }
  //===================================================================================================

  @Override
  public Result<PlayerWarrior> findPlayerHero(String userName, String heroName) {
    return ifGameDeleting(false)
            .map(fineContext -> ifGameRan(false))
            .map(context -> context.findUserByName(userName)
                    // запросим данные воина
                    .map(player -> player.findHero(heroName)));
  }

  //===================================================================================================
}
