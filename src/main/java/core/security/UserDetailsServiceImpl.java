package core.security;

import core.database.entity.security.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static javax.transaction.Transactional.TxType.REQUIRED;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class UserDetailsServiceImpl implements UserDetailsService {

  private static final String GET_USER_BY_NAME = "from User where name = :name";

  @Autowired
  SessionFactory sessionFactory;

  @Override
  @Transactional(REQUIRED)
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    Session session = sessionFactory.getCurrentSession();
    User player = (User) session.createQuery(GET_USER_BY_NAME).setParameter("name", username).uniqueResult();
    if (player == null) {
      throw new BadCredentialsException("Неверные логин или пароль");
    }
    List<GrantedAuthority> auth = new ArrayList<>();
    player.getRoles().stream().forEach(role -> auth.add(new SimpleGrantedAuthority(role.getName())));


    org.springframework.security.core.userdetails.User user = new ExpandedUserDetails(username, player.getPasswordBlake2(), player.isEnabled(), true, true, true
            , auth, player.getId());
    return user;
  }

  public class ExpandedUserDetails extends org.springframework.security.core.userdetails.User {
    private Long databaseId;

    public ExpandedUserDetails(String username, String password, boolean enabled
            , boolean accountNonExpired, boolean credentialsNonExpired
            , boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities
            , Long databaseId) {
      super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
      this.databaseId = databaseId;
    }

    public Long getDatabaseId() {
      return databaseId;
    }
  }
}
