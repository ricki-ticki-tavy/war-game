package core.entity.warrior;

import api.core.Result;
import api.entity.warrior.WarriorBaseClassInfo;
import api.enums.ArmorClassEnum;
import api.game.action.InfluenceResult;
import core.entity.warrior.base.AbstractWarriorBaseClass;
import core.entity.warrior.base.WarriorBaseClassInfoImpl;
import core.entity.warrior.base.WarriorSBaseAttributesImpl;
import core.entity.weapon.ShortSword;
import core.entity.weapon.Sword;
import core.system.ResultImpl;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static core.entity.ModelsGraphicInfo.VIKING_CLASS;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Viking extends AbstractWarriorBaseClass {

  public static WarriorBaseClassInfo classInfo = new WarriorBaseClassInfoImpl(
          "Викинг"
          , "Воин с легкой броней. Что-то тоже может. Не помню"
          , Viking.class
          , Collections.EMPTY_LIST
          , Collections.EMPTY_LIST
          , 30
          , 0
          , VIKING_CLASS);

  public Viking() {
    super(classInfo.getName(), classInfo.getDescription());
    setWarriorSBaseAttributes(new WarriorSBaseAttributesImpl(2000, 0, 1, 240, 2)
            .setMaxHealth(2000)
            .setMaxManna(0)
            .setMaxActionPoints(240)
            .setMaxAbilityActionPoints(1)
            .setArmorClass(ArmorClassEnum.ARMOR_1)
            .setDeltaCostMove(0)
            .setMaxDefenseActionPoints(60)
            .setSummonable(false)
    );

    setSupportedWeaponClasses(Stream.of(
            ShortSword.class
            , Sword.class
    ).collect(Collectors.toList()));
  }
  //===================================================================================================

  @Override
  public Result<InfluenceResult> innerWarriorUnderAttack(InfluenceResult attackResult) {
    return ResultImpl.success(attackResult);
  }
  //===================================================================================================

  @Override
  protected WarriorBaseClassInfo getInfo() {
    return classInfo;
  }
  //===================================================================================================


  @Override
  public WarriorBaseClassInfo getWarriorBaseClassInfo() {
    return classInfo;
  }
  //===================================================================================================
}
