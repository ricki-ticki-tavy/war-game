package core.entity.warrior.base;

import api.entity.ability.AbilityInfo;
import api.entity.graphics.GraphicsInfo;
import api.entity.magic.SpellInfo;
import api.entity.warrior.WarriorBaseClass;
import api.entity.warrior.WarriorBaseClassInfo;

import java.util.Collections;
import java.util.List;

/**
 * Класс информации о базовом игровом классе воина
 */
public class WarriorBaseClassInfoImpl implements WarriorBaseClassInfo {
  private final String name;
  private final String description;
  private final List<SpellInfo> spellInfos;
  private final List<AbilityInfo> abilityInfos;
  private final Class<? extends WarriorBaseClass> baseClass;
  private final GraphicsInfo graphicsInfo;
  protected int goldCost = 0;
  protected int owningGoldCost = 0;

  public WarriorBaseClassInfoImpl(String name
          , String description
          , Class<? extends WarriorBaseClass> baseClass
          , List<SpellInfo> spellInfos
          , List<AbilityInfo> abilityInfos
          , int goldCost
          , int owningGoldCost
          , GraphicsInfo graphicsInfo) {
    this.name = name;
    this.description = description;
    this.baseClass = baseClass;
    this.spellInfos = Collections.unmodifiableList(spellInfos);
    this.abilityInfos = Collections.unmodifiableList(abilityInfos);
    this.graphicsInfo = graphicsInfo;
    this.goldCost = goldCost;
    this.owningGoldCost = owningGoldCost;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public List<SpellInfo> getSpellsInfo() {
    return spellInfos;
  }

  @Override
  public List<AbilityInfo> getAbilitiesInfo() {
    return abilityInfos;
  }

  @Override
  public Class<? extends WarriorBaseClass> getWarriorClass() {
    return baseClass;
  }

  @Override
  public GraphicsInfo getGraphicsInfo() {
    return graphicsInfo;
  }

  @Override
  public int getGoldCost() {
    return goldCost;
  }

  @Override
  public int getOwningGoldCost() {
    return owningGoldCost;
  }
}
