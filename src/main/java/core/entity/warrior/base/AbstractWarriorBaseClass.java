package core.entity.warrior.base;


import api.core.Context;
import api.core.Core;
import api.core.Event;
import api.core.Result;
import api.entity.ability.Ability;
import api.entity.magic.SpellInfo;
import api.entity.warrior.Warrior;
import api.entity.warrior.WarriorBaseClass;
import api.entity.warrior.WarriorBaseClassInfo;
import api.entity.weapon.Weapon;
import api.enums.OwnerTypeEnum;
import api.game.action.InfluenceResult;
import core.entity.abstracts.AbstractOwnerImpl;
import core.game.CoreImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static core.system.ResultImpl.fail;
import static core.system.ResultImpl.success;
import static core.system.error.GameErrors.SPELL_FOR_WARRIOR_NOT_FOUND_BY_NAME_FOR_CLASS;
import static core.system.error.GameErrors.WARRIOR_BASE_ATTRS_IS_FINAL;

public abstract class AbstractWarriorBaseClass extends AbstractOwnerImpl<Warrior> implements WarriorBaseClass {

  protected WarriorSBaseAttributesImpl warriorSBaseAttributes;
  protected List<Class<? extends Weapon>> supportedWeapons;
  protected final Map<String, Ability> abilities = new ConcurrentHashMap<>(10);
  //===================================================================================================
  //===================================================================================================

  public AbstractWarriorBaseClass(String title, String description) {
    super(null, OwnerTypeEnum.WARRIOR, "wrr", title, description);
    this.warriorSBaseAttributes = null;
  }
  //===================================================================================================

  @Override
  public void setWarriorSBaseAttributes(WarriorSBaseAttributesImpl warriorSBaseAttributes) {
    if (this.warriorSBaseAttributes != null) {
      WARRIOR_BASE_ATTRS_IS_FINAL.error();
    } else {
      this.warriorSBaseAttributes =  warriorSBaseAttributes;
    }

  }
  //===================================================================================================

  @Override
  public WarriorSBaseAttributesImpl getBaseAttributes() {
    return warriorSBaseAttributes;
  }
  //===================================================================================================

  @Override
  public Map<String, Ability> getAbilities() {
    return new HashMap<>(abilities);
  }
  //===================================================================================================

  @Override
  public boolean finishRound(Context context) {
    return false;
  }
  //===================================================================================================

  @Override
  public boolean startRound(Context context) {
    return false;
  }
  //===================================================================================================

  @Override
  public boolean isUsedInThisRound() {
    return false;
  }
  //===================================================================================================

  @Override
  public int getAbilityActionPoints() {
    return 0;
  }
  //===================================================================================================

  @Override
  public void fireEvent(Event event) {

  }
  //===================================================================================================

  @Override
  public List<Class<? extends Weapon>> getSupportedWeaponClasses() {
    return supportedWeapons;
  }
  //===================================================================================================

  @Override
  public void setSupportedWeaponClasses(List<Class<? extends Weapon>> supportedWeaponClasses) {
    this.supportedWeapons = supportedWeaponClasses;
  }
  //===================================================================================================

  @Override
  public int getHandsCount() {
    return warriorSBaseAttributes.getHandsCount();
  }
  //===================================================================================================

  @Override
  public boolean isSummonable() {
    return false;
  }
  //===================================================================================================

  @Override
  public WarriorBaseClass attachToWarrior(Warrior owner) {
    this.owner = owner;
    this.setContext(null);
    return this;
  }
  //===================================================================================================

  @Override
  public Result<InfluenceResult> defenceWarrior(InfluenceResult attackResult) {
    return success(attackResult);
  }
  //===================================================================================================

  @Override
  public Result<SpellInfo> findSpell(String spellName) {
    return getInfo().getSpellsInfo().stream()
            .filter(spellInfo -> spellInfo.getName().equalsIgnoreCase(spellName))
            .map(spellInfo -> success(spellInfo))
            .findFirst()
            .orElse(fail(SPELL_FOR_WARRIOR_NOT_FOUND_BY_NAME_FOR_CLASS.getError(spellName, getTitle())));
  }
  //===================================================================================================

  @Override
  public Result<List<SpellInfo>> getSpells() {
    return success(getInfo().getSpellsInfo());
  }
  //===================================================================================================

  protected abstract WarriorBaseClassInfo getInfo();
  //===================================================================================================

  /**
   * Зарегистрировать игровой класс воина и получить информацию по нему
   * @return
   */
  private WarriorBaseClassInfo registerInCatalog(Core core) {
    WarriorBaseClassInfo info = getInfo();
    // регистрируем класс воина
    ((CoreImpl)core).innerRegisterWarriorBaseClass(info);
    // регистрируем заклинания
    info.getSpellsInfo().stream()
            .forEach(spellInfo -> core.registerSpellForWarriorClass(spellInfo.getName(), spellInfo));
    return info;
  }
  //===================================================================================================

  /**
   * СТАТИЧЕСКИЙ Метод для регистрации базового игрового класса воина в ядре
   * @param warriorBaseClass
   * @return
   */
  public static WarriorBaseClassInfo registerWarriorBaseClass(Core core, Class<? extends WarriorBaseClass> warriorBaseClass){
    // получим экземпляр базового игрового класса
    AbstractWarriorBaseClass baseWarriorClass = (AbstractWarriorBaseClass)((CoreImpl)core).getBeanFactory().getBean(warriorBaseClass);
    return baseWarriorClass.registerInCatalog(core);
  }
  //===================================================================================================


}
