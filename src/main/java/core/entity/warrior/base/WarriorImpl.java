package core.entity.warrior.base;

import api.core.EventDataContainer;
import api.core.Owner;
import api.core.Result;
import api.entity.ability.Ability;
import api.entity.ability.Influencer;
import api.entity.ability.Modifier;
import api.entity.magic.Spell;
import api.entity.magic.SpellInfo;
import api.entity.artifct.Artifact;
import api.entity.warrior.*;
import api.entity.weapon.Weapon;
import api.enums.*;
import api.game.action.InfluenceResult;
import api.game.map.Player;
import api.geo.Coords;
import com.google.gson.GsonBuilder;
import api.dto.artifact.ArtifactDto;
import api.dto.warrior.WarriorShortDto;
import api.dto.weapon.WeaponShortDto;
import core.entity.abstracts.AbstractOwnerImpl;
import core.game.action.InfluenceResultImpl;
import core.system.error.GameError;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static api.enums.EventType.*;
import static api.enums.PlayerPhaseType.ATACK_PHASE;
import static core.system.ResultImpl.fail;
import static core.system.ResultImpl.success;
import static core.system.error.GameErrors.*;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class WarriorImpl extends AbstractOwnerImpl<Player> implements Warrior {

  protected Map<Integer, WarriorSHand> hands;
  protected WarriorBaseClass warriorBaseClass;
  protected volatile Coords coords;
  protected boolean summoned;
  protected WarriorSBaseAttributes attributes;
  protected final Map<String, Influencer> influencers = new ConcurrentHashMap<>(50);
  protected Long databaseId = null;

  protected volatile boolean touchedAtThisTurn = false;
  // контратака в этомцикле защиты уже сделана
  protected volatile boolean contrattackUsedAtThisTurn = false;
  protected final AtomicBoolean dead = new AtomicBoolean(false);

  protected volatile Coords originalCoords;
  protected volatile boolean moveLocked;
  protected volatile boolean rollbackAvailable;
  protected volatile int treatedActionPointsForMove;
  protected final Map<String, Artifact<Warrior>> artifacts = new ConcurrentHashMap<>(10);

  protected PlayerPhaseType warriorPhase = null;

  protected final Map<String, Class<? extends Ability>> unsupportedAbilities = new ConcurrentHashMap<>(20);


  @Autowired
  private BeanFactory beanFactory;
  //===================================================================================================
  //===================================================================================================
  //===================================================================================================

  public WarriorImpl(Player owner, WarriorBaseClass warriorBaseClass, String title, Coords coords, boolean summoned) {
    super(owner, OwnerTypeEnum.WARRIOR, "wrr", title, title);
    this.warriorBaseClass = warriorBaseClass;
    this.attributes = warriorBaseClass.getBaseAttributes().clone();
    this.summoned = summoned;
    this.coords = new Coords(coords);
    int handsCount = warriorBaseClass.getHandsCount();
    hands = new ConcurrentHashMap(2);
    while (handsCount-- > 0) {
      hands.put(hands.size(), new WarriorSHandImpl());
    }
    this.warriorBaseClass.attachToWarrior(this);
  }
  //===================================================================================================

  @Override
  public Player getOwner() {
    return super.getOwner();
  }
  //===================================================================================================

  @Override
  public Long getDatabaseId() {
    return databaseId;
  }
  //===================================================================================================

  @Override
  public Warrior setDatabaseId(Long databaseId) {
    this.databaseId = databaseId;
    return this;
  }
  //===================================================================================================

  @Override
  public Result<Artifact<Warrior>> giveArtifactToWarrior(Class<? extends Artifact<Warrior>> artifactClass) {
    Result<Artifact<Warrior>> result = success(null);
    return result.mapSafe(nullArt -> {
      Artifact<Warrior> artifact = beanFactory.getBean(artifactClass
              , this);
      return attachArtifact(artifact);
    });
  }
  //===================================================================================================

  @Override
  public Result<Artifact<Warrior>> attachArtifact(Artifact<Warrior> artifact) {
    Result<Artifact<Warrior>> result;
    if (!artifact.getOwnerTypeForArtifact().equals(OwnerTypeEnum.WARRIOR)) {
      // "В игре %s воин '%s %s' игрока %s не может взять артефакт '%s'. Артефактом может владеть %s
      fail(ARTIFACT_WRONG_TYPE_FOR_WARRIOR.getError(
              getContext().getGameName()
              , warriorBaseClass.getTitle()
              , title
              , getOwner().getId()
              , artifact.getTitle()
              , artifact.getOwnerTypeForArtifact().getTitle()));
    }
    if (artifacts.get(artifact.getTitle()) != null) {
      // уже есть такой артефакт. даем ошибку
      // "В игре %s. воин '%s %s' игрока '%s' уже владеет артефактом '%s'."
      result = fail(ARTIFACT_WARRIOR_ALREADY_HAS_IT.getError(
              getContext().getGameName()
              , warriorBaseClass.getTitle()
              , title
              , owner.getId()
              , artifact.getTitle()));
    } else {
      artifact.attachToOwner(this);
      artifacts.put(artifact.getTitle(), artifact);
      // применить сразу действие артефакта
      artifact.applyToOwner(warriorPhase);
      // отправить сообщение
      getContext().fireGameEvent(null, ARTIFACT_TAKEN_BY_WARRIOR, new EventDataContainer(this, artifact), null);
      result = success(artifact);
    }
    return result;
  }
  //===================================================================================================

  @Override
  public Result<List<Artifact<Warrior>>> getArtifacts() {
    return success(new ArrayList<Artifact<Warrior>>(artifacts.values()));
  }
  //===================================================================================================

  @Override
  public WarriorBaseClass getWarriorBaseClass() {
    return warriorBaseClass;
  }
  //===================================================================================================

  @Override
  public boolean isSummoned() {
    return summoned;
  }
  //===================================================================================================

  @Override
  public List<WarriorSHand> getHands() {
    return new LinkedList(hands.values());
  }
  //===================================================================================================

  @Override
  public List<Weapon> getWeapons() {
    Set<Weapon> weaponSet = new HashSet<>(5);
    hands.values().stream().forEach(hand ->
            hand.getWeapons().stream().forEach(weapon -> weaponSet.add(weapon)));

    return new ArrayList<>(weaponSet);
  }
  //===================================================================================================

  @Override
  public Result<Weapon> findWeaponById(String weaponId) {
    return hands.values().stream()
            .filter(warriorSHand -> warriorSHand.hasWeapon(weaponId))
            .findFirst().map(warriorSHand -> warriorSHand.getWeaponById(weaponId))
            .map(weapon -> success(weapon))
            .orElse(fail(generateWeapoNotFoundError(weaponId)));
  }
  //===================================================================================================

  @Override
  public Result<Artifact<Warrior>> findArtifactById(String artifactId) {
    return artifacts.values().stream()
            .filter(artifact -> artifact.getId().equals(artifactId))
            .findFirst()
            .map(artifact -> success(artifact))
            .orElse(fail(generateArtifactNotFoundError(artifactId)));
  }
  //===================================================================================================

  private Coords innerGetTranslatedToGameCoords() {
    // если игра уже в стадии игры, а не расстановки, юнит не трогали или если не заблокирована возможность отката, то
    // берем OriginalCoords в противном случае берем Coords
    return getContext().isGameRan()
            // игра идет
            && isRollbackAvailable() && !isMoveLocked()
            ? originalCoords
            : coords;
  }
  //===================================================================================================

  @Override
  public Coords getTranslatedToGameCoords() {
    return new Coords(innerGetTranslatedToGameCoords());
  }
  //===================================================================================================

  @Override
  public int calcMoveCost(Coords to) {
    Coords from = innerGetTranslatedToGameCoords();

    return (int) Math.round((double) getWarriorSMoveCost() * Math.sqrt((double) ((from.getX() - to.getX()) * (from.getX() - to.getX())
            + (from.getY() - to.getY()) * (from.getY() - to.getY()))) / (double) getContext().getLevelMap().getMapCellSize());
  }
  //===================================================================================================

  @Override
  public int calcDistanceTo(Coords to) {
    return getContext().calcDistance(innerGetTranslatedToGameCoords(), to);
  }
  //===================================================================================================

  @Override
  public int calcDistanceToTarget(Coords to) {
    return getContext().calcDistance(coords, to);
  }
  //===================================================================================================

  @Override
  public Result<Warrior> moveWarriorTo(Coords to) {
    Result result;
    if (!moveLocked) {
      treatedActionPointsForMove = calcMoveCost(to);
      // установить признак только когда игра уже идет. На этапе расстановки он не имеет значения
      setTouchedOnThisTurn(getContext().isGameRan());
      this.coords = new Coords(to);
      result = success(this);
    } else {
      // новые координаты воина уже заморожены и не могут быть отменены
      // Воин %s (id %s) игрока %s в игре %s (id %s) не может более выполнить перемещение в данном ходе
      result =
              fail(WARRIOR_CAN_T_MORE_MOVE_ON_THIS_TURN.getError(
                      getWarriorBaseClass().getTitle()
                      , getId()
                      , getOwner().getId()
                      , getContext().getGameName()
                      , getContext().getContextId()));
    }
    return result;
  }
  //===================================================================================================

  @Override
  public Coords getCoords() {
    return new Coords(this.coords);
  }
  //===================================================================================================

  @Override
  public Result takeWeapon(Class<? extends Weapon> weaponClass) {
    Result result = null;
    Weapon weapon = beanFactory.getBean(weaponClass);
    if (weapon.getNeededHandsCountToTakeWeapon() > 0) {
      int freePoints = 2 - hands.values().stream().map(hand -> hand.isFree() ? 0 : 1).reduce(0, (acc, chg) -> acc += chg);
      if (freePoints < weapon.getNeededHandsCountToTakeWeapon()) {
        result = fail(WARRIOR_HANDS_NO_FREE_SLOTS.getError(String.valueOf(freePoints)
                , weapon.getTitle()
                , String.valueOf(weapon.getNeededHandsCountToTakeWeapon())));
      }
    }

    if (result == null) {
      AtomicInteger points = new AtomicInteger(weapon.getNeededHandsCountToTakeWeapon());
      // место есть в руках. Ищем свободную руку
      hands.values().stream()
              .filter(hand -> hand.isFree() && points.get() > 0)
              .forEach(hand -> {
                points.decrementAndGet();
                hand.addWeapon(weapon);
                weapon.setOwner(this);
              });
      // применим оружие
      restoreAttributesAvailableForRestoration(warriorPhase);
      result = success(weapon);
    }
    getContext().fireGameEvent(null, WEAPON_TAKEN, new EventDataContainer(this, weapon, result), null);
    return result;
  }
  //===================================================================================================

  @Override
  public Result<Weapon> dropWeapon(String weaponInstanceId) {
    AtomicReference<Result<Weapon>> foundWeapon = new AtomicReference<>(null);
    hands.values().stream().filter(hand -> hand.hasWeapon(weaponInstanceId))
            .forEach(warriorSHand -> {
              foundWeapon.set(success(warriorSHand.removeWeapon(weaponInstanceId)));
              // применить удаление оружия
              restoreAttributesAvailableForRestoration(warriorPhase);
            });

    Result result = Optional.ofNullable(foundWeapon.get())
            .orElse(fail(generateWeapoNotFoundError(weaponInstanceId)));

    getContext().fireGameEvent(null
            , result.isSuccess() ? WEAPON_DROPPED : WEAPON_TRY_TO_DROP
            , new EventDataContainer(this, result.isSuccess() ? result.getResult() : weaponInstanceId, result)
            , null);

    return result;
  }
  //===================================================================================================

  @Override
  public Result<Artifact<Warrior>> dropArtifact(String artifactInstanceId) {
    return findArtifactById(artifactInstanceId)
            .map(artifact -> {
              // долой из списка. Так как не может быть более одного одинакового артефакта, то тут они хранятся по именам
              artifacts.remove(artifact.getTitle());
              // сразу отменить действие.
              restoreAttributesAvailableForRestoration(null);
              // отправить сообщение
              getContext().fireGameEvent(null, ARTIFACT_DROPPED_BY_WARRIOR, new EventDataContainer(this, artifact), null);
              return success(artifact);
            });
  }
  //===================================================================================================

  public Result<Warrior> ifWarriorAlied(Warrior warrior, boolean isAllied) {
    Result<Warrior> warriorResult = owner.findWarriorById(warrior.getId());
    if (warriorResult.isSuccess() == isAllied) {
      // утверждение совпало
      warriorResult = success(warrior);
    } else {
      warriorResult = isAllied
              // ждали дружественного, а он - враг
              // "В игре %s (id %s)  воин '%s %s' (id %s) не является врагом для воина '%s %s' (id %s) игрока %s %s"
              ? fail(WARRIOR_ATTACK_TARGET_WARRIOR_IS_NOT_ALIED.getError(
              getContext().getGameName()
              , getContext().getContextId()
              , warrior.getWarriorBaseClass().getTitle()
              , warrior.getTitle()
              , warrior.getId()
              , getWarriorBaseClass().getTitle()
              , getTitle()
              , getId()
              , getOwner().getId()
              , ""))
              // "В игре %s (id %s)  воин '%s %s' (id %s) является враждебным для воина '%s %s' (id %s) игрока %s %s"
              : fail(WARRIOR_ATTACK_TARGET_WARRIOR_IS_ALIED.getError(
              getContext().getGameName()
              , getContext().getContextId()
              , warrior.getWarriorBaseClass().getTitle()
              , warrior.getTitle()
              , warrior.getId()
              , getWarriorBaseClass().getTitle()
              , getTitle()
              , getId()
              , getOwner().getId()
              , ""));
    }
    return warriorResult;
  }
  //===================================================================================================

  public Result<InfluenceResult> attackWarrior(Warrior targetWarrior, String weaponId) {
    // проверим, что это не дружественный воин
    return ifWarriorAlied(targetWarrior, false)
            // Найдем у своего воинаоружие
            .map(fineTargetWarrior -> findWeaponById(weaponId)
                    // вдарим
                    .map(weapon -> weapon.attack(fineTargetWarrior, null)));
  }
  //===================================================================================================

  @Override
//  public Result<List<Result<InfluenceResult>>> castSpell(String spellName, Map<Integer, String> givenSpellParameters) {
  public Result castSpell(String spellName, Map<Integer, String> givenSpellParameters) {
    // проверить есть ли такое заклинание
    return warriorBaseClass.findSpell(spellName)
            // проверить есть ли запас очков применения способностей
            .map(spellInfo -> spellInfo.getAbilityActionPointsCost() > 0 && !attributes.isAbilityActionPointsRestored()
                    // "Воин '%s %s' еще не готов применить способность %s"
                    ? fail(SPELL_FOR_WARRIOR_CAN_NOT_CAST_NOT_ENOUGH_ABILITY_POINTS.getError(
                    warriorBaseClass.getTitle()
                    , title
                    , spellName))
                    : success(spellInfo))
            // проверить есть ли запас магии
            .map(spellInfo -> ((SpellInfo)spellInfo).getMannaCost() > 0 && attributes.getManna() < ((SpellInfo)spellInfo).getMannaCost()
                    // "Для заклинания %s воину '%s %s' не хватает магии."
                    ? fail(SPELL_FOR_WARRIOR_THERE_IS_NOT_ENOUGH_MANNA.getError(
                    spellName
                    , warriorBaseClass.getTitle()
                    , title))
                    : success(spellInfo))
            // проверить есть ли очки действия
            .map(spellInfo -> ((SpellInfo)spellInfo).getActionPointsCost() > 0
                    && (attributes.getActionPoints() - treatedActionPointsForMove) < ((SpellInfo)spellInfo).getActionPointsCost()
                    // "Для заклинания %s воину '%s %s' не хватает магии."
                    ? fail(SPELL_FOR_WARRIOR_THERE_IS_NOT_ENOUGH_ACTION_POINTS.getError(
                    spellName
                    , warriorBaseClass.getTitle()
                    , title))
                    : (Result<SpellInfo>) success(spellInfo))
            .map(spellInfo -> {
              // создадим экземпляр класса заклинания
              Spell<Warrior> spell = beanFactory.getBean(((SpellInfo)spellInfo).getSpellClass(), this);
              return spell.parseParameters(givenSpellParameters)
                      .map(spellParameterMap -> spell.cast(spellParameterMap))
                      .peak(results -> {
                        // прошло успешно
                        // блокировка отката действия
                        lockRollback();
                        // запрет дальнейшего перемещения
                        lockMove();
                        // признак, что в этом ходе юнит задействован
                        setTouchedOnThisTurn(true);
                        // уменьшим магию воина
                        attributes.addManna(-((SpellInfo)spellInfo).getMannaCost());
                        // очки способностей
                        attributes.addAbilityActionPoints(-((SpellInfo)spellInfo).getAbilityActionPointsCost());
                        // очки действия
                        attributes.addActionPoints(-((SpellInfo)spellInfo).getActionPointsCost());
                      });
            });
  }
  //===================================================================================================

  @Override
  public void contrAttackWarrior(InfluenceResult attackResult) {
    // Размер воина (в "пикселях")
    int warriorSize = getContext().getGameRules().getWarriorSize();

    // размер клетки карты
    int mapCellSize = getContext().getLevelMap().getMapCellSize();

    // вычислим расстояние до обидчика. при этом считаем не расстояние между центрами юнитов, а расстояния от края юнита до края
    // юнита. При условии, что они круглые расстояние сократится с расстояния между центрами на два радиуса фигур
    // или, что равнозначно на размер юнита
    int distanceToTarget = calcDistanceTo(attackResult.getActor().getCoords()) - warriorSize;

    AtomicInteger maxPower = new AtomicInteger(-2);
    final Weapon[] selectedWeapon = new Weapon[]{null};

    // соберем все оружие БЛИЖНЕЙ атаки, что есть
    getWeapons().stream()
            // оружие должно наносить урон в ближнем бою
            .filter(weapon -> weapon.isCanDealMelleDamage()
                    // должно хватать очков действия для удара
                    && weapon.getMeleeAttackCost() <= attributes.getActionPoints()
                    // и оно должно достать до обидчика
                    && weapon.getMeleeAttackRange() * mapCellSize >= distanceToTarget)
            // Соберем в set с исключением дубликатов
            .collect(Collectors.toSet()).stream()
            // найдем наиболее сильное оружие из доступного
            .forEach(weapon -> {
              if (maxPower.get() < (weapon.getMeleeMinDamage() << 1 + weapon.getMeleeMaxDamage() << 1) >> 1) {
                selectedWeapon[0] = weapon;
                maxPower.set((weapon.getMeleeMinDamage() << 1 + weapon.getMeleeMaxDamage() << 1) >> 1);
              }
            });

    if (selectedWeapon[0] != null) {
      // признак, что контратака в этот ход была применена
      contrattackUsedAtThisTurn = true;
      // нанесем удар
      selectedWeapon[0].attack(attackResult.getActor(), attackResult);
    }
  }
  //===================================================================================================

  /**
   * ПРобует выполнить блокировку физической атаки
   *
   * @param shield
   * @param attackResult
   * @param modifier
   * @return
   */
  private BlockHitResult innerTryToBlockHit(Weapon shield, InfluenceResult attackResult, Modifier modifier) {
    BlockHitResult result = BlockHitResult.NOT_APPLICABLE;
    if (modifier.isSuccessful()) {
      // его надо пробовать отражать.
      int defenceProbability = shield.getDefenceProbability(attackResult.getAttackerWeapon().getWeaponClass());

      if (defenceProbability > 0) {
        // может отразить в принципе
        // Проверим, что есть очки действия для этого щита
        if (shield.getDefenceCost() <= 0 && shield.getDefenceCost() < attributes.getActionPoints()) {
          // не хватает очков
          result = BlockHitResult.NOT_ENOUGH_ACTION_POINTS;
        } else {
          // можно. вроде
          // спишем очки действия.
          attributes.addActionPoints(-shield.getDefenceCost());

          // попробуем отразить
          if (getContext().getCore().getRandom(0, 101) <= defenceProbability) {
            // удар успешно отражен
            // отправим сообщение
            getContext().fireGameEvent(null, WARRIOR_HIT_WAS_BLOCKED, new EventDataContainer(attackResult, modifier), null);
            // выключим модификатор
            modifier.block();
            // успех
            result = BlockHitResult.SUCCESS;
          } else {
            result = BlockHitResult.FAILED;
          }
        }
      }
    }
    return result;
  }
  //===================================================================================================

  @Override
  public Result<InfluenceResult> defenceWarrior(InfluenceResult attackResult) {
    // процент отражения урона для заданного класса брони
    int armor = getAttributes().getArmorClass().getArmorPercent();

    // соберем все, что может блокировать атаку
    List<Weapon> weaponsForBlock = hands.values().stream()
            // оружие с каждой руки
            .flatMap(warriorSHand -> warriorSHand.getWeapons().stream())
            // только класса щитов
            .filter(weapon -> weapon.getWeaponClass().equals(WeaponClassEnum.SMALL_SHIELD)
                    || weapon.getWeaponClass().equals(WeaponClassEnum.LARGE_SHIELD))
            // свернем в лист и выкинем дубликаты
            .collect(Collectors.toList());

    attackResult.getInfluencers().stream()
            // фильтр по незаблокированным
            .filter(influencer -> influencer.isSuccessful())
            // все модификаторы
            .map(influencer -> influencer.getModifier())
            // физического воздействия
            .filter(modifier -> modifier.getModifierClass().equals(ModifierClass.WEAPON))
            // уменьшим воздействие от физического урона
            .forEach(modifier -> {
              // теперь попробуем отразить удар, если есть возможность
              if (weaponsForBlock.size() > 0) {
                weaponsForBlock.stream()
                        .filter(weapon -> {
                          BlockHitResult blockHitResult = innerTryToBlockHit(weapon, attackResult, modifier);
                          // Будем делать попытки каждым щитом, если попытка предыдущим щитом не увенчалась успехом или на
                          // нее не хватило очков действия
                          return blockHitResult.equals(BlockHitResult.SUCCESS);
                        })
                        .findFirst();
              }

              // броня у воина есть. пробуем уменьшить удар, если он не отражен
              // Проверим можно ли отбить это оружие в принципе
              if (armor > 0 && modifier.isSuccessful() && !attackResult.getAttackerWeapon().isUnrejectable()) {
                int damage = modifier.getLastCalculatedValue() * (100 - armor) / 100;
                // сохраним обратно в модификатор
                modifier.setLastCalculatedValue(damage < 0 ? 0 : damage);
              }
            });

    // соберем все влияния с воина и игрока
//    List<Ability> allDefenceAbilities = new ArrayList<>(20);
//    getWarriorBaseClass().getAbilities().values().stream()
//            .filter(ability -> ability.)

    // пробуем отразить неотраженные воздействия


    return success(attackResult);
  }
  //===================================================================================================

  private GameError generateWeapoNotFoundError(String weaponId) {
    //"В игре %s (id %s) у игрока %s воин '%s %s' (id %s) не имеет оружия с id '%s'"
    return WARRIOR_WEAPON_NOT_FOUND.getError(
            getContext().getGameName()
            , getContext().getContextId()
            , getOwner().getId()
            , getWarriorBaseClass().getTitle()
            , getTitle()
            , getId()
            , weaponId);

  }
  //===================================================================================================

  private GameError generateArtifactNotFoundError(String artifactId) {
    //"В игре %s (id %s) у игрока %s воин '%s %s' (id %s) не имеет оружия с id '%s'"
    return ARTIFACT_NOT_FOUND_BY_WARRIOR.getError(
            getContext().getGameName()
            , getContext().getContextId()
            , getOwner().getId()
            , getWarriorBaseClass().getTitle()
            , getTitle()
            , getId()
            , artifactId);

  }
  //===================================================================================================

  @Override
  public WarriorSBaseAttributes getAttributes() {
    return attributes;
  }
  //===================================================================================================

  /**
   * Применяет все влияния, оказываемые на юнит
   *
   * @param playerPhaseType
   * @return
   */
  private Result<Warrior> applayInfluences(PlayerPhaseType playerPhaseType) {
    // TODO соберем все влияния, что наложены на воина.
    return success(this);

  }
  //===================================================================================================

  /**
   * Восстанавливает значения атрибутов, которые могут быть восстановлены на заданный режим.
   * Например кол-во очков действия для режима хода или защиты, максимальный запас здоровья и прочее
   */
  public void restoreAttributesAvailableForRestoration(PlayerPhaseType playerPhaseType) {
    attributes.setMaxAbilityActionPoints(attributes.getMaxAbilityActionPoints());
    attributes.setActionPoints(playerPhaseType == ATACK_PHASE
            ? attributes.getMaxActionPoints()
            : (attributes.getMaxDefenseActionPoints() + attributes.getActionPoints() / 6));
    attributes.setLuckMeleeAtack(getWarriorBaseClass().getBaseAttributes().getLuckMeleeAtack());
    attributes.setLuckRangeAtack(getWarriorBaseClass().getBaseAttributes().getLuckRangeAtack());
    attributes.setLuckDefense(getWarriorBaseClass().getBaseAttributes().getLuckDefense());
    attributes.setDeltaCostMove(getWarriorBaseClass().getBaseAttributes().getDeltaCostMove());
    attributes.setMannaRejuvenation(getWarriorBaseClass().getBaseAttributes().getMannaRejuvenation());
    // класс брони
    attributes.setArmorClass(
            // все руки
            hands.values().stream()
                    // всё оружие
                    .flatMap(warriorSHand -> warriorSHand.getWeapons().stream())
                    // соберем в кучу, исключая дубликаты
                    .collect(Collectors.toSet()).stream()
                    // отберем то оружие, что имеет класс брони
                    .filter(weapon -> weapon.getArmorClass() != null)
                    // получим класс брони оружия
                    .map(weapon -> weapon.getArmorClass())
                    // суммируем все классы брони, отталкиваясь от класса брони базового класса
                    .reduce(getWarriorBaseClass().getBaseAttributes().getArmorClass(), (resArmor, wepArmor) -> resArmor.add(wepArmor))
    );

    // если это начало хода
    if (playerPhaseType != null && playerPhaseType.equals(ATACK_PHASE)) {
      // регенерация магии
      attributes.addManna(attributes.getMannaRejuvenation());
      // Восстановление очков способностей
      attributes.addAbilityActionPoints(1);

    }
    // движение не заблокировано
    moveLocked = false;
    // на перемещение не использованио ничего
    treatedActionPointsForMove = 0;
    // возврат в начальную координату возможен
    rollbackAvailable = true;
    // начальные координаты
    originalCoords = new Coords(coords);
    // не использовался в этом ходе / защите
    setTouchedOnThisTurn(false);
    // не пользовался контратакой в этом ходе
    contrattackUsedAtThisTurn = false;

    // обновить спосоности
    warriorBaseClass.getAbilities().values().stream().forEach(ability -> ability.revival());

    // восстановить оружие.
    getWeapons().stream().forEach(weapon -> weapon.revival());

    // применить способности класса воина
    InfluenceResult influenceResult = new InfluenceResultImpl(this.getOwner(), this, null, this.getOwner(), this, 0);
    warriorBaseClass.getAbilities().values().stream()
            .filter(ability -> ability.getOwnerTypeForAbility().equals(OwnerTypeEnum.WARRIOR))
            .forEach(ability -> ability.buildForTarget(this).stream()
                    .forEach(influencer -> influencer.applyToWarrior(influenceResult)));

    // применить влияния артефактов с учетом фаза атака/защита
    artifacts.values().stream()
            .forEach(artifact -> artifact.applyToOwner(playerPhaseType));

    // применить влияния оружия. Те, которые направлены на воина-владельца оружия
    getWeapons().stream().forEach(weapon -> weapon.getAbilities().stream()
            .filter(ability -> ability.getTargetType().equals(TargetTypeEnum.THIS_WARRIOR)
                    && ability.getActivePhase().contains(playerPhaseType))
            .forEach(ability -> ability.buildForTarget(this).stream()
                    .forEach(influencer -> influencer.applyToWarrior(InfluenceResultImpl.forPositive(this))
                    )
            )
    );

    //Применения артефактов игрока
    getOwner().getArtifacts()
            // переберем все артефакты
            .peak(playerArtifacts -> playerArtifacts.stream()
                    // все способности артефакта
                    .forEach(playerArtifact -> playerArtifact.getAbilities().stream()
                            // направленные на свои юниты
                            .filter(ability -> ability.getTargetType().equals(TargetTypeEnum.ALLIED_WARRIOR)
                                    // фильтр по фазе
                                    && ability.getActivePhase().contains(playerPhaseType))
                            // сформировать влияния для воина
                            .forEach(fineAbility -> fineAbility.buildForTarget(this).stream()
                                    // применить каждое из влияний
                                    .forEach(influencer -> influencer.applyToWarrior(InfluenceResultImpl.forPositive(this)))))
            );
  }
  //===================================================================================================

  /**
   * Плдготовить воина к одной из двух фаз
   *
   * @param playerPhaseType
   */
  private Result<Warrior> prepareToPhase(PlayerPhaseType playerPhaseType) {
    restoreAttributesAvailableForRestoration(playerPhaseType);
    warriorPhase = playerPhaseType;
    return applayInfluences(playerPhaseType)
            .peak(warrior -> warrior.getOwner().findContext()
                    .peak(context -> getContext().fireGameEvent(null
                            , playerPhaseType == PlayerPhaseType.DEFENSE_PHASE ? WARRIOR_PREPARED_TO_DEFENCE : WARRIOR_PREPARED_TO_ATTACK
                            , new EventDataContainer(this), null)));
  }
  //===================================================================================================

  @Override
  public Result<Warrior> prepareToDefensePhase() {
    return prepareToPhase(PlayerPhaseType.DEFENSE_PHASE);
  }
  //===================================================================================================

  @Override
  public Result<Warrior> prepareToAttackPhase() {
    return prepareToPhase(ATACK_PHASE);
  }
  //===================================================================================================

  @Override
  public Result<Influencer> addInfluenceToWarrior(Modifier modifier, Owner source, LifeTimeUnit lifeTimeUnit, int lifeTime) {
    Influencer influencer = new InfluencerImpl(this, source, lifeTimeUnit, lifeTime, modifier);
    influencer.attachToOwner(influencer.getOwner());
    influencers.put(influencer.getId(), influencer);
    getContext().fireGameEvent(null, WARRIOR_INFLUENCER_ADDED
            , new EventDataContainer(influencer, this), null);
    return success(influencer);
  }
  //===================================================================================================

  @Override
  public Result<Influencer> addInfluenceToWarrior(Influencer influencer) {
    influencer.attachToOwner(this);
    influencers.put(influencer.getId(), influencer);
    getContext().fireGameEvent(null, WARRIOR_INFLUENCER_ADDED
            , new EventDataContainer(influencer, this), null);
    return success(influencer);
  }
  //===================================================================================================

  @Override
  public Result<List<Influencer>> getWarriorSInfluencers() {
    return success(new ArrayList<>(influencers.values()));
  }
  //===================================================================================================

  /**
   * Удаление влияния
   *
   * @param influencer
   * @param silent     не отправлять уведомления о действии
   */
  void innerRemoveInfluencerFromWarrior(Influencer influencer, boolean silent) {
    influencers.remove(influencer.getId());
    if (!silent) {
      getContext().fireGameEvent(null, WARRIOR_INFLUENCER_REMOVED
              , new EventDataContainer(influencer, this), null);
    }
  }
  //===================================================================================================

  @Override
  public Result<Influencer> removeInfluencerFromWarrior(Influencer influencer, boolean silent) {
    influencer.removeFromWarrior(silent);
    innerRemoveInfluencerFromWarrior(influencer, silent);
    return success(influencer);
  }
  //===================================================================================================

  public int getWarriorSActionPoints(boolean forMove) {
    return forMove  // для перемещения (либо у юнита остатки после атаки и прочего, либо он свежак)
            // он свежак. Им еще не действовали в этом ходу
            || !isMoveLocked()
            // им действовали но есть возможности откатиться
            || isRollbackAvailable()
            // для движения
            ? attributes.getActionPoints()
            // для нанесения атаки
            : (attributes.getActionPoints() - treatedActionPointsForMove);
  }
  //===================================================================================================

  @Override
  public Coords getOriginalCoords() {
    return originalCoords;
  }
  //===================================================================================================

  @Override
  public int getWarriorSMoveCost() {
    return getAttributes().getArmorClass().getMoveCost()
            + getAttributes().getDeltaCostMove();
  }
  //===================================================================================================

  @Override
  public boolean isMoveLocked() {
    return moveLocked;
  }
  //===================================================================================================

  @Override
  public void lockMove() {
    this.moveLocked = true;
  }
  //===================================================================================================

  @Override
  public void lockRollback() {
    this.rollbackAvailable = false;
    // спишем очки за еремещение
    attributes.addActionPoints(-treatedActionPointsForMove);
    treatedActionPointsForMove = 0;
    this.originalCoords = new Coords(coords);
  }
  //===================================================================================================

  @Override
  public Result<Warrior> rollbackMove() {
    Result<Warrior> result;
    // если откат не заблокирован
    if (isRollbackAvailable()) {
      coords = new Coords(originalCoords);
      // снять признак задействованности в данном ходе
      setTouchedOnThisTurn(false);
      // обнулить использованные очки
      setTreatedActionPointsForMove(0);

      result = success(this);
      // уведомление
      getContext().fireGameEvent(null
              , WARRIOR_MOVE_ROLLEDBACK
              , new EventDataContainer(this, result)
              , null);
    } else {
      // В игре %s (id %s) игрок %s не может откатить перемещение воина '%s %s' (id %s) так как откат
      // заблокирован последующими действиями
      result = fail(WARRIOR_CAN_T_ROLLBACK_MOVE.getError(
              getContext().getGameName()
              , getContext().getContextId()
              , getOwner().getId()
              , getWarriorBaseClass().getTitle()
              , getTitle()
              , getId()));
    }
    return result;
  }
  //===================================================================================================

  @Override
  public boolean isTouchedAtThisTurn() {
    return touchedAtThisTurn;
  }
  //===================================================================================================

  @Override
  public boolean isContrattackUsedAtThisTurn() {
    return contrattackUsedAtThisTurn;
  }
  //===================================================================================================

  @Override
  public Warrior setTouchedOnThisTurn(boolean touchedAtThisTurn) {
    this.touchedAtThisTurn = touchedAtThisTurn;
    return this;
  }
  //===================================================================================================

  @Override
  public boolean isRollbackAvailable() {
    return rollbackAvailable;
  }
  //===================================================================================================

  @Override
  public int getTreatedActionPointsForMove() {
    return treatedActionPointsForMove;
  }
  //===================================================================================================

  @Override
  public Warrior setTitle(String title) {
    this.title = title;
    return this;
  }
  //===================================================================================================

  @Override
  public Map<String, Class<? extends Ability>> getUnsupportedAbilities() {
    return new HashMap<>(unsupportedAbilities);
  }
  //===================================================================================================

  @Override
  public void setTreatedActionPointsForMove(int treatedActionPointsForMove) {
    this.treatedActionPointsForMove = treatedActionPointsForMove;
  }
  //===================================================================================================

  @Override
  public Result<Warrior> die() {
    // удалим все влияния
    Optional<Result<Influencer>> removeResult = influencers.values().stream()
            .map(influencer -> influencer.removeFromWarrior(true))
            .filter(influencerResult -> influencerResult.isFail())
            .findFirst();

    dead.set(true);
    // Если не было ошибок, то все хорошо
    return removeResult.isPresent()
            ? fail(removeResult.get().getError())
            : success(this);
  }
  //===================================================================================================

  @Override
  public boolean isDead() {
    return dead.get();
  }
  //===================================================================================================

  @Override
  public String serializeEquipmentToJson() {
    AtomicInteger weaponCounter= new AtomicInteger(-1);

    WarriorShortDto dto = new WarriorShortDto()
            .setTitle(title)
            .setDatabaseId(databaseId)
            .setWarriorBaseClass(warriorBaseClass.getTitle())
            .setArtifacts(artifacts.values().stream().map(artifact -> ArtifactDto.fromEntity(artifact)).collect(Collectors.toSet()))
            .setWeapons(getWeapons().stream().map(weapon -> WeaponShortDto.fromEntity(weapon)).collect(Collectors.toSet()));

    return new GsonBuilder().create().toJson(dto);
  }
  //===================================================================================================

  @Override
  public WarriorBaseClassInfo getWarriorBaseClassInfo() {
    return warriorBaseClass.getWarriorBaseClassInfo();
  }
  //===================================================================================================

  @Override
  public PlayerPhaseType getWarriorPhase() {
    return warriorPhase;
  }
  //===================================================================================================
}
