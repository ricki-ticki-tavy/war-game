package core.entity.warrior.base;

import api.core.Context;
import api.core.Event;
import api.core.Owner;
import api.core.Result;
import api.entity.ability.Influencer;
import api.entity.ability.Modifier;
import api.entity.warrior.Warrior;
import api.enums.EventType;
import api.enums.LifeTimeUnit;
import api.game.action.InfluenceResult;
import api.game.map.Player;
import core.entity.abstracts.AbstractOwnerImpl;
import core.entity.player.PlayerImpl;
import core.game.action.InfluenceResultImpl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import static core.system.ResultImpl.success;
import static core.system.error.GameErrors.SYSTEM_OBJECT_ALREADY_INITIALIZED;

public class InfluencerImpl extends AbstractOwnerImpl implements Influencer {
  private LifeTimeUnit lifeTimeUnit;
  private int lifeTime;
  private Warrior targetWarrior;
  private final Modifier modifier;
  private String id = UUID.randomUUID().toString();
  private String consumerId = null;
  private final List<Influencer> children;
  //===================================================================================================
  //===================================================================================================

  @Override
  public Influencer attachToOwner(Owner owner) {
    if (this.owner != null && owner != this.owner) {
      SYSTEM_OBJECT_ALREADY_INITIALIZED.getError(getTitle() + "  " + getId(), "owner");
    }
    this.owner = owner;
    if (lifeTimeUnit.getEventType().equals(EventType.ROUND_FULL)
            || lifeTimeUnit.getEventType().equals(EventType.PLAYER_TAKES_TURN)
            || lifeTimeUnit.getEventType().equals(EventType.PLAYER_LOOSE_TURN)) {
      consumerId = getContext().subscribeEvent(this::onTimeEvent, lifeTimeUnit.getEventType());
    }
    return this;
  }
  //===================================================================================================

  /**
   * @param targetWarrior Тот, на кого это влияние направлено
   * @param owner         кто является источникомвлияния. Кто его наложил.
   * @param lifeTimeUnit  единицы, в которых измеряется время жизни влияния
   * @param lifeTime      время жизни влияния
   * @param modifier      модификатор влияния
   */
  public InfluencerImpl(Warrior targetWarrior, Owner owner, LifeTimeUnit lifeTimeUnit, int lifeTime, Modifier modifier) {
    super(owner, null, null, "", "");
    this.modifier = modifier;
    this.lifeTimeUnit = lifeTimeUnit;
    this.lifeTime = lifeTime;
    this.targetWarrior = targetWarrior;
    this.children = new ArrayList<>(10);
    this.owner = owner;

//    attachToOwner(owner);
  }
  //===================================================================================================

  @Override
  public Context getContext() {
    return owner.getContext();
  }
  //===================================================================================================

  @Override
  public String getId() {
    return id;
  }
  //===================================================================================================

  @Override
  public String getTitle() {
    return modifier.getTitle();
  }
  //===================================================================================================

  @Override
  public String getDescription() {
    return modifier.getDescription();
  }
  //===================================================================================================

  /**
   * срабатывает когда наступает событие, по которому измеряется время жизни
   *
   * @param event
   */
  private void onTimeEvent(Event event) {
    // для событий потери или приобретения хода - сравноваем игрока
    if (((event.getEventType().equals(EventType.PLAYER_TAKES_TURN)
            || event.getEventType().equals(EventType.PLAYER_LOOSE_TURN)) && owner.getOwner() == event.getSource(Player.class))
            // или игровой круг
            || event.getEventType().equals(EventType.ROUND_FULL)) {
      // событие пришло от нашего источника
      // применим его действие
      InfluenceResult influenceResult = new InfluenceResultImpl(
              targetWarrior.getOwner()
              , targetWarrior
              , null
              , targetWarrior.getOwner()
              , targetWarrior
              , 0);
      // сгенерируем влияния для воздействия

      innerApplyToWarrior(influenceResult, LifeTimeUnit.JUST_NOW);
      // проверим остался ли воин еще жив
      Result<Boolean> dieResult = ((PlayerImpl)targetWarrior.getOwner()).checkForWarriorHasDie(influenceResult);

      if (--lifeTime <= 0) {
        // пора удаляться из списка влияния
        targetWarrior.removeInfluencerFromWarrior(this, false);
      }
    }
  }
  //===================================================================================================

  @Override
  public Warrior getTargetWarrior() {
    return targetWarrior;
  }
  //===================================================================================================

  private void unsubscribe() {
    if (consumerId != null) {
      getContext().unsubscribeEvent(consumerId, lifeTimeUnit.getEventType());
      consumerId = null;
    }
  }
  //===================================================================================================

  @Override
  public Result<Influencer> removeFromWarrior(boolean silent) {
    unsubscribe();
    ((WarriorImpl) targetWarrior).innerRemoveInfluencerFromWarrior(this, silent);
    return success(this);
  }
  //===================================================================================================

  @Override
  public Influencer addChild(Influencer influencer) {
    this.children.add(influencer);
    return this;
  }
  //===================================================================================================

  @Override
  public Influencer addChildren(Collection<Influencer> influencers) {
    this.children.addAll(influencers);
    return this;
  }
  //===================================================================================================

  @Override
  public List<Influencer> getChildren() {
    return new ArrayList(children);
  }
  //===================================================================================================

  @Override
  public Modifier getModifier() {
    return modifier;
  }
  //===================================================================================================

  @Override
  public boolean isSuccessful() {
    return modifier.isSuccessful();
  }
  //===================================================================================================

  private Result<Modifier> applayFutureToWarrior(InfluenceResult influenceResult) {
    return success(influenceResult.getTarget().addInfluenceToWarrior(
            modifier
            , influenceResult.getTarget()
            , lifeTimeUnit
            , lifeTime)
            .getResult().getModifier());
  }
  //===================================================================================================

  /**
   * Внутренный метод, позволяющий при обработке вместо текущего LifeTimeUnit использовать другой
   *
   * @param influenceResult
   * @param replaceLifeTimeUnit   - для длительно дейстыующих влияний в этот параметр передается JUST_NOW, когда они
   *                              должны оказать непосредственное воздействие , а не закрепиться за целью
   * @return
   */
  protected Result<Warrior> innerApplyToWarrior(InfluenceResult influenceResult, LifeTimeUnit replaceLifeTimeUnit) {
    // Уже всерассчитано. Применяем значение, рассчитанное заранее (getLastCalculatedValue())
    Result<Warrior> result;
    if (replaceLifeTimeUnit!= null){
      // сгенерировать новое значение
      modifier.getValue();
    }
    LifeTimeUnit currenLifeTimeUnit = replaceLifeTimeUnit == null
            ? lifeTimeUnit
            : replaceLifeTimeUnit;
    if (isSuccessful()) {
      result = (currenLifeTimeUnit.equals(LifeTimeUnit.JUST_NOW)
              ? modifier.applyModifier(influenceResult)
              : applayFutureToWarrior(influenceResult))
              .map(fineModifier -> {
                children.stream()
                        .forEach(child -> ((InfluencerImpl)child).innerApplyToWarrior(influenceResult, replaceLifeTimeUnit));
                return success(targetWarrior);
              });

    } else {
      result = success(targetWarrior);
    }
    return result;
  }
  //===================================================================================================

  @Override
  public Result<Warrior> applyToWarrior(InfluenceResult influenceResult) {
    return innerApplyToWarrior(influenceResult, null);
  }
  //===================================================================================================

  @Override
  public LifeTimeUnit getLifeTimeUnit() {
    return lifeTimeUnit;
  }
  //===================================================================================================

  @Override
  public int getLifeTime() {
    return lifeTime;
  }
  //===================================================================================================

}
