package core.entity.weapon;

import api.entity.weapon.WeaponClassInfo;
import api.enums.WeaponClassEnum;
import core.entity.weapon.base.WeaponClassInfoImpl;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.UUID;

import static core.entity.ModelsGraphicInfo.SMALL_AXE;

/**
 * топор
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Axe extends AbstractWeaponImpl {

  public static final WeaponClassInfo classInfo = new WeaponClassInfoImpl("Топор"
          , null
          , "Топор. Ближний и дальний бой "
          , Axe.class
          , 10
          , 0);

  private final String OUID = "WepAxe_" + UUID.randomUUID().toString();

  public Axe() {
    super();
    this.id = OUID;
    this.title = classInfo.getName();
    this.description = classInfo.getDescription();
    this.meleeMinDamage = 100;
    this.meleeMaxDamage = 500;
    this.meleeAttackCost = 40;
    this.unrejectable = false;
    this.useCountPerRound = -1;
    this.totalRangedUseCount = -1;
    this.dealsRangedDamage = false;
    this.dealsMeleeDamage = true;
    this.meleeAttackRange = 2;
    this.neededHandsCountToTakeWeapon = 1;
    this.graphicsInfo = SMALL_AXE;

  }

  @Override
  public WeaponClassEnum getWeaponClass() {
    return WeaponClassEnum.MELEE;
  }

  @Override
  public WeaponClassInfo getWeaponClassInfo() {
    return classInfo;
  }
}
