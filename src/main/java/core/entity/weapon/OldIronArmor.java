package core.entity.weapon;

import api.entity.weapon.WeaponClassInfo;
import api.enums.WeaponClassEnum;
import core.entity.weapon.base.WeaponClassInfoImpl;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.UUID;

import static core.entity.ModelsGraphicInfo.OLD_IRON_ARMOR;

/**
 *
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class OldIronArmor extends AbstractWeaponImpl {

  public static final WeaponClassInfo classInfo = null;//new WeaponClassInfoImpl();

  public static final String CLASS_NAME = "Старая броня";
  private final String OUID = "WepArm_" + UUID.randomUUID().toString();

  public OldIronArmor() {
    super();
    this.id = OUID;
    this.title = CLASS_NAME;
    this.description = "Старая броня";
    this.meleeMinDamage = 0;
    this.meleeMaxDamage = 0;
    this.meleeAttackCost = 0;
    this.unrejectable = false;
    this.useCountPerRound = -1;
    this.totalRangedUseCount = -1;
    this.dealsRangedDamage = false;
    this.dealsMeleeDamage = false;
    this.neededHandsCountToTakeWeapon = 0;
    this.graphicsInfo = OLD_IRON_ARMOR;
  }

  @Override
  public WeaponClassEnum getWeaponClass() {
    return WeaponClassEnum.MELEE;
  }

  @Override
  public WeaponClassInfo getWeaponClassInfo() {
    return classInfo;
  }

}
