package core.entity.weapon;

import api.entity.weapon.WeaponClassInfo;
import api.enums.WeaponClassEnum;
import core.entity.weapon.base.WeaponClassInfoImpl;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.UUID;

import static core.entity.ModelsGraphicInfo.SHORT_BOW;

/**
 * Обычный меч
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Bow extends AbstractWeaponImpl {

  public static final WeaponClassInfo classInfo = new WeaponClassInfoImpl("Простой лук"
          , "острие лука"
          , "Простой лук. Двуручное оружие. При ближнем бое работает как кинжал"
          , Bow.class
          , 15
          , 0);

  private final String OUID = "WepBow_" + UUID.randomUUID().toString();

  public Bow() {
    super();
    this.id = OUID;
    this.title = classInfo.getName();
    this.description = classInfo.getDescription();
    this.meleeMinDamage = 100;
    this.meleeMaxDamage = 300;
    this.rangedMinDamage = 100;
    this.rangedMaxDamage = 700;
    this.meleeAttackCost = 40;
    this.rangedAttackCost = 120;
    this.unrejectable = false;
    this.useCountPerRound = -1;
    this.totalRangedUseCount = -1;
    this.dealsRangedDamage = true;
    this.dealsMeleeDamage = true;
    this.minRangedAttackRange = 2;
    this.maxRangedAttackRange = 40;
    this.meleeAttackRange = 2;
    this.fadeRangeStart = 20;
    this.fadeDamagePercentPerLength = 3;
    this.neededHandsCountToTakeWeapon = 2;
    this.secondWeaponName = classInfo.getSecondName();
    this.graphicsInfo = SHORT_BOW;
  }

  @Override
  public WeaponClassEnum getWeaponClass() {
    return WeaponClassEnum.SMALL_RANGED;
  }

  @Override
  public WeaponClassInfo getWeaponClassInfo() {
    return classInfo;
  }

}
