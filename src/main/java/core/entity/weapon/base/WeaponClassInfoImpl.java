package core.entity.weapon.base;

import api.entity.weapon.Weapon;
import api.entity.weapon.WeaponClassInfo;

public class WeaponClassInfoImpl implements WeaponClassInfo {
  private String name;
  private String secondName;
  private String description;
  private int goldCost;
  private int owningGoldCost;
  private Class<? extends Weapon> weaponClass;

  public WeaponClassInfoImpl(String name
          , String secondName
          , String description
          , Class<? extends Weapon> weaponClass
          , int goldCost
          , int owningGoldCost) {
    this.name = name;
    this.secondName = secondName;
    this.description = description;
    this.weaponClass = weaponClass;
    this.goldCost = goldCost;
    this.owningGoldCost = owningGoldCost;
  }

  @Override
  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  @Override
  public int getGoldCost() {
    return goldCost;
  }

  @Override
  public int getOwningGoldCost() {
    return owningGoldCost;
  }

  @Override
  public Class<? extends Weapon> getWeaponClass() {
    return weaponClass;
  }

  @Override
  public String getSecondName() {
    return secondName;
  }
}
