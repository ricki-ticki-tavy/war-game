package core.entity.weapon;

import api.entity.weapon.WeaponClassInfo;
import api.enums.WeaponClassEnum;
import core.entity.weapon.base.WeaponClassInfoImpl;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.UUID;

import static core.entity.ModelsGraphicInfo.SWORD;

/**
 * Обычный меч
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Sword extends AbstractWeaponImpl {

  public static final WeaponClassInfo classInfo = new WeaponClassInfoImpl("Простой меч"
          , null
          , "Простой меч. Ближний бой "
          , Sword.class
          , 10
          , 0
  );

  private final String OUID = "WepSwd_" + UUID.randomUUID().toString();

  public Sword() {
    super();
    this.id = OUID;
    this.title = classInfo.getName();
    this.description = classInfo.getDescription();
    this.meleeMinDamage = 100;
    this.meleeMaxDamage = 500;
    this.meleeAttackCost = 40;
    this.unrejectable = false;
    this.useCountPerRound = -1;
    this.totalRangedUseCount = -1;
    this.dealsRangedDamage = false;
    this.dealsMeleeDamage = true;
    this.meleeAttackRange = 2;
    this.neededHandsCountToTakeWeapon = 1;
    this.graphicsInfo = SWORD;

  }

  @Override
  public WeaponClassEnum getWeaponClass() {
    return WeaponClassEnum.MELEE;
  }

  @Override
  public WeaponClassInfo getWeaponClassInfo() {
    return classInfo;
  }

}
