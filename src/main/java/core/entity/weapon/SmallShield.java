package core.entity.weapon;

import api.entity.weapon.WeaponClassInfo;
import api.enums.ArmorClassEnum;
import api.enums.WeaponClassEnum;
import core.entity.weapon.base.WeaponClassInfoImpl;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.UUID;

import static core.entity.ModelsGraphicInfo.SMALL_SHIELD;

/**
 * маленький щит
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SmallShield extends AbstractWeaponImpl {

  public static final WeaponClassInfo classInfo = new WeaponClassInfoImpl("Маленький круглый щит"
          , null
          , "Маленький круглый щит"
          , SmallShield.class
          , 7
          , 0);

  private final String OUID = "WepShield_" + UUID.randomUUID().toString();

  public SmallShield() {
    super();
    this.id = OUID;
    this.title = classInfo.getName();
    this.description = classInfo.getDescription();
    this.unrejectable = false;
    this.useCountPerRound = -1;
    this.totalRangedUseCount = -1;
    this.dealsRangedDamage = false;
    this.dealsMeleeDamage = true;
    this.meleeAttackRange = 1;
    this.neededHandsCountToTakeWeapon = 1;
    this.armorClass = ArmorClassEnum.ARMOR_1;
    this.defenceCost = 20;
    this.graphicsInfo = SMALL_SHIELD;
  }

  @Override
  public WeaponClassEnum getWeaponClass() {
    return WeaponClassEnum.SMALL_SHIELD;
  }

  @Override
  public int getDefenceProbability(WeaponClassEnum weaponClass) {
    switch (weaponClass) {
      case SMALL_SHIELD:
        return 45;
      case SHORT_MELEE:
      case MELEE:
        return 35;
      case LONG_MELEE:
        return 10;
      case LARGE_SHIELD:
        return 26;
      case HAVY_MELEE:
        return 5;
      default:
        return 0;
    }
  }

  @Override
  public WeaponClassInfo getWeaponClassInfo() {
    return classInfo;
  }

}
