package core.entity.magic;

import api.entity.magic.SpellInfo;
import api.entity.magic.SpellParameter;
import api.enums.SignOfInfluenceEnum;
import api.enums.SpellParameterEnum;
import api.game.map.Player;
import core.entity.ability.lighting.AbilityLightingForSpell;
import core.entity.magic.base.AbstractSpellImpl;
import core.entity.magic.base.SpellInfoImpl;
import core.entity.magic.base.SpellParameterImpl;
import javafx.util.Pair;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Заклинание молнии
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SpellLighting extends AbstractSpellImpl<Player> {

  public static final SpellInfo spellinfo = new SpellInfoImpl(
          "Молния"
          , "Наносит уром молнией воину"
          , SignOfInfluenceEnum.NEGATIVE
          , 0
          , 0
          , 10
          , SpellLighting.class
  , Arrays.stream(new SpellParameter[]{
          SpellParameterImpl.spellParameter(
                  "Выберете вражеского воина"
                  , SpellParameterEnum.ENEMY_WARRIOR
                  , null)
  }).collect(Collectors.toList()));

  @Autowired
  BeanFactory beanFactory;

  @PostConstruct
  public void init() {
    this.abilities.put(AbilityLightingForSpell.UNIT_NAME, beanFactory.getBean(AbilityLightingForSpell.class, this, 10, 800));
  }

  public SpellLighting(Player owner) {
    super(owner, spellinfo.getSpellSign(), spellinfo.getMannaCost(), "SpellLgt_", spellinfo.getName(), spellinfo.getDescription());
    this.requiredParams.putAll(spellinfo.getRequiredAdditionalParams());
  }
}

