package core.entity.magic.base;

import api.core.Result;
import api.entity.magic.Spell;
import api.entity.magic.SpellParameter;
import api.entity.warrior.Warrior;
import api.enums.OwnerTypeEnum;
import api.enums.SpellParameterEnum;
import api.game.map.Player;
import api.geo.Coords;
import core.system.error.GameErrors;
import javafx.util.Pair;

import static core.system.ResultImpl.fail;
import static core.system.ResultImpl.success;

/**
 * Реализация параметра заклинания
 */
public class SpellParameterImpl implements SpellParameter {

  private static final String ERROR_START_MESSAGE = "Параметр имеет тип ";
  private String title;
  private SpellParameterEnum parameterType;
  private Integer intValue = null;
  private Warrior warriorValue = null;
  private Player playerValue = null;
  private Coords coordsValue;
  private Pair<Integer, Integer> integerRestrictions = null;
  private Pair<Coords, Integer> coordsRestrictions = null;

  @Override
  public String getTitle() {
    return title;
  }
  //===================================================================================

  @Override
  public SpellParameterEnum getType() {
    return parameterType;
  }
  //===================================================================================

  @Override
  public Pair<Integer, Integer> getIntegerRestrictions() {
    return integerRestrictions == null ? null : new Pair<>(integerRestrictions.getKey(), integerRestrictions.getValue());
  }
  //===================================================================================

  @Override
  public Pair<Coords, Integer> getCoordsRestrictions() {
    return coordsRestrictions == null
            ? null
            : new Pair<>(
            coordsRestrictions.getKey() != null
                    ? new Coords(coordsRestrictions.getKey())
                    : null
            , coordsRestrictions.getValue());
  }
  //===================================================================================

  @Override
  public SpellParameter setIntegerValue(Integer value) {
    if (!parameterType.equals(SpellParameterEnum.INTEGER)) {
      // "Недопустимое значение параметра %s (%s). %s
      throw GameErrors.SYSTEM_BAD_PARAMETER.getError(title, String.valueOf(value)
              , ERROR_START_MESSAGE + parameterType.getTitle());
    }
    this.intValue = value;
    return this;
  }
  //===================================================================================

  @Override
  public Integer getIntegerValue() {
    return intValue;
  }
  //===================================================================================

  @Override
  public SpellParameter setWarriorValue(Warrior warrior) {
    if (!parameterType.equals(SpellParameterEnum.ALLIED_WARRIOR)
            && !parameterType.equals(SpellParameterEnum.ENEMY_WARRIOR)) {
      // "Недопустимое значение параметра %s (%s). %s
      throw GameErrors.SYSTEM_BAD_PARAMETER.getError(title
              , warrior.getWarriorBaseClass().getTitle() + " " + warrior.getTitle()
              , ERROR_START_MESSAGE + parameterType.getTitle());
    }
    this.warriorValue = warrior;
    return this;
  }
  //===================================================================================

  @Override
  public Warrior getWarriorValue() {
    return warriorValue;
  }
  //===================================================================================

  @Override
  public SpellParameter setPlayerValue(Player player) {
    if (!parameterType.equals(SpellParameterEnum.PLAYER)) {
      // "Недопустимое значение параметра %s (%s). %s
      throw GameErrors.SYSTEM_BAD_PARAMETER.getError(title, player.getId(), ERROR_START_MESSAGE + parameterType.getTitle());
    }
    this.playerValue = player;
    return this;
  }
  //===================================================================================

  @Override
  public Player getPlayerValue() {
    return playerValue;
  }
  //===================================================================================

  @Override
  public Coords getCoordsValue() {
    return coordsValue;
  }
  //===================================================================================

  @Override
  public SpellParameter setCoordsValue(Coords coords) {
    if (parameterType.equals(SpellParameterEnum.INTEGER)) {
      // "Недопустимое значение параметра %s (%s). %s
      throw GameErrors.SYSTEM_BAD_PARAMETER.getError(title, coords.toString()
              , ERROR_START_MESSAGE + parameterType.getTitle());
    }
    this.coordsValue = coords;
    return this;
  }
  //===================================================================================

  private SpellParameterImpl(String title, SpellParameterEnum parameterType) {
    this.title = title;
    this.parameterType = parameterType;
  }
  //===================================================================================

  private SpellParameter setIntegerLimitations(Pair<Integer, Integer> integerLimitations) {
    if (integerLimitations == null) {
      this.integerRestrictions = integerLimitations;
    } else {
      this.integerRestrictions = new Pair<>(integerLimitations.getKey(), integerLimitations.getValue());
    }
    return this;
  }
  //===================================================================================

  private SpellParameter setCoordsLimitations(Pair<Coords, Integer> coordsLimitations) {
    if (coordsLimitations == null) {
      this.coordsRestrictions = coordsLimitations;
    } else {
      this.coordsRestrictions = new Pair<>(coordsLimitations.getKey(), coordsLimitations.getValue());
    }
    return this;
  }
  //===================================================================================

  public static SpellParameter spellParameter(String name
          , SpellParameterEnum parameterType
          , int defaultIntegerValue
          , Pair<Integer, Integer> integerLimitations) {
    return new SpellParameterImpl(name, parameterType)
            .setIntegerLimitations(integerLimitations)
            .setIntegerValue(defaultIntegerValue);
  }
  //===================================================================================

  public static SpellParameter spellParameter(String name
          , SpellParameterEnum parameterType
          , Pair<Coords, Integer> coordsLimitations) {
    return new SpellParameterImpl(name, parameterType)
            .setCoordsLimitations(coordsLimitations);
  }
  //===================================================================================

  @Override
  public SpellParameter clone() {
    SpellParameterImpl clon = new SpellParameterImpl(title, parameterType);
    clon.intValue = this.intValue;
    clon.warriorValue = this.warriorValue;
    clon.playerValue = this.playerValue;

    clon.integerRestrictions = integerRestrictions != null
            ? new Pair<>(this.integerRestrictions.getKey(), this.integerRestrictions.getValue())
            : null;
    clon.coordsRestrictions = coordsRestrictions != null
            ? new Pair<>(
            this.coordsRestrictions.getKey() == null
                    ? null
                    : new Coords(this.coordsRestrictions.getKey())
            , this.coordsRestrictions.getValue())
            : null;

    return clon;
  }
  //===================================================================================

  private void checkAndPrepareCoordsLimitations(Spell theSpell) {
    // если есть ограничения координат
    if (getCoordsRestrictions() != null
            // и центр ограничивающей окружности не задан
            && coordsRestrictions.getKey() == null
            // и владелец заклинания воин
            && theSpell.getThisOwnerType().equals(OwnerTypeEnum.WARRIOR)) {
      // зададим координаты текущей позицией воина. Фактическими, а не приведенными
      coordsRestrictions = new Pair<>(((Warrior) theSpell.getOwner()).getCoords(), coordsRestrictions.getValue());
    }
  }
  //===================================================================================

  @Override
  public Result<SpellParameter> fillFromString(Spell theSpell, String givenParameter) {
    Result result = null;

    switch (getType()) {
      case PLAYER:
        // параметр - игрок. Это должен быть не сам я
        result = theSpell.getContext().findUserByName(givenParameter)
                .peak(player -> setPlayerValue(player));
        break;
      case ENEMY_WARRIOR:
        // параметр - вражеский воин.
        result = theSpell.getContext().findWarriorById(givenParameter)
                .map(warrior -> {
                  setWarriorValue(warrior);
                  Player castOwner = theSpell.getThisOwnerType().equals(OwnerTypeEnum.WARRIOR)
                          ? (Player) theSpell.getOwner().getOwner()
                          : (Player) theSpell.getOwner();

                  // проверка центра координат ограничивающей окружности для заклинания воина
                  checkAndPrepareCoordsLimitations(theSpell);

                  return warrior.getOwner() == castOwner
                          //  "в игре %s игрок %s пробовал применить заклинание '%s', но цель '%s %s' собственный воин."
                          ? fail(GameErrors.SPELL_TARGET_IS_ALLIED.getError(
                          theSpell.getContext().getGameName()
                          , theSpell.getOwner().getId()
                          , theSpell.getTitle()
                          , warrior.getWarriorBaseClass().getTitle()
                          , warrior.getTitle()))

                          : (getCoordsRestrictions() == null
                          // или расстояние меньше или равно разрешенному
                          || theSpell.getContext().calcDistance(getCoordsRestrictions().getKey(), warrior.getCoords()) <= getCoordsRestrictions().getValue()
                          ? success(true)

                          // "в игре %s  %s пробовал применить заклинание '%s' но цель '%s %s' выходят за границы."
                          : fail(GameErrors.SPELL_TARGET_IS_OUT_OF_RANGE.getError(
                          theSpell.getContext().getGameName()
                          , theSpell.getOwner().getId()
                          , theSpell.getTitle()
                          , warrior.getWarriorBaseClass().getTitle()
                          , warrior.getTitle())));
                });
        break;
      case ALLIED_WARRIOR:
        // параметр - свой воин.
        result = theSpell.getContext().findWarriorById(givenParameter)
                .map(warrior -> {
                  setWarriorValue(warrior);

                  Player castOwner = theSpell.getThisOwnerType().equals(OwnerTypeEnum.WARRIOR)
                          ? (Player) theSpell.getOwner().getOwner()
                          : (Player) theSpell.getOwner();

                  return warrior.getOwner() != castOwner
                          ? fail(GameErrors.SPELL_TARGET_IS_NOT_ALLIED.getError(
                          theSpell.getContext().getGameName()
                          , theSpell.getOwner().getId()
                          , theSpell.getTitle()
                          , warrior.getWarriorBaseClass().getTitle()
                          , warrior.getTitle()))

                          : (getCoordsRestrictions() == null
                          // или расстояние меньше или равно разрешенному
                          || theSpell.getContext().calcDistance(getCoordsRestrictions().getKey(), warrior.getCoords()) <= getCoordsRestrictions().getValue()
                          ? success(true)

                          // "в игре %s  %s пробовал применить заклинание '%s' но цель '%s %s' выходят за границы."
                          : fail(GameErrors.SPELL_TARGET_IS_OUT_OF_RANGE.getError(
                          theSpell.getContext().getGameName()
                          , theSpell.getOwner().getId()
                          , theSpell.getTitle()
                          , warrior.getWarriorBaseClass().getTitle()
                          , warrior.getTitle())));
                });
        break;
      case MAP_POINT:
        // параметр -координаты на карте.
        Coords coords = Coords.valueOf(givenParameter);
        if (coords == null) {
          // не распарсилось
          // "в игре %s игрок %s пробовал применить заклинание, но координаты '%s' не валидны."
          result = fail(GameErrors.SPELL_MAP_POINTS_UNPARSEABLE.getError(
                  theSpell.getContext().getGameName()
                  , theSpell.getOwner().getId()
                  , theSpell.getTitle()
                  , givenParameter));
        } else {
          // нет ограничений по дальности
          result = getCoordsRestrictions() == null
                  // или расстояние меньше или равно разрешенному
                  || theSpell.getContext().calcDistanceMm(getCoordsRestrictions().getKey(), coords) <= getCoordsRestrictions().getValue()
                  // тогда успех
                  ? success(true)
                  //"в игре %s игрок %s пробовал применить заклинание '%s', но координаты '%s' выходят за границы."
                  : fail(GameErrors.SPELL_MAP_POINTS_OUT_OF_RANGE.getError(
                  theSpell.getContext().getGameName()
                  , theSpell.getOwner().getId()
                  , theSpell.getTitle()
                  , coords.toString()));
        }
        break;
      case INTEGER:
        // число
        Integer val = Integer.parseInt(givenParameter);
        setIntegerValue(val);
        result = (getIntegerRestrictions() == null
                || (val >= getIntegerRestrictions().getKey() && val <= getIntegerRestrictions().getValue()))
                ? success(true)
                //"в игре %s игрок %s пробовал применить заклинание '%s', но число '%s' выходят за границы."
                : fail(GameErrors.SPELL_INTEGER_OUT_OF_RANGE.getError(
                theSpell.getContext().getGameName()
                , theSpell.getOwner().getId()
                , theSpell.getTitle()
                , givenParameter));
        break;
    }
    return result.isFail()
            ? result
            : success(this);
  }
  //===================================================================================


}
