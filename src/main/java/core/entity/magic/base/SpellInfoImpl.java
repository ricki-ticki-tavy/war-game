package core.entity.magic.base;

import api.entity.magic.Spell;
import api.entity.magic.SpellInfo;
import api.entity.magic.SpellParameter;
import api.enums.SignOfInfluenceEnum;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Класс информации о заклинании
 */
public class SpellInfoImpl implements SpellInfo {
  private final String name;
  private final String description;
  private final int mannaCost;
  private final int actionPointsCost;
  private final int abilityActionPointsCost;
  private final Class<? extends Spell> spellClass;
  private final SignOfInfluenceEnum spellSign;
  private final Map<Integer, SpellParameter> requiredAdditionalParams = new HashMap<>();
  private final Integer influenceRadius;

  public SpellInfoImpl(String name
          , String description
          , SignOfInfluenceEnum spellSign
          , Integer influenceRadius
          , int actionPointsCost
          , int abilityActionPointsCost
          , int mannaCost
          , Class<? extends Spell> spellClass
          , List<SpellParameter> requiredAdditionalParams) {
    this.name = name;
    this.description = description;
    this.actionPointsCost = actionPointsCost;
    this.mannaCost = mannaCost;
    this.spellClass = spellClass;
    this.abilityActionPointsCost = abilityActionPointsCost;
    this.spellSign = spellSign;
    this.influenceRadius = influenceRadius;
    if (requiredAdditionalParams != null) {
      requiredAdditionalParams.forEach(spellParameter -> this.requiredAdditionalParams.put(this.requiredAdditionalParams.size(), spellParameter));
    }
  }

  public SpellInfoImpl(String name
          , String description
          , SignOfInfluenceEnum spellSign
          , int actionPointsCost
          , int abilityActionPointsCost
          , int mannaCost
          , Class<? extends Spell> spellClass
          , List<SpellParameter> requiredAdditionalParams) {
    this(name, description, spellSign, null, actionPointsCost, abilityActionPointsCost, mannaCost, spellClass, requiredAdditionalParams);
  }

  public SpellInfoImpl(String name
          , String description
          , SignOfInfluenceEnum spellSign
          , Integer influenceRadius
          , int mannaCost
          , Class<? extends Spell> spellClass
          , List<SpellParameter> requiredAdditionalParams) {
    this(name, description, spellSign, influenceRadius, 0, 0, mannaCost, spellClass, requiredAdditionalParams);
  }

  public SpellInfoImpl(String name
          , String description
          , SignOfInfluenceEnum spellSign
          , int mannaCost
          , Class<? extends Spell> spellClass
          , List<SpellParameter> requiredAdditionalParams) {
    this(name, description, spellSign, null, 0, 0, mannaCost, spellClass, requiredAdditionalParams);
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public int getMannaCost() {
    return mannaCost;
  }

  @Override
  public int getActionPointsCost() {
    return actionPointsCost;
  }

  @Override
  public int getAbilityActionPointsCost() {
    return abilityActionPointsCost;
  }

  @Override
  public Class<? extends Spell> getSpellClass() {
    return spellClass;
  }

  @Override
  public Map<Integer, SpellParameter> getRequiredAdditionalParams() {
    return this.requiredAdditionalParams;
  }

  @Override
  public SignOfInfluenceEnum getSpellSign() {
    return spellSign;
  }

  @Override
  public Integer getInfluenceRadius() {
    return influenceRadius;
  }
}
