package core.entity.magic.base;

import api.core.EventDataContainer;
import api.core.Owner;
import api.core.Result;
import api.entity.ability.Ability;
import api.entity.ability.Influencer;
import api.entity.ability.Modifier;
import api.entity.magic.Spell;
import api.entity.magic.SpellParameter;
import api.entity.warrior.Warrior;
import api.enums.LifeTimeUnit;
import api.enums.ModifierClass;
import api.enums.OwnerTypeEnum;
import api.enums.SignOfInfluenceEnum;
import api.game.action.InfluenceResult;
import api.game.map.Player;
import core.entity.ability.base.BaseModifier;
import core.entity.abstracts.AbstractOwnerImpl;
import core.entity.warrior.base.InfluencerImpl;
import core.game.action.InfluenceResultImpl;
import core.system.error.GameErrors;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static api.enums.AttributeEnum.NO_ONE;
import static api.enums.EventType.WARRIOR_WAS_SPELLCASTED_BY_PLAYER;
import static api.enums.EventType.WARRIOR_WAS_SPELLCASTED_BY_WARRIOR;
import static core.system.ResultImpl.fail;
import static core.system.ResultImpl.success;
import static core.system.error.GameErrors.*;

/**
 * Базовый класс заклинания. Все заклинания наследуются от него
 */
public abstract class AbstractSpellImpl<S extends Owner> extends AbstractOwnerImpl<S> implements Spell<S> {
  protected int magicCost;
  protected int actionPointsCost;
  protected final Map<Integer, SpellParameter> requiredParams = new ConcurrentHashMap<>(10);
  protected OwnerTypeEnum ownerType;
  // Способности заклнания
  protected final Map<String, Ability> abilities = new ConcurrentHashMap<>(10);
  // Тип влияния
  protected final SignOfInfluenceEnum spellSign;

  //радиус влияния от точки применения.
  private final Integer influenceRadius;

  /**
   * @param owner       - Владелец заклинания. Игрок или воин
   * @param spellSign   -
   * @param idPrefix
   * @param title
   * @param description
   */
  public AbstractSpellImpl(S owner, SignOfInfluenceEnum spellSign, int magicCost, String idPrefix, String title, String description, Integer influenceRadius) {
    super(owner, detectOwnerType(owner)
            , StringUtils.isEmpty(idPrefix) ? "spell_" : idPrefix
            , title
            , description);
    this.spellSign = spellSign;
    this.magicCost = magicCost;
    this.influenceRadius = influenceRadius;
  }
  //===============================================================

  public AbstractSpellImpl(S owner, SignOfInfluenceEnum spellSign, int magicCost, String idPrefix, String title, String description) {
    this(owner, spellSign, magicCost, idPrefix, title, description, null);
  }
  //===============================================================

  public AbstractSpellImpl(S owner, SignOfInfluenceEnum spellSign, int magicCost, int actionPointsCost, String idPrefix, String title, String description, Integer influenceRadius) {
    this(owner, spellSign, magicCost, idPrefix, title, description, influenceRadius);
    this.actionPointsCost = actionPointsCost;
  }
  //===============================================================

  public AbstractSpellImpl(S owner, SignOfInfluenceEnum spellSign, int magicCost, int actionPointsCost, String idPrefix, String title, String description) {
    this(owner, spellSign, magicCost, actionPointsCost, idPrefix, title, description, null);
  }
  //===============================================================

  @Override
  public OwnerTypeEnum getAvailableOwnerType() {
    return ownerType;
  }
  //===============================================================

  @Override
  public SignOfInfluenceEnum getSpellSign() {
    return spellSign;
  }
  //===============================================================

  @Override
  public Map<Integer, SpellParameter> getRequiredParameters() {
    Map<Integer, SpellParameter> params = new HashMap<>(requiredParams.size());
    for (int key = 0; key < requiredParams.size(); key++) {
      params.put(key, requiredParams.get(key).clone());
    }
    return params;
  }
  //===============================================================

  /**
   * метод должен быть переопределен в наследниках, которые хотят воздействовать более чем однин юнит.
   * Метод вызывается если нет ни одного параметра, указывающего цель воздействия: на игрока или юнит.
   *
   * @param parameters
   * @return
   */
  protected List<Warrior> buildWarriorsListForApplay(Map<Integer, SpellParameter> parameters) {
    return Collections.EMPTY_LIST;
  }
  //===============================================================

  /**
   * Метод для переопределения в сложных заклинаниях, реализующих сложную логику. Если метод не переопределен,
   * то работает стандартный механизм, подходящий для подавляющего большинства заклинаний
   *
   * @param parameters
   * @return
   */
  protected Result<List<Result<InfluenceResult>>> innerSpecialCast(Map<Integer, SpellParameter> parameters) {
    return fail(SYSTEM_NOT_REALIZED.getError("innerSpecialCast"));
  }
  //===============================================================

  // TODO     подставим в координатные ограничения параметров, где координната центра null координату воина. Фактическую

  @Override
  public Result<List<Result<InfluenceResult>>> cast(Map<Integer, SpellParameter> parameters) {
    Result<List<Result<InfluenceResult>>> result = null;
    Vector<Warrior> targetWarriors = new Vector();
    Vector<Player> targetPlayer = new Vector();
    parameters.values().stream()
            .forEach(spellParameter -> {
              switch (spellParameter.getType()) {
                case ENEMY_WARRIOR:
                case ALLIED_WARRIOR:
                  targetWarriors.add(spellParameter.getWarriorValue());
                  break;
                case PLAYER:
                  targetPlayer.add(spellParameter.getPlayerValue());
                  break;
              }
            });

    // если нет цели, то запросим список целей
    if (targetPlayer.isEmpty() && targetWarriors.isEmpty()) {
      targetWarriors.addAll(buildWarriorsListForApplay(parameters));
    }

    // проверим снова есть ли объекты, к которым применимо заклинание
    if (targetPlayer.isEmpty() && targetWarriors.isEmpty()) {
      // нету таких    "в игре %s  %s пробовал применить заклинание '%s' но оно неприменимо ни к кому."
      result = fail(SPELL_THERE_IS_NOT_TARGETS_TO_APPLY_TO.getError(
              getContext().getGameName()
              , owner.getTitle()
              , title));
    } else {
      // применим заклнание к цели
      result = innerSpecialCast(parameters);
      if (result.isFail(SYSTEM_NOT_REALIZED)) {
        // метод не определен. стандартная логика заклинания

        // при универсальном механизме не может быть одновременно разных целей
        // определим цели
        AtomicInteger warriorPresents = new AtomicInteger(0);
        AtomicInteger playerPresents = new AtomicInteger(0);
        parameters.values().stream()
                .forEach(spellParameter -> {
                  switch (spellParameter.getType()) {
                    case ENEMY_WARRIOR:
                    case ALLIED_WARRIOR:
                      warriorPresents.set(1);
                      break;
                    case PLAYER:
                      playerPresents.set(1);
                      break;
                  }
                });
        if (warriorPresents.get() + playerPresents.get() > 1) {
          // смешанные цели
          result = fail(SPELL_MIXED_TARGET_ON_BASE_ALGORITHM.getError(title));
        } else {
          // все норм.

          // найдем игрока, который так или иначе наложил заклинание
          Player castOwner = getThisOwnerType().equals(OwnerTypeEnum.WARRIOR)
                  ? (Player) getOwner().getOwner()
                  : (Player) getOwner();

          if (warriorPresents.get() > 0) {
            // это юниты. Формируем результат влияния и передаем его в стандартный механизм атаки
            // для каждого воина врага

            List<Result<InfluenceResult>> spellResults = spellSign.equals(SignOfInfluenceEnum.NEGATIVE)
                    // Урон - идет как атака
                    ? targetWarriors.stream()
                    .map(warrior -> innerCastNegativeToWarrior(castOwner, warrior))
                    .collect(Collectors.toList())
                    // полезное. Идет прямое воздействие на объекты
                    : targetWarriors.stream()
                    .map(warrior -> innerCastPositiveToWarrior(castOwner, warrior))
                    .collect(Collectors.toList());

            result = success(spellResults);
          }
        }
      }
    }

    return result;
  }

  //===============================================================

  /**
   * Готовит структуру для оказания влияния на воина
   * @param castOwner
   * @param warrior
   * @param signOfInfluence
   * @return
   */
  private InfluenceResult buildInfluenceResult(Player castOwner, Warrior warrior, SignOfInfluenceEnum signOfInfluence) {
    InfluenceResult influenceResult = new InfluenceResultImpl(
            castOwner
            , getThisOwnerType().equals(OwnerTypeEnum.WARRIOR) ? (Warrior) owner : null
            , this
            , warrior.getOwner()
            , warrior
            , 0);

    // корневой модификатор. Не меняем атрибутов, а ставим признак, что это заклинание
    Modifier modifier = new BaseModifier(
            owner.getContext()
            , title
            , description
            , signOfInfluence
            , ModifierClass.SPELL
            , NO_ONE
            , 0
            , 0
            , 100
            , 0);

    // корневое влияние
    Influencer influencer = new InfluencerImpl(
            warrior, this, LifeTimeUnit.JUST_NOW, 1
            , modifier);

    // добавим корневое влияние в результат влияния
    influenceResult.addInfluencer(influencer);

    // соберем все влияния заклинания
    abilities.values().stream()
//            .filter(ability -> ability..equals(ENEMY_WARRIOR))
            .forEach(ability -> influencer.addChildren(ability.buildForTarget(warrior)));
    return influenceResult;
  }
  //===============================================================

  /**
   * Применить негативное заклинание заклинание к воину. Все проверки уже должны быть пройдены. Для внутреннего вызова
   *
   * @param castOwner
   * @param warrior
   * @return
   */
  private Result<InfluenceResult> innerCastNegativeToWarrior(Player castOwner, Warrior warrior) {

    InfluenceResult influenceResult = buildInfluenceResult(castOwner, warrior, SignOfInfluenceEnum.NEGATIVE);

    Result<InfluenceResult> spellResult = warrior.getOwner().defenceWarrior(influenceResult);

    getContext().fireGameEvent(null
            , getThisOwnerType().equals(OwnerTypeEnum.WARRIOR)
                    ? WARRIOR_WAS_SPELLCASTED_BY_WARRIOR
                    : WARRIOR_WAS_SPELLCASTED_BY_PLAYER
            , new EventDataContainer(influenceResult)
            , null);

    return spellResult;
  }
//===============================================================

  /**
   * Применить позитивное заклинание к воину. Все проверки уже должны быть пройдены. Для внутреннего вызова
   *
   * @param castOwner
   * @param warrior
   * @return
   */
  private Result<InfluenceResult> innerCastPositiveToWarrior(Player castOwner, Warrior warrior) {

    InfluenceResult influenceResult = new InfluenceResultImpl(
            castOwner
            , getThisOwnerType().equals(OwnerTypeEnum.WARRIOR) ? (Warrior) owner : null
            , this
            , warrior.getOwner()
            , warrior
            , 0);

    abilities.values().stream()
            .flatMap(ability -> ability.buildForTarget(warrior).stream())
    .forEach(influencer -> {
      influenceResult.addInfluencer(influencer);
      influencer.applyToWarrior(influenceResult);
    });

    return success(influenceResult);
  }
//===============================================================

  @Override
  public Result<Map<Integer, SpellParameter>> parseParameters(Map<Integer, String> givenParameters) {
    Result result = null;

    // Для начала преобразуем строки к параметрам
    Map<Integer, SpellParameter> requiredParameters = getRequiredParameters();
    if (requiredParameters.size() != givenParameters.size()) {
      result = fail(GameErrors.SYSTEM_BAD_PARAMETERS.getError("Набор параметров не соответствует заявленному для заклинания " + getTitle()));
    } else {
      // распарсим параметры
      for (int key = 0; key < requiredParameters.size(); key++) {
        SpellParameter requiredParameter = requiredParameters.get(key);
        String givenParameter = givenParameters.get(key);
        // если параметр кривой, то завершим проверку
        if ((result = requiredParameter.fillFromString(this, givenParameter)).isFail()) {
          break;
        }
      }
    }
    // если не было ошибок, то вернем распарсенные параметры
    return result != null && result.isFail() ? result : success(requiredParameters);
  }
//===============================================================

  @Override
  public int getActionPointsCost() {
    return actionPointsCost;
  }
//===============================================================

  @Override
  public int getMagicCost() {
    return magicCost;
  }
}
