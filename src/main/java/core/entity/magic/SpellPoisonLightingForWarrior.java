package core.entity.magic;

import api.entity.magic.SpellInfo;
import api.entity.magic.SpellParameter;
import api.entity.warrior.Warrior;
import api.enums.SignOfInfluenceEnum;
import api.enums.SpellParameterEnum;
import core.entity.ability.lighting.AbilityLightingForSpell;
import core.entity.ability.poison.AbilityPoisonArrowForSpell;
import core.entity.magic.base.AbstractSpellImpl;
import core.entity.magic.base.SpellInfoImpl;
import core.entity.magic.base.SpellParameterImpl;
import javafx.util.Pair;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Заклинание молнии и 3-х ходовой ядовитой стрелы для воина
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SpellPoisonLightingForWarrior extends AbstractSpellImpl<Warrior> {

  public static final SpellInfo spellinfo = new SpellInfoImpl(
          "Ядовитая молния шамана"
          , "Наносит уром молнией и, в течение 3-х ходов, ядом"
          , SignOfInfluenceEnum.NEGATIVE
          , 120
          , 0
          , 10
          , SpellPoisonLightingForWarrior.class
          , Arrays.stream(new SpellParameter[]{
          SpellParameterImpl.spellParameter(
                  "Выберете вражеского воина"
                  , SpellParameterEnum.ENEMY_WARRIOR
                  , new Pair<>(null, 290))
  }).collect(Collectors.toList()));

  @Autowired
  BeanFactory beanFactory;

  @PostConstruct
  public void init() {
    this.abilities.put(AbilityLightingForSpell.UNIT_NAME, beanFactory.getBean(AbilityLightingForSpell.class, this, 10, 800));
    this.abilities.put(AbilityPoisonArrowForSpell.UNIT_NAME, beanFactory.getBean(AbilityPoisonArrowForSpell.class, this, 3, 10, 200));
  }

  public SpellPoisonLightingForWarrior(Warrior owner) {
    super(owner, spellinfo.getSpellSign(), spellinfo.getMannaCost(), "SpellLgt_", spellinfo.getName(), spellinfo.getDescription());
    this.requiredParams.putAll(spellinfo.getRequiredAdditionalParams());
  }
}
