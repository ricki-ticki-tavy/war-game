package core.entity.magic;

import api.entity.magic.SpellInfo;
import api.entity.magic.SpellParameter;
import api.entity.warrior.Warrior;
import api.enums.SignOfInfluenceEnum;
import api.enums.SpellParameterEnum;
import core.entity.ability.lighting.AbilityLightingForSpell;
import core.entity.ability.poison.AbilityPoisonArrowForSpell;
import core.entity.magic.base.AbstractSpellImpl;
import core.entity.magic.base.SpellInfoImpl;
import core.entity.magic.base.SpellParameterImpl;
import javafx.util.Pair;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Заклинание огненного дождя для воина
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SpellFireRainForWarrior extends AbstractSpellImpl<Warrior> {

  public static final SpellInfo spellinfo = new SpellInfoImpl(
          "Огненный дождь"
          , "Наносит уром огнем по площади"
          , SignOfInfluenceEnum.NEGATIVE
          , 80
          , 120
          , 0
          , 10
          , SpellFireRainForWarrior.class
          , Arrays.stream(new SpellParameter[]{
          SpellParameterImpl.spellParameter(
                  "Выберете место на карте"
                  , SpellParameterEnum.MAP_POINT
                  , new Pair<>(null, 150))
  }).collect(Collectors.toList()));

  @Autowired
  BeanFactory beanFactory;

  @PostConstruct
  public void init() {
    this.abilities.put(AbilityLightingForSpell.UNIT_NAME, beanFactory.getBean(AbilityLightingForSpell.class, this, 10, 800));
  }

  public SpellFireRainForWarrior(Warrior owner) {
    super(owner, spellinfo.getSpellSign(), spellinfo.getMannaCost(), "SpellLgt_", spellinfo.getName(), spellinfo.getDescription());
    this.requiredParams.putAll(spellinfo.getRequiredAdditionalParams());
  }
}
