package core.entity.player;

import api.core.Context;
import api.core.EventDataContainer;
import api.core.Owner;
import api.core.Result;
import api.core.database.service.PlayerWarriorService;
import api.entity.ability.Influencer;
import api.entity.ability.Modifier;
import api.entity.magic.Spell;
import api.entity.artifct.Artifact;
import api.entity.warrior.Warrior;
import api.entity.weapon.Weapon;
import api.enums.LifeTimeUnit;
import api.enums.OwnerTypeEnum;
import api.game.action.InfluenceResult;
import api.game.map.Player;
import api.geo.Coords;
import api.geo.Rectangle;
import core.database.entity.warrior.PlayerWarrior;
import core.entity.abstracts.AbstractOwnerImpl;
import core.entity.warrior.base.WarriorImpl;
import core.system.ResultImpl;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static api.enums.EventType.*;
import static api.enums.OwnerTypeEnum.PLAYER;
import static core.system.ResultImpl.fail;
import static core.system.error.GameErrors.*;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PlayerImpl extends AbstractOwnerImpl implements Player {

  public static final String ALL_SPELLS_NAME = "магия";

  @Autowired
  BeanFactory beanFactory;

  @Autowired
  PlayerWarriorService playerWarriorService;

  private String userName;
  private Rectangle startZone;
  private final Map<String, Warrior> warriors = new ConcurrentHashMap();
  private final Map<String, Artifact<Player>> artifacts = new ConcurrentHashMap<>(10);
  private volatile boolean readyToPlay;
  private Long databaseId;
  private int startZoneIndex = -1;

  /**
   * Заклинания, использованные в данном ходе и доступные для использования
   */
  private Map<String, Boolean> spellUsedAtThisTurn = new ConcurrentHashMap(10);
  private boolean artifactWasDroppedOrTakenAtThisRound = false;

  /**
   * запас магии
   */
  private final AtomicInteger mannaPoints = new AtomicInteger(0);

  public PlayerImpl(String userName, Long databaseId) {
    super(null, PLAYER, "ply", userName, "Игрок '" + userName + "'");
    this.databaseId = databaseId;
    this.userName = userName;
    this.readyToPlay = false;
    spellUsedAtThisTurn.put(ALL_SPELLS_NAME, false);// признак для свободно выбранного заклинания, что не
    // использовались в этом ходе
  }
  //===================================================================================================

  @Override
  public Result<Warrior> createWarrior(String warriorBaseClassName, Coords coords) {
    if (isReadyToPlay()) {
      // игрок уже готов к игре. Следовательно добавить юнит не может
      return fail(USER_IS_READY_TO_PLAY.getError(getId(), getContext().getGameName(), getContext().getContextId()));
    }
    //Создадим нового воина
    return findContext()
            .map(fineContext -> fineContext.getCore().findWarriorBaseClassByName(warriorBaseClassName)
                    .map(info -> {
                      Warrior warrior = beanFactory.getBean(Warrior.class, this
                              , beanFactory.getBean(info.getWarriorClass()), "", coords, false);
                      // поместим в массив
                      warriors.put(warrior.getId(), warrior);
                      ((WarriorImpl) warrior).restoreAttributesAvailableForRestoration(null);
                      Result result = ResultImpl.success(warrior);
                      getContext().fireGameEvent(null, WARRIOR_ADDED, new EventDataContainer(warrior, result), Collections.EMPTY_MAP);
                      return result;
                    }));
  }
  //===================================================================================================

  @Override
  public Long getDatabaseId() {
    return databaseId;
  }
  //===================================================================================================

  @Override
  public Map<String, Warrior> getOriginWarriors() {
    return warriors;
  }
  //===================================================================================================

  @Override
  public List<Warrior> getWarriors() {
    return new ArrayList(warriors.values());
  }
  //===================================================================================================

  @Override
  public void setStartZone(Rectangle startZone) {
    this.startZone = startZone;
  }
  //===================================================================================================

  @Override
  public Rectangle getStartZone() {
    return startZone;
  }
  //===================================================================================================

  @Override
  public Result replaceContext(Context newContext) {
    Result result;
    if (this.getContext() != null && (newContext == null || !this.getContext().getContextId().equals(newContext.getContextId()))) {
      if ((result = this.getContext().disconnectPlayer(this)).isFail())
        return result;
    }

    replaceContextSilent(newContext);
    return ResultImpl.success(this);
  }
  //===================================================================================================

  @Override
  public Result<Player> replaceContextSilent(Context newContext) {
    setContext(newContext);
    this.readyToPlay = false;
    this.warriors.clear();
    this.artifacts.clear();
    this.startZone = null;
    this.startZoneIndex = -1;
    return ResultImpl.success(this);
  }
  //===================================================================================================

  @Override
  public Result<Context> findContext() {
    return getContext() == null
            ? fail(USER_NOT_CONNECTED_TO_ANY_GAME.getError(getId()))
            : ResultImpl.success(getContext());
  }
  //===================================================================================================

  @Override
  public String getId() {
    return userName;
  }
  //===================================================================================================

  @Override
  public boolean equals(Object obj) {
    return (obj instanceof Player) && ((Player) obj).getId().equals(getId());
  }
  //===================================================================================================

  @Override
  public Result<Warrior> findWarriorById(String warriorId) {
    return Optional.ofNullable(warriors.get(warriorId))
            .map(foundWarrior -> ResultImpl.success(foundWarrior))
            .orElse(fail(WARRIOR_NOT_FOUND_AT_PLAYER_BY_NAME.getError(
                    getContext().getGameName()
                    , getContext().getContextId()
                    , getId()
                    , warriorId)));
  }
  //===================================================================================================

  @Override
  public Result<Player> setReadyToPlay(boolean ready) {
    this.readyToPlay = ready;
    Result result = ResultImpl.success(this);
    getContext().fireGameEvent(null, PLAYER_CHANGED_ITS_READY_TO_PLAY_STATUS
            , new EventDataContainer(getContext(), this, result), null);

    // приготовить все флажки
    rejuvenate();

    // выставить запас магии
    mannaPoints.set(getContext().getGameRules().getMaxMannaPoints());

    return result;
  }
  //===================================================================================================

  @Override
  public boolean isReadyToPlay() {
    return readyToPlay;
  }
  //===================================================================================================

  @Override
  public Result<Warrior> moveWarriorTo(Warrior warrior, Coords to) {
    return warrior.moveWarriorTo(to);
  }
  //===================================================================================================

  @Override
  public Result<Warrior> rollbackMove(String warriorId) {
    return findWarriorById(warriorId)
            .map(warrior -> warrior.rollbackMove());
  }
  //===================================================================================================

  @Override
  public Result<Warrior> ifWarriorCanMoveAtThisTurn(Warrior warrior) {
    return getContext().isGameRan()
            // и этим юнитом не делалось движений
            && !warrior.isTouchedAtThisTurn()
            // и предел используемых за ход юнитов достигнут
            && getWarriorsTouchedAtThisTurn().getResult().size() >= getContext().getGameRules().getMovesCountPerTurnForEachPlayer()
            ? fail(PLAYER_UNIT_MOVES_ON_THIS_TURN_ARE_EXCEEDED
            .getError(warrior.getOwner().getId(), String.valueOf(getContext().getGameRules().getMovesCountPerTurnForEachPlayer())))
            : ResultImpl.success(warrior);
  }
  //===================================================================================================

  @Override
  public Result<Warrior> ifWarriorCanActsAtThisTurn(Warrior warrior) {
    return getContext().ifGameRan(true)
            .map(fineContext ->
                    // и этим юнитом не делалось движений
                    !warrior.isTouchedAtThisTurn()
                            // и предел используемых за ход юнитов достигнут
                            && getWarriorsTouchedAtThisTurn().getResult().size() < getContext().getGameRules().getMovesCountPerTurnForEachPlayer()
                            // или юнит уже задействован в этом ходе
                            || warrior.isTouchedAtThisTurn()
                            ? ResultImpl.success(warrior)
                            : fail(PLAYER_UNIT_MOVES_ON_THIS_TURN_ARE_EXCEEDED
                            .getError(
                                    warrior.getOwner().getId()
                                    , String.valueOf(getContext().getGameRules().getMovesCountPerTurnForEachPlayer()))));
  }
  //===================================================================================================

  @Override
  public Result<Player> clear() {
    List<Warrior> warriorsList = new ArrayList<>(warriors.values());
    warriors.clear();
    artifacts.clear();
    this.startZone = null;
    this.startZoneIndex = -1;

    if (getContext() != null) {
      warriorsList.stream().forEach(warrior -> innerRemoveWarrior(warrior));
    }


    return ResultImpl.success(this);
  }
  //===================================================================================================

  private Result<Warrior> innerRemoveWarrior(Warrior warrior) {
    warriors.remove(warrior.getId());
    // отписаться ото всех событий котрые должны удалять влияния на юнит
    warrior.getWarriorSInfluencers()
            .peak(influencers -> influencers.stream().forEach(influencer -> influencer.removeFromWarrior(true)));
    //  пошлем событие
    Result<Warrior> result = ResultImpl.success(warrior);
    getContext().fireGameEvent(null, WARRIOR_REMOVED, new EventDataContainer(warrior, result), null);
    return result;

  }
  //===================================================================================================

  @Override
  public Result<Warrior> removeWarrior(String warriorId) {
    return readyToPlay ? fail(USER_IS_READY_TO_PLAY.getError(getId(), getContext().getGameName(), getContext().getContextId()))
            : findWarriorById(warriorId)
            .map(warrior -> innerRemoveWarrior(warrior));
  }
  //===================================================================================================

  @Override
  public Result<Weapon> giveWeaponToWarrior(String warriorId, Class<? extends Weapon> weaponClass) {
    return findWarriorById(warriorId)
            .map(foundWarrior -> foundWarrior.takeWeapon(weaponClass));
  }
  //===================================================================================================

  @Override
  public Result<Artifact<Warrior>> giveArtifactToWarrior(String warriorId, Class<? extends Artifact<Warrior>> artifactClass) {
    return findWarriorById(warriorId)
            .map(foundWarrior -> foundWarrior.giveArtifactToWarrior(artifactClass));
  }
  //===================================================================================================

  @Override
  public Result<Weapon> dropWeaponByWarrior(String warriorId, String weaponId) {
    return findWarriorById(warriorId)
            .map(warrior -> warrior.dropWeapon(weaponId));
  }
  //===================================================================================================

  @Override
  public Result<Artifact<Warrior>> dropArtifactByWarrior(String warriorId, String artifactId) {
    return findWarriorById(warriorId)
            .map(foundWarrior -> foundWarrior.dropArtifact(artifactId));
  }
  //===================================================================================================

  @Override
  public Result<Weapon> findWeaponById(String warriorId, String weaponId) {
    return findWarriorById(warriorId)
            .map(foundWarrior -> foundWarrior.findWeaponById(weaponId));
  }
  //===================================================================================================

  @Override
  public Result<Player> prepareToDefensePhase() {
    Result<Warrior> warriorResult;
    // восстановим значения воинов. свои влияния воин собирает сам
    for (Warrior warrior : warriors.values())
      if ((warriorResult = warrior.prepareToDefensePhase()).isFail()) {
        return (Result<Player>) fail(warriorResult.getError());
      }

    // Отправим сообщение о завершении хода
    getContext().fireGameEvent(null, PLAYER_LOOSE_TURN, new EventDataContainer(this), null);

    return ResultImpl.success(this);
  }
  //===================================================================================================

  @Override
  public Result<InfluenceResult> attackWarrior(String attackerWarriorId, String targetWarriorId, String weaponId) {
    // Найти юнит, который будем атаковать
    return getContext().getLevelMap().findWarriorById(targetWarriorId)
            // найти юнит которым будем атаковать
            .map(targetWarrior -> findWarriorById(attackerWarriorId)
                    // проверить, что этот юнит может атаковать в этом ходу
                    .map(attackerWarrior -> ifWarriorCanActsAtThisTurn(attackerWarrior)
                            // Атаковать
                            .map(fineAttackerWarrior -> fineAttackerWarrior.attackWarrior(targetWarrior, weaponId))));
  }
  //===================================================================================================

  @Override
  public Result<InfluenceResult> innerAttachToAttackToWarrior(InfluenceResult attackResult) {
    return ResultImpl.success(attackResult); //
  }
  //===================================================================================================

  /**
   * проверить остались ли у юнита единицы жизни. Если нет, то "убить" его на карте
   *
   * @param targetResult
   * @return true, если воин умер
   */
  public Result<Boolean> checkForWarriorHasDie(InfluenceResult targetResult) {
    Result<Boolean> result;
    // проверим остался ли воин еще жив
    if (targetResult.getTarget().getAttributes().getHealth() <= 0) {
      // воин погиб
      targetResult.getTarget().die();
      //удалить воина из массива воинов
      warriors.remove(targetResult.getTarget().getId());
      // сообщение
      getContext().fireGameEvent(null, WARRIOR_WAS_DIE, new EventDataContainer(targetResult), null);

      result = ResultImpl.success(true);
    } else {
      result = ResultImpl.success(false);
    }
    return result;
  }
  //===================================================================================================

  @Override
  public Result<InfluenceResult> defenceWarrior(InfluenceResult attackResult) {
    // возможность воину отбить удары и / или ослабить воздействие вредных влияний
    return attackResult.getTarget().defenceWarrior(attackResult)
            // теперь рпименим оставшиеся влияния к воину
            .map(fineAttackResult -> {
              // собственно применим полученные поражения
              fineAttackResult.getInfluencers().stream()
                      // считаем, что уже все значения в модифиерах расчитаны жестко и не изменяются от запроса к запросу
                      .forEach(influencer -> influencer.applyToWarrior(attackResult));
              // проверим остался ли воин еще жив
              Result<Boolean> dieResult = checkForWarriorHasDie(attackResult);

              // Если эта атака не была контрударом,
              if (attackResult.getParentInfluence() == null
                      // и воин жив
                      && !dieResult.getResult()
                      // и это не заклинание
                      && attackResult.getAttackerWeapon() != null && attackResult.getAttackerSpell() == null) {
                // Нанесем контрудар
                // Наносил ли в этом ходу воин контрудар он сам разберется
                attackResult.getTarget().contrAttackWarrior(attackResult);
              }
              return ResultImpl.success(attackResult);
            });
  }
  //===================================================================================================

  /**
   * Сбросить основные флажки и перевести игрока в состояние нового хода
   */
  private void rejuvenate() {
    // сбросить признак, что в этом круге был взят артефакт
    artifactWasDroppedOrTakenAtThisRound = false;

    // сбросить признаки использования всех видов магии в этом ходу
    spellUsedAtThisTurn.keySet().stream().collect(Collectors.toList()).stream()
            .forEach(key -> spellUsedAtThisTurn.put(key, false));

    // добавить очки магии
    if (mannaPoints.addAndGet(getContext().getGameRules().getRestorationMannaPointsPerTotalRound()) >
            getContext().getGameRules().getMaxMannaPoints()) {
      mannaPoints.set(getContext().getGameRules().getMaxMannaPoints());
    }
  }
  //===================================================================================================

  @Override
  public Result<Player> prepareToAttackPhase() {
    Result<Warrior> warriorResult;
    // восстановим значения воинов. свои влияния воин собирает сам
    for (Warrior warrior : warriors.values()) {
      if ((warriorResult = warrior.prepareToAttackPhase()).isFail()) {
        return (Result<Player>) fail(warriorResult.getError());
      }
    }

    rejuvenate();
    // Отправим сообщение о завершении хода
    getContext().fireGameEvent(null, PLAYER_TAKES_TURN, new EventDataContainer(this), null);

    return ResultImpl.success(this);
  }
  //===================================================================================================

  @Override
  public boolean isArtifactWasDroppedOrTakenAtThisRound() {
    return artifactWasDroppedOrTakenAtThisRound;
  }
  //===================================================================================================

  @Override
  public Result<Influencer> addInfluenceToWarrior(String warriorId, Modifier modifier, Owner source, LifeTimeUnit lifeTimeUnit, int lifeTime) {
    return findWarriorById(warriorId)
            .map(warrior -> warrior.addInfluenceToWarrior(modifier, source, lifeTimeUnit, lifeTime))
            // добавить слушатель события
            .peak(influencer -> {
            });
  }
  //===================================================================================================

//  private Result<Warrior> check
  //===================================================================================================

  @Override
  public Result<List<Result<InfluenceResult>>> castSpell(Class<? extends Spell<Player>> spellClass, Map<Integer, String> givenSpellParameters) {
    Result<InfluenceResult> oResult = null;

    Spell<Player> spell = beanFactory.getBean(spellClass, this);
    return spell.parseParameters(givenSpellParameters)
            .map(spellParameterMap -> {
              // не применял ли зиклинания в этом ходу
              if (spellUsedAtThisTurn.get(ALL_SPELLS_NAME)) {
                // уже применялось "в игре %s  %s уже применял в этом ходу заклинание. Надо дождаться следующего хода."
                fail(SPELL_CAN_NOT_BE_USED_AT_THIS_TURN.getError(
                        getContext().getGameName()
                        , id));
              } else
                // проверим хватает ли очков магии на заклинание
                if (spell.getMagicCost() > mannaPoints.get()) {
                  // "в игре %s  %s пробовал применить заклинание '%s' но не достаточно магии."
                  return fail(SPELL_THERE_IS_NOT_ENOUGH_MANNA.getError(
                          getContext().getGameName()
                          , id
                          , spell.getTitle()));
                }
              return spell.cast(spellParameterMap);
            })
            .peak(influenceResult -> {
              // спишем очки за заклинание и отметим, что заклинание использовано в данных ход
              mannaPoints.addAndGet(-spell.getMagicCost());
              spellUsedAtThisTurn.put(ALL_SPELLS_NAME, true);
            });
  }
  //===================================================================================================

  @Override
  public Result<List<Result<InfluenceResult>>> castSpellByWarrior(String warriorId, String spellName, Map<Integer, String> givenSpellParameters) {
    // Найти spellcaster
    return findWarriorById(warriorId)
            // проверить, что этот юнит может атаковать в этом ходу
            .map(spellCasterWarrior -> ifWarriorCanActsAtThisTurn(spellCasterWarrior)
                    // Атаковать
                    .map(fineSpellCasterWarrior -> fineSpellCasterWarrior.castSpell(spellName, givenSpellParameters)));
  }
  //===================================================================================================

  @Override
  public Result<List<Warrior>> getWarriorsTouchedAtThisTurn() {
    return ResultImpl.success(warriors.values()
            .stream()
            .filter(warrior -> warrior.isTouchedAtThisTurn())
            .collect(Collectors.toList()));
  }
  //===================================================================================================

  @Override
  public Result<Artifact<Player>> takeArtifact(Class<? extends Artifact<Player>> artifactClass) {
    Result<Artifact<Player>> result;
    // проверим, что в этот круг игрок не брал артифакт
    if (getContext().isGameRan() && artifactWasDroppedOrTakenAtThisRound) {
      //"В игре %s игрок %s не может в этом ходу взять артефакт '%s'. Надо дождаться следующего хода"
      result = fail(ARTIFACT_CAN_NOT_TAKE_AT_THIS_TURN.getError(
              getContext().getGameName()
              , title
              , artifactClass.toString()));
    } else {
      Artifact<Player> artifact = beanFactory.getBean(artifactClass, this);
      if (artifacts.get(artifact.getTitle()) != null) {
        // дубликат артифакта.
        // "В игре %s. игрока '%s' уже владеет артефактом '%s'."
        result = fail(ARTIFACT_PLAYER_ALREADY_HAS_IT.getError(getContext().getGameName(), title, artifact.getTitle()));
      } else if (!artifact.getOwnerTypeForArtifact().equals(OwnerTypeEnum.PLAYER)) {
        // "В игре %s игрок %s не может взять артефакт '%s'. Артефактом может владеть %s
        result = fail(ARTIFACT_WRONG_TYPE_FOR_PLAYER.getError(
                getContext().getGameName()
                , title
                , artifact.getTitle()
                , artifact.getOwnerTypeForArtifact().getTitle()));
      } else {
        // добавим артефакт
        artifacts.put(artifact.getTitle(), artifact);

        artifactWasDroppedOrTakenAtThisRound = getContext().isGameRan();

        // кинуть сообщение
        getContext().fireGameEvent(null, ARTIFACT_TAKEN_BY_PLAYER, new EventDataContainer(artifact, this), null);

        result = ResultImpl.success(artifact);
      }
    }
    return result;
  }
  //===================================================================================================

  @Override
  public Result<Artifact<Player>> dropArtifact(String artifactInstanceId) {
    // для начала найдем артефакт
    return findPlayersArtifactById(artifactInstanceId)
            .map(playerArtifact -> {
              Result<Artifact<Player>> result;
              // проверим, что в этот круг игрок не брал артефакт
              if (getContext().isGameRan() && artifactWasDroppedOrTakenAtThisRound) {
                //"В игре %s игрок %s не может в этом ходу взять артефакт '%s'. Надо дождаться следующего хода"
                result = fail(ARTIFACT_CAN_NOT_DROP_AT_THIS_TURN.getError(
                        getContext().getGameName()
                        , title
                        , playerArtifact.toString()));
              } else {
                // Можно его выкинуть
                artifacts.remove(playerArtifact.getTitle());
                // признак, что действие с артефактами не доступно в этом ходе
                artifactWasDroppedOrTakenAtThisRound = getContext().isGameRan();
                // сообщение
                getContext().fireGameEvent(null, ARTIFACT_TAKEN_BY_PLAYER
                        , new EventDataContainer(this, playerArtifact), null);
                result = ResultImpl.success(playerArtifact);
              }
              return result;
            });
  }
  //===================================================================================================

  @Override
  public Result<Artifact<Player>> findPlayersArtifactById(String artifactId) {
    Optional<Artifact<Player>> foundArtifactOptional = artifacts.values().stream()
            .filter(playerArtifact -> playerArtifact.getId().equals(artifactId))
            .findFirst();
    return foundArtifactOptional.isPresent()
            ? ResultImpl.success(foundArtifactOptional.get())
            // "В игре %s у игрока %s не имеет артефакта с id '%s'"
            : fail(ARTIFACT_NOT_FOUND_BY_PLAYER.getError(getContext().getGameName(), title, artifactId));
  }
  //===================================================================================================

  @Override
  public Result<List<Artifact<Player>>> getArtifacts() {
    return ResultImpl.success(new ArrayList(artifacts.values()));
  }
  //===================================================================================================

  @Override
  public Result<List<PlayerWarrior>> getAllHeroes() {
    return playerWarriorService.findAllHeroes(getId());
  }
  //===================================================================================================

  @Override
  public Result<PlayerWarrior> findHero(String heroName) {
    return playerWarriorService.findHeroByName(getId(), heroName);
//            .map(playerWarrior -> playerWarrior != null
//                    ? success(playerWarrior)
//                    // "В игре %s не найден герой %s игрока %s"
//                    : fail(GameErrors.WARRIOR_EQUIPMENT_NOT_FOUND.getError(getContext().getGameName(), heroName, getId())));

  }

  //===================================================================================================

  @Override
  public int getManna() {
    return mannaPoints.get();
  }
  //===================================================================================================

  @Override
  public int getStartZoneIndex() {
    return startZoneIndex;
  }

  @Override
  public Player setStartZoneIndex(int startZoneIndex) {
    this.startZoneIndex = startZoneIndex;
    return this;
  }
  //===================================================================================================

}
