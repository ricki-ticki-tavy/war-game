package core.entity;

import api.entity.graphics.GraphicsInfo;
import api.entity.graphics.ModelInfo;
import api.entity.graphics.Vector3;

import static api.entity.graphics.ModelType.OBJ;

public class ModelsGraphicInfo {

  public static final GraphicsInfo SKELETON_CLASS = new GraphicsInfo()
          .setBaseViewModel(
                  new ModelInfo()
                          .setMaterialsFile("models/mercenaries/skeleton2/baseview/skeleton2.mtl")
                          .setModelFile("models/mercenaries/skeleton2/baseview/skeleton2.obj")
                          .setModelType(OBJ)
                          .setScale(4F)
                          .setFloorLevel(0.F))
          .setLargeIcon("models/textures/gui/icons/skeleton/skeletonLarge.png");

  public static final GraphicsInfo VIKING_CLASS = new GraphicsInfo()
          .setBaseViewModel(
                  new ModelInfo()
                          .setMaterialsFile("models/mercenaries/don-flip/baseview/don-flip.mtl")
                          .setModelFile("models/mercenaries/don-flip/baseview/don-flip.obj")
                          .setModelType(OBJ)
                          .setScale(0.27F)
                          .setFloorLevel(0F))
          .setLargeIcon("models/textures/gui/icons/viking/vikingLarge.png");

  public static final GraphicsInfo VITYAZ_CLASS = new GraphicsInfo()
          .setBaseViewModel(
                  new ModelInfo()
                          .setMaterialsFile("models/mercenaries/skeleton/baseview/skeleton.mtl")
                          .setModelFile("models/mercenaries/skeleton/baseview/skeleton.obj")
                          .setModelType(OBJ)
                          .setScale(0.9F)
                          .setFloorLevel(0.65F));

  public static final GraphicsInfo VERWOLF_SHAMAN_CLASS = new GraphicsInfo()
          .setBaseViewModel(
                  new ModelInfo()
                          .setMaterialsFile("models/mercenaries/skeleton/baseview/skeleton.mtl")
                          .setModelFile("models/mercenaries/skeleton/baseview/skeleton.obj")
                          .setModelType(OBJ)
                          .setScale(0.9F)
                          .setFloorLevel(0.65F));


  public static final GraphicsInfo CLAYMORE = new GraphicsInfo()
          .setBaseViewModel(
                  new ModelInfo()
                          .setMaterialsFile("models/weapons/claymore/Mini_claymore.mtl")
                          .setModelFile("models/weapons/claymore/Mini_claymore.obj")
                          .setModelType(OBJ)
                          .setScale(0.15F)
                          .setFloorLevel(0F)
                          .setRotationX((float) (-Math.PI / 2.0))
                          .setRotationZ((float) (Math.PI / 2.0))
                          .setCenter(new Vector3(0, 0.18, 0)))
          .setCountOnForm(2);

  public static final GraphicsInfo SHORT_BOW = new GraphicsInfo()
          .setBaseViewModel(
                  new ModelInfo()
                          .setMaterialsFile("models/weapons/simpleBow/bow.mtl")
                          .setModelFile("models/weapons/simpleBow/bow.obj")
                          .setModelType(OBJ)
                          .setScale(0.15F)
                          .setFloorLevel(0F)
                          .setCenter(new Vector3(0.2, 0, 0)));

  public static final GraphicsInfo KITE_SHIELD = new GraphicsInfo()
          .setBaseViewModel(
                  new ModelInfo()
                          .setMaterialsFile("models/weapons/kiteShield/kiteshield.mtl")
                          .setModelFile("models/weapons/kiteShield/kiteshield.obj")
                          .setModelType(OBJ)
                          .setScale(1F)
                          .setFloorLevel(0F)
                          .setRotationX((float) Math.PI / 2F)
                          .setCenter(new Vector3(0, 0.015D / 0.07D, 0)));


  public static final GraphicsInfo SMALL_SHIELD = new GraphicsInfo()
          .setBaseViewModel(
                  new ModelInfo()
                          .setMaterialsFile("models/weapons/smallVikingShield/vikingshield.mtl")
                          .setModelFile("models/weapons/smallVikingShield/vikingshield.obj")
                          .setModelType(OBJ)
                          .setScale(1F)
                          .setFloorLevel(0F));

  public static final GraphicsInfo SWORD = new GraphicsInfo()
          .setBaseViewModel(
                  new ModelInfo()
                          .setMaterialsFile("models/weapons/longSwordOfSun/longswordofsun.mtl")
                          .setModelFile("models/weapons/longSwordOfSun/longswordofsun.obj")
                          .setModelType(OBJ)
                          .setScale(7F)
                          .setFloorLevel(0F)
                          .setRotationX(-0.7F)
                          .setRotationY(0.52F)
                          .setRotationZ(0.5F)
                          .setCenter(new Vector3(0, -0.035D / 0.07D, 0))
          )
          .setCountOnForm(2);

  public static final GraphicsInfo SHORT_SWORD = new GraphicsInfo()
          .setBaseViewModel(
                  new ModelInfo()
                          .setMaterialsFile("models/weapons/shortSword/shortsword.mtl")
                          .setModelFile("models/weapons/shortSword/shortsword.obj")
                          .setModelType(OBJ)
                          .setScale(2F)
//                          .setFloorLevel(0F)
//                          .setRotationX(-0.7F)
//                          .setRotationY(0.52F)
//                          .setRotationZ(0.5F)
                          .setCenter(new Vector3(0, -0.2, 0))
          )
          .setCountOnForm(2);

  public static final GraphicsInfo SMALL_AXE = new GraphicsInfo()
          .setBaseViewModel(
                  new ModelInfo()
                          .setMaterialsFile("models/weapons/smallAxe/smallaxe.mtl")
                          .setModelFile("models/weapons/smallAxe/smallaxe.obj")
                          .setModelType(OBJ)
                          .setFloorLevel(0F)
                          .setScale(100F)
                          .setCenter(new Vector3(0, -0.2, 0))
          )
          .setCountOnForm(2);

  public static final GraphicsInfo OLD_IRON_ARMOR = new GraphicsInfo()
          .setBaseViewModel(
                  new ModelInfo()
                          .setMaterialsFile("models/weapons/oldIronArmor/ironarmor.mtl")
                          .setModelFile("models/weapons/oldIronArmor/ironarmor.obj")
                          .setModelType(OBJ)
                          .setFloorLevel(0F)
                          .setScale(130F)
                          .setCenter(new Vector3(0, -0.3, 0))
          );

  public static final GraphicsInfo ARTIFACT_HEALING_FIAL_FOR_PLAYER_CLASS = new GraphicsInfo()
          .setBaseViewModel(
                  null)
          .setLargeIcon("models/textures/gui/icons/artifacts/fial.png");

  public static final GraphicsInfo ARTIFACT_CAPTAN_SABRE_FOR_PLAYER_CLASS = new GraphicsInfo()
          .setBaseViewModel(
                  null)
          .setLargeIcon("models/textures/gui/icons/artifacts/sabre.png");

  public static final GraphicsInfo ARTIFACT_FORE_CLOVER_FOR_PLAYER_CLASS = new GraphicsInfo()
          .setBaseViewModel(
                  null)
          .setLargeIcon("models/textures/gui/icons/artifacts/4clover.png");


}
