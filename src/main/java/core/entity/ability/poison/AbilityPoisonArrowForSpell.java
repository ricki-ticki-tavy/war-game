package core.entity.ability.poison;

import api.core.Owner;
import api.entity.ability.Influencer;
import api.entity.warrior.Warrior;
import api.enums.*;
import core.entity.ability.base.AbstractAbilityImpl;
import core.entity.ability.base.BaseModifier;
import core.entity.warrior.base.InfluencerImpl;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Способность ядовитой стрелы, наносящей урон ядом в течение нескольких ходов
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AbilityPoisonArrowForSpell extends AbstractAbilityImpl {

  public static final String UNIT_NAME = "Медленный яд";

  private int occurrenceCount;
  private int minDamage;
  private int maxDamage;
  //===================================================================================================
  //===================================================================================================

  /**
   * @param owner            владелец способности
   * @param occurrenceCount  кол-во раз, которые наносится урон, велючая момент выстрела
   * @param minDamage        минимальный урон
   * @param maxDamage        максимальный урон
   *
   */
  public AbilityPoisonArrowForSpell(Owner owner, int occurrenceCount, int minDamage, int maxDamage) {
    super(owner, -1, "AblPoisonArrow_", UNIT_NAME, "Урон ядом"); // бесконечно

    this.occurrenceCount = occurrenceCount;
    this.minDamage = minDamage;
    this.maxDamage = maxDamage;

    ownerTypeForAbility = OwnerTypeEnum.SPELL;
    targetType = TargetTypeEnum.ENEMY_WARRIOR;
    this.useCount.set(-1);
  }
  //===================================================================================================

  @Override
  protected List<Influencer> buildInfluencers(Warrior target) {
    List<Influencer> result = new ArrayList<>(1);
    result.add(new InfluencerImpl(target, this
            , LifeTimeUnit.JUST_NOW, 1
            , new BaseModifier(getContext(), title, description
            , SignOfInfluenceEnum.NEGATIVE
            , ModifierClass.POISON
            , AttributeEnum.HEALTH
            , minDamage, maxDamage
            , 100, 0)));
    if (occurrenceCount > 1) {
      result.add(new InfluencerImpl(target, this
              , LifeTimeUnit.TURN_START, occurrenceCount - 1
              , new BaseModifier(getContext(), title, description
              , SignOfInfluenceEnum.NEGATIVE
              , ModifierClass.POISON
              , AttributeEnum.HEALTH
              , minDamage, maxDamage
              , 100, 0)));
    }
    return result;
  }
  //===================================================================================================


}
