package core.entity.ability.base;

import api.core.Context;
import api.core.EventDataContainer;
import api.core.Result;
import api.entity.ability.Modifier;
import api.entity.warrior.Warrior;
import api.entity.warrior.WarriorSBaseAttributes;
import api.enums.AttributeEnum;
import api.enums.EventType;
import api.enums.ModifierClass;
import api.enums.SignOfInfluenceEnum;
import api.game.action.InfluenceResult;
import core.system.ResultImpl;

import static api.enums.EventType.WARRIOR_WAS_CONTR_ATTACKED_BY_TARGET;
import static api.enums.EventType.WARRIOR_WAS_SPELLCASTED_BY_WARRIOR;
import static api.enums.SignOfInfluenceEnum.NEGATIVE;
import static api.enums.SignOfInfluenceEnum.POSITIVE;

public class BaseModifier implements Modifier {

  protected String title;
  protected String description;
  protected AttributeEnum attribute;
  protected int probability;
  protected int minValue, maxValue;
  protected Context context;
  protected int calculatedValue;
  protected ModifierClass modifierClass;
  protected int luck;
  protected boolean luckyRollOfDice = false;
  protected boolean hitSuccess = false;
  protected SignOfInfluenceEnum signOfInfluence;
  protected boolean blocked = false;


  public BaseModifier() {
  }
  //===================================================================================================

  @Override
  public String getTitle() {
    return title;
  }
  //===================================================================================================

  @Override
  public String getDescription() {
    return description;
  }
  //===================================================================================================

  @Override
  public AttributeEnum getAttribute() {
    return attribute;
  }
  //===================================================================================================

  @Override
  public int getProbability() {
    return 0;
  }
  //===================================================================================================

  @Override
  public Result<Integer> getValue() {
    luckyRollOfDice = luck > 0 && luck >= context.getCore().getRandom(1, 100);
    hitSuccess = probability == 100 || context.getCore().getRandom(0, 100) <= probability;
    // вероятность попадания или удача
    if (hitSuccess || luckyRollOfDice) {
      calculatedValue = minValue == maxValue
              ? minValue
              : context.getCore().getRandom(minValue, maxValue);
      // если была удача, то удвоим его, либо сделаем результат максимальным, если удвенное значение менее максимума
      if (luckyRollOfDice) {
        calculatedValue *= 2;
        if (calculatedValue < maxValue) {
          calculatedValue = maxValue;
        }
      }
    } else {
      calculatedValue = 0;
    }
    return ResultImpl.success(calculatedValue);
  }
  //===================================================================================================

  @Override
  public int getMinValue() {
    return minValue;
  }
  //===================================================================================================

  @Override
  public int getMaxValue() {
    return maxValue;
  }
  //===================================================================================================

  public BaseModifier(Context context
          , String title, String description, SignOfInfluenceEnum signOfInfluence
          , ModifierClass modifierClass
          , AttributeEnum attribute, int minValue, int maxValue
          , int probability, int luck) {
    this.title = title;
    this.description = description;
    this.attribute = attribute;
    this.minValue = minValue;
    this.maxValue = maxValue;
    this.probability = probability;
    this.context = context;
    this.modifierClass = modifierClass;
    this.luck = luck;
    this.signOfInfluence = signOfInfluence;
    getValue();
  }
  //===================================================================================================


  @Override
  public Context getContext() {
    return context;
  }
  //===================================================================================================

  @Override
  public int getLastCalculatedValue() {
    return calculatedValue;
  }
  //===================================================================================================

  @Override
  public Modifier setLastCalculatedValue(int value) {
    this.calculatedValue = value;
    return this;
  }
  //===================================================================================================

  @Override
  public ModifierClass getModifierClass() {
    return modifierClass;
  }
  //===================================================================================================

  @Override
  public int getLuck() {
    return luck;
  }
  //===================================================================================================

  @Override
  public boolean isLuckyRollOfDice() {
    return luckyRollOfDice;
  }
  //===================================================================================================

  @Override
  public boolean isHitSuccess() {
    return hitSuccess;
  }
  //===================================================================================================

  @Override
  public Modifier addLuck(int delta) {
    luck += delta;
    return this;
  }
  //===================================================================================================

  private void innerApplyToWarrior(Warrior target) {
    WarriorSBaseAttributes attributes = target.getAttributes();
    switch (getAttribute()) {
      case HEALTH:
        attributes.addHealth(signOfInfluence.equals(POSITIVE) ? calculatedValue : -calculatedValue);
        break;
      case RANGED_ATTACK_LUCK:
        attributes.setLuckRangeAtack(attributes.getLuckRangeAtack()
                + (signOfInfluence.equals(POSITIVE) ? calculatedValue : -calculatedValue));
        break;
      case MELEE_ATTACK_LUCK:
        attributes.setLuckMeleeAtack(attributes.getLuckMeleeAtack()
                + (signOfInfluence.equals(POSITIVE) ? calculatedValue : -calculatedValue));
        break;
      case ATTACK_LUCK:
        attributes.setLuckMeleeAtack(attributes.getLuckMeleeAtack()
                + (signOfInfluence.equals(POSITIVE) ? calculatedValue : -calculatedValue));
        attributes.setLuckRangeAtack(attributes.getLuckRangeAtack()
                + (signOfInfluence.equals(POSITIVE) ? calculatedValue : -calculatedValue));
        break;
    }
  }
  //===================================================================================================

  @Override
  public Result<Modifier> applyModifier(InfluenceResult influenceResult) {
    // Уже всерассчитано. Применяем значение, рассчитанное заранее (getLastCalculatedValue())

    innerApplyToWarrior(influenceResult.getTarget());
    if (signOfInfluence.equals(NEGATIVE) && !attribute.equals(AttributeEnum.NO_ONE)) {
      // Отправим сообщение об атаке
        influenceResult.getTarget().getContext()
                .fireGameEvent(null
                        , influenceResult.getParentInfluence() == null
                                ? EventType.WARRIOR_WAS_ATTACKED_BY_ENEMY
                                : WARRIOR_WAS_CONTR_ATTACKED_BY_TARGET
                        , new EventDataContainer(influenceResult, this)
                        , null);
    } else if (signOfInfluence.equals(POSITIVE) && !attribute.equals(AttributeEnum.NO_ONE) && influenceResult.getAttackerSpell() != null) {
      influenceResult.getTarget().getContext()
              .fireGameEvent(null
                      , influenceResult.getActor() == null
                              ? EventType.WARRIOR_WAS_SPELLCASTED_BY_PLAYER
                              : WARRIOR_WAS_SPELLCASTED_BY_WARRIOR
                      , new EventDataContainer(influenceResult, this)
                      , null);
    }
    return ResultImpl.success(this);
  }
//===================================================================================================

  @Override
  public SignOfInfluenceEnum getSignOfInfluence() {
    return signOfInfluence;
  }
//===================================================================================================

  @Override
  public boolean isBlocked() {
    return blocked;
  }
//===================================================================================================

  @Override
  public Modifier block() {
    blocked = true;
    return this;
  }
//===================================================================================================

  @Override
  public boolean isSuccessful() {
    return !blocked && (hitSuccess || luckyRollOfDice);
  }
  //===================================================================================================

}
