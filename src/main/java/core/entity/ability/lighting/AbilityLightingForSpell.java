package core.entity.ability.lighting;

import api.core.Owner;
import api.entity.ability.Influencer;
import api.entity.warrior.Warrior;
import api.enums.*;
import core.entity.ability.base.AbstractAbilityImpl;
import core.entity.ability.base.BaseModifier;
import core.entity.warrior.base.InfluencerImpl;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * способность молнии. Для заклинаний
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AbilityLightingForSpell extends AbstractAbilityImpl{
  public static final String UNIT_NAME = "Молния";

  private int minDamage;
  private int maxDamage;

  public AbilityLightingForSpell(Owner owner, int minDamage, int maxDamage) {
    super(owner, -1, "AblLight_", UNIT_NAME, "Урон молнией"); // бесконечно

    this.minDamage = minDamage;
    this.maxDamage = maxDamage;

    ownerTypeForAbility = OwnerTypeEnum.WEAPON;
    targetType = TargetTypeEnum.ENEMY_WARRIOR;
    // нет ограничений по применению
    useCount.set(-1);
  }
  //===================================================================================================

  @Override
  protected List<Influencer> buildInfluencers(Warrior target) {
    List<Influencer> result = new ArrayList<>(1);
    result.add(new InfluencerImpl(target, this
            , LifeTimeUnit.JUST_NOW, 1
            , new BaseModifier(getContext(), title, description
            , SignOfInfluenceEnum.NEGATIVE
            , ModifierClass.MAGIC
            , AttributeEnum.HEALTH
            , minDamage, maxDamage
            , 100, 0)));
    return result;
  }
  //===================================================================================================


}
