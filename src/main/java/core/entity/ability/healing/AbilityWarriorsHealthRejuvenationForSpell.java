package core.entity.ability.healing;

import api.core.Owner;
import api.entity.ability.Influencer;
import api.entity.warrior.Warrior;
import api.enums.*;
import core.entity.ability.base.AbstractAbilityImpl;
import core.entity.ability.base.BaseModifier;
import core.entity.warrior.base.InfluencerImpl;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Способность лечения воина. для заклинаний
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AbilityWarriorsHealthRejuvenationForSpell extends AbstractAbilityImpl {

  private int minHealingPoints, maxHealingPoints;
  //===================================================================================================
  //===================================================================================================

  /**
   * @param owner               владелец способности
   * @param minHealingPoints    минимальное лечение жизни
   * @param maxHealingPoints    Максимальное лечение жизни
   *
   */
  public AbilityWarriorsHealthRejuvenationForSpell(Owner owner, int minHealingPoints, int maxHealingPoints) {
    super(owner, -1, "AblRej_", "лечение", "Лечение"); // бесконечно

    this.minHealingPoints = minHealingPoints;
    this.maxHealingPoints = maxHealingPoints;

    ownerTypeForAbility = OwnerTypeEnum.SPELL;
    targetType = TargetTypeEnum.THIS_WARRIOR;
    this.useCount.set(-1);
  }
  //===================================================================================================

  @Override
  protected List<Influencer> buildInfluencers(Warrior target) {
    List<Influencer> result = new ArrayList<>(1);
    result.add(new InfluencerImpl(target, this
            , LifeTimeUnit.JUST_NOW, 1
            , new BaseModifier(getContext(), title, description
            , SignOfInfluenceEnum.POSITIVE
            , ModifierClass.WEAPON
            , AttributeEnum.HEALTH
            , minHealingPoints, maxHealingPoints
            , 100, 0)));
    return result;
  }
  //===================================================================================================


}
