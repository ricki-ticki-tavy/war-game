package core.entity.ability.healing;

import api.core.Owner;
import api.entity.ability.Influencer;
import api.entity.warrior.Warrior;
import api.enums.*;
import core.entity.ability.base.AbstractAbilityImpl;
import core.entity.ability.base.BaseModifier;
import core.entity.warrior.base.InfluencerImpl;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Способность лечения, действующего в течение нескольких ходов
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AbilityLongTimeHealingForSpell extends AbstractAbilityImpl {

  public static final String UNIT_NAME = "Лечение";

  private int occurrenceCount;
  private int minHealing;
  private int maxHealing;
  //===================================================================================================
  //===================================================================================================

  /**
   * @param owner            владелец способности
   * @param occurrenceCount  кол-во раз, которые лечится цель, велючая момент выстрела
   * @param minHealing        минимальное лечение
   * @param maxHealing        максимальное лечение
   *
   */
  public AbilityLongTimeHealingForSpell(Owner owner, int occurrenceCount, int minHealing, int maxHealing) {
    super(owner, -1, "AblLongHealing_", UNIT_NAME, "Лечение"); // бесконечно

    this.occurrenceCount = occurrenceCount;
    this.minHealing = minHealing;
    this.maxHealing = maxHealing;

    ownerTypeForAbility = OwnerTypeEnum.SPELL;
    targetType = TargetTypeEnum.ALLIED_WARRIOR;
    this.useCount.set(-1);
  }
  //===================================================================================================

  @Override
  protected List<Influencer> buildInfluencers(Warrior target) {
    List<Influencer> result = new ArrayList<>(1);
    result.add(new InfluencerImpl(target, this
            , LifeTimeUnit.JUST_NOW, 1
            , new BaseModifier(getContext(), title, description
            , SignOfInfluenceEnum.POSITIVE
            , ModifierClass.MAGIC
            , AttributeEnum.HEALTH
            , minHealing, maxHealing
            , 100, 0)));
    if (occurrenceCount > 1) {
      result.add(new InfluencerImpl(target, this
              , LifeTimeUnit.TURN_START, occurrenceCount - 1
              , new BaseModifier(getContext(), title, description
              , SignOfInfluenceEnum.POSITIVE
              , ModifierClass.MAGIC
              , AttributeEnum.HEALTH
              , minHealing, maxHealing
              , 100, 0)));
    }
    return result;
  }
  //===================================================================================================


}
