package core.entity.artifact.player;

import api.entity.artifct.ArtifactClassInfo;
import api.enums.OwnerTypeEnum;
import api.game.map.Player;
import core.entity.artifact.base.AbstractArtifactImpl;
import core.entity.artifact.base.ArtifactClassInfoImpl;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

import static core.entity.ModelsGraphicInfo.ARTIFACT_CAPTAN_SABRE_FOR_PLAYER_CLASS;
import static core.entity.ModelsGraphicInfo.ARTIFACT_HEALING_FIAL_FOR_PLAYER_CLASS;

/**
 * Артефакт повышения урона . Для игрока
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ArtifactCapitansSabreForPlayer extends AbstractArtifactImpl<Player>{
  public static ArtifactClassInfo classInfo = new ArtifactClassInfoImpl(
          "Капитанская сабля"
          , "Повышает урон в рукопашной схватке"
          , ArtifactCapitansSabreForPlayer.class
          , 0
          , 0
          , ARTIFACT_CAPTAN_SABRE_FOR_PLAYER_CLASS);

  private int level;

  @Autowired
  BeanFactory beanFactory;

  @PostConstruct
  public void initAbilities(){
//    Ability luckForRangedAttackForWarrior = beanFactory.getBean(AbilityWarriorsHealthRejuvenationForArtifact.class, this, level, -1, -1);
//    this.abilities.put(luckForRangedAttackForWarrior.getTitle(), luckForRangedAttackForWarrior);

  }

  public ArtifactCapitansSabreForPlayer(Player owner){
    super(owner
            , OwnerTypeEnum.PLAYER
            , "Art_WRA"
            , classInfo.getName()
            , classInfo.getName());
    level = 0;
  }
}
