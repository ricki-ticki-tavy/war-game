package core.entity.artifact.base;

import api.entity.artifct.Artifact;
import api.entity.artifct.ArtifactClassInfo;
import api.entity.graphics.GraphicsInfo;
import api.entity.weapon.WeaponClassInfo;

public class ArtifactClassInfoImpl implements ArtifactClassInfo {
  private String name;
  private String description;
  private int goldCost;
  private int owningGoldCost;
  private Class<? extends Artifact> artifactClass;
  private GraphicsInfo graphicsInfo;

  public ArtifactClassInfoImpl(String name
          , String description
          , Class<? extends Artifact> artifactClass
          , int goldCost
          , int owningGoldCost
          , GraphicsInfo graphicsInfo) {
    this.name = name;
    this.description = description;
    this.artifactClass = artifactClass;
    this.goldCost = goldCost;
    this.owningGoldCost = owningGoldCost;
    this.graphicsInfo = graphicsInfo;
  }

  @Override
  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  @Override
  public int getGoldCost() {
    return goldCost;
  }

  @Override
  public int getOwningGoldCost() {
    return owningGoldCost;
  }

  public Class<? extends Artifact> getArtifactClass() {
    return artifactClass;
  }

  /**
   * Информация о графике для этого класса
   *
   * @return
   */
  public GraphicsInfo getGraphicsInfo() {
    return graphicsInfo;
  }

  ;


}
