package core.database;


import core.database.entity.security.Role;
import core.database.entity.security.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class DatabaseInitializer {
  private static final Logger logger = LoggerFactory.getLogger(DatabaseInitializer.class);

  public static final String ROLES_COUNTER = "select count(id) as cnt from Role";
  public static final String ROLES_ALL = "from Role";
  public static final String USERS_COUNTER = "select count(id) as cnt from User";

  public static final String[] roleNames = new String[]{Role.ROLE_ADMIN_NAME, Role.ROLE_PLAYER_NAME};

  @Autowired
  SessionFactory sessionFactory;

  @Transactional(Transactional.TxType.REQUIRED)
  public void initDatabase() {
    Session session = sessionFactory.getCurrentSession();
    Set<Role> roles = new HashSet<>();

    if ((Long) session.createQuery(ROLES_COUNTER).uniqueResult() == 0) {
      // инитим роли
      logger.warn("Роли пусты. Заполнение справочника ролей");
      Arrays.stream(roleNames).forEach(roleName -> {
        Role role = new Role(roleName);
        role.setId((Long) session.save(role));
        roles.add(role);
      });
      session.flush();
    } else {
      // грузанем все роли
      roles.addAll(session.createQuery(ROLES_ALL).list());
    }

    if ((Long) session.createQuery(USERS_COUNTER).uniqueResult() == 0) {
      // инитим пользователя-админа
      logger.warn("Пользователи пусты. Создадим администратора");
//      User player = new User("admin", "YjdsqGfhjkm2019");
      User player = new User("admin", "1234").seteMail("admin@admin.ru");
      player.setRoles(roles);
      session.save(player);
      session.flush();
    }
  }
}
