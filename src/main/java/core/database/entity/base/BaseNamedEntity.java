package core.database.entity.base;

import javax.persistence.*;

/**
 * Базовый класс для именованных сущностей
 */
@MappedSuperclass
public class BaseNamedEntity {
  private Long id;
  private String name;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public Long getId() {
    return id;
  }

  public BaseNamedEntity setId(Long id) {
    this.id = id;
    return this;
  }

  @Column(nullable = false, length = 64)
  public String getName() {
    return name;
  }

  public BaseNamedEntity setName(String name) {
    this.name = name;
    return this;
  }

  public BaseNamedEntity(String name){
    this.name = name;
  }

  public BaseNamedEntity(){

  }

}
