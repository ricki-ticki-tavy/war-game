package core.database.entity.security;

import core.database.entity.base.BaseNamedEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Entity;

/**
 * Сущность роли в базе
 */
@Entity
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class Role extends BaseNamedEntity {

  public static final String ROLE_PLAYER_NAME = "player";
  public static final String ROLE_ADMIN_NAME = "admin";


  public Role(String name) {
    super(name);
  }

  public Role() {
    super();
  }
}
