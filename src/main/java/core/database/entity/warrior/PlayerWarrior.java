package core.database.entity.warrior;

import core.database.entity.base.BaseNamedEntity;
import core.database.entity.security.User;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;

/**
 * сущности воинов. Каждый воин может иметь несколько видов омуниции
 */
@Entity
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class PlayerWarrior extends BaseNamedEntity {

  private User player;
  private Long expirience;
  private Integer deathCount;
  private Set<WarriorEquipment> warriorEquipments;
  private String warriorBaseClassName;

  //----------------------------------------------------------

  public PlayerWarrior(){
    deathCount = 0;
    expirience = 0L;
    warriorEquipments = new HashSet<>();
  }
  //----------------------------------------------------------

  public PlayerWarrior(User entityPlayer, String warriorBaseClassName, String name) {
    this();
    this.warriorBaseClassName = warriorBaseClassName;
    this.player = entityPlayer;
    this.setName(name);
  }
  //----------------------------------------------------------
  //----------------------------------------------------------

  /**
   * игрок, которому принадлежит юнит
   * @return
   */
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  public User getPlayer() {
    return player;
  }

  public void setPlayer(User player) {
    this.player = player;
  }
  //----------------------------------------------------------

  /**
   * Опыт юнита,накопленный с последней смерти
   * @return
   */
  public Long getExpirience() {
    return expirience;
  }

  public void setExpirience(Long expirience) {
    this.expirience = expirience;
  }
  //----------------------------------------------------------

  /**
   * счетчик смертей
   */
  public Integer getDeathCount() {
    return deathCount;
  }

  public void setDeathCount(Integer deathCount) {
    this.deathCount = deathCount;
  }
  //----------------------------------------------------------

  /**
   * варианты омуниции для этого воина
   * @return
   */
  @OneToMany(orphanRemoval = true, mappedBy = "warrior",  fetch = FetchType.EAGER)
  public Set<WarriorEquipment> getWarriorEquipments() {
    return warriorEquipments;
  }

  public void setWarriorEquipments(Set<WarriorEquipment> warriorEquipments) {
    this.warriorEquipments = warriorEquipments;
  }
  //----------------------------------------------------------

  /**
   * Название базового класса воина
   * @return
   */
  public String getWarriorBaseClassName() {
    return warriorBaseClassName;
  }

  public void setWarriorBaseClassName(String warriorBaseClassName) {
    this.warriorBaseClassName = warriorBaseClassName;
  }
  //----------------------------------------------------------


}
