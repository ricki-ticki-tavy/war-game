package core.database.entity.warrior;

import core.database.entity.base.BaseNamedEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

/**
 * Вариант омуниции воина. Хранится в сериализованном в JSON виде для десериализации
 */
@Entity
public class WarriorEquipment extends BaseNamedEntity {
  private int cost;
  private String serializedJson;
  private PlayerWarrior warrior;
  //----------------------------------------------------------
  //----------------------------------------------------------

  public WarriorEquipment(){
    cost = 0;
  }
  //----------------------------------------------------------

  public WarriorEquipment(PlayerWarrior entityWarrior, String name, String serializedJson){
    this();
    this.setName(name);
    this.warrior = entityWarrior;
    this.serializedJson = serializedJson;
    // добавим набор аммуниции в наборы аммуниции дляэтого воина.
    entityWarrior.getWarriorEquipments().add(this);
  }
  //----------------------------------------------------------

  /**
   * Последняя стоимость амуниции
   * @return
   */
  public int getCost() {
    return cost;
  }

  public void setCost(int cost) {
    this.cost = cost;
  }
  //----------------------------------------------------------

  /**
   * сериализованное представление вооруженного воина
   * @return
   */
  @Column(length = 1024)
  public String getSerializedJson() {
    return serializedJson;
  }

  public void setSerializedJson(String serializedJson) {
    this.serializedJson = serializedJson;
  }
  //----------------------------------------------------------

  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  public PlayerWarrior getWarrior() {
    return warrior;
  }

  public void setWarrior(PlayerWarrior warrior) {
    this.warrior = warrior;
  }
  //----------------------------------------------------------
}
