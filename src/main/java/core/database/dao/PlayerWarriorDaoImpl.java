package core.database.dao;

import api.core.Result;
import api.core.database.dao.PlayerWarriorDao;
import api.core.database.dao.SecurityDao;
import api.entity.warrior.Warrior;
import api.game.map.Player;
import core.database.dao.base.AbstractDao;
import core.database.entity.warrior.PlayerWarrior;
import core.database.entity.warrior.WarriorEquipment;
import core.system.ResultImpl;
import core.system.error.GameErrors;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

import static core.system.ResultImpl.fail;
import static core.system.ResultImpl.success;

/**
 * доступ к данным воинов игрока в БД
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PlayerWarriorDaoImpl extends AbstractDao implements PlayerWarriorDao {

  private static final String GET_ALL_WARRIORS_QUERY = "from PlayerWarrior ew where ew.player.name = :userName order by ew.id";
  private static final String GET_WARRIOR_FOR_USER_ID_AND_NAME_QUERY = "from PlayerWarrior ew where ew.player.id = :userId and ew.name = :warriorName order by ew.id";
  private static final String GET_WARRIOR_FOR_USER_NAME_AND_NAME_QUERY = "from PlayerWarrior ew where ew.player.name = :userName and ew.name = :warriorName  order by ew.id";

  @Autowired
  SecurityDao securityDao;

  @Override
  public Result<List<PlayerWarrior>> findAllWarriors(String userName) {
    return success(true)
            .mapSafe(o -> success(getSession().createQuery(GET_ALL_WARRIORS_QUERY)
                    .setParameter("userName", userName)
                    .list()));
  }

  @Override
  public Result<PlayerWarrior> findHeroByName(String userName, String heroName) {
    return success(true)
            .mapSafe(o -> success(getSession().createQuery(GET_WARRIOR_FOR_USER_NAME_AND_NAME_QUERY)
                    .setParameter("userName", userName)
                    .setParameter("warriorName", heroName)
                    .uniqueResult()));
  }

  @Override
  public Result<WarriorEquipment> saveWarriorEquipment(Player player, Warrior warrior, String warriorEquipmentName) {
    return success(true)
            .mapSafe(o -> {
              final Session session = getSession();
              // Найдем запись воине
              PlayerWarrior entityWarrior = (PlayerWarrior) session.createQuery(GET_WARRIOR_FOR_USER_ID_AND_NAME_QUERY)
                      .setParameter("userId", player.getDatabaseId())
                      .setParameter("warriorName", warrior.getTitle())
                      .uniqueResult();
              return (entityWarrior == null
                      ? ResultImpl.fail(GameErrors.SYSTEM_RUNTIME_ERROR.getError("fake"))
                      : ResultImpl.success(entityWarrior))
                      .mapFail(o1 -> // в базе еще нет структуры данного воина
                              // Создадим ее. Найдем запись плеера
                              securityDao.loadByName(player.getId())
                                      .map(user -> {
                                        // Создадим запись воина в БД
                                        PlayerWarrior newPlayerWarrior = new PlayerWarrior(user, warrior.getWarriorBaseClass().getTitle(), warrior.getTitle());
                                        newPlayerWarrior.setId((Long) session.save(newPlayerWarrior));
                                        return success(newPlayerWarrior);
                                      }))
                      // теперь сохраним саму конфигурацию аммуниции
                      .map(presentPlayerWarrior -> {
                        // если есть набор аммуниции с таким названием, то заменим его
                        for (WarriorEquipment entityWarriorEquipment : ((PlayerWarrior) presentPlayerWarrior).getWarriorEquipments()) {
                          if (entityWarriorEquipment.getName().equalsIgnoreCase(warriorEquipmentName)) {
                            entityWarrior.getWarriorEquipments().remove(entityWarriorEquipment);
                            session.remove(entityWarriorEquipment);
                            break;
                          }
                        }

                        WarriorEquipment warriorEquipment =
                                new WarriorEquipment((PlayerWarrior) presentPlayerWarrior
                                        , warriorEquipmentName
                                        , warrior.serializeEquipmentToJson());
                        session.saveOrUpdate(warriorEquipment);
                        session.saveOrUpdate(presentPlayerWarrior);
                        session.flush();
                        return success(warriorEquipment);
                      });
            });
  }
  //===================================================================================================================

  @Override
  public Result<WarriorEquipment> renameWarriorEquipment(Player player, Warrior warrior, String oldEquipmentName, String newEquipmentName) {
    return success(true)
            .mapSafe(o -> {
              final Session session = getSession();
              // Найдем запись воине
              PlayerWarrior entityWarrior = (PlayerWarrior) session.createQuery(GET_WARRIOR_FOR_USER_ID_AND_NAME_QUERY)
                      .setParameter("userId", player.getDatabaseId())
                      .setParameter("warriorName", warrior.getTitle())
                      .uniqueResult();

              return (entityWarrior == null
                      // "Игра %s (id %s) игрок %s не имеет воина с именем %s"
                      ? ResultImpl.fail(GameErrors.WARRIOR_HERO_NOT_FOUND_AT_PLAYER_BY_NAME.getError(player.getContext().getGameName()
                      , player.getContext().getContextId()
                      , player.getId()
                      , warrior.getTitle()))
                      : ResultImpl.success(entityWarrior))
                      // воин есть точно. Найдем конфигурацию
                      .map(presentPlayerWarrior -> {

                        WarriorEquipment foundEquipment = null;
                        // если есть набор аммуниции с таким названием, то заменим его имя
                        for (WarriorEquipment entityWarriorEquipment : ((PlayerWarrior) presentPlayerWarrior).getWarriorEquipments()) {
                          if (entityWarriorEquipment.getName().equalsIgnoreCase(oldEquipmentName)) {
                            // нашли нашу экипировку

                            // проветим, есть ли другая экипировка с новым названием
                            if (((PlayerWarrior) presentPlayerWarrior).getWarriorEquipments().stream()
                                    .anyMatch(warriorEquipment -> warriorEquipment.getName().equalsIgnoreCase(newEquipmentName)
                                            && warriorEquipment != entityWarriorEquipment)) {
                              // есть уже экипировка с таким именем и она не та, что переименовывается
                              // "Игра %s (id %s) игрок %s уже имеет экипировку \"%s\" у воина \"%s\""
                              return fail(GameErrors.WARRIOR_HERO_EQUIPMENT_DUPLICATE_BY_NAME.getError(player.getContext().getGameName()
                                      , player.getContext().getContextId()
                                      , player.getId()
                                      , newEquipmentName
                                      , warrior.getTitle()));
                            }

                            entityWarriorEquipment.setName(newEquipmentName);
                            session.saveOrUpdate(entityWarriorEquipment);
                            session.saveOrUpdate(presentPlayerWarrior);
                            session.flush();
                            foundEquipment = entityWarriorEquipment;
                            break;
                          }
                        }

                        return (foundEquipment != null
                                ? success(foundEquipment)
                                // "Игра %s (id %s) игрок %s не имеет экипировки \"%s\" у воина с \"%s\""
                                : fail(GameErrors.WARRIOR_HERO_EQUIPMENT_NOT_FOUND_BY_NAME.getError(player.getContext().getGameName()
                                , player.getContext().getContextId()
                                , player.getId()
                                , oldEquipmentName
                                , warrior.getTitle()))
                        );
                      });
            });
  }
  //===================================================================================================================


  @Override
  public Result<WarriorEquipment> removeWarriorEquipment(Player player, Warrior warrior, String equipmentName) {
    return success(true)
            .mapSafe(o -> {
              final Session session = getSession();
              // Найдем запись воине
              PlayerWarrior entityWarrior = (PlayerWarrior) session.createQuery(GET_WARRIOR_FOR_USER_ID_AND_NAME_QUERY)
                      .setParameter("userId", player.getDatabaseId())
                      .setParameter("warriorName", warrior.getTitle())
                      .uniqueResult();

              return (entityWarrior == null
                      // "Игра %s (id %s) игрок %s не имеет воина с именем %s"
                      ? ResultImpl.fail(GameErrors.WARRIOR_HERO_NOT_FOUND_AT_PLAYER_BY_NAME.getError(player.getContext().getGameName()
                      , player.getContext().getContextId()
                      , player.getId()
                      , warrior.getTitle()))
                      : ResultImpl.success(entityWarrior))
                      // воин есть точно. Найдем конфигурацию
                      .map(presentPlayerWarrior -> {

                        WarriorEquipment foundEquipment = null;
                        // если есть набор аммуниции с таким названием, то заменим его имя
                        for (WarriorEquipment entityWarriorEquipment : ((PlayerWarrior) presentPlayerWarrior).getWarriorEquipments()) {
                          if (entityWarriorEquipment.getName().equalsIgnoreCase(equipmentName)) {
                            // нашли нашу экипировку
                            // удалим ее
                            session.remove(entityWarriorEquipment);
                            ((PlayerWarrior) presentPlayerWarrior).getWarriorEquipments().remove(entityWarriorEquipment);
                            // если экипировка была последней, то удалим воина тоже
                            // TODO сделать удаление опциональным
                            if (((PlayerWarrior) presentPlayerWarrior).getWarriorEquipments().size() == 0) {
                              session.remove(presentPlayerWarrior);
                            } else {
                              session.saveOrUpdate(presentPlayerWarrior);
                            }
                            session.flush();
                            foundEquipment = entityWarriorEquipment;
                            break;
                          }
                        }

                        return (foundEquipment != null
                                ? success(foundEquipment)
                                // "Игра %s (id %s) игрок %s не имеет экипировки \"%s\" у воина с \"%s\""
                                : fail(GameErrors.WARRIOR_HERO_EQUIPMENT_NOT_FOUND_BY_NAME.getError(player.getContext().getGameName()
                                , player.getContext().getContextId()
                                , player.getId()
                                , equipmentName
                                , warrior.getTitle()))
                        );
                      });
            });
  }
}
