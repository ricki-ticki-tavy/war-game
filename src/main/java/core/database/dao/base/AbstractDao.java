package core.database.dao.base;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Абстрактный DAO
 */
public abstract class AbstractDao {
  @Autowired
  protected SessionFactory sessionFactory;

  /**
   * Получитьтекущую сессию
   * @return
   */
  protected Session getSession(){
    return sessionFactory.getCurrentSession();
  }
}
