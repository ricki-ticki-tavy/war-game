package core.database.service;

import api.core.MailService;
import api.core.Result;
import api.core.database.dao.SecurityDao;
import api.core.database.service.UserService;
import core.database.entity.security.Role;
import core.database.entity.security.User;
import core.database.entity.security.UserAccountOperationRequest;
import core.system.error.GameErrors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static core.system.ResultImpl.fail;
import static core.system.ResultImpl.success;

/**
 * реализация класса работы спользователями в БД
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Transactional(Transactional.TxType.REQUIRED)
@Profile("!mockDb")
public class UserServiceImpl implements UserService {

  @Autowired
  SecurityDao securityDao;

  @Autowired
  MailService mailService;

  @Value("${core.security.register.letterTemplateName:registerLetter.html}")
  String registerTemplateName;

  @Value("${core.security.register.letterSubject:Регистрация на WAR-GAME.BIZ}")
  String registerSubject;

  @Value("${core.security.restoreUserAccount.letterTemplateName:restoreAccountPhase1Letter.html}")
  String restoreUserAccountTemplateName;

  @Value("${core.security.restoreUserAccount.letterSubject:Восстановление доступа на сайте WAR-GAME.BIZ}")
  String restoreUserAccountSubject;

  @Value("${core.security.accountPasswordChanged.letterTemplateName:passwordChangedLetter.html}")
  String accountPasswordChangedTemplateName;

  @Value("${core.security.accountPasswordChanged.letterSubject:Восстановление доступа на сайте WAR-GAME.BIZ. Пароль сменен}")
  String accountPasswordChangedSubject;

  @Override
  public Result<User> loadByName(String userName) {
    return securityDao.loadByName(userName);
  }
  //========================================================================================

  @Override
  public Result<UserAccountOperationRequest> registerUser(String userName, String password, String replyPassword, String eMail) {
    // проверка паролей
    return StringUtils.isEmpty(password.trim())
            || StringUtils.isEmpty(replyPassword.trim())
            || !password.equals(replyPassword)
            ? fail(GameErrors.USER_PASSWORDS_NOT_EQUALS.getError())
            : success(true)
            // заполненность имени пользователя
            .map(o -> StringUtils.isEmpty(userName.trim())
                    ? fail(GameErrors.USER_NAME_IS_EMPTY.getError())
                    : success(true))
            // заполненность электронной почты
            .map(o -> StringUtils.isEmpty(eMail.trim())
                    ? fail(GameErrors.USER_EMAIL_IS_EMPTY.getError())
                    : success(true))
            // проверка на повтор имени пользователя
            .map(o -> securityDao.findLiveRegisterUserRequestByUsername(userName))
            .map(o -> o != null
                    ? fail(GameErrors.USER_NAME_ALREADY_USED.getError(userName))
                    : success(true))
            .map(o -> securityDao.findUserByName(userName))
            .map(o -> o != null
                    ? fail(GameErrors.USER_NAME_ALREADY_USED.getError(userName))
                    : success(true))
            // проверка на повтор использования почты
            .map(o -> securityDao.findLiveRegisterUserRequestByeMail(eMail))
            .map(o -> o != null
                    ? fail(GameErrors.USER_EMAIL_ALREADY_USED.getError(eMail))
                    : success(true))
            .map(o -> securityDao.findUserByeMail(eMail))
            .map(o -> o != null
                    ? fail(GameErrors.USER_EMAIL_ALREADY_USED.getError(eMail))
                    : success(true))
            // проверки пройдены. Создадим запись о регистрации
            .map(o -> securityDao.saveRegisterUserRequest(userName, password, eMail))
            // отправим письмо на почту
            .map(o -> {
              Map<String, String> params = new HashMap<>(1);
              params.put("confirmCode", ((UserAccountOperationRequest) o).getConfirmCode());
              params.put("userName", ((UserAccountOperationRequest) o).getName());
              return mailService.sendTemplateMail(eMail, registerSubject, registerTemplateName, params)
                      .map(s -> success(o));
            });
  }
  //========================================================================================

  @Override
  public Result<User> confirmRegisterUser(String confirmCode) {
    return securityDao.findLiveRegisterUserRequestByConfirmCode(confirmCode)
            .map(registerUserRequest -> ((Result<UserAccountOperationRequest>) (registerUserRequest == null
                    ? fail(GameErrors.USER_CONFIRM_CODE_IS_OUTDATED.getError())
                    : success(registerUserRequest))))
            // ссылка действительна.
            .map(registerUserRequest -> securityDao.findRoleByName(Role.ROLE_PLAYER_NAME)
                    .map(role ->
                            // Создадим пользователя на основе записи запроса
                            securityDao.saveUser(((User) new User()
                                    .setName(registerUserRequest.getName()))
                                    .setPasswordBlake2(registerUserRequest.getPasswordBlacke2())
                                    .setRoles(Collections.singleton(role))
                                    .seteMail(registerUserRequest.geteMail()))
                                    .map(user ->
                                            // удалим ненужную теперь запись о регистрации
                                            securityDao.removeRegisterUserRequest(registerUserRequest)
                                                    .map(registerUserRequest1 -> success(user)))
                    )

            );
  }
  //========================================================================================

  @Override
  public Result<UserAccountOperationRequest> restoreAccountAccess(String userName, String eMail) {
    // или логин или почта должны быть заполнены
    return (StringUtils.isEmpty(userName) && StringUtils.isEmpty(eMail)
            ? fail(GameErrors.USER_RESTORE_ACCOUNT_FIELDS_ARE_EMPTY.getError())
            : success(true))
            .map(o ->
                    // Если заполнен логин, то ищем пользователя с заданным логином
                    !StringUtils.isEmpty(userName)
                            ? securityDao.findUserByName(userName)
                            // иначе ищем пользователя по почте
                            : securityDao.findUserByeMail(eMail)
            )
            // провеим, что юзер найден
            .map(foundUser -> foundUser == null
                    ? fail(GameErrors.USER_RESTORE_ACCOUNT_DOES_NOT_EXISTS.getError())
                    : success( foundUser)
            )
            // сохраним запись
            .map(foundUser -> securityDao.saveRestoreUserAccountAccess((User)foundUser))
            // отправим на почту письмо
            .map(request -> {
              Map<String, String> params = new HashMap<>(1);
              params.put("confirmCode", ((UserAccountOperationRequest) request ).getConfirmCode());
              params.put("userName", ((UserAccountOperationRequest) request ).getName());
              return mailService.sendTemplateMail(((UserAccountOperationRequest) request).geteMail()
                      , restoreUserAccountSubject
                      , restoreUserAccountTemplateName
                      , params)
                      .map(s -> success(request));
            });

  }
  //========================================================================================

  @Override
  public Result<User> restoreAccountAccessSetNewPassword(String confirmCode, String password, String retypePassword) {
    // проверка паролей
    return (StringUtils.isEmpty(password)
            || StringUtils.isEmpty(retypePassword)
            || !retypePassword.equals(password)

            ? fail(GameErrors.USER_PASSWORDS_NOT_EQUALS.getError())
            : success(true))
            .map(o ->
                    //найдем запрос на смену пароля
                    securityDao.findLiveRestoreUserAccountRequestByConfirmCode(confirmCode)
            )
            // провеим, что запись найдена
            .map(foundRequest -> foundRequest == null
                    ? fail(GameErrors.USER_CONFIRM_CODE_IS_OUTDATED.getError())
                    : success( foundRequest)
            )
            // сменим парроль
            .map(foundRequest -> securityDao.setNewPasswordForUserName(((UserAccountOperationRequest)foundRequest).getName(), password, confirmCode))
            // отправим на почту письмо
            .map(user -> {
              Map<String, String> params = new HashMap<>(1);
              params.put("userName", (((User)user).getName()));
              return mailService.sendTemplateMail(((User)user).geteMail()
                      , accountPasswordChangedSubject
                      , accountPasswordChangedTemplateName
                      , params)
                      .map(s -> success(((User)user)));
            });
  }
  //========================================================================================

}
