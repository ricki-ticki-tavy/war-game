package core.database.service;

import api.core.Result;
import api.core.database.dao.PlayerWarriorDao;
import api.core.database.service.PlayerWarriorService;
import api.entity.warrior.Warrior;
import api.game.map.Player;
import core.database.entity.warrior.PlayerWarrior;
import core.database.entity.warrior.WarriorEquipment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Реализация сервиса работы с воинами игроков в БД
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Transactional(Transactional.TxType.REQUIRED)
@Profile("!mockDb")
public class PlayerWarriorServiceImpl implements PlayerWarriorService {

  @Autowired
  PlayerWarriorDao playerWarriorDao;

  @Override
  public Result<List<PlayerWarrior>> findAllHeroes(String userName) {
    return playerWarriorDao.findAllWarriors(userName);
  }

  @Override
  public Result<PlayerWarrior> findHeroByName(String userName, String heroName) {
    return playerWarriorDao.findHeroByName(userName, heroName);
  }

  @Override
  public Result<WarriorEquipment> saveWarriorEquipment(Player player, Warrior warrior, String warriorEquipmentName) {
    return playerWarriorDao.saveWarriorEquipment(player, warrior, warriorEquipmentName);
  }

  @Override
  public Result<WarriorEquipment> renameWarriorEquipment(Player player, Warrior warrior, String oldEquipmentName, String newEquipmentName) {
    return playerWarriorDao.renameWarriorEquipment(player, warrior, oldEquipmentName, newEquipmentName);
  }

  @Override
  public Result<WarriorEquipment> removeWarriorEquipment(Player player, Warrior warrior, String equipmentName) {
    return playerWarriorDao.removeWarriorEquipment(player, warrior, equipmentName);
  }
}
