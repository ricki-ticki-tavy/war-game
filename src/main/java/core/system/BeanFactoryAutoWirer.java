package core.system;

import api.core.Core;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * класс для получения BeanFactory в статических методах
 */
@Component
public class BeanFactoryAutoWirer {

  @Autowired
  public BeanFactory beanFactory;

  @Autowired
  public Core core;
}
