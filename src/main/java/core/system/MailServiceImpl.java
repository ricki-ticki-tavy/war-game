package core.system;


import api.core.MailService;
import api.core.Result;
import core.system.error.GameErrors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.internet.MimeMessage;
import javax.transaction.Transactional;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import static core.system.ResultImpl.fail;
import static core.system.ResultImpl.success;

/**
 * Сервис отправки или получения почты
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Transactional(Transactional.TxType.REQUIRED)
public class MailServiceImpl implements MailService {

  public static final String MAIL_TEMPLATES_PREFIX = "templates/mail/";

  @Autowired
  private JavaMailSender mailSender;

  @Value("${spring.mail.from.server}")
  String mailServerFrom;

  @Value("${spring.mail.username}")
  String mailUserName;

  @Override
  public Result<String> sendMail(String recipient, String subject, String body) {
    try {
      MimeMessage message = mailSender.createMimeMessage();
      // Enable the multipart flag!
      MimeMessageHelper helper = new MimeMessageHelper(message, true);
      helper.setTo(recipient);
      helper.setText(body, true);
      helper.setSubject(subject);
      helper.setFrom(mailServerFrom);
//      helper.setFrom(mailUserName + "@" + mailServerFrom);
//      ClassPathResource file = new ClassPathResource("cat.jpg");
//      helper.addInline("id101", file);
      mailSender.send(message);
      return success(body);
    } catch (Throwable th) {
      return fail(GameErrors.EMAIL_SEND_ERROR.getError(th.getLocalizedMessage()));
    }
  }
  //=========================================================================================


  @Override
  public Result<String> sendTemplateMail(String recipient, String subject, String templateName, Map<String, String> params) {
    String templateBody;

    // Загрузим шаблон. Они хранятся все в UTF-8
    try(InputStream is = this.getClass().getClassLoader().getResourceAsStream(MAIL_TEMPLATES_PREFIX + templateName)){
      byte[] buffer = new byte[is.available()];
      is.read(buffer);
      templateBody = new String(buffer, "UTF-8");
    } catch (IOException ie){
      return fail(GameErrors.EMAIL_LETTER_TEMPLATE_READ_ERROR.getError(ie.getLocalizedMessage()));
    }

    // Проведем замену переменных значениями
    for (String paramName : params.keySet()){
      templateBody = templateBody.replace("${" + paramName + "}", params.get(paramName));
    }

    // Отправим письмо
    return sendMail(recipient, subject, templateBody);
  }
}
// war-game.biz@yandex.ru
