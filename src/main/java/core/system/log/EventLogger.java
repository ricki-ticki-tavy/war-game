package core.system.log;

import api.core.Context;
import api.core.Event;
import api.core.Result;
import api.dto.artifact.ArtifactDto;
import api.dto.warrior.WarriorDto;
import api.dto.weapon.WeaponDto;
import api.entity.ability.Influencer;
import api.entity.ability.Modifier;
import api.entity.artifct.Artifact;
import api.entity.warrior.Warrior;
import api.entity.weapon.Weapon;
import api.game.action.InfluenceResult;
import api.game.map.Player;
import core.game.CoreImpl;
import core.system.error.GameErrors;
import core.system.log.messages.ContextEventMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import static web.socket.controller.WebSocketsController.CONTEXT_PUBLIC_MESSAGES_CHANNEL;
import static web.socket.controller.WebSocketsController.CONTEXT_PRIVATE_MESSAGES_CHANNEL;
import static web.socket.controller.WebSocketsController.TOPIC_PREFIX;

/**
 * Логирование игровых событий
 */
@Component
public class EventLogger {

  public static final String WARRIOR_PARAM = "warrior";
  public static final String ARTIFACT_PARAM = "artifact";
  public static final String PLAYER_PARAM = "player";
  public static final String WEAPON_PARAM = "weapon";


  @Autowired
  private SimpMessagingTemplate simpMessagingTemplate;

  public Event logGameEvent(Event event) {
    switch (event.getEventType()) {
      case ROUND_FULL: {
        // "В игре %s (id %s) завершился игровой круг"
        logger.info(event.getEventType().getFormattedMessage(
                event.getSourceContext().getGameName()
                , event.getSourceContext().getContextId()));
        break;
      }
      case PLAYER_LOGGED_IN: {
        // "Вход игрока %s. %s"
        logger.info(event.getEventType().getFormattedMessage(event.getSource(Player.class).getTitle()
                , event.getSource(Result.class).toString()));
        break;
      }
      case PLAYER_CONNECTED: {
        // "Игрок '%s' присоединился к игре (контекст '%s'). Теперь в игре %s игроков из %s. %s"
        logger.info(event.getEventType().getFormattedMessage(
                event.getSource(Player.class).getTitle()
                , event.getSourceContext().getContextId()
                , String.valueOf(event.getSourceContext().getLevelMap().getPlayers().size())
                , String.valueOf(event.getSourceContext().getLevelMap().getMaxPlayerCount())
                , event.getSource(Result.class).toString()));
        if (!event.getSourceContext().isTechnical()) {
          simpMessagingTemplate.convertAndSend(TOPIC_PREFIX + CONTEXT_PUBLIC_MESSAGES_CHANNEL
                  , new ContextEventMessage(event, null)
                          .addParameter(PLAYER_PARAM, event.getSource(Player.class).getId()));
        }
        break;
      }
      case PLAYER_RECONNECTED: {
        // "Игрок '%s' повторно подключился к игре (контекст '%s'). В игре %s игроков из %s. %s"
        logger.info(event.getEventType().getFormattedMessage(
                event.getSource(Player.class).getTitle()
                , event.getSourceContext().getContextId()
                , String.valueOf(event.getSourceContext().getLevelMap().getPlayers().size())
                , String.valueOf(event.getSourceContext().getLevelMap().getMaxPlayerCount())
                , event.getSource(Result.class).toString()));
        if (!event.getSourceContext().isTechnical()) {
          simpMessagingTemplate.convertAndSend(TOPIC_PREFIX + CONTEXT_PUBLIC_MESSAGES_CHANNEL
                  , new ContextEventMessage(event, null)
                          .addParameter(PLAYER_PARAM, event.getSource(Player.class).getId()));
        }
        break;
      }
      case PLAYER_DISCONNECTED: {
        // "Игрок '%s' покинул игру (контекст '%s'). Теперь в игре %s игроков из %s. %s"
        logger.info(event.getEventType().getFormattedMessage(
                event.getSource(Player.class).getTitle()
                , event.getSourceContext().getContextId()
                , String.valueOf(event.getSourceContext().getLevelMap().getPlayers().size())
                , String.valueOf(event.getSourceContext().getLevelMap().getMaxPlayerCount())
                , event.getSource(Result.class).toString()));
        if (!event.getSourceContext().isTechnical()) {
          simpMessagingTemplate.convertAndSend(TOPIC_PREFIX + CONTEXT_PUBLIC_MESSAGES_CHANNEL
                  , new ContextEventMessage(event, null)
                          .addParameter(PLAYER_PARAM, event.getSource(Player.class).getId()));
        }
        break;
      }
      case PLAYER_CHANGED_ITS_READY_TO_PLAY_STATUS: {
        // "Игрок '%s' в игре %s (контекст '%s') сообщил о %s."
        logger.info(event.getEventType().getFormattedMessage(
                event.getSource(Player.class).getId()
                , event.getSourceContext().getGameName()
                , event.getSourceContext().getContextId()
                , event.getSource(Player.class).isReadyToPlay() ? "готовности" : "продолжении подготовки"));
        simpMessagingTemplate.convertAndSend(TOPIC_PREFIX + CONTEXT_PUBLIC_MESSAGES_CHANNEL
                , new ContextEventMessage(event, null)
                        .addParameter(PLAYER_PARAM, event.getSource(Player.class).getId()));
        break;
      }
      case PLAYER_LOOSE_TURN:
      case PLAYER_TAKES_TURN:
        //"Игрок '%s' в игре %s (контекст '%s') завершил ход."
        logger.info(event.getEventType().getFormattedMessage(
                event.getSource(Player.class).getTitle()
                , event.getSourceContext().getGameName()
                , event.getSourceContext().getContextId()));
        simpMessagingTemplate.convertAndSend(TOPIC_PREFIX + CONTEXT_PRIVATE_MESSAGES_CHANNEL + event.getSourceContext().getContextId()
                , new ContextEventMessage(event, null, false)
                        .addParameter(PLAYER_PARAM, event.getSource(Player.class).getId()));
        break;
      case PLAYER_LOOSE_THE_MATCH:
        // "В игре %s игрок %s потерял всех бойцов, и выбыл из игры"
      case PLAYER_WINS_THE_MATCH:
        // "В игре %s игрок %s  победил всех соперников и выиграл матч"
        logger.info(event.getEventType().getFormattedMessage(
                event.getSourceContext().getGameName()
                , event.getSource(Player.class).getTitle()));
        simpMessagingTemplate.convertAndSend(TOPIC_PREFIX + CONTEXT_PUBLIC_MESSAGES_CHANNEL
                , new ContextEventMessage(event, null)
                        .addParameter(PLAYER_PARAM, event.getSource(Player.class).getId()));
        break;

      case PLAYER_DECAMPED_FROM_ARENA:
      // "В игре %s игрок %s сдался, и выбыл из игры"
      logger.info(event.getEventType().getFormattedMessage(
              event.getSourceContext().getGameName()
              , event.getSource(Player.class).getId()));
        simpMessagingTemplate.convertAndSend(TOPIC_PREFIX + CONTEXT_PRIVATE_MESSAGES_CHANNEL + event.getSourceContext().getContextId()
                , new ContextEventMessage(event, null, false)
                        .addParameter(PLAYER_PARAM, event.getSource(Player.class).getId()));
        break;


      case WARRIOR_ADDED: {
        // "В игре '%s' (контекст '%s') игроком '%s' добавлен воин '%s'"
        logger.info(event.getEventType().getFormattedMessage(
                event.getSourceContext().getGameName()
                , event.getSourceContext().getContextId()
                , event.getSource(Warrior.class).getOwner().getTitle()
                , event.getSource(Warrior.class).getWarriorBaseClass().getTitle()));

        simpMessagingTemplate.convertAndSend(TOPIC_PREFIX + CONTEXT_PRIVATE_MESSAGES_CHANNEL + event.getSourceContext().getContextId()
                , new ContextEventMessage(event, null)
                        .addParameter(PLAYER_PARAM, event.getSource(Warrior.class).getOwner().getId())
                        .addParameter(WARRIOR_PARAM, WarriorDto.fromEntity(event.getSource(Warrior.class)))
        );
        break;
      }
      case WARRIOR_MOVED: {
        // "В игре '%s' (id %s) игрок '%s' переместил юнит '%s %s' (id %s) на координаты '%s'"
        logger.info(event.getEventType().getFormattedMessage(
                event.getSourceContext().getGameName()
                , event.getSourceContext().getContextId()
                , event.getSource(Warrior.class).getOwner().getId()
                , event.getSource(Warrior.class).getWarriorBaseClass().getTitle()
                , event.getSource(Warrior.class).getTitle()
                , event.getSource(Warrior.class).getId()
                , event.getSource(Warrior.class).getCoords().toString()));
        simpMessagingTemplate.convertAndSend(TOPIC_PREFIX + CONTEXT_PRIVATE_MESSAGES_CHANNEL + event.getSourceContext().getContextId()
                , new ContextEventMessage(event, null)
                        .addParameter(PLAYER_PARAM, event.getSource(Warrior.class).getOwner().getId())
                        .addParameter(WARRIOR_PARAM, WarriorDto.fromEntity(event.getSource(Warrior.class)))
        );
        break;
      }
      case WARRIOR_MOVE_ROLLEDBACK: {
        // "В игре '%s' (id %s) игрок '%s' отменил движение юнитом '%s %s' (id %s) и вернул его на координаты '%s'"
        logger.info(event.getEventType().getFormattedMessage(
                event.getSourceContext().getGameName()
                , event.getSourceContext().getContextId()
                , event.getSource(Warrior.class).getOwner().getId()
                , event.getSource(Warrior.class).getWarriorBaseClass().getTitle()
                , event.getSource(Warrior.class).getTitle()
                , event.getSource(Warrior.class).getId()
                , event.getSource(Warrior.class).getCoords().toString()));
        simpMessagingTemplate.convertAndSend(TOPIC_PREFIX + CONTEXT_PRIVATE_MESSAGES_CHANNEL + event.getSourceContext().getContextId()
                , new ContextEventMessage(event, null)
                        .addParameter(PLAYER_PARAM, event.getSource(Warrior.class).getOwner().getId())
                        .addParameter(WARRIOR_PARAM, WarriorDto.fromEntity(event.getSource(Warrior.class)))
        );
        break;
      }
      case WARRIOR_REMOVED: {
        //   "В игре '%s' (контекст '%s') игроком '%s' удален воин '%s' (id %s)"
        logger.info(event.getEventType().getFormattedMessage(event.getSourceContext().getGameName()
                , event.getSourceContext().getContextId()
                , event.getSource(Warrior.class).getOwner().getTitle()
                , event.getSource(Warrior.class).getWarriorBaseClass().getTitle()
                , event.getSource(Warrior.class).getId()));
        break;
      }
      case WARRIOR_INFLUENCER_ADDED: {
        //   "В игре '%s' (контекст '%s') у игрока '%s' воину '%s' (id %s) добавлено влияние '%s' (id %s)"
        logger.info(event.getEventType().getFormattedMessage(event.getSourceContext().getGameName()
                , event.getSourceContext().getContextId()
                , event.getSource(Influencer.class).getTargetWarrior().getOwner().getId()
                , event.getSource(Influencer.class).getTargetWarrior().getWarriorBaseClass().getTitle()
                , event.getSource(Influencer.class).getTargetWarrior().getId()
                , event.getSource(Influencer.class).getTitle()
                , event.getSource(Influencer.class).getId()));
        break;
      }
      case WARRIOR_INFLUENCER_REMOVED: {
        //   "В игре '%s' (контекст '%s') у игрока '%s' с воина '%s' (id %s) снято влияние '%s' (id %s)"
        logger.info(event.getEventType().getFormattedMessage(event.getSourceContext().getGameName()
                , event.getSourceContext().getContextId()
                , event.getSource(Influencer.class).getTargetWarrior().getOwner().getId()
                , event.getSource(Influencer.class).getTargetWarrior().getWarriorBaseClass().getTitle()
                , event.getSource(Influencer.class).getTargetWarrior().getId()
                , event.getSource(Influencer.class).getTitle()
                , event.getSource(Influencer.class).getId()));
        break;
      }
      case WARRIOR_PREPARED_TO_DEFENCE:
      case WARRIOR_PREPARED_TO_ATTACK:
        //   "В игре '%s' (контекст '%s') у игрока '%s' воин '%s (%s)' (id %s) подготовился к защите"
        //   "В игре '%s' (контекст '%s') у игрока '%s' воин '%s (%s)' (id %s) подготовился к действиям и атаке"
        logger.info(event.getEventType().getFormattedMessage(
                event.getSourceContext().getGameName()
                , event.getSourceContext().getContextId()
                , event.getSource(Warrior.class).getOwner().getId()
                , event.getSource(Warrior.class).getTitle()
                , event.getSource(Warrior.class).getWarriorBaseClass().getTitle()
                , event.getSource(Warrior.class).getId()));
        break;
      case WARRIOR_WAS_CONTR_ATTACKED_BY_TARGET:
      case WARRIOR_WAS_ATTACKED_BY_ENEMY:
        //   "В игре '%s' воин '%s %s' игрока '%s' нанес оружием %s воину '%s %s' игрока %s %s единиц урона"
        logger.info(event.getEventType().getFormattedMessage(
                event.getSourceContext().getGameName()
                , event.getSource(InfluenceResult.class).getActor() == null ? "" : event.getSource(InfluenceResult.class).getActor().getWarriorBaseClass().getTitle()
                , event.getSource(InfluenceResult.class).getActor() == null ? "" : event.getSource(InfluenceResult.class).getActor().getTitle()
                , event.getSource(InfluenceResult.class).getActorPlayer().getTitle()
                , event.getSource(Modifier.class).getTitle()
//                , event.getSource(InfluenceResult.class).getAttackerWeapon().getTitle()
                , event.getSource(InfluenceResult.class).getTarget().getWarriorBaseClass().getTitle()
                , event.getSource(InfluenceResult.class).getTarget().getTitle()
                , event.getSource(InfluenceResult.class).getTargetPlayer().getTitle()
                , String.valueOf(event.getSource(Modifier.class).getLastCalculatedValue())));
        break;
      case WARRIOR_WAS_SPELLCASTED_BY_WARRIOR:
        // "В игре '%s' воин '%s %s' игрока %s наложил заклинание %s на воина '%s %s' игрока %s")
        logger.info(event.getEventType().getFormattedMessage(
                event.getSourceContext().getGameName()
                , event.getSource(InfluenceResult.class).getActor().getWarriorBaseClass().getTitle()
                , event.getSource(InfluenceResult.class).getActor().getTitle()
                , event.getSource(InfluenceResult.class).getActorPlayer().getTitle()
                , event.getSource(InfluenceResult.class).getAttackerSpell().getTitle()
                , event.getSource(InfluenceResult.class).getTarget().getWarriorBaseClass().getTitle()
                , event.getSource(InfluenceResult.class).getTarget().getTitle()
                , event.getSource(InfluenceResult.class).getTargetPlayer().getTitle()));
        break;
      case WARRIOR_WAS_SPELLCASTED_BY_PLAYER:
        //   "В игре '%s' игрок %s наложил заклинание %s на воина '%s %s' игрока %s"
        logger.info(event.getEventType().getFormattedMessage(
                event.getSourceContext().getGameName()
                , event.getSource(InfluenceResult.class).getActorPlayer().getTitle()
                , event.getSource(InfluenceResult.class).getAttackerSpell().getTitle()
                , event.getSource(InfluenceResult.class).getTarget().getWarriorBaseClass().getTitle()
                , event.getSource(InfluenceResult.class).getTarget().getTitle()
                , event.getSource(InfluenceResult.class).getTargetPlayer().getTitle()));
        break;
      case WARRIOR_WAS_DIE:
        //   "В игре '%s' у игрока '%s' пал (сдох) воин '%s (%s)'. Больше он никого не потревожит."
        logger.info(event.getEventType().getFormattedMessage(
                event.getSourceContext().getGameName()
                , event.getSource(InfluenceResult.class).getActorPlayer().getId()
                , event.getSource(InfluenceResult.class).getActor().getWarriorBaseClass().getTitle()
                , event.getSource(InfluenceResult.class).getActor().getTitle()));
        simpMessagingTemplate.convertAndSend(TOPIC_PREFIX + CONTEXT_PRIVATE_MESSAGES_CHANNEL + event.getSourceContext().getContextId()
                , new ContextEventMessage(event, null)
                        .addParameter(PLAYER_PARAM, event.getSource(InfluenceResult.class).getActorPlayer().getId())
                        .addParameter(WARRIOR_PARAM, WarriorDto.fromEntity(event.getSource(InfluenceResult.class).getActor()))
        );
        break;
      case WARRIOR_HIT_WAS_BLOCKED:
        //   "В игре '%s' воин '%s %s' игрока '%s' отбил удар воина '%s %s' "
        logger.info(event.getEventType().getFormattedMessage(
                event.getSourceContext().getGameName()
                , event.getSource(InfluenceResult.class).getTarget().getWarriorBaseClass().getTitle()
                , event.getSource(InfluenceResult.class).getTarget().getTitle()
                , event.getSource(InfluenceResult.class).getTargetPlayer().getId()
                , event.getSource(InfluenceResult.class).getActor().getWarriorBaseClass().getTitle()
                , event.getSource(InfluenceResult.class).getActor().getTitle()));
        break;
      case WARRIOR_ATTACK_MISS:
      case WARRIOR_ATTACK_MISS_BUT_LUCK:
      case WARRIOR_ATTACK_LUCK: {
        //   "В игре '%s' у игрока '%s' воин '%s %s' хорошо метил, но промахнулся"
        logger.info(event.getEventType().getFormattedMessage(
                event.getSourceContext().getGameName()
                , event.getSource(InfluenceResult.class).getActor().getOwner().getTitle()
                , event.getSource(InfluenceResult.class).getActor().getWarriorBaseClass().getTitle()
                , event.getSource(InfluenceResult.class).getActor().getTitle()));
        break;
      }
      case WEAPON_TAKEN: {
        // "В игре '%s' игрок '%s' снарядил юнит '%s' (id '%s') оружием '%s'. %s"
        logger.info(event.getEventType().getFormattedMessage(event.getSourceContext().getGameName()
                , event.getSource(Warrior.class).getOwner().getTitle()
                , event.getSource(Warrior.class).getWarriorBaseClass().getTitle()
                , event.getSource(Warrior.class).getWarriorBaseClass().getId()
                , event.getSource(Weapon.class).getTitle()
                , event.getSource(Result.class).toString()));
        if (!event.getSourceContext().isTechnical()) {
          simpMessagingTemplate.convertAndSend(TOPIC_PREFIX + CONTEXT_PRIVATE_MESSAGES_CHANNEL + event.getSourceContext().getContextId()
                  , new ContextEventMessage(event, null)
                          .addParameter(PLAYER_PARAM, event.getSource(Warrior.class).getOwner().getId())
                          .addParameter(WARRIOR_PARAM, WarriorDto.fromEntity(event.getSource(Warrior.class)))
                          .addParameter(WEAPON_PARAM, WeaponDto.fromEntity(event.getSource(Weapon.class))));
        }
        break;
      }
      case WEAPON_TRY_TO_DROP: {
        // "В игре '%s' игрок '%s' попытался убрать у юнита '%s' (id '%s') оружие id '%s'. %s"
        logger.info(event.getEventType().getFormattedMessage(event.getSourceContext().getGameName()
                , event.getSource(Warrior.class).getOwner().getTitle()
                , event.getSource(Warrior.class).getWarriorBaseClass().getTitle()
                , event.getSource(Warrior.class).getWarriorBaseClass().getId()
                , event.getSource(String.class)
                , event.getSource(Result.class).toString()));
        break;
      }
      case WEAPON_DROPPED: {
        // "В игре '%s' игрок '%s' убрал у юнита '%s %s' оружие '%s'."
        logger.info(event.getEventType().getFormattedMessage(
                event.getSourceContext().getGameName()
                , event.getSource(Warrior.class).getOwner().getId()
                , event.getSource(Warrior.class).getWarriorBaseClass().getTitle()
                , event.getSource(Warrior.class).getTitle()
                , event.getSource(Weapon.class).getTitle()));
        if (!event.getSourceContext().isTechnical()) {
          simpMessagingTemplate.convertAndSend(TOPIC_PREFIX + CONTEXT_PRIVATE_MESSAGES_CHANNEL + event.getSourceContext().getContextId()
                  , new ContextEventMessage(event, null)
                          .addParameter(PLAYER_PARAM, event.getSource(Warrior.class).getOwner().getId())
                          .addParameter(WARRIOR_PARAM, WarriorDto.fromEntity(event.getSource(Warrior.class)))
                          .addParameter(WEAPON_PARAM, WeaponDto.fromEntity(event.getSource(Weapon.class))));
        }
        break;
      }
      case ARTIFACT_TAKEN_BY_WARRIOR:
      case ARTIFACT_DROPPED_BY_WARRIOR: {
        // "В игре '%s' воин '%s %s' игрока '%s' взял/выбросил артефакт %s"
        logger.info(event.getEventType().getFormattedMessage(
                event.getSourceContext().getGameName()
                , event.getSource(Warrior.class).getOwner().getId()
                , event.getSource(Warrior.class).getWarriorBaseClass().getTitle()
                , event.getSource(Warrior.class).getTitle()
                , event.getSource(Artifact.class).getTitle()));
        if (!event.getSourceContext().isTechnical()) {
          simpMessagingTemplate.convertAndSend(TOPIC_PREFIX + CONTEXT_PRIVATE_MESSAGES_CHANNEL + event.getSourceContext().getContextId()
                  , new ContextEventMessage(event, null)
                          .addParameter(PLAYER_PARAM, event.getSource(Warrior.class).getOwner().getId())
                          .addParameter(WARRIOR_PARAM, WarriorDto.fromEntity(event.getSource(Warrior.class)))
                          .addParameter(ARTIFACT_PARAM, ArtifactDto.fromEntity(event.getSource(Artifact.class))));
        }
        break;
      }
      case ARTIFACT_TAKEN_BY_PLAYER:
      case ARTIFACT_DROPPED_BY_PLAYER:
        // "В игре '%s' игрок '%s'  взял/выбросил артефакт %s"
        logger.info(event.getEventType().getFormattedMessage(
                event.getSourceContext().getGameName()
                , event.getSource(Player.class).getId()
                , event.getSource(Artifact.class).getTitle()));
        if (!event.getSourceContext().isTechnical()) {
          simpMessagingTemplate.convertAndSend(TOPIC_PREFIX + CONTEXT_PRIVATE_MESSAGES_CHANNEL + event.getSourceContext().getContextId()
                  , new ContextEventMessage(event, null)
                          .addParameter(PLAYER_PARAM, event.getSource(Player.class).getId())
                          .addParameter(ARTIFACT_PARAM, ArtifactDto.fromEntity(event.getSource(Artifact.class))));
        }
        break;
      case ARTIFACT_PLAYER_LIMIT_WAS_INCREASED:
        // "В игре '%s' всем игрокам увеличено доступное кол-во артефаков до %s."
        logger.info(event.getEventType().getFormattedMessage(
                event.getSourceContext().getGameName()
                , String.valueOf(event.getSource(Integer.class))));
        break;
      case GAME_CONTEXT_CREATED: {
        //   "Создание контекста '%s'. %s"
        logger.info(event.getEventType().getFormattedMessage(event.getSourceContext().getContextId()
                , event.getSource(Player.class).getTitle()
                , event.getSource(Result.class).toString()));
        break;
      }
      case GAME_CONTEXT_CREATE: {
        //  "Создание контекста игры '%s' игроком '%'. %s"
        logger.info(event.getEventType().getFormattedMessage(event.getSource(String[].class)[0]
                , event.getSource(String[].class)[1]
                , event.getSource(Result.class).toString()));
        break;
      }
      case GAME_CONTEXT_REMOVED: {
        //  "Удаление контекста игры '%s' (владелец '%s'). %s"
        logger.info(event.getEventType().getFormattedMessage(event.getSource(Context.class).getContextId()
                , event.getSource(Context.class).getContextCreator().getTitle()
                , event.getSource(Result.class).toString()));
        if (!event.getSourceContext().isTechnical()) {
          simpMessagingTemplate.convertAndSend(TOPIC_PREFIX + CONTEXT_PUBLIC_MESSAGES_CHANNEL
                  , new ContextEventMessage(event, null));
//          simpMessagingTemplate.convertAndSend(TOPIC_PREFIX + CONTEXT_PRIVATE_MESSAGES_CHANNEL + event.getSourceContext().getContextId()
//                  , new ContextEventMessage(event, null));
        }
        break;
      }
      case GAME_CONTEXT_LOAD_MAP: {
        // "игра '%s' Контекст %s : загрузка карты '%s' игроком '%s'. тип игры %s.  %s"
        logger.info(event.getEventType().getFormattedMessage(event.getSourceContext().getGameName()
                , event.getSourceContext().getContextId()
                , event.getSource(String.class)
                , event.getSource(Player.class).getTitle()
                , event.getSource(Boolean.class) ? "скрытая" : "открыта для всех"
                , event.getSource(Result.class).toString()));
        break;
      }
      case GAME_CONTEXT_GAME_HAS_BEGAN: {
        // "Игра '%s' (id '%s') Началась. В игре %s игрока(ов)"
        logger.info(event.getEventType().getFormattedMessage(event.getSourceContext().getGameName()
                , event.getSourceContext().getContextId()
                , String.valueOf(event.getSourceContext().getLevelMap().getPlayers().size())));
        simpMessagingTemplate.convertAndSend(TOPIC_PREFIX + CONTEXT_PUBLIC_MESSAGES_CHANNEL
                , new ContextEventMessage(event, null));
        simpMessagingTemplate.convertAndSend(TOPIC_PREFIX + CONTEXT_PRIVATE_MESSAGES_CHANNEL + event.getSourceContext().getContextId()
                , new ContextEventMessage(event, null, false));
//        simpMessagingTemplate.convertAndSend(TOPIC_PREFIX + CONTEXT_PRIVATE_MESSAGES_CHANNEL + event.getSourceContext().getContextId()
//                , new ContextEventMessage(event, null));
        break;
      }
      case GAME_CONTEXT_VISIBILITY_CHANGED: {
        // "Видимость игры '%s' сменена на '%s'"
        logger.info(event.getEventType().getFormattedMessage(event.getSourceContext().getGameName()
                , "" + event.getSourceContext().isHidden()));
        simpMessagingTemplate.convertAndSend(TOPIC_PREFIX + CONTEXT_PUBLIC_MESSAGES_CHANNEL
                , new ContextEventMessage(event, null));
        break;
      }
      default: {
        logger.error(GameErrors.UNKNOWN_EVENT_FOR_LOGGER.getError(event.getEventType().getFormattedMessage()).getMessage());
      }
    }
    return event;
  }

  private static final Logger logger = LoggerFactory.getLogger(CoreImpl.class);
}
