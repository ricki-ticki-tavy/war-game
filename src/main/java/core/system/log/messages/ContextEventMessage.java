package core.system.log.messages;

import api.core.Event;
import api.dto.core.ContextDto;
import api.enums.EventType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ContextEventMessage {
  private ContextDto context;
  private List<String> users;
  private EventType eventType;
  private Map<String, Object> params;

  public ContextDto getContext() {
    return context;
  }

  public ContextEventMessage setContextId(ContextDto context) {
    this.context = context;
    return this;
  }

  public List<String> getUsers() {
    return users;
  }

  public ContextEventMessage setUsers(List<String> users) {
    this.users = users;
    return this;
  }

  public EventType getEventType() {
    return eventType;
  }

  public ContextEventMessage setEventType(EventType eventType) {
    this.eventType = eventType;
    return this;
  }

  public Map<String, Object> getParams() {
    return params;
  }

  public ContextEventMessage setParams(Map<String, Object> params) {
    this.params = params;
    return this;
  }

  public ContextEventMessage addParameter(String paramName, Object value) {
    if (params == null) {
      params = new HashMap<>();
    }
    params.put(paramName, value);
    return this;
  }

  public ContextEventMessage(Event event
          , Map<String, Object> params
          , Boolean shortDto) {
    this.context = ContextDto.fromEntity(event.getSourceContext(), shortDto);
    this.users = event.getSourceContext().getLevelMap().getPlayers().stream().map(player -> player.getId()).collect(Collectors.toList());
    this.eventType = event.getEventType();
    this.params = params;
  }

  public ContextEventMessage(Event event
          , Map<String, Object> params) {
    this(event, params, true);
  }


}
