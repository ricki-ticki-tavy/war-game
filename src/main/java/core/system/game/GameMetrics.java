package core.system.game;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Параметры игры, загруженные из файланастроек
 */
@Component
public class GameMetrics {
  @Value("${core.game.metrics.unitSizeInPix:30}")
  private int unitSizeInPix;

  /**
   * Коэффициент увеличения общей стоимости после добавлоения нового предмета
   */
  @Value("${core.game.metrics.weaponGoldCostGain:1.4F}")
  private float weaponGoldCostGain;
  @Value("${core.game.metrics.warriorGoldCostGain:1.1F}")
  private float warriorGoldCostGain;

  public float getWeaponGoldCostGain() {
    return weaponGoldCostGain;
  }

  public void setWeaponGoldCostGain(float weaponGoldCostGain) {
    this.weaponGoldCostGain = weaponGoldCostGain;
  }

  public float getWarriorGoldCostGain() {
    return warriorGoldCostGain;
  }

  public void setWarriorGoldCostGain(float warriorGoldCostGain) {
    this.warriorGoldCostGain = warriorGoldCostGain;
  }

  /**
   * Размер юнита в пикселях карты
   * @return
   */
  public int getUnitSizeInPix() {
    return unitSizeInPix;
  }
}
