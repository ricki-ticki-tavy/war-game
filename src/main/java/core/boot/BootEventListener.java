package core.boot;

import core.database.DatabaseInitializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

/**
 * Слушатель старта приложения
 */
public class BootEventListener  implements ApplicationListener<ContextRefreshedEvent> {

  @Autowired
  DatabaseInitializer databaseInitializer;

  public void onApplicationEvent(ContextRefreshedEvent event) {
    if (event != null) {
      databaseInitializer.initDatabase();
    }
  }

}
