package api.geo;

import api.geo.Coords;

/**
 * Прямоугольник
 */
public class Rectangle {
  private Coords topLeftConner;
  private Coords bottomRightConner;
  private String color;

  public Rectangle(Coords topLeftConner, Coords bottomRightConner, String color){
    this.topLeftConner = topLeftConner;
    this.bottomRightConner = bottomRightConner;
    this.color = color;
  }

  public Coords getTopLeftConner() {
    return topLeftConner;
  }

  public Coords getBottomRightConner() {
    return bottomRightConner;
  }

  public String getColor(){
    return color;
  }
}
