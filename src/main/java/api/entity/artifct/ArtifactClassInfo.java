package api.entity.artifct;

import api.entity.graphics.GraphicsInfo;
import api.entity.weapon.Weapon;

/**
 * Статические данные об артефакте
 */
public interface ArtifactClassInfo {

  /**
   * Название
   * @return
   */
  String getName();

  /**
   * Описание
   * @return
   */
  String getDescription();

  /**
   * Класс заклинания
   * @return
   */
  Class<? extends Artifact> getArtifactClass();

  /**
   * Цена предмета за применение. в золоте
   * @return
   */
  int getGoldCost();

  /**
   * Цена предмета за Возможность его применения в дальнейшем. в золоте
   * @return
   */
  int getOwningGoldCost();

  /**
   * Информация о графике для этого класса
   * @return
   */
  GraphicsInfo getGraphicsInfo();



}
