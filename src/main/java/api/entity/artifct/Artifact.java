package api.entity.artifct;

import api.core.Owner;
import api.entity.ability.Ability;
import api.enums.OwnerTypeEnum;
import api.enums.PlayerPhaseType;

import java.util.List;

/**
 * артефакт. Предмет, которым владеет игрок или юнит.
 */
public interface Artifact<T extends Owner> extends Owner<T> {
  /**
   * Возвращает тип владельца, который может владеть данным артифактом
   *
   * @return
   */
  OwnerTypeEnum getOwnerTypeForArtifact();

  /**
   * Возвращает список способностей артефакта
   *
   * @return
   */
  List<Ability> getAbilities();

  /**
   * Восстановить артефакт, если есть ограничения на кол-во действий на ход
   *
   * @return
   */
  Artifact revival();

  /**
   * Задать владельца артефакта. Артефакт может переходить от владельца к владельцу
   *
   * @param owner
   * @return
   */
  Artifact attachToOwner(T owner);

  /**
   * применить действие артефакта на владельца
   *
   * @return
   */
  Artifact<T> applyToOwner(PlayerPhaseType phase);

  /**
   * Цена предмета за применение. в золоте
   * @return
   */
  int getGoldCost();

  /**
   * Цена предмета за Возможность его применения в дальнейшем. в золоте
   * @return
   */
  int getOwningGoldCost();
}
