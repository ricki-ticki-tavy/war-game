package api.entity.warrior;

import api.entity.ability.AbilityInfo;
import api.entity.graphics.GraphicsInfo;
import api.entity.magic.SpellInfo;

import java.util.List;

/**
 * Класс описания базового игрового класса воина
 */
public interface WarriorBaseClassInfo {
  /**
   * Название
   * @return
   */
  String getName();

  /**
   * Описание
   * @return
   */
  String getDescription();

  /**
   * доступные заклинания
   * @return
   */
  List<SpellInfo> getSpellsInfo();

  /**
   * способности
   * @return
   */
  List<AbilityInfo> getAbilitiesInfo();

  /**
   * Класс заклинания
   * @return
   */
  Class<? extends WarriorBaseClass> getWarriorClass();


  /**
   * Информация о графике для этого класса
   * @return
   */
  GraphicsInfo getGraphicsInfo();

  /**
   * Цена предмета за применение. в золоте
   * @return
   */
  int getGoldCost();

  /**
   * Цена предмета за Возможность его применения в дальнейшем. в золоте
   * @return
   */
  int getOwningGoldCost();
}
