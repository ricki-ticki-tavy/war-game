package api.entity.ability;

/**
 * Класс описания способности
 */
public interface AbilityInfo {
  /**
   * Название
   * @return
   */
  String getName();

  /**
   * Описание
   * @return
   */
  String getDescription();

  /**
   * Класс способности
   * @return
   */
  Class<? extends Ability> getAbilityClass();
}
