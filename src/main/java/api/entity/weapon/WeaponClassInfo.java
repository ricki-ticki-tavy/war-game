package api.entity.weapon;

/**
 * Статические данные об предмете
 */
public interface WeaponClassInfo {

  /**
   * Название
   * @return
   */
  String getName();

  /**
   * Название
   * @return
   */
  String getSecondName();

  /**
   * Описание
   * @return
   */
  String getDescription();

  /**
   * Класс заклинания
   * @return
   */
  Class<? extends Weapon> getWeaponClass();

  /**
   * Цена предмета за применение. в золоте
   * @return
   */
  int getGoldCost();

  /**
   * Цена предмета за Возможность его применения в дальнейшем. в золоте
   * @return
   */
  int getOwningGoldCost();

}
