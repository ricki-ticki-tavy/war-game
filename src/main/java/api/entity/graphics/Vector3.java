package api.entity.graphics;

public class Vector3 {
  private double x, y, z;

  public Vector3(){

  }

  public Vector3(double x, double y, double z){
     set(x, y, z);
  }

  public double getX() {
    return x;
  }

  public Vector3 setX(double x) {
    this.x = x;
    return this;
  }

  public double getY() {
    return y;
  }

  public Vector3 setY(double y) {
    this.y = y;
    return this;
  }

  public double getZ() {
    return z;
  }

  public Vector3 setZ(double z) {
    this.z = z;
    return this;
  }

  public Vector3 set(double x, double y, double z){
    this.x = x;
    this.y = y;
    this.z = z;
    return this;
  }
}
