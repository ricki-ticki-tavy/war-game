package api.entity.graphics;

/**
 * Информация о графике объекта
 */
public class GraphicsInfo {
  private ModelInfo baseViewModel;
  private String largeIcon;
  private String smallIcon;
  private int countOnForm = 1;

  /**
   * Модель для представления на постаменте и на выборе класса нового героя
   * @return
   */
  public ModelInfo getBaseViewModel() {
    return baseViewModel;
  }

  public GraphicsInfo setBaseViewModel(ModelInfo baseViewModel) {
    this.baseViewModel = baseViewModel;
    return this;
  }


  /**
   * Количество экземпляров, создаваемых на форме. Используется для оружия и артифактов
   * @return
   */
  public int getCountOnForm() {
    return countOnForm;
  }

  public GraphicsInfo setCountOnForm(int countOnForm) {
    this.countOnForm = countOnForm;
    return this;
  }

  /**
   * Изображение для кнопок
   * @return
   */
  public String getLargeIcon() {
    return largeIcon;
  }

  public GraphicsInfo setLargeIcon(String largeIcon) {
    this.largeIcon = largeIcon;
    return this;
  }

  /**
   * Маленькая иконка
   * @return
   */
  public String getSmallIcon() {
    return smallIcon;
  }

  public GraphicsInfo setSmallIcon(String smallIcon) {
    this.smallIcon = smallIcon;
    return this;
  }
}
