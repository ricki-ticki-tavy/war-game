package api.entity.graphics;

public enum ModelType{
  OBJ("Object", ".obj");
  String name;
  String ext;

  ModelType(String name, String ext){
    this.name = name;
    this.ext = ext;

  }
}
