package api.entity.graphics;

/**
 * Информация о 3D модели
 */
public class ModelInfo {
  private String materialsFile;
  private String modelFile;
  private ModelType modelType;
  private float scale = 1;
  private float floorLevel = 0;
  private float rotationX = 0;
  private float rotationY = 0;
  private float rotationZ = 0;

  private Vector3 center = new Vector3(0, 0, 0);

  /**
   * файл материалов модели
   * @return
   */
  public String getMaterialsFile() {
    return materialsFile;
  }

  public ModelInfo setMaterialsFile(String materialsFile) {
    this.materialsFile = materialsFile;
    return this;
  }

  /**
   * Файл самой модели
   * @return
   */
  public String getModelFile() {
    return modelFile;
  }

  public ModelInfo setModelFile(String modelFile) {
    this.modelFile = modelFile;
    return this;
  }

  /**
   * Тип файла. На его основе выбирается загрузчик
   * @return
   */
  public ModelType getModelType() {
    return modelType;
  }

  public ModelInfo setModelType(ModelType modelType) {
    this.modelType = modelType;
    return this;
  }

  /**
   * масштабирование модели
   * @return
   */
  public float getScale() {
    return scale;
  }

  public ModelInfo setScale(float scale) {
    this.scale = scale;
    return this;
  }

  /**
   * нулевое значение для Y координаты.
   * @return
   */
  public float getFloorLevel() {
    return floorLevel;
  }

  public ModelInfo setFloorLevel(float floorLevel) {
    this.floorLevel = floorLevel;
    return this;
  }


  public float getRotationX() {
    return rotationX;
  }

  public ModelInfo  setRotationX(float rotationX) {
    this.rotationX = rotationX;
    return this;
  }

  public float getRotationY() {
    return rotationY;
  }

  public ModelInfo  setRotationY(float rotationY) {
    this.rotationY = rotationY;
    return this;
  }

  public float getRotationZ() {
    return rotationZ;
  }

  public ModelInfo  setRotationZ(float rotationZ) {
    this.rotationZ = rotationZ;
    return this;
  }

  public Vector3 getCenter() {
    return center;
  }

  public ModelInfo setCenter(Vector3 center) {
    this.center = center;
    return this;
  }
}
