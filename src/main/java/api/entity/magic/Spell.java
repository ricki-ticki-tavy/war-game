package api.entity.magic;

import api.core.Owner;
import api.core.Result;
import api.enums.OwnerTypeEnum;
import api.enums.SignOfInfluenceEnum;
import api.game.action.InfluenceResult;

import java.util.List;
import java.util.Map;

/**
 * Заклинание, используемое игроком или воином
 */
public interface Spell<O> extends Owner{
  /**
   * Тип владельца для этого заклинания.
   * @return
   */
  OwnerTypeEnum getAvailableOwnerType();

  /**
   * Получить параметры, требуемые заклинанию
   * @return
   */
  Map<Integer, SpellParameter> getRequiredParameters();

  /**
   * Применить заклинание
   * @param parameters - Заполненные параметры, полученные ранее из метода getRequiredParameters
   * @return   - список результатов воздействий так как цельюзаклинания могут быть более одного воина
   */
  Result<List<Result<InfluenceResult>>> cast(Map<Integer, SpellParameter> parameters);

  /**
   * Получить стоимость наложения заклинания в очках магии
   * @return
   */
  int getMagicCost();

  /**
   * Получить стоимость наложения заклинания в очках действия
   * @return
   */
  int getActionPointsCost();

  /**
   * Парсинг входных параметров из строковых значений
   * @param givenParameters
   * @return
   */
  Result<Map<Integer, SpellParameter>> parseParameters(Map<Integer, String> givenParameters);

  /**
   * Возвращает тип заклинания по воздействию: негативное или позитивное
   * @return
   */
  SignOfInfluenceEnum getSpellSign();

}
