package api.entity.magic;

import api.core.Result;
import api.entity.warrior.Warrior;
import api.enums.SpellParameterEnum;
import api.game.map.Player;
import api.geo.Coords;
import javafx.util.Pair;

/**
 * Входной параметр заклинания
 */
public interface SpellParameter {

  /**
   * Получить название параметра
   * @return
   */
  String getTitle();

  /**
   * Получить тип параметра заклинания
   * @return
   */
  SpellParameterEnum getType();

  /**
   * получить ограничения для числового параметра
   * @return Pair\<min, max>
   */
  Pair<Integer, Integer> getIntegerRestrictions();

  /**
   * получить ограничения для точки на карте или любого воина. Указывается центр круга и его радиус в котором надо
   * выбрать объект либо точку на курте
   * @return Pair\<min, max>
   */
  Pair<Coords, Integer> getCoordsRestrictions();

  /**
   * Установить числовое значение параметра
   * @param value
   * @return
   */
  SpellParameter setIntegerValue(Integer value);

  /**
   * Получить числовое значение параметра, введенное пользователем
   * @return
   */
  Integer getIntegerValue();

  /**
   * Установить значение параметра, типа воина.Не зависимо от его принадлежности
   * @param warrior
   * @return
   */
  SpellParameter setWarriorValue(Warrior warrior);

  /**
   * Получить выбранного пользователем воина
   * @return
   */
  Warrior getWarriorValue();

  /**
   * Установить значение типа Игрока, выбранного пользователем
   * @param player
   * @return
   */
  SpellParameter setPlayerValue(Player player);

  /**
   * Получитьплеера, выбранного пользователем
   * @return
   */
  Player getPlayerValue();

  /**
   * Получить координаты на карте
   * @return
   */
  Coords getCoordsValue();

  /**
   * Установить значение координат на карте
   * @return
   */
  SpellParameter setCoordsValue(Coords coords);

  /**
   * Полное клонирование, включая клонирование внутренних объектов
   * @return
   */
  SpellParameter clone();

  /**
   * Заполнить параметр методом парсинга строкового параметра
   * @param givenParameter
   * @param spellOwner   - заклинание, для которого выполняется парсинг
   * @return
   */
  Result<SpellParameter> fillFromString(Spell spellOwner, String givenParameter);

  }
