package api.entity.magic;

import api.enums.SignOfInfluenceEnum;

import java.util.Map;

/**
 *
 */
public interface SpellInfo {
  /**
   * Название
   * @return
   */
  String getName();

  /**
   * Описание
   * @return
   */
  String getDescription();

  /**
   * Очки действия для наложения заклинания (толькодля заклинаний воинов)
   * @return
   */
  int getActionPointsCost();

  /**
   * Очки действия для способностей. Сколько очков применения способностей заберет заклинание (толькодля заклинаний воинов)
   * Сделано для того, чтобы особые спосодности можно было реализовывать в виде заклинаний. Но применять эти
   * заклинания можно раз в N ходов в отличие от обычных заклинаний.
   *    Для настоящих заклинаний всегда возвращать 0
   * @return
   */
  int getAbilityActionPointsCost();

  /**
   * Очки магии
   * @return
   */
  int getMannaCost();

  /**
   * Возвращает список дополнительных параметров
   * @return
   */
  Map<Integer, SpellParameter> getRequiredAdditionalParams();

  /**
   * Получить знак эффекта заклинания
   * @return
   */
  SignOfInfluenceEnum getSpellSign();

  /**
   * Получить радиус вияния заклинания, если оно действует на более, чем одну цель
   * @return
   */
  Integer getInfluenceRadius();

  /**
   * Класс заклинания
   * @return
   */
  Class<? extends Spell> getSpellClass();
}
