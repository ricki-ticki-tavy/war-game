package api.enums;

/**
 * Результат попытки блокировки удара щитом
 */
public enum BlockHitResult {
  NOT_ENOUGH_ACTION_POINTS("недостаточно очков действия")
  , FAILED("не удалось")
  , NOT_APPLICABLE("не способен отразить")
  , SUCCESS("Удалось")
  ;
  private String title;

  public String getTitle() {
    return title;
  }

  private BlockHitResult(String title){
    this.title = title;
  }

}
