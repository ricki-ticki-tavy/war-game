package api.enums;

/**
 * Тип действия
 */
public enum SignOfInfluenceEnum {
  POSITIVE("Положительное")
  , NEGATIVE("Отрицательное");

  private String title;

  public String getTitle() {
    return title;
  }

  private SignOfInfluenceEnum(String title){
    this.title = title;
  }
}
