package api.enums;

/**
 * Класс влияния, оказываемого модификатором. Нужно для введения сопротивляемости влияниям.
 */
public enum ModifierClass {
  WEAPON("Оружие")
  , SPELL("Заклинание")
  , FIRE("Огонь")
  , POISON("Яд")
  , COLD("Холод")
  , MAGIC("Магия")
  ;
  private String title;

  public String getTitle() {
    return title;
  }

  private ModifierClass(String title){
    this.title = title;
  }

}
