package api.enums;

import java.util.Arrays;

public enum ArmorClassEnum {
  ARMOR_0("Нет", 0, 0, 6),
  ARMOR_1("легкая", 1, 20, 8),
  ARMOR_2("Средняя", 2, 36, 12),
  ARMOR_3("Тяжелая", 3, 49, 15),
  ARMOR_4("Великая", 4, 59, 20),
  ARMOR_5("Легендарная", 5, 67, 30)
  ;

  private int armorValue;
  private int moveCost;
  private int armorPercent;
  private String title;

  public int getArmorValue() {
    return armorValue;
  }

  public int getMoveCost() {
    return moveCost;
  }

  public String getTitle(){
    return title;
  }

  public int getArmorPercent() {
    return armorPercent;
  }

  /**
   * Класс брони, измененный на заданное значение брони
   * @return
   */
  public ArmorClassEnum add(int delta){
    int armor = armorValue + delta;
    if (armor > ArmorClassEnum.values().length - 1){
      armor = ArmorClassEnum.values().length - 1;
    }
    final int fArmor = armor;
    return Arrays.stream(ArmorClassEnum.values()).filter(armorClassEnum -> armorClassEnum.armorValue == fArmor).findFirst().get();
  }

  /**
   * Класс брони, измененный на заданное значение брони
   * @return
   */
  public ArmorClassEnum add(ArmorClassEnum delta){
    return add(delta.getArmorValue());
  }

  private ArmorClassEnum(String title, int armorValue, int armorPercent, int moveCost){
    this.title = title;
    this.armorValue = armorValue;
    this.moveCost = moveCost;
    this.armorPercent = armorPercent;
  }


}
