package api.enums;

/**
 * Класс оружия
 */
public enum WeaponClassEnum {
  SMALL_SHIELD("маленькие щиты")
  , LARGE_SHIELD("Большые щиты")
  , SMALL_MELEE("Ножи, кинжалы")
  , SHORT_MELEE("Короткие мечи, короткие сабли")
  , MELEE("мечи, когти, сабли, топоры т.п.")
  , LONG_MELEE("Длинные мечи, цепы")
  , HAVY_MELEE("Дубины, двуручные мечи, молоты")
  , SMALL_RANGED("луки")
  , RANGED("Длинные луки, метательные топоры")
  , HAVY_RANGED("Копья")
  ;
  private String title;

  public String getTitle(){
    return title;
  }

  WeaponClassEnum(String title){
    this.title = title;
  }
}
