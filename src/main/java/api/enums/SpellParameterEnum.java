package api.enums;

/**
 * Тип входного параметра для заклинания.
 */
public enum SpellParameterEnum {
    ALLIED_WARRIOR("Дружественный воин")
    , ENEMY_WARRIOR("Вражеский воин")
    , MAP_POINT("Место на карте")
    , PLAYER("Игрок")
    , INTEGER("Число");

  private String title;

  public String getTitle() {
    return title;
  }

  SpellParameterEnum(String title) {
    this.title = title;
  }
}
