package api.core;

import api.entity.artifct.ArtifactClassInfo;
import api.entity.magic.SpellInfo;
import api.entity.artifct.Artifact;
import api.entity.warrior.Warrior;
import api.entity.warrior.WarriorBaseClass;
import api.entity.warrior.WarriorBaseClassInfo;
import api.entity.weapon.Weapon;
import api.entity.weapon.WeaponClassInfo;
import api.enums.EventType;
import api.game.map.Player;
import api.game.map.metadata.GameRules;
import api.game.map.metadata.xml.LevelMapMetaDataXml;
import api.dto.warrior.WarriorShortDto;

import java.util.List;
import java.util.function.Consumer;

/**
 * взаимодействие с игрой.
 */
public interface Core {
  int getRandom(int min, int max);

  /**
   * Получить список доступных карт
   * @return
   */
  Result<List<LevelMapMetaDataXml>> getLevelMapsList();

  /**
   * Создать  игру
   *
   * @param userGameCreator
   * @param gameRules
   * @param mapFileName
   * @param gameName
   * @param hidden
   * @return
   */
  Result<Context> createGameContext(Player userGameCreator, GameRules gameRules, String mapFileName, String gameName, boolean hidden);

  /**
   * Создать технический контекст
   *
   * @param ownerUser
   * @return
   */
  Result<Context> createTechnicalContext(Player ownerUser);

  /**
   * Найти контекст по его UID
   * @param contextId
   * @return
   */
  Result<Context> findGameContextByUID(String contextId);

  /**
   * Удалить игру и ее контекст с сервера
   * @param contextId
   */
  Result<Context> removeGameContext(String contextId);

  /**
   * Отправить сообщение о событии
   * Событие сначала обрабатывается обработчиками привязанными к контексту, п потом без привязки к контексту,
   *
   * @param event
   * @return
   */
  Event fireEvent(Event event);

  /**
   * Подписаться на событие
   *
   * @param context    контекст, в рамках которого интересует данное событие. Если null, то по всем контекстам
   * @param consumer
   * @param eventTypes
   * @return
   */
  String subscribeEvent(Context context, Consumer<Event> consumer, EventType... eventTypes);

  /**
   * Отписаться от события
   *
   * @param context    контекст, в рамках которого интересует данное событие. Если null, то по всем контекстам
   * @param consumerId
   * @param eventTypes
   * @return
   */
  Result<Context> unsubscribeEvent(Context context, String consumerId, EventType... eventTypes);

  /**
   * подключить к игре игрока.
   *
   * @param playerName
   * @param playerId
   * @return
   */
  Result loginPlayer(String playerName, Long playerId);

  /**
   * выполнить выход игрока из логическогоядра.
   *
   * @param playerName
   * @return
   */
  Result logoutPlayer(String playerName);

  /**
   * Вернуть пользователя из списка вошедших в игру
   * @param playerName
   * @return
   */
  Result<Player> findUserByName(String playerName);

  /**
   * Получить список активных контекстов
   * @return
   */
  List<Context> getContextList();

  /**
   * Возвращает имена базовых классов воинов
   * @return
   */
  Result<List<String>> getBaseWarriorClasses();

  /**
   * Возвращает имена базовых классов вооружения
   * @return
   */
  Result<List<String>> getWeaponClasses();

  /**
   * Ищет базовый класс воина по его названию
   * @param className
   * @return
   */
  Result<WarriorBaseClassInfo> findWarriorBaseClassByName(String className);

  /**
   * Ищет базовый класс воина по его названию
   * @param weaponName
   * @return
   */
  Result<Class<? extends Weapon>> findWeaponClassByName(String weaponName);

  /**
   * Ищет базовый класс воина по его названию
   * @param weaponName
   * @return
   */
  Result<WeaponClassInfo> findWeaponByName(String weaponName);

  /**
   * Ищет базовый класс артефакта для воина по его названию
   * @param artifactName
   * @return
   */
  Result<Class<? extends Artifact<Warrior>>> findArtifactForWarrior(String artifactName);

  /**
   * Ищет базовый класс артефакта для игрока по его названию
   * @param artifactName
   * @return
   */
  Result<Class<? extends Artifact<Player>>> findArtifactForPlayer(String artifactName);

  /**
   * Возвращает все артефакты для воина
   * @return
   */
  Result<List<Artifact>> getWarriorArtifacts();

  /**
   * Возвращает все артефакты для игрока
   * @return
   */
  Result<List<ArtifactClassInfo>> getPlayerArtifacts();

  /**
   * Ищет базовый класс заклинания для игрока по его названию
   * @param spellName
   * @return
   */
  Result<SpellInfo> findSpellForPlayer(String spellName);

  /**
   * Возвращает список звклинаний игрока
   * @return
   */
  Result<List<SpellInfo>> getSpellsForPlayer();

  /**
   * Возвращает список звклинаний игрока
   * @return
   */
  Result<List<SpellInfo>> getSpellsForWarrior();

  /**
   * Ищет базовый класс заклинания для воина по его названию
   * @param spellName
   * @return
   */
  Result<SpellInfo> findSpellForWarrior(String spellName);

  /**
   * Зарегистрировать базовый класс воина
   *
   * @param warriorBaseClass
   */
  void registerWarriorBaseClass(Class<? extends WarriorBaseClass> warriorBaseClass);

  /**
   * Зарегистрировать базовый класс оружия
   *
   * @param weaponClassInfo
   */
  void registerWeaponClass(WeaponClassInfo weaponClassInfo);

  /**
   * Зарегистрировать базовый класс артефакта для юнита (воина)
   *
   * @param className
   * @param artifactClass
   */
  void registerArtifactForWarriorClass(String className, Class<? extends Artifact> artifactClass);

  /**
   * Зарегистрировать базовый класс заклинания для игрока
   *
   * @param className
   * @param spellInfo
   */
  void registerSpellForPlayerClass(String className, SpellInfo spellInfo);

  /**
   * Зарегистрировать базовый класс заклинания для воина
   *
   * @param className
   * @param spellInfo
   */
  void registerSpellForWarriorClass(String className, SpellInfo spellInfo);

  /**
   * Зарегистрировать базовый класс артефакта для игрока
   *
   * @param classInfo
   */
  void registerArtifactForPlayerClass(ArtifactClassInfo classInfo);


  /**
   * Рассчитать стоимость оружия с учетом всех его артефактов
   * @return
   */
  Result<Integer> calculatePrice(Weapon weapon);

  /**
   * Рассчитать стоимость воина с учетом всех его артефактов и оружия
   * @return
   */
  Result<Integer> calculatePrice(Warrior warrior);

  /**
   * Рассчитать стоимость воина с учетом всех его артефактов и оружия
   * @return
   */
  Result<Integer> calculatePrice(WarriorShortDto warriorShortDto);

  /**
   * Рассчитать стоимость всех воина с учетом всех их артефактов и оружия
   * @return
   */
  Result<Integer> calculatePrice(Player player);

  /**
   * Вернуть список активных пользователей. Те, что в чате
   * @return
   */
  List<String>getActiveUsersList();

}
