package api.core;

import api.entity.ability.Influencer;
import api.entity.magic.Spell;
import api.entity.artifct.Artifact;
import api.entity.warrior.Warrior;
import api.entity.weapon.Weapon;
import api.enums.EventType;
import api.game.action.InfluenceResult;
import api.game.map.LevelMap;
import api.game.map.Player;
import api.game.map.metadata.GameRules;
import api.game.map.metadata.xml.LevelMapMetaDataXml;
import api.geo.Coords;
import core.database.entity.warrior.PlayerWarrior;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

/**
 * Контекст игровой
 */
public interface Context {

  /**
   * Признак технического контекста. Такие контексты не будут публиковаться в списке игр и видны только
   * их создателю. Такие контексты необходимы для редактированя воинов или отрядов.
   * @return
   */
  boolean isTechnical();

  /**
   * Подписаться на событие
   *
   * @param eventTypes
   * @param consumer
   * @return
   */
  String subscribeEvent(Consumer<Event> consumer, EventType... eventTypes);

  /**
   * Отписаться от события
   *
   * @param consumerId
   * @param eventTypes
   * @return
   */
  Result<Context> unsubscribeEvent(String consumerId, EventType... eventTypes);

  /**
   * UUID контекста (сессии)
   *
   * @return
   */
  String getContextId();

  /**
   * Название игры
   *
   * @return
   */
  String getGameName();


  /**
   * Признак публикуется ли игра в списке или доступна только по ссылке
   *
   * @return
   */
  boolean isHidden();

  /**
   * Получить имена игроков, начавших игру. Только для контекста с начатой игрой
   * @return
   */
  Result<List<String>> getFrozenListOfPlayers();

  /**
   * Успех, если текущий статус игры совпадает с переданным
   * @return
   */
  Result<Context> ifGameRan(boolean state);

  /**
   * Успех, если текущий статус игры совпадает с переданным
   * @return
   */
  Result<Context> ifGameDeleting(boolean state);

  /**
   * Утверждает,чтотип контекстасовпадает с указанным. Если так, то успех
   * @param isTechnical
   * @return
   */
  Result<Context> ifTechnicalStatusIs(boolean isTechnical);

  /**
   * Проверка допустимости новых координат воина
   * @param warrior
   * @param newCoords
   * @return
   */
  Result<Context> ifNewWarriorSCoordinatesAreAvailable(Warrior warrior, Coords newCoords);

  /**
   * Проверяет возможно ли выполнение атаки или применения способности данным юнитом в этом ходе
   * @param userName
   * @param warriorId
   * @return
   */
  Result<Warrior> ifWarriorCanActsAtThisTurn(String userName, String warriorId);

  /**
   * возвращает текущий статус игры
   * @return
   */
  boolean isGameRan();

  /**
   * Контекст в процессе удаления
   * @return
   */
  boolean isDeleting();

  /**
   * Перевод контекста в состояние начала удаления
   * @return
   */
  Result<Context> initDelete();

  /**
   * Возвращает игровой движок
   *
   * @return
   */
  Core getCore();

  /**
   * Инициировать срабатывание события
   *
   * @param gameEvent
   * @return
   */
  void fireGameEvent(Event gameEvent);

  /**
   * Инициировать срабатывание события
   *
   * @return
   */
  void fireGameEvent(
          Event causeEvent
          , EventType eventType
          , EventDataContainer source
          , Map<String, Object> params);

  /**
   * Получить карту игры со всеми игроками и воинами
   *
   * @return
   */
  LevelMap getLevelMap();

  /**
   * Загрузить карту
   *
   * @param gameRules
   * @param map
   * @return
   */
  Result<Context> loadMap(GameRules gameRules, LevelMapMetaDataXml map, String gameName, boolean hidden);

  /**
   * Создать и подключить к игре игрока.
   *
   * @param player
   * @return
   */
  Result connectPlayer(Player player);

  /**
   * Сбежать с поля боя
   * @return
   */
  Result<Player> decampFromTheArena(String userName);

  /**
   * Отключить пользователя от игры.
   *
   * @param player
   * @return
   */
  Result disconnectPlayer(Player player);

  /**
   * Найти юнит по его коду независимо от того, какому игроку он принадлежит
   * @param warriorId
   * @return
   */
  Result<Warrior> findWarriorById(String warriorId);

  /**
   * Создать воина на карте при начальной расстановке
   *
   * @param baseWarriorClass
   * @return
   */
  Result<Warrior> createWarrior(String userName, String baseWarriorClass, Coords coords);

  /**
   * Атаковать выбранным оружием другого воина
   * @param userName
   * @param attackerWarriorId
   * @param targetWarriorId
   * @param weaponId
   * @return
   */
  Result<InfluenceResult> attackWarrior(String userName, String attackerWarriorId, String targetWarriorId, String weaponId);

  /**
   * Переместить юнит на новые координаты
   * @param userName
   * @param warriorId
   * @param newCoords
   * @return
   */
  Result<Warrior> moveWarriorTo(String userName, String warriorId, Coords newCoords);

  /**
   * Откатить перемещение воина
   * @return
   */
  Result<Warrior> rollbackMove(String userName, String warriorId);

  /**
   * Возвращает координаты,куда можно переместить перемещаемого юнита, исходя из того, куда его хотят переместить
   * @param userName
   * @param warriorId
   * @param coords
   * @return
   */
  Result<Coords> whatIfMoveWarriorTo(String userName, String warriorId, Coords coords);

  /**
   * Удалить юнит игроком
   * @param userName
   * @param warriorId
   * @return
   */
  Result<Warrior> removeWarrior(String userName, String warriorId);

  /**
   * Вооружить воина предметом
   * @param userName
   * @param warriorId
   * @param weaponClass
   * @return
   */
  Result<Weapon> giveWeaponToWarrior(String userName, String warriorId, Class<? extends Weapon> weaponClass);

  /**
   * Бросить оружие
   * @param userName
   * @param warriorId
   * @param weaponId
   * @return
   */
  Result<Weapon> dropWeaponByWarrior(String userName, String warriorId, String weaponId);

  /**
   * дать воину артефакт. Если такой артефакт такого типа уже есть, то будет отказ
   * @param userName      код игрока, которому принадлежит воин
   * @param warriorId     код воина, которому дается артефакт
   * @param artifactClass класс даваемого артефакта
   * @return
   */
  Result<Artifact<Warrior>> giveArtifactToWarrior(String userName, String warriorId, Class<? extends Artifact<Warrior>> artifactClass);

  /** выбросить артефакт воина
   *
   * @param userName
   * @param artifactInstanceId
   * @param warriorId
   * @return
   */
  Result<Artifact<Warrior>> dropArtifactByWarrior(String userName, String warriorId, String artifactInstanceId);

  /**
   * Наложить заклинание игроком
   * @param userName                  - игрок
   * @param spellClass                  - класс заклинания
   * @param givenSpellParameters   - параметры, необходимые заклинанию
   * @return
   */
  Result<List<Result<InfluenceResult>>> castSpellByPlayer(String userName, Class<? extends Spell<Player>> spellClass, Map<Integer, String> givenSpellParameters);

  /**
   * Наложить заклинание воином
   * @param userName                  - игрок
   * @param warriorId                 - id воина
   * @param spellName                  - Название заклинания
   * @param givenSpellParameters   - параметры, необходимые заклинанию
   * @return
   */
  Result<List<Result<InfluenceResult>>> castSpellByWarrior(String userName, String warriorId, String spellName, Map<Integer, String> givenSpellParameters);

  /**
   * Дать игроку артефакт
   * @param userName
   * @param artifactClass
   * @return
   */
  Result<Artifact<Player>> giveArtifactToPlayer(String userName, Class<? extends Artifact<Player>> artifactClass);

  /**
   * бросить артефакт игроком
   * @param userName
   * @param artifactInstanceId
   * @return
   */
  Result<Artifact<Player>> dropArtifactByPlayer(String userName, String artifactInstanceId);

  /**
   * Найти оружие по его id
   * @param userName
   * @param warriorId
   * @param weaponId
   * @return
   */
  Result<Weapon> findWeaponById(String userName, String warriorId, String weaponId);

  /**
   * Получить пользователя - создателя игры
   *
   * @return
   */
  Player getContextCreator();

  /**
   * Получить параметры игры
   *
   * @return
   */
  GameRules getGameRules();

  /**
   * Установить признак готовности
   * @param player
   * @param readyToGame
   * @return
   */
  Result<Player> setPlayerReadyToGameState(Player player, boolean readyToGame);

  /**
   * Найти пользователя в контексте
   *
   * @param userName
   * @return
   */
  Result<Player> findUserByName(String userName);

  /**
   * Получить игрока, который сейчас ходит
   *
   * @return
   */
  Result<Player> getPlayerOwnsThisTurn();

  /**
   * Это утверждение, что переданный игрок и является ходящим сейчас
   *
   * @return
   */
  Result<Player> ifPlayerOwnsTheTurnEqualsTo(Player player, String... args);

  /**
   * Передача хода следующему игроку
   * @param userName
   * @return
   */
  Result<Player> nextTurn(String userName);

  /**
   * Получить список оказываемых влияний на юнит
   * @return
   */
  Result<List<Influencer>> getWarriorSInfluencers(String userName, String warriorId);

  /**
   * Получить расстояние в "пикелях" от координаты FROM до  координаты TO.
   * @param from
   * @param to
   * @return
   */
  int calcDistance(Coords from, Coords to);

  /**
   * Получить расстояние в "1/10" игровой клетки от координаты FROM до  координаты TO.
   * @param from
   * @param to
   * @return
   */
  int calcDistanceMm(Coords from, Coords to);

  /**
   * Вернуть кол-во кругов игры. Сколько раз по кругу сходили все игроки
   * @return
   */
  Result<Integer> getRoundsCount();


  /**
   * Сохранить в БД экипировку воина пользователя
   * @param userName
   * @param warriorId
   * @param ammunitionName
   * @return
   */
  Result<Warrior> saveWarriorAmmunition(String userName, String warriorId, String ammunitionName);

  /**
   *  Переименовать вариант экипировки воина
   * @param userName                - Пользователь
   * @param warriorId                - код воина
   * @param oldEquipmentName        - Старое название
   * @param newEquipmentName        - Новое название
   * @return
   */
  Result<Warrior> renameWarriorEquipment(String userName, String warriorId, String oldEquipmentName, String newEquipmentName);

  /**
   *  Удалить вариант экипировки воина. Если аммуниций у воина не остается, то он тоже удаляется
   * @param userName       - Пользователь
   * @param warriorId       - Пользователь
   * @param equipmentName - название эеипировки
   * @return
   */
  Result<Warrior> removeWarriorEquipment(String userName, String warriorId, String equipmentName);

  /**
   * Загрузить из БД экипировки воина пользователя
   * @param userName
   * @param heroName
   * @return
   */
  Result<PlayerWarrior> findPlayerHero(String userName, String heroName);


  /**
   * Получить список героев пользователя
   * @param userName
   * @return
   */
  Result<List<PlayerWarrior>> getPlayerHeroes(String userName);

  /**
   * Получить список ID всех воинов игрока
   * @param userName       - Пользоватьель
   * @param ownerUserName  - Игрок, чьих воинов нужно получить. Если пусто, то будут возвращены воины всех игроков карты
   * @return
   */
  Result<String> getPlayerWarriorsAsJson(String userName, String ownerUserName);


  /**
   * Установить новое значение видимости игры в списке игр
   * @param userName
   * @param hidden
   * @return
   */
  Result<Context> setGameVisibleMode(String userName, boolean hidden);

}
