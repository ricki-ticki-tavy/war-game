package api.core.database.dao;

import api.core.Result;
import core.database.entity.security.Role;
import core.database.entity.security.User;
import core.database.entity.security.UserAccountOperationRequest;

/**
 * Доступ к данным в базе для безопасности и всем, что связано с пользователями
 */
public interface SecurityDao {
  /**
   * Загрузить пользователя по имени
   * @param userName
   * @return   существующий пользователь, либо ошибка
   */
  Result<User> loadByName(String userName);

  /**
   * Создание записи регистрации пользователя
   * @param userName
   * @param password
   * @param eMail
   * @return
   */
  Result<UserAccountOperationRequest> saveRegisterUserRequest(String userName, String password, String eMail);

  /**
   * Создание записи запроса на восстановление доступа к учетной записи (к смене пароля)
   * @param user
   * @return
   */
  Result<UserAccountOperationRequest> saveRestoreUserAccountAccess(User user);

  /**
   * Удалить запись о регистрации пользователя
   * @param registerUserRequest - код подтверждения, запись которого надо удалить
   * @return
   */
  Result<UserAccountOperationRequest> removeRegisterUserRequest(UserAccountOperationRequest registerUserRequest);

  /**
   * Найти не просроченный запрос на создание пользователя по почте
   * @param eMail
   * @return
   */
  Result<UserAccountOperationRequest> findLiveRegisterUserRequestByeMail(String eMail);

  /**
   * Найти не просроченный запрос на создание пользователя по почте
   * @param UserName
   * @return
   */
  Result<UserAccountOperationRequest> findLiveRegisterUserRequestByUsername(String UserName);

  /**
   * Найти не просроченный запрос на создание пользователя по коду подтверждения регистрации
   * @param confirmCode
   * @return
   */
  Result<UserAccountOperationRequest> findLiveRegisterUserRequestByConfirmCode(String confirmCode);

  /**
   * Найти не просроченный запрос на восстановление доступа к учетной записи по коду подтверждения
   * @param confirmCode
   * @return
   */
  Result<UserAccountOperationRequest> findLiveRestoreUserAccountRequestByConfirmCode(String confirmCode);

  /**
   * Найти пользователя по почте
   *
   * @param eMail
   * @return
   */
  Result<User> findUserByeMail(String eMail);

  /**
   * Найти пользователя по имени.
   *
   * @param userName
   * @return   - ошибку , либо результат поиска
   */
  Result<User> findUserByName(String userName);

  /**
   * Поиск роли по имени.
   * @return результат поиска роли, либо ошибку
   */
  Result<Role> findRoleByName(String roleName);

  /**
   * Сохраняет в БД пользователя
   * @param user
   * @return
   */
  Result<User> saveUser(User user);

  /**
   * Сохраняет в БД пользователя
   * @param confirmCode
   * @param userName
   * @param password
   * @return
   */
  Result<User> setNewPasswordForUserName(String userName, String password, String confirmCode);
}
