package api.core.database.dao;

import api.core.Result;
import api.entity.warrior.Warrior;
import api.game.map.Player;
import core.database.entity.warrior.PlayerWarrior;
import core.database.entity.warrior.WarriorEquipment;

import java.util.List;

/**
 * DAO воины и обмундирование для пользователя
 */
public interface PlayerWarriorDao {
  /**
   * Получить всех воинов и все варианты аммуниции каждого для игрока
   * @return
   */
  Result<List<PlayerWarrior>> findAllWarriors(String userName);

    /**
     * Получить по имени воина все варианты его аммуниции
     * @param userName
     * @param heroName
     * @return
     */
  Result<PlayerWarrior> findHeroByName(String userName, String heroName);

  /**
   * Сохранить аммуницию воина
   * @param player
   * @param warrior
   * @param warriorEquipmentName
   * @return
   */
  Result<WarriorEquipment> saveWarriorEquipment(Player player, Warrior warrior, String warriorEquipmentName);

  /**
   * переименовать аммуницию воина
   * @param player
   * @param warrior
   * @param oldEquipmentName
   * @param newEquipmentName
   * @return
   */
  Result<WarriorEquipment> renameWarriorEquipment(Player player, Warrior warrior, String oldEquipmentName, String newEquipmentName);

  /**
   * переименовать аммуницию воина. Если аммуниций у воина не остается, то он тоже удаляется
   * @param player
   * @param warrior
   * @param equipmentName
   * @return
   */
  Result<WarriorEquipment> removeWarriorEquipment(Player player, Warrior warrior, String equipmentName);

}
