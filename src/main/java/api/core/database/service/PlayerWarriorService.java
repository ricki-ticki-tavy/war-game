package api.core.database.service;

import api.core.Result;
import api.entity.warrior.Warrior;
import api.game.map.Player;
import core.database.entity.warrior.PlayerWarrior;
import core.database.entity.warrior.WarriorEquipment;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Сервис работы с воинами и обмундированием для пользователя
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface PlayerWarriorService {

  /**
   * Получить всех воинов и все варианты аммуниции каждого для игрока
   * @param userName
   * @return
   */
  Result<List<PlayerWarrior>> findAllHeroes(String userName);

  /**
   * Получить по имени воина и все варианты его аммуниции
   * @param userName
   * @param heroName
   * @return
   */
  Result<PlayerWarrior> findHeroByName(String userName, String heroName);

  /**
   * Сохранить аммуницию воина
   * @param player
   * @param warrior
   * @param warriorEquipmentName
   * @return
   */
  Result<WarriorEquipment> saveWarriorEquipment(Player player, Warrior warrior, String warriorEquipmentName);

  /**
   *  Переименовать вариант экипировки воина
   * @param player                - Пользователь
   * @param warrior               - код воина
   * @param oldEquipmentName      - Старое название
   * @param newEquipmentName      - Новое название
   * @return
   */
  Result<WarriorEquipment> renameWarriorEquipment(Player player, Warrior warrior, String oldEquipmentName, String newEquipmentName);

  /**
   *  Удалить вариант экипировки воина. Если аммуниций у воина не остается, то он тоже удаляется
   * @param player          - Пользователь
   * @param warrior         - Пользователь
   * @param equipmentName   - название эеипировки
   * @return
   */
  Result<WarriorEquipment> removeWarriorEquipment(Player player, Warrior warrior, String equipmentName);

}
