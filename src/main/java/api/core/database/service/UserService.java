package api.core.database.service;

import api.core.Result;
import core.database.entity.security.User;
import core.database.entity.security.UserAccountOperationRequest;

/**
 * Сервис работы с пользователями
 */
public interface UserService {

  /**
   * Загрузить пользователя по имени
   * @param userName
   * @return
   */
  Result<User> loadByName(String userName);

  /**
   * Обработка запроса не регистрацию полльзователя
   * @param userName
   * @param password
   * @param eMail
   * @return
   */
  Result<UserAccountOperationRequest> registerUser(String userName, String password, String replyPassword, String eMail);

  /**
   * Подтверждение регистрации пользователя. Проходитт по коду подтверждения, выданному при регистрации в методе
   * registerUser
   *
   * @param confirmCode
   * @return
   */
  Result<User> confirmRegisterUser(String confirmCode);

  /**
   * Восстановления доступа к учетной записи. Запрос на смену пароля.
   *
   * @param userName
   * @param eMail
   * @return
   */
  Result<UserAccountOperationRequest> restoreAccountAccess(String userName, String eMail);

  /**
   * Восстановления доступа к учетной записи. Запрос смены пароля.
   *
   * @param confirmCode - код, полученный из метода @link[restoreAccountAccess]
   * @param password
   * @param retypePassword
   * @return
   */
  Result<User> restoreAccountAccessSetNewPassword(String confirmCode, String password, String retypePassword);

}
