package api.game.map.metadata.xml;

import api.game.map.metadata.xml.CoordsXml;

import javax.xml.bind.annotation.*;

/**
 * Прямоугольник
 */
@XmlType(propOrder = {"topLeftConner", "bottomRightConner", "color"})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "reqtangle")
public class RectangleXml {

  @XmlElement(name = "topLeftConner")
  public CoordsXml topLeftConner;

  @XmlElement(name = "bottomRightConner")
  public CoordsXml bottomRightConner;

  @XmlElement(name = "color")
  public String color;
}
