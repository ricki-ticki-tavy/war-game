package api.game.action;

import api.core.Event;
import api.entity.ability.Influencer;
import api.entity.magic.Spell;
import api.entity.warrior.Warrior;
import api.entity.weapon.Weapon;
import api.game.map.Player;

import java.util.List;

/**
 * Результат воздействия, атаки
 */
public interface InfluenceResult {

  /**
   * Получить воздействовавшего, атаковавшего юнита. Может быть NULL если атаковал сам игрок с помощью магии
   * @return
   */
  Warrior getActor();

  /**
   * Получить оружие с помощью которого была выполнена атака. Может быть NULL в случае атаки магией
   * @return
   */
  Weapon getAttackerWeapon();

  /**
   * Получить заклинание,которым ведется атака. Может быть null в случае атаки обычным оружием
   * @return
   */
  Spell getAttackerSpell();

  /**
   * Получить атаковавшего игрока
   * @return
   */
  Player getActorPlayer();

  /**
   * Получить атакованного игрока
   * @return
   */
  Player getTargetPlayer();

  /**
   * Получить того, кого атаковали
   * @return
   */
  Warrior getTarget();

  /**
   * Получить сообщение, топравленное при атаке
   * @return
   */
  Event getEvent();

  /**
   * Возвращает влияния на цель. В том числе и прямое поражение от оружия
   * @return
   */
  List<Influencer> getInfluencers();

  /**
   * Добавить влияние атаки на цель
   * @param influencer
   * @return
   */
  InfluenceResult addInfluencer(Influencer influencer);

  /**
   * Получить кол-во очков, затраченных на атаку
   * @return
   */
  int getConsumedActionPoints();

  /**
   * Получить результат ответного воздействия (контратаки). Таким образом можно проследить всю цепочку, а так же предотвратить
   * котратаку на контратаку.
   * @return
   */
  InfluenceResult getContrInfluence();

  /**
   * Установить родительское (основное) воздействия
   * @param parentInfluence
   * @return
   */
  InfluenceResult setParentInfluence(InfluenceResult parentInfluence);

  /**
   * Получить результат родительского воздействия (атаки). Таким образом можно проследить всю цепочку, а так же предотвратить
   * котратаку на контратаку.
   * @return
   */
  InfluenceResult getParentInfluence();

  /**
   * Установить результаты контрвоздействия
   * @param contrInfluence
   * @return
   */
  InfluenceResult setContrInfluence(InfluenceResult contrInfluence);
}

