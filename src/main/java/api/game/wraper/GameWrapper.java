package api.game.wraper;

import api.core.Context;
import api.core.Core;
import api.core.Result;
import api.dto.core.ContextDto;
import api.entity.ability.Influencer;
import api.entity.artifct.Artifact;
import api.entity.warrior.Warrior;
import api.entity.weapon.Weapon;
import api.game.action.InfluenceResult;
import api.game.map.Player;
import api.game.map.metadata.GameRules;
import api.game.map.metadata.xml.LevelMapMetaDataXml;
import api.geo.Coords;

import java.util.List;
import java.util.Map;

/**
 * Класс, собирающий под собой весь функционал игры
 */
public interface GameWrapper {
  /**
   * регистрация всех сущностей
   */
  void init();

  /**
   * Содание экземпляра авторизованного пользователя
   */
  Result<Player> login(String userName, Long userId);


  /**
   * выполнить выход игрока из логическогоядра.
   *
   * @param playerName
   * @return
   */
  Result logout(String playerName);

  /**
   * Получить список доступных карт
   * @return
   */
  Result<List<LevelMapMetaDataXml>> getLevelMapsList(String userName);

  /**
   * Подключиться к игре
   * @param userName
   * @param contextId
   * @return
   */
  Result<Player> connectToGame(String userName, String contextId);

  /**
   * Создать новую игру
   *
   * @param ownerUserName
   * @param gameRules
   * @param mapName
   * @param gameName
   * @param hidden
   * @return
   */
  Result<Context> createGame(String ownerUserName
          , GameRules gameRules
          , String mapName
          , String gameName
          , boolean hidden);


  /**
   * Создать технический контекст
   *
   * @param ownerUserName
   * @return
   */
  Result<Context> createTechnicalContext(String ownerUserName);

  /**
   * Получить список игровых контекстов
   * @return
   */
  Result<List<Context>> getGamesList();

  /**
   * Добавить воина игроку
   * @param userName
   * @param className
   * @return
   */
  Result<Warrior> createWarrior(String contextId, String userName, String className, Coords coords);

  /**
   * Атаковать выбранным оружием другого воина
   * @param contextId
   * @param userName
   * @param attackerWarriorId
   * @param targetWarriorId
   * @param weaponId
   * @return
   */
  Result<InfluenceResult> attackWarrior(String contextId, String userName, String attackerWarriorId, String targetWarriorId, String weaponId);

  /**
   * Переместить юнит на заданные координаты
   * @param userName
   * @param warriorId
   * @param coords
   * @return
   */
  Result<Warrior> moveWarriorTo(String contextId, String userName, String warriorId, Coords coords);

  /**
   * Откатить перемещение воина
   * @return
   */
  Result<Warrior> rollbackMove(String contextId, String userName, String warriorId);

  /**
   * Возвращает координаты,куда можно переместить перемещаемого юнита, исходя из того, куда его хотят переместить
   * @param contextId
   * @param userName
   * @param warriorId
   * @param coords
   * @return
   */
  Result<Coords> whatIfMoveWarriorTo(String contextId, String userName, String warriorId, Coords coords);

  /**
   * Вооружить воина предметом
   * @param userName
   * @param warriorId
   * @param weaponName
   * @return
   */
  Result<Weapon> giveWeaponToWarrior(String contextId, String userName, String warriorId, String weaponName);

  /**
   * Бросить оружие
   * @param contextId
   * @param userName
   * @param warriorId
   * @param weaponId
   * @return
   */
  Result<Weapon> dropWeaponByWarrior(String contextId, String userName, String warriorId, String weaponId);

  /**
   * дать воину артефакт. Если такой артефакт такого типа уже есть, то будет отказ. Доступно только на этапе
   * расстановки войска
   * @param userName      код игрока, которому принадлежит воин
   * @param warriorId     код воина, которому дается артефакт
   * @param artifactName  название выдаваемого артефакта
   * @return
   */
  Result<Artifact<Warrior>> giveArtifactToWarrior(String contextId, String userName, String warriorId, String artifactName);

  /** выбросить артефакт воина
   *
   * @param contextId
   * @param userName
   * @param artifactInstanceId
   * @param warriorId
   * @return
   */
  Result<Artifact<Warrior>> dropArtifactByWarrior(String contextId, String userName, String warriorId, String artifactInstanceId);

  /**
   * Наложить заклинание игроком
   * @param contextId              - контекст
   * @param userName               - игрок
   * @param spellName              - название заклинания
   * @param givenSpellParameters   - параметры, необходимые заклинанию
   * @return
   */
  Result<List<Result<InfluenceResult>>> castSpellByPlayer(String contextId, String userName, String spellName, Map<Integer, String> givenSpellParameters);

  /**
   * Наложить заклинание воином
   * @param contextId              - контекст
   * @param userName               - игрок
   * @param warriorId              - id воина, который должен наложить заклинание
   * @param spellName              - название заклинания
   * @param givenSpellParameters   - параметры, необходимые заклинанию
   * @return
   */
  Result<List<Result<InfluenceResult>>> castSpellByWarrior(String contextId, String userName, String warriorId, String spellName, Map<Integer, String> givenSpellParameters);

  /**
   * Дать игроку артефакт
   * @param contextId
   * @param userName
   * @param artifactName
   * @return
   */
  Result<Artifact<Player>> giveArtifactToPlayer(String contextId, String userName, String artifactName);

  /**
   * бросить артефакт игроком
   * @param userName
   * @param artifactInstanceId
   * @return
   */
  Result<Artifact<Player>> dropArtifactByPlayer(String contextId, String userName, String artifactInstanceId);

  /**
   * Найти юнит по его коду независимо от того, какому игроку он принадлежит
   * @param contextId
   * @param warriorId
   * @return
   */
  Result<Warrior> findWarriorById(String contextId, String warriorId);

  /**
   * Проверяет возможно ли выполнение атаки или применения способности данным юнитом в этом ходе
   * @param contextId
   * @param userName
   * @param warriorId
   * @return
   */
  Result<Warrior> ifWarriorCanActsAtThisTurn(String contextId, String userName, String warriorId);

  /**
   * Найти оружие по его id
   * @param contextId
   * @param userName
   * @param warriorId
   * @param weaponId
   * @return
   */
  Result<Weapon> findWeaponById(String contextId, String userName, String warriorId, String weaponId);

  /**
   * Удалить юнит игроком
   * @param contextId
   * @param userName
   * @param warriorId
   * @return
   */
  Result<Warrior> removeWarrior(String contextId, String userName, String warriorId);

  /**
   * Уведомить, что игрок закончил расстановку и готов к игре
   * @param userName
   * @return
   */
  Result<Player> playerReadyToPlay(String userName, boolean readyToPlay);

  /**
   * Возвращает игрока, которому сейчас принадлежит ход
   * @return
   */
  Result<Player> getPlayerOwnsTheRound(String contextId);

  /**
   * Возвращает имена базовых классов воинов
   * @return
   */
  Result<List<String>> getBaseWarriorClasses();

  /**
   * Возвращает имена базовых классов вооружения
   * @return
   */
  Result<List<String>> getWeaponClasses();

  /**
   * Передача хода следующему игроку
   * @param contextId
   * @param userName
   * @return
   */
  Result<Player> nextTurn(String contextId, String userName);

  /**
   * Получить список оказываемых влияний на юнит
   * @return
   */
  Result<List<Influencer>> getWarriorSInfluencers(String contextId, String userName, String warriorId);

  /**
   * Вернуть кол-во кругов игры. Сколько раз по кругу сходили все игроки
   * @return
   */
  Result<Integer> getRoundsCount(String contextId);

  Core getCore();


  /**
   * Сохранить в БД экипировку воина пользователя
   * @param contextId
   * @param userName
   * @param warriorId
   * @param ammunitionName
   * @return
   */
  Result<Warrior> saveWarriorAmmunition(String contextId, String userName, String warriorId, String ammunitionName);

  /**
   *  Переименовать вариант экипировки воина
   * @param contextId
   * @param userName                 - Пользователь
   * @param warriorId                - код воина
   * @param oldEquipmentName        - Старое название
   * @param newEquipmentName        - Новое название
   * @return
   */
  Result<Warrior> renameWarriorEquipment(String contextId, String userName, String warriorId, String oldEquipmentName, String newEquipmentName);

  /**
   *  Удалить вариант экипировки воина. Если аммуниций у воина не остается, то он тоже удаляется
   * @param contextId
   * @param userName       - Пользователь
   * @param warriorId       - Пользователь
   * @param equipmentName - название эеипировки
   * @return
   */
  Result<Warrior> removeWarriorEquipment(String contextId, String userName, String warriorId, String equipmentName);

  /**
   * Получить список героев пользователя
   * @param contextId
   * @param userName
   * @return
   */
  Result<String> getPayerHeroesAsJson(String contextId, String userName);

  /**
   * Получить геря и все варианты амуниции по имени
   * @param contextId
   * @param userName
   * @return
   */
  Result<String> getPayerHeroAsJson(String contextId, String userName, String heroName);

  /**
   * Получить список базовых классов воинов
   * @param contextId
   * @return
   */
  Result<String> getWarriorBaseClassesAsJson(String contextId);

  /**
   * Получить список видов оружия
   * @param contextId
   * @return
   */
  Result<String> getWeaponClassesAsJson(String contextId);

  /**
   * Получить список заклинаний игрока
   * @param contextId
   * @return
   */
  Result<String> getPlayerSpellsAsJson(String contextId);

  /**
   * Получить список заклинаний воинов
   * @param contextId
   * @return
   */
  Result<String> getWarriorSpellsAsJson(String contextId);

/**
   * Получить список артефактов воина
   * @param contextId
   * @return
   */
  Result<String> getWarriorArtifactsAsJson(String contextId);

/**
   * Получить список артефактов игрока
   * @param contextId
   * @return
   */
  Result<String> getPlayerArtifactsAsJson(String contextId);


  /**
   * Получить Подробную информацию о всех воинов игрока или всех игроков
   * @param contextId
   * @param userName       - Пользоватьель
   * @param ownerUserName  - Игрок, чьих воинов нужно получить. Если пусто, то будут возвращены воины всех игроков карты
   * @return
   */
  Result<String> getPlayerWarriorsAsJson(String contextId, String userName, String ownerUserName);

  /**
   * Получить список активных в чате пользователей
   * @param userName
   * @return
   */
  Result<List<String>> getActiveUsersList(String userName);

  /**
   * Установить новое значение видимости игры в списке игр
   * @param contextId
   * @param userName
   * @param hidden
   * @return
   */
  Result<Context> setGameVisibleMode(String contextId, String userName, boolean hidden);

  /**
   * Запрос имеющегося у игрока активного игрового контекста, если такой есть
   * @param userName
   * @return
   */
  Result<ContextDto> getExistingGameContext(String userName);

  /**
   * Получить всю информацию и данные по игре
   * @param userName
   * @return
   */
  Result<ContextDto> getGameFullData(String userName);

  /**
   * Запрос списка активных игр
   * @param userName
   * @return
   */
  Result<List<ContextDto>> getActiveGameList(String userName);

  /**
   * Бегство отряда игрока с арены
   * @param contextId
   * @param userName
   * @return
   */
  Result<Player> decampFromTheArena(String contextId, String userName);

}
