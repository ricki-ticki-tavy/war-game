package api.dto.common;

import api.entity.graphics.GraphicsInfo;
import api.entity.warrior.WarriorBaseClassInfo;
import api.dto.ability.AbilityDto;
import api.dto.spell.SpellDto;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Класс - обертка для сериализации базовых классов воинов в JSON
 */
public class HeroBaseClassDto {
  private String title;
  private String description;
  private List<AbilityDto> abilities;
  private List<SpellDto> spells;
  private GraphicsInfo graphicsInfo;

  //===============================================================================
  //===============================================================================

  /**
   * Название
   *
   * @return
   */
  public String getTitle() {
    return title;
  }

  public HeroBaseClassDto setTitle(String title) {
    this.title = title;
    return this;
  }
  //===============================================================================

  /**
   * Описание
   *
   * @return
   */
  public String getDescription() {
    return description;
  }

  public HeroBaseClassDto setDescription(String description) {
    this.description = description;
    return this;
  }
  //===============================================================================

  /**
   * Способности
   *
   * @return
   */
  public List<AbilityDto> getAbilities() {
    return abilities;
  }

  public HeroBaseClassDto setAbilities(List<AbilityDto> abilities) {
    this.abilities = abilities;
    return this;
  }
  //===============================================================================

  /**
   * заклинания
   *
   * @return
   */
  public List<SpellDto> getSpells() {
    return spells;
  }

  public HeroBaseClassDto setSpells(List<SpellDto> spells) {
    this.spells = spells;
    return this;
  }
  //===============================================================================

  public GraphicsInfo getGraphicsInfo() {
    return graphicsInfo;
  }
  //===============================================================================

  public HeroBaseClassDto setGraphicsInfo(GraphicsInfo graphicsInfo) {
    this.graphicsInfo = graphicsInfo;
    return this;
  }
  //===============================================================================

  public static HeroBaseClassDto fromInfo(WarriorBaseClassInfo warriorBaseClass) {
    return new HeroBaseClassDto()
            .setAbilities(AbilityDto.listFromInfos(warriorBaseClass.getAbilitiesInfo()))
            .setSpells(SpellDto.listFromInfos(warriorBaseClass.getSpellsInfo()))
            .setTitle(warriorBaseClass.getName())
            .setDescription(warriorBaseClass.getDescription())
            .setGraphicsInfo(warriorBaseClass.getGraphicsInfo());


  }
  //===============================================================================

  public static List<HeroBaseClassDto> listFromInfos(List<WarriorBaseClassInfo> warriorBaseClasses) {
    return warriorBaseClasses.stream()
            .map(warriorBaseClass -> HeroBaseClassDto.fromInfo(warriorBaseClass))
            .collect(Collectors.toList());
  }
  //===============================================================================

}
