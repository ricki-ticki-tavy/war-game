package api.dto.common;

import api.core.Core;
import core.database.entity.warrior.PlayerWarrior;
import api.dto.warrior.WarriorEquipmentDto;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Класс для сериализации ответа на запрос списка воинов, созданных и сохраненных игроком
 * Передавать нужно лист этих классов
 */
public class PlayerHeroDto {
  private String title;
  private String baseClassName;
  private Set<WarriorEquipmentDto> equipments;
  private int expirience;
  private int deathCount;
  private int goldPrice;

  //===============================================================================
  //===============================================================================

  /**
   * Название воина
   *
   * @return
   */
  public String getTitle() {
    return title;
  }

  public PlayerHeroDto setTitle(String title) {
    this.title = title;
    return this;
  }
  //===============================================================================

  /**
   * Название базового класса воина
   *
   * @return
   */
  public String getBaseClassName() {
    return baseClassName;
  }

  public PlayerHeroDto setBaseClassName(String baseClassName) {
    this.baseClassName = baseClassName;
    return this;
  }
  //===============================================================================

  /**
   * Накопленный опыт
   *
   * @return
   */
  public int getExpirience() {
    return expirience;
  }

  public PlayerHeroDto setExpirience(int expirience) {
    this.expirience = expirience;
    return this;
  }
  //===============================================================================

  /**
   * Кол-во смертей
   *
   * @return
   */
  public int getDeathCount() {
    return deathCount;
  }

  public PlayerHeroDto setDeathCount(int deathCount) {
    this.deathCount = deathCount;
    return this;
  }
  //===============================================================================


  public int getGoldPrice() {
    return goldPrice;
  }
  //===============================================================================

  public PlayerHeroDto setGoldPrice(int goldPrice) {
    this.goldPrice = goldPrice;
    return this;
  }
  //===============================================================================

  /**
   * пары название экипировки - сериализованные данные экипировки
   *
   * @return
   */
  public Set<WarriorEquipmentDto> getEquipments() {
    return equipments;
  }

  public PlayerHeroDto setEquipments(Set<WarriorEquipmentDto> equipments) {
    List<WarriorEquipmentDto> equips = new ArrayList<>(equipments);
    Collections.sort(equips, new Comparator<WarriorEquipmentDto>(){
      public int compare(WarriorEquipmentDto o2, WarriorEquipmentDto o1){
        return o1.getId().compareTo(o2.getId());
      }
    });

    this.equipments = new TreeSet(equips);
    return this;
  }
  //===============================================================================

  public static PlayerHeroDto fromEntity(PlayerWarrior playerWarrior, Core core) {
    return new PlayerHeroDto()
            .setBaseClassName(playerWarrior.getWarriorBaseClassName())
            .setDeathCount(playerWarrior.getDeathCount())
            .setExpirience(playerWarrior.getExpirience().intValue())
            .setTitle(playerWarrior.getName())
            .setGoldPrice(core.findWarriorBaseClassByName(playerWarrior.getWarriorBaseClassName()).getResult().getGoldCost())
            .setEquipments(playerWarrior.getWarriorEquipments()
                    .stream()
                    .map(warriorEquipment -> WarriorEquipmentDto.fromEntity(warriorEquipment, core))
                    .collect(Collectors.toSet()));
  }
  //===============================================================================

  public static List<PlayerHeroDto> listFromEntity(List<PlayerWarrior> playerWarriors, Core core) {
    return playerWarriors.stream()
            .map(playerWarrior -> fromEntity(playerWarrior, core))
            .collect(Collectors.toList());
  }
  //===============================================================================
}
