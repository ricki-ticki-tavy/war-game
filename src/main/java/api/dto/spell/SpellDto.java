package api.dto.spell;

import api.entity.magic.SpellInfo;
import api.enums.SignOfInfluenceEnum;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Класс-обертка для сериализации в JSON описания заклинания
 */
public class SpellDto {
  private String title;
  private String description;
  private int magicCost;
  private int actionPoints;
  private SignOfInfluenceEnum spellSign;
  private Integer influenceRadius;
  private List<SpellParameterDto> additionalParameters;

  //===============================================================================
  //===============================================================================


  /**
   * Название
   *
   * @return
   */
  public String getTitle() {
    return title;
  }

  public SpellDto setTitle(String title) {
    this.title = title;
    return this;
  }
  //===============================================================================

  /**
   * Описание
   *
   * @return
   */
  public String getDescription() {
    return description;
  }

  public SpellDto setDescription(String description) {
    this.description = description;
    return this;
  }
  //===============================================================================

  /**
   * Стоимость в единицах магии
   *
   * @return
   */
  public int getMagicCost() {
    return magicCost;
  }

  public SpellDto setMagicCost(int magicCost) {
    this.magicCost = magicCost;
    return this;
  }
  //===============================================================================

  /**
   * стоимость в единицах действия
   *
   * @return
   */
  public int getActionPoints() {
    return actionPoints;
  }

  public SpellDto setActionPoints(int actionPoints) {
    this.actionPoints = actionPoints;
    return this;
  }
  //===============================================================================

  public List<SpellParameterDto> getAdditionalParameters() {
    return additionalParameters;
  }

  public SpellDto setAdditionalParameters(List<SpellParameterDto> additionalParameters) {
    this.additionalParameters = additionalParameters;
    return this;
  }


  //===============================================================================

  public SignOfInfluenceEnum getSpellSign() {
    return spellSign;
  }

  public SpellDto setSpellSign(SignOfInfluenceEnum spellSign) {
    this.spellSign = spellSign;
    return this;
  }
  //===============================================================================

  public Integer getInfluenceRadius() {
    return influenceRadius;
  }

  public SpellDto setInfluenceRadius(Integer influenceRadius) {
    this.influenceRadius = influenceRadius;
    return this;
  }
  //===============================================================================
  //===============================================================================

  public static SpellDto fromInfo(SpellInfo spell) {
    return new SpellDto()
            .setTitle(spell.getName())
            .setDescription(spell.getDescription())
            .setActionPoints(spell.getActionPointsCost())
            .setMagicCost(spell.getMannaCost())
            .setSpellSign(spell.getSpellSign())
            .setInfluenceRadius(spell.getInfluenceRadius())
            .setAdditionalParameters(spell.getRequiredAdditionalParams().values().stream()
                    .map(spellParameter -> SpellParameterDto.fromSpellParameter(spellParameter))
                    .collect(Collectors.toList()));
  }
  //===============================================================================

  public static List<SpellDto> listFromInfos(List<SpellInfo> spells) {
    return spells.stream()
            .map(spell -> SpellDto.fromInfo(spell))
            .collect(Collectors.toList());
  }
  //===============================================================================
}
