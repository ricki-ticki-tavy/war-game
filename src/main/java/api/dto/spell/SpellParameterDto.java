package api.dto.spell;

import api.entity.magic.SpellParameter;
import api.geo.Coords;
import javafx.util.Pair;

/**
 * трансферный класс для параметра заклинания
 */
public class SpellParameterDto {
  private String title;
  private String type;
  private String typeTitle;
  private Integer intMin;
  private Integer intMax;
  private Coords middleCoords;
  private Integer distanceMax;

  private SpellParameterDto(){

  }

  public static SpellParameterDto fromSpellParameter(SpellParameter spellParameter){
    SpellParameterDto spellParameterDto = new SpellParameterDto();

    spellParameterDto.title = spellParameter.getTitle();
    spellParameterDto.typeTitle = spellParameter.getType().getTitle();
    spellParameterDto.type = spellParameter.getType().name();

    if (spellParameter.getIntegerRestrictions() != null){
        spellParameterDto.intMin = spellParameter.getIntegerRestrictions().getKey();
        spellParameterDto.intMax = spellParameter.getIntegerRestrictions().getValue();
    }

    if (spellParameter.getCoordsRestrictions() != null){
        spellParameterDto.middleCoords = spellParameter.getCoordsRestrictions().getKey();
        spellParameterDto.distanceMax = spellParameter.getCoordsRestrictions().getValue();
    }

    return spellParameterDto;
  }

}
