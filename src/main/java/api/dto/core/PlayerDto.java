package api.dto.core;

import api.dto.artifact.ArtifactDto;
import api.dto.warrior.WarriorDto;
import api.game.map.Player;
import api.geo.Rectangle;

import java.util.Set;
import java.util.stream.Collectors;

public class PlayerDto {
  private String userName;
  private int manna;
  private boolean artifactWasDroppedOrTakenAtThisRound;
  private boolean readyToPlay;
  private Set<WarriorDto> warriors;
  private Set<ArtifactDto> artifacts;
  private Set<String> spellsUsedAtThisTurn;
  private Rectangle startZone;

  public String getUserName() {
    return userName;
  }

  public PlayerDto setUserName(String userName) {
    this.userName = userName;
    return this;
  }

  public Rectangle getStartZone() {
    return startZone;
  }

  public PlayerDto setStartZone(Rectangle startZone) {
    this.startZone = startZone;
    return this;
  }

  public int getManna() {
    return manna;
  }

  public PlayerDto setManna(int manna) {
    this.manna = manna;
    return this;
  }

  public boolean isArtifactWasDroppedOrTakenAtThisRound() {
    return artifactWasDroppedOrTakenAtThisRound;
  }

  public PlayerDto setArtifactWasDroppedOrTakenAtThisRound(boolean artifactWasDroppedOrTakenAtThisRound) {
    this.artifactWasDroppedOrTakenAtThisRound = artifactWasDroppedOrTakenAtThisRound;
    return this;
  }

  public boolean isReadyToPlay() {
    return readyToPlay;
  }

  public PlayerDto setReadyToPlay(boolean readyToPlay) {
    this.readyToPlay = readyToPlay;
    return this;
  }

  public Set<WarriorDto> getWarriors() {
    return warriors;
  }

  public PlayerDto setWarriors(Set<WarriorDto> warriors) {
    this.warriors = warriors;
    return this;
  }

  public Set<ArtifactDto> getArtifacts() {
    return artifacts;
  }

  public PlayerDto setArtifacts(Set<ArtifactDto> artifacts) {
    this.artifacts = artifacts;
    return this;
  }

  public Set<String> getSpellsUsedAtThisTurn() {
    return spellsUsedAtThisTurn;
  }

  public PlayerDto setSpellsUsedAtThisTurn(Set<String> spellsUsedAtThisTurn) {
    this.spellsUsedAtThisTurn = spellsUsedAtThisTurn;
    return this;
  }

  public static PlayerDto fromEntity(Player player) {
    return fromEntity(player, false);
  }

  public static PlayerDto fromEntity(Player player, boolean shortDto) {
    PlayerDto playerDto = new PlayerDto()
            .setUserName(player.getId())
            .setReadyToPlay(player.isReadyToPlay());

    if (!shortDto) {
      playerDto
              .setManna(player.getManna())
              .setStartZone(player.getStartZone())
              .setArtifactWasDroppedOrTakenAtThisRound(player.isArtifactWasDroppedOrTakenAtThisRound())
              .setWarriors(player.getWarriors().stream()
                      .map(warrior -> WarriorDto.fromEntity(warrior))
                      .collect(Collectors.toSet()))
              .setArtifacts(player.getArtifacts().getResult().stream()
                      .map(playerArtifact -> ArtifactDto.fromEntity(playerArtifact))
                      .collect(Collectors.toSet()));
    }

    return playerDto;
  }
}
