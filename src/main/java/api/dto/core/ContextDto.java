package api.dto.core;

import api.core.Context;
import api.game.map.Player;
import api.game.map.metadata.GameRules;
import api.game.map.metadata.xml.LevelMapMetaDataXml;

import java.util.Set;
import java.util.stream.Collectors;

public class ContextDto {
  private String contextId;
  private String ownedByUser;
  private boolean gameRan;
  private boolean hidden;
  private boolean technical;
  private String gameName;
  private Set<PlayerDto> players;

  private int maxWarriorsCount;
  private int maxSummonedWarriorsCount;
  private int movesPerRoundCount;
  private int playersCount;
  private int gold;

  private LevelMapMetaDataXml mapMetadata;
  private GameRules gameRules;

  private String playerOwnsThisTurn;

  public Set<PlayerDto> getPlayers() {
    return players;
  }

  public ContextDto setPlayers(Set<PlayerDto> players) {
    this.players = players;
    return this;
  }


  public String getContextId() {
    return contextId;
  }

  public ContextDto setContextId(String contextId) {
    this.contextId = contextId;
    return this;
  }

  public String getOwnedByUser() {
    return ownedByUser;
  }

  public ContextDto setOwnedByUser(String ownedByUser) {
    this.ownedByUser = ownedByUser;
    return this;
  }

  public boolean isGameRan() {
    return gameRan;
  }

  public ContextDto setGameRan(boolean gameRan) {
    this.gameRan = gameRan;
    return this;
  }

  public boolean isTechnical() {
    return technical;
  }

  public ContextDto setTechnical(boolean technical) {
    this.technical = technical;
    return this;
  }

  public String getGameName() {
    return gameName;
  }

  public ContextDto setGameName(String gameName) {
    this.gameName = gameName;
    return this;
  }

  public boolean isHidden() {
    return hidden;
  }

  public ContextDto setHidden(boolean hidden) {
    this.hidden = hidden;
    return this;
  }

  public int getMaxWarriorsCount() {
    return maxWarriorsCount;
  }

  public ContextDto setMaxWarriorsCount(int maxWarriorsCount) {
    this.maxWarriorsCount = maxWarriorsCount;
    return this;
  }

  public int getMaxSummonedWarriorsCount() {
    return maxSummonedWarriorsCount;
  }

  public ContextDto setMaxSummonedWarriorsCount(int maxSummonedWarriorsCount) {
    this.maxSummonedWarriorsCount = maxSummonedWarriorsCount;
    return this;
  }

  public int getMovesPerRoundCount() {
    return movesPerRoundCount;
  }

  public ContextDto setMovesPerRoundCount(int movesPerRoundCount) {
    this.movesPerRoundCount = movesPerRoundCount;
    return this;
  }

  public int getPlayersCount() {
    return playersCount;
  }

  public ContextDto setPlayersCount(int playersCount) {
    this.playersCount = playersCount;
    return this;
  }

  public int getGold() {
    return gold;
  }

  public ContextDto setGold(int gold) {
    this.gold = gold;
    return this;
  }

  public LevelMapMetaDataXml getMapMetadata() {
    return mapMetadata;
  }

  public ContextDto setMapMetadata(LevelMapMetaDataXml mapMetadata) {
    this.mapMetadata = mapMetadata;
    return this;
  }

  public GameRules getGameRules() {
    return gameRules;
  }

  public ContextDto setGameRules(GameRules gameRules) {
    this.gameRules = gameRules;
    return this;
  }

  public String getPlayerOwnsThisTurn() {
    return playerOwnsThisTurn;
  }

  public ContextDto setPlayerOwnsThisTurn(String playerOwnsThisTurn) {
    this.playerOwnsThisTurn = playerOwnsThisTurn;
    return this;
  }

  public static ContextDto fromEntity(Context context, boolean shortDto) {
    Player resultTurnOwner = context.getLevelMap().getPlayerOwnsThisTurn().getResult();
    ContextDto contextDto = new ContextDto()
            .setMapMetadata(context.getLevelMap().getMapMetadata())
            .setGameRules(context.getGameRules())
            .setContextId(context.getContextId())
            .setGameName(context.getGameName())
            .setHidden(context.isHidden())
            .setPlayerOwnsThisTurn(resultTurnOwner != null ? resultTurnOwner.getId() : null)
            .setOwnedByUser(context.getContextCreator().getId())
            .setGameRan(context.isGameRan())
            .setTechnical(context.isTechnical())
            .setPlayersCount(context.getLevelMap().getMaxPlayerCount())
            .setMaxWarriorsCount(context.getGameRules().getMaxStartCreaturePerPlayer())
            .setMaxSummonedWarriorsCount(context.getGameRules().getMaxStartCreaturePerPlayer())
            .setMovesPerRoundCount(context.getGameRules().getMovesCountPerTurnForEachPlayer())
            .setGold(context.getCore().calculatePrice(context.getContextCreator()).getResult())
            .setPlayers(context.getLevelMap().getPlayers().stream()
                    .map(player -> PlayerDto.fromEntity(player, shortDto))
                    .collect(Collectors.toSet()));

    return contextDto;
  }
}
