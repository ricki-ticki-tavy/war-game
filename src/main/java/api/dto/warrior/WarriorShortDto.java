package api.dto.warrior;

import api.dto.artifact.ArtifactDto;
import api.dto.weapon.WeaponShortDto;

import java.util.Set;

/**
 * Трасферный объект юнита (воина). Для сохранения в БД / восстановления
 */
public class WarriorShortDto {
  /**
   * оружие. мэп - названия оружия ## номер   + список названий артефактов в оружии
   */
  public Set<WeaponShortDto> weapons;

  /**
   * базовый класс
   */
  public String warriorBaseClass;

  /**
   * Код в базе
   */
  public Long databaseId;

  /**
   * Артефакты
   */
  public Set<ArtifactDto> artifacts;

  /**
   * Имя воина
   */
  public String title;

  public WarriorShortDto setWeapons(Set<WeaponShortDto> weapons) {
    this.weapons = weapons;
    return this;
  }

  public WarriorShortDto setWarriorBaseClass(String warriorBaseClass) {
    this.warriorBaseClass = warriorBaseClass;
    return this;
  }

  public WarriorShortDto setDatabaseId(Long databaseId) {
    this.databaseId = databaseId;
    return this;
  }

  public WarriorShortDto setArtifacts(Set<ArtifactDto> artifacts) {
    this.artifacts = artifacts;
    return this;
  }

  public WarriorShortDto setTitle(String title) {
    this.title = title;
    return this;
  }

  public Set<WeaponShortDto> getWeapons() {
    return weapons;
  }

  public String getWarriorBaseClass() {
    return warriorBaseClass;
  }

  public Long getDatabaseId() {
    return databaseId;
  }

  public Set<ArtifactDto> getArtifacts() {
    return artifacts;
  }

  public String getTitle() {
    return title;
  }
}
