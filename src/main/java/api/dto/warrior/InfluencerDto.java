package api.dto.warrior;

import api.entity.ability.Influencer;
import api.enums.LifeTimeUnit;

import java.util.Set;
import java.util.stream.Collectors;

public class InfluencerDto {

  private LifeTimeUnit lifeTimeUnit;
  private int lifeTime;
  private ModifierDto modifier;
  private String id;
  private Set<InfluencerDto> children;

  public LifeTimeUnit getLifeTimeUnit() {
    return lifeTimeUnit;
  }

  public InfluencerDto setLifeTimeUnit(LifeTimeUnit lifeTimeUnit) {
    this.lifeTimeUnit = lifeTimeUnit;
    return this;
  }

  public int getLifeTime() {
    return lifeTime;
  }

  public InfluencerDto setLifeTime(int lifeTime) {
    this.lifeTime = lifeTime;
    return this;
  }

  public ModifierDto getModifier() {
    return modifier;
  }

  public InfluencerDto setModifier(ModifierDto modifier) {
    this.modifier = modifier;
    return this;
  }

  public String getId() {
    return id;
  }

  public InfluencerDto setId(String id) {
    this.id = id;
    return this;
  }

  public Set<InfluencerDto> getChildren() {
    return children;
  }

  public InfluencerDto setChildren(Set<InfluencerDto> children) {
    this.children = children;
    return this;
  }

  public static InfluencerDto fromEntity(Influencer influencer) {
    return new InfluencerDto()
            .setLifeTimeUnit(influencer.getLifeTimeUnit())
            .setLifeTime(influencer.getLifeTime())
            .setModifier(ModifierDto.fromEntity(influencer.getModifier()))
            .setId(influencer.getId())
            .setChildren(influencer.getChildren().stream()
                    .map(influencer1 -> InfluencerDto.fromEntity(influencer1))
                    .collect(Collectors.toSet()));
  }

}
