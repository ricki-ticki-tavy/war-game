package api.dto.warrior;

import api.entity.warrior.Warrior;
import api.enums.PlayerPhaseType;
import api.geo.Coords;
import api.dto.artifact.ArtifactDto;
import api.dto.weapon.WeaponShortDto;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * Трасферный объект юнита (воина). Содержит максимум информации для передачи на клиента
 */
public class WarriorDto {
  /**
   * базовый класс
   */
  private String warriorBaseClass;

  /**
   * Код в базе
   */
  private Long databaseId;

  /**
   * Имя воина
   */
  private String title;

  /**
   * UUID воина
   */
  private String id;

  /**
   * Имя игрока-владельца
   */
  private String ownedByUser;

  /**
   * Стоимость воина со всеми его предметами
   */
  private int goldCost = 0;

  /**
   * Координаты воина
   */
  private Coords coords;

  /**
   * Призванный воин
   */
  private boolean summoned;

  /**
   * Начальные координаты в начале хода
   */
  private Coords originalCoords;

  /**
   * Здоровье
   */
  private int health;

  /**
   * запас магии
   */
  private int manna;

  /**
   * уровень брони
   */
  private int armor;

  /**
   * цена перемещения
   */
  private int moveCost;

  /**
   * Оставшееся кол-во очков действия для применения способностей
   */
  private int abilityActionPoints;

  /**
   * Оставшееся кол-во очков действия для перемещения, атаки и защиты
   */
  private int actionPoints;

  /**
   * Удача при рукопашной атаке
   */
  private int luckMeleeAtack;

  /**
   * Удача при дистанционной атаке
   */
  private int luckRangeAtack;

  /**
   * Удача при защите
   */
  private int luckDefense;

  /**
   * Артефакты
   */
  private Set<ArtifactDto> artifacts;

  /**
   * Оружие воина
   */
  private Set<WeaponShortDto> weapons;

  /**
   * Применялся в этом ходе
   */
  private boolean touchedAtThisTurn;

  /**
   * Выполнил контратаку в этом ходе
   */
  private boolean contrattackUsedAtThisTurn;

  /**
   * Мертв
   */
  private boolean dead;

  /**
   * Двигаться не может
   */
  private boolean moveLocked;

  /**
   * Откат перемещения заблокирован
   */
  private boolean rollbackAvailable;

  /**
   * Использовано очков действия в этом ходу на перемещение
   */
  private int treatedActionPointsForMove;

  /**
   * Фаза воина (атака / защита)
   */
  private PlayerPhaseType warriorPhase;

  /**
   * Влияния
   */
  private Set<InfluencerDto> influencers;


  public WarriorDto setWeapons(Set<WeaponShortDto> weapons) {
    this.weapons = weapons;
    return this;
  }

  public WarriorDto setWarriorBaseClass(String warriorBaseClass) {
    this.warriorBaseClass = warriorBaseClass;
    return this;
  }

  public WarriorDto setDatabaseId(Long databaseId) {
    this.databaseId = databaseId;
    return this;
  }

  public WarriorDto setArtifacts(Set<ArtifactDto> artifacts) {
    this.artifacts = artifacts;
    return this;
  }

  public Set<ArtifactDto> getArtifacts() {
    return artifacts;
  }

  public WarriorDto setTitle(String title) {
    this.title = title;
    return this;
  }

  public Set<WeaponShortDto> getWeapons() {
    return weapons;
  }

  public String getWarriorBaseClass() {
    return warriorBaseClass;
  }

  public Long getDatabaseId() {
    return databaseId;
  }

  public String getTitle() {
    return title;
  }

  public String getId() {
    return id;
  }

  public WarriorDto setId(String id) {
    this.id = id;
    return this;
  }

  public String getOwnedByUser() {
    return ownedByUser;
  }

  public WarriorDto setOwnedByUser(String ownedByUser) {
    this.ownedByUser = ownedByUser;
    return this;
  }

  public Coords getCoords() {
    return coords;
  }

  public WarriorDto setCoords(Coords coords) {
    this.coords = coords;
    return this;
  }

  public boolean isSummoned() {
    return summoned;
  }

  public WarriorDto setSummoned(boolean summoned) {
    this.summoned = summoned;
    return this;
  }

  public Coords getOriginalCoords() {
    return originalCoords;
  }

  public WarriorDto setOriginalCoords(Coords originalCoords) {
    this.originalCoords = originalCoords;
    return this;
  }

  public int getHealth() {
    return health;
  }

  public WarriorDto setHealth(int health) {
    this.health = health;
    return this;
  }

  public int getManna() {
    return manna;
  }

  public WarriorDto setManna(int manna) {
    this.manna = manna;
    return this;
  }

  public int getArmor() {
    return armor;
  }

  public WarriorDto setArmor(int armor) {
    this.armor = armor;
    return this;
  }

  public int getMoveCost() {
    return moveCost;
  }

  public WarriorDto setMoveCost(int moveCost) {
    this.moveCost = moveCost;
    return this;
  }

  public int getAbilityActionPoints() {
    return abilityActionPoints;
  }

  public WarriorDto setAbilityActionPoints(int abilityActionPoints) {
    this.abilityActionPoints = abilityActionPoints;
    return this;
  }

  public int getActionPoints() {
    return actionPoints;
  }

  public WarriorDto setActionPoints(int actionPoints) {
    this.actionPoints = actionPoints;
    return this;
  }

  public int getLuckMeleeAtack() {
    return luckMeleeAtack;
  }

  public WarriorDto setLuckMeleeAtack(int luckMeleeAtack) {
    this.luckMeleeAtack = luckMeleeAtack;
    return this;
  }

  public int getLuckRangeAtack() {
    return luckRangeAtack;
  }

  public WarriorDto setLuckRangeAtack(int luckRangeAtack) {
    this.luckRangeAtack = luckRangeAtack;
    return this;
  }

  public int getLuckDefense() {
    return luckDefense;
  }

  public WarriorDto setLuckDefense(int luckDefense) {
    this.luckDefense = luckDefense;
    return this;
  }

  public int getGoldCost() {
    return goldCost;
  }

  public WarriorDto setGoldCost(int goldCost) {
    this.goldCost = goldCost;
    return this;
  }

  public boolean isTouchedAtThisTurn() {
    return touchedAtThisTurn;
  }

  public WarriorDto setTouchedAtThisTurn(boolean touchedAtThisTurn) {
    this.touchedAtThisTurn = touchedAtThisTurn;
    return this;
  }

  public boolean isContrattackUsedAtThisTurn() {
    return contrattackUsedAtThisTurn;
  }

  public WarriorDto setContrattackUsedAtThisTurn(boolean contrattackUsedAtThisTurn) {
    this.contrattackUsedAtThisTurn = contrattackUsedAtThisTurn;
    return this;
  }

  public boolean isDead() {
    return dead;
  }

  public WarriorDto setDead(boolean dead) {
    this.dead = dead;
    return this;
  }

  public boolean isMoveLocked() {
    return moveLocked;
  }

  public WarriorDto setMoveLocked(boolean moveLocked) {
    this.moveLocked = moveLocked;
    return this;
  }

  public boolean isRollbackAvailable() {
    return rollbackAvailable;
  }

  public WarriorDto setRollbackAvailable(boolean rollbackAvailable) {
    this.rollbackAvailable = rollbackAvailable;
    return this;
  }

  public int getTreatedActionPointsForMove() {
    return treatedActionPointsForMove;
  }

  public WarriorDto setTreatedActionPointsForMove(int treatedActionPointsForMove) {
    this.treatedActionPointsForMove = treatedActionPointsForMove;
    return this;
  }

  public PlayerPhaseType getWarriorPhase() {
    return warriorPhase;
  }

  public WarriorDto setWarriorPhase(PlayerPhaseType warriorPhase) {
    this.warriorPhase = warriorPhase;
    return this;
  }

  public Set<InfluencerDto> getInfluencers() {
    return influencers;
  }

  public WarriorDto setInfluencers(Set<InfluencerDto> influencers) {
    this.influencers = influencers;
    return this;
  }

  public static WarriorDto fromEntity(Warrior warrior) {
    return new WarriorDto()
            .setId(warrior.getId())
            .setTitle(warrior.getTitle())
            .setOwnedByUser(warrior.getOwner().getId())
            .setDatabaseId(warrior.getDatabaseId())
            .setWarriorBaseClass(warrior.getWarriorBaseClass().getTitle())
            .setCoords(warrior.getCoords())
            .setOriginalCoords(warrior.getOriginalCoords())
            .setSummoned(warrior.isSummoned())
            .setArmor(warrior.getAttributes().getArmorClass().getArmorValue())
            .setMoveCost(warrior.getWarriorSMoveCost())
            .setActionPoints(warrior.getAttributes().getActionPoints())
            .setAbilityActionPoints(warrior.getAttributes().getAbilityActionPoints())
            .setHealth(warrior.getAttributes().getHealth())
            .setManna(warrior.getAttributes().getManna())
            .setLuckDefense(warrior.getAttributes().getLuckDefense())
            .setLuckMeleeAtack(warrior.getAttributes().getLuckMeleeAtack())
            .setLuckRangeAtack(warrior.getAttributes().getLuckRangeAtack())
            .setWeapons(warrior.getWeapons().stream().map(weapon -> WeaponShortDto.fromEntity(weapon)).collect(Collectors.toSet()))
            .setArtifacts(warrior.getArtifacts().getResult().stream()
                    .map(artifact ->
                            ArtifactDto.fromEntity(artifact))
                    .collect(Collectors.toSet()))
            .setTouchedAtThisTurn(warrior.isTouchedAtThisTurn())
            .setContrattackUsedAtThisTurn(warrior.isContrattackUsedAtThisTurn())
            .setDead(warrior.isDead())
            .setMoveLocked(warrior.isMoveLocked())
            .setRollbackAvailable(warrior.isRollbackAvailable())
            .setTreatedActionPointsForMove(warrior.getTreatedActionPointsForMove())
            .setWarriorPhase(warrior.getWarriorPhase())
            .setInfluencers(warrior.getWarriorSInfluencers().getResult().stream()
                    .map(influencer -> InfluencerDto.fromEntity(influencer))
                    .collect(Collectors.toSet()));
  }


}
