package api.dto.warrior;

import api.entity.ability.Modifier;
import api.enums.AttributeEnum;
import api.enums.ModifierClass;
import api.enums.SignOfInfluenceEnum;

/**
 * Трансферный клас для модификаторов
 */
public class ModifierDto {
  protected String title;
  protected String description;
  protected AttributeEnum attribute;
  protected int probability;
  protected int minValue, maxValue;
  protected int calculatedValue;
  protected ModifierClass modifierClass;
  protected int luck;
  protected SignOfInfluenceEnum signOfInfluence;

  public String getTitle() {
    return title;
  }

  public ModifierDto setTitle(String title) {
    this.title = title;
    return this;
  }

  public String getDescription() {
    return description;
  }

  public ModifierDto setDescription(String description) {
    this.description = description;
    return this;
  }

  public AttributeEnum getAttribute() {
    return attribute;
  }

  public ModifierDto setAttribute(AttributeEnum attribute) {
    this.attribute = attribute;
    return this;
  }

  public int getProbability() {
    return probability;
  }

  public ModifierDto setProbability(int probability) {
    this.probability = probability;
    return this;
  }

  public int getMinValue() {
    return minValue;
  }

  public ModifierDto setMinValue(int minValue) {
    this.minValue = minValue;
    return this;
  }

  public int getMaxValue() {
    return maxValue;
  }

  public ModifierDto setMaxValue(int maxValue) {
    this.maxValue = maxValue;
    return this;
  }

  public int getCalculatedValue() {
    return calculatedValue;
  }

  public ModifierDto setCalculatedValue(int calculatedValue) {
    this.calculatedValue = calculatedValue;
    return this;
  }

  public ModifierClass getModifierClass() {
    return modifierClass;
  }

  public ModifierDto setModifierClass(ModifierClass modifierClass) {
    this.modifierClass = modifierClass;
    return this;
  }

  public int getLuck() {
    return luck;
  }

  public ModifierDto setLuck(int luck) {
    this.luck = luck;
    return this;
  }

  public SignOfInfluenceEnum getSignOfInfluence() {
    return signOfInfluence;
  }

  public ModifierDto setSignOfInfluence(SignOfInfluenceEnum signOfInfluence) {
    this.signOfInfluence = signOfInfluence;
    return this;
  }

  public static ModifierDto fromEntity(Modifier modifier){
    return new ModifierDto()
            .setTitle(modifier.getTitle())
            .setDescription(modifier.getDescription())
            .setAttribute(modifier.getAttribute())
            .setProbability(modifier.getProbability())
            .setMinValue(modifier.getMinValue())
            .setMaxValue(modifier.getMaxValue())
            .setCalculatedValue(modifier.getLastCalculatedValue())
            .setModifierClass(modifier.getModifierClass())
            .setLuck(modifier.getLuck())
            .setSignOfInfluence(modifier.getSignOfInfluence());
  }
}
