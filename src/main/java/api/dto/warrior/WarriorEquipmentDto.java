package api.dto.warrior;

import api.core.Core;
import com.google.gson.GsonBuilder;
import core.database.entity.warrior.WarriorEquipment;

/**
 * Клас для обертывания экипировок воина
 */
public class WarriorEquipmentDto implements Comparable<WarriorEquipmentDto>{
  private Long id;
  private String name;
  private int cost;
  private WarriorShortDto stuff;

  public Long getId() {
    return id;
  }

  public WarriorEquipmentDto setId(Long id) {
    this.id = id;
    return this;
  }

  public String getName() {
    return name;
  }

  public WarriorEquipmentDto setName(String name) {
    this.name = name;
    return this;
  }

  public int getCost() {
    return cost;
  }

  public WarriorEquipmentDto setCost(int cost) {
    this.cost = cost;
    return this;
  }

  public WarriorShortDto getSerializedJson() {
    return stuff;
  }

  public WarriorEquipmentDto setSerializedJson(WarriorShortDto warriorDto) {
    this.stuff = warriorDto;
    return this;
  }

  private WarriorEquipmentDto setSerializedJson(String json){
    stuff = new GsonBuilder().create().fromJson(json, WarriorShortDto.class);
    return this;
  }

  public static WarriorEquipmentDto fromEntity(WarriorEquipment source, Core core){
    WarriorEquipmentDto result = new WarriorEquipmentDto()
            .setId(source.getId())
            .setName(source.getName())
            .setSerializedJson(source.getSerializedJson());

    result.setCost(core.calculatePrice(result.getSerializedJson()).getResult());
    return result;
  }

  @Override
  public int compareTo(WarriorEquipmentDto o) {
    return this.id.compareTo(o.getId());
  }
}
