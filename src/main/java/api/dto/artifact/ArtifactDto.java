package api.dto.artifact;

import api.entity.artifct.Artifact;
import api.entity.artifct.ArtifactClassInfo;
import api.entity.graphics.GraphicsInfo;
import api.dto.ability.AbilityDto;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Класс-обертка для сериализации в JSON описания артефакта
 */
public class ArtifactDto {
  private String id;
  private String title;
  private String description;
  private List<AbilityDto> abilities;
  private GraphicsInfo graphicsInfo;

  //===============================================================================
  //===============================================================================

  /**
   * Название
   *
   * @return
   */
  public String getTitle() {
    return title;
  }

  public ArtifactDto setTitle(String title) {
    this.title = title;
    return this;
  }
  //===============================================================================

  /**
   * Описание
   *
   * @return
   */
  public String getDescription() {
    return description;
  }

  public ArtifactDto setDescription(String description) {
    this.description = description;
    return this;
  }
  //===============================================================================

  /**
   * Способности
   *
   * @return
   */
  public List<AbilityDto> getAbilities() {
    return abilities;
  }

  public ArtifactDto setAbilities(List<AbilityDto> abilities) {
    this.abilities = abilities;
    return this;
  }
  //===============================================================================

  public GraphicsInfo getGraphicsInfo() {
    return graphicsInfo;
  }

  public ArtifactDto setGraphicsInfo(GraphicsInfo graphicsInfo) {
    this.graphicsInfo = graphicsInfo;
    return this;
  }
  //===============================================================================

  public String getId() {
    return id;
  }

  public ArtifactDto setId(String id) {
    this.id = id;
    return this;
  }
  //===============================================================================

  public static ArtifactDto fromEntity(Artifact artifact) {
    return new ArtifactDto()
            .setId(artifact.getId())
            .setTitle(artifact.getTitle())
            .setDescription(artifact.getDescription())
            .setAbilities(AbilityDto.listFromEntities(artifact.getAbilities()));
  }
  //===============================================================================

  public static ArtifactDto fromEntity(ArtifactClassInfo artifactClassInfo) {
    return new ArtifactDto()
            .setId("")
            .setTitle(artifactClassInfo.getName())
            .setDescription(artifactClassInfo.getDescription())
            .setGraphicsInfo(artifactClassInfo.getGraphicsInfo())
            .setAbilities(null);
  }
  //===============================================================================

  public static List<ArtifactDto> listFromEntities(Collection<Artifact> artifacts) {
    return artifacts.stream()
            .map(artifact -> ArtifactDto.fromEntity(artifact))
            .collect(Collectors.toList());
  }
  //===============================================================================

  public static List<ArtifactDto> listFromEntitiesInfo(Collection<ArtifactClassInfo> artifactClassInfos) {
    return artifactClassInfos.stream()
            .map(classInfo -> ArtifactDto.fromEntity(classInfo))
            .collect(Collectors.toList());
  }
  //===============================================================================
}
