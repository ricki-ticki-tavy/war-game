package api.dto.weapon;

import api.entity.graphics.GraphicsInfo;
import api.entity.weapon.Weapon;
import api.dto.ability.AbilityDto;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Класс-обертка для сериализации в JSON описания вооружения и предметов
 */
public class WeaponDto {
  private String title;
  private String description;
  private boolean hasRangeAttack;
  private int minRangeDamage;
  private int maxRangeDamage;
  private int maxRangeDistance;
  private int minRangeDistance;
  private int fadeRangeStart;
  private int fadeDamagePercentPerLength;

  private int chargesCount;

  private boolean hasMeleeAttack;
  private int minMeleeDamage;
  private int maxMeleeDamage;
  private int maxMeleeRange;
  private int handsCount;

  private int attackCost;

  private int baseCost;

  private String weaponClass;
  private String armor;

  private GraphicsInfo graphicsInfo;

  private List<AbilityDto> abilities;

  //===============================================================================
  //===============================================================================

  /**
   * Название
   * @return
   */
  public String getTitle() {
    return title;
  }

  public WeaponDto setTitle(String title) {
    this.title = title;
    return this;
  }
  //===============================================================================

  /**
   * Описание
   * @return
   */
  public String getDescription() {
    return description;
  }

  public WeaponDto setDescription(String description) {
    this.description = description;
    return this;
  }
  //===============================================================================


  /**
   * Способность дистанционной атаки
   * @return
   */
  public boolean isHasRangeAttack() {
    return hasRangeAttack;
  }

  public WeaponDto setHasRangeAttack(boolean hasRangeAttack) {
    this.hasRangeAttack = hasRangeAttack;
    return this;
  }
  //===============================================================================

  /**
   * Минимальное дистанционное поражение
   * @return
   */
  public int getMinRangeDamage() {
    return minRangeDamage;
  }

  public WeaponDto setMinRangeDamage(int minRangeDamage) {
    this.minRangeDamage = minRangeDamage;
    return this;
  }
  //===============================================================================

  /**
   * максимальное дистанционное поражение
   * @return
   */
  public int getMaxRangeDamage() {
    return maxRangeDamage;
  }

  public WeaponDto setMaxRangeDamage(int maxRangeDamage) {
    this.maxRangeDamage = maxRangeDamage;
    return this;
  }
  //===============================================================================

  /**
   * максимальная дальность выстрела
   * @return
   */
  public int getMaxRangeDistance() {
    return maxRangeDistance;
  }

  public WeaponDto setMaxRangeDistance(int maxRangeDistance) {
    this.maxRangeDistance = maxRangeDistance;
    return this;
  }
  //===============================================================================

  /**
   * Минимальная дальность для выстрела
   * @return
   */
  public int getMinRangeDistance() {
    return minRangeDistance;
  }

  public WeaponDto setMinRangeDistance(int minRangeDistance) {
    this.minRangeDistance = minRangeDistance;
    return this;
  }
  //===============================================================================

  /**
   * Кол-во зарядов
   * @return
   */
  public int getChargesCount() {
    return chargesCount;
  }

  public WeaponDto setChargesCount(int chargesCount) {
    this.chargesCount = chargesCount;
    return this;
  }
  //===============================================================================

  /**
   * Спосоность наносить атаку ближнего боя
   * @return
   */
  public boolean isHasMeleeAttack() {
    return hasMeleeAttack;
  }

  public WeaponDto setHasMeleeAttack(boolean hasMeleeAttack) {
    this.hasMeleeAttack = hasMeleeAttack;
    return this;
  }
  //===============================================================================

  /**
   * минимальный урон ближней атаки
   * @return
   */
  public int getMinMeleeDamage() {
    return minMeleeDamage;
  }

  public WeaponDto setMinMeleeDamage(int minMeleeDamage) {
    this.minMeleeDamage = minMeleeDamage;
    return this;
  }
  //===============================================================================

  /**
   * максимальный урон ближней атаки
   * @return
   */
  public int getMaxMeleeDamage() {
    return maxMeleeDamage;
  }

  public WeaponDto setMaxMeleeDamage(int maxMeleeDamage) {
    this.maxMeleeDamage = maxMeleeDamage;
    return this;
  }
  //===============================================================================

  /**
   * максимальная дальность ближней атаки
   * @return
   */
  public int getMaxMeleeRange() {
    return maxMeleeRange;
  }

  public WeaponDto setMaxMeleeRange(int maxMeleeRange) {
    this.maxMeleeRange = maxMeleeRange;
    return this;
  }
  //===============================================================================

  /**
   * Кол-во рук для использования оружия
   * @return
   */
  public int getHandsCount() {
    return handsCount;
  }

  public WeaponDto setHandsCount(int handsCount) {
    this.handsCount = handsCount;
    return this;
  }
  //===============================================================================

  /**
   * Цена атаки оружием в очках действия
   * @return
   */
  public int getAttackCost() {
    return attackCost;
  }

  public WeaponDto setAttackCost(int attackCost) {
    this.attackCost = attackCost;
    return this;
  }
  //===============================================================================

  /**
   * Базовая цена оружия (золото)
   * @return
   */
  public int getBaseCost() {
    return baseCost;
  }

  public WeaponDto setBaseCost(int baseCost) {
    this.baseCost = baseCost;
    return this;
  }
  //===============================================================================

  /**
   * Тип предмета.
   * @return
   */
  public String getWeaponClass() {
    return weaponClass;
  }

  public WeaponDto setWeaponClass(String weaponClass) {
    this.weaponClass = weaponClass;
    return this;
  }
  //===============================================================================

  /**
   * клвсс брони для щита
   * @return
   */
  public String getArmor() {
    return armor;
  }

  public WeaponDto setArmor(String armor) {
    this.armor = armor;
    return this;
  }
  //===============================================================================

  /**
   * Способности предмета
   * @return
   */
  public List<AbilityDto> getAbilities() {
    return abilities;
  }

  public WeaponDto setAbilities(List<AbilityDto> abilities) {
    this.abilities = abilities;
    return this;
  }
  //===============================================================================

  /**
   * Информация о графике и моделях
   * @return
   */
  public GraphicsInfo getGraphicsInfo() {
    return graphicsInfo;
  }

  public WeaponDto setGraphicsInfo(GraphicsInfo graphicsInfo) {
    this.graphicsInfo = graphicsInfo;
    return this;
  }
  //===============================================================================

  public int getFadeRangeStart() {
    return fadeRangeStart;
  }

  public WeaponDto setFadeRangeStart(int fadeRangeStart) {
    this.fadeRangeStart = fadeRangeStart;
    return this;
  }
  //===============================================================================

  public int getFadeDamagePercentPerLength() {
    return fadeDamagePercentPerLength;
  }

  public WeaponDto setFadeDamagePercentPerLength(int fadeDamagePercentPerLength) {
    this.fadeDamagePercentPerLength = fadeDamagePercentPerLength;
    return this;
  }


  //===============================================================================
  //===============================================================================

  public static WeaponDto fromEntity(Weapon weapon){
    return new WeaponDto()
            .setTitle(weapon.getTitle())
            .setDescription(weapon.getDescription())
            .setWeaponClass(weapon.getWeaponClass().getTitle())
            .setHandsCount(weapon.getNeededHandsCountToTakeWeapon())
            .setHasRangeAttack(weapon.isCanDealRangedDamage())
            .setMinRangeDamage(weapon.getRangedMinDamage())
            .setMaxRangeDamage(weapon.getRangedMaxDamage())
            .setMinRangeDistance(weapon.getRangedAttackMinRange())
            .setMaxRangeDistance(weapon.getRangedAttackMaxRange())

            .setFadeRangeStart(weapon.getFadeRangeStart())
            .setFadeDamagePercentPerLength(weapon.getFadeDamagePercentPerLength())

            .setHasMeleeAttack(weapon.isCanDealMelleDamage())
            .setMinMeleeDamage(weapon.getMeleeMinDamage())
            .setMaxMeleeDamage(weapon.getMeleeMaxDamage())
            .setMaxMeleeRange(weapon.getMeleeAttackRange())
            .setArmor(weapon.getArmorClass() == null ? "" : weapon.getArmorClass().getTitle())
            .setAbilities(AbilityDto.listFromEntities(weapon.getAbilities()))
            .setGraphicsInfo(weapon.getGraphicsInfo());
  }
  //===============================================================================

  public static List<WeaponDto> listFromEntities(Collection<Weapon> weapons){
    return weapons.stream()
            .map(ability -> WeaponDto.fromEntity(ability))
            .collect(Collectors.toList());
  }
  //===============================================================================
}
