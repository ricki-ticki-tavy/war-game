package api.dto.weapon;

import api.entity.weapon.Weapon;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * Класс-обертка с краткими данными по предмету аммуниции
 */
public class WeaponShortDto {
  private String id;
  private String title;
  private Set<String> abilities;

  public String getId() {
    return id;
  }

  public WeaponShortDto setId(String id) {
    this.id = id;
    return this;
  }

  public String getTitle() {
    return title;
  }

  public WeaponShortDto setTitle(String title) {
    this.title = title;
    return this;
  }

  public Set<String> getAbilities() {
    return abilities;
  }

  public WeaponShortDto setAbilities(Set<String> abilities) {
    this.abilities = abilities;
    return this;
  }

  public static WeaponShortDto fromEntity(Weapon weapon){
    return new WeaponShortDto()
            .setId(weapon.getId())
            .setTitle(weapon.getTitle())
            .setAbilities(weapon.getAbilities().stream().map(ability -> ability.getTitle()).collect(Collectors.toSet()));
  }
}
