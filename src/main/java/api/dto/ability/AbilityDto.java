package api.dto.ability;

import api.entity.ability.Ability;
import api.entity.ability.AbilityInfo;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Класс-обертка для сериализации в JSON описания способности
 */
public class AbilityDto {
  private String title;
  private String description;

  //===============================================================================
  //===============================================================================

  /**
   * Название
   *
   * @return
   */
  public String getTitle() {
    return title;
  }

  public AbilityDto setTitle(String title) {
    this.title = title;
    return this;
  }
  //===============================================================================

  /**
   * Описание
   *
   * @return
   */
  public String getDescription() {
    return description;
  }

  public AbilityDto setDescription(String description) {
    this.description = description;
    return this;
  }
  //===============================================================================

  public static AbilityDto fromInfo(AbilityInfo ability) {
    return new AbilityDto()
            .setTitle(ability.getName())
            .setDescription(ability.getDescription());
  }
  //===============================================================================

  public static List<AbilityDto> listFromInfos(Collection<AbilityInfo> abilityInfos) {
    return abilityInfos.stream()
            .map(ability -> AbilityDto.fromInfo(ability))
            .collect(Collectors.toList());
  }
  //===============================================================================

  public static List<AbilityDto> listFromEntities(Collection<Ability> abilities) {
    return abilities.stream()
            .map(ability -> new AbilityDto()
                    .setTitle(ability.getTitle())
                    .setDescription(ability.getDescription()))
            .collect(Collectors.toList());
  }
  //===============================================================================
}
