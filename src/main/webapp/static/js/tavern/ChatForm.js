/**
 * Форма чата.
 * @param parentDom
 * @constructor
 */
GAME.ChatForm = function (topicName, sayToAllTopic, user, arenaMode) {
    // Чат-форма стоит на арене
    this.ground = arenaMode;

    this.inputMessage = document.getElementById("inputMessage");
    document.getElementById("sendMessageForm").onsubmit = this.__sendMessage__.bind(this);
    this.messagesList = document.getElementById("messagesList");
    this.messagesListContainer = document.getElementById("messagesListContainer");
    this.userList = document.getElementById("userList");

    this.topicName = topicName;
    this.sayToAllTopic = sayToAllTopic;

    GAME.webSock.subscribeToTopic("/topic/" + topicName, this.__incomeChatMessage__.bind(this));
    GAME.webSock.subscribeToTopic("/topic/" + topicName + "-whisper-" + user.toLowerCase(), this.__incomeChatMessage__.bind(this));

    this.userPlates = [];
    this.messagePlates = []; // только для режима арены

    GAME.apiRestClient.getActiveUsersList(function(result){
        const users = JSON.parse(result.result);
        users.forEach(function(userName){
            this.__addOrRemoveUserFromList__({
                name: userName,
                connected: true,
            })

        }.bind(this));
    }.bind(this))

    if (this.ground){
        // включить подчистку сообщений из чата
        window.setInterval(this.__cleanOldMessages.bind(this), 1000);
    }
};

GAME.ChatForm.prototype = {
    constructor: GAME.ChatForm,

    /**
     * Реакция на нажатие кнопки Отправить сообщение
     * @private
     */
    __sendMessage__: function (e) {
        e.preventDefault();
        if (this.inputMessage.value.trim() != ""){
            GAME.webSock.send("/wargame/" + this.sayToAllTopic, {name: "", message: this.inputMessage.value});
            this.inputMessage.value = "";
        }
    },
    //==================================================================================

    __cleanOldMessages: function(){
        const oldPlates = this.messagePlates;
        this.messagePlates = [];

        const now = new Date();
        const maxLive = GAME.userSettingsKeeper.chat.arenaMessageLiveTimeSec * 1000;

        oldPlates.forEach(function(messagePlate){
            if ((now - messagePlate.plateDateTime) >= maxLive){
                // пора убирать
                this.messagesList.removeChild(messagePlate);
            } else {
                // пока оставим в массиве
                this.messagePlates.push(messagePlate);
            }
        }.bind(this));
    },
    //==================================================================================

    /**
     * Обработка события прихода сообщения в чат
     * @param messageText
     * @private
     */
    __incomeChatMessage__: function (messageText) {

        // посмотрим находится ли скроллер в самом низу. Если да, то после добавления сообщения будем пролистывать
        const mustScrollDown = this.messagesListContainer.scrollTop
            == (this.messagesListContainer.scrollHeight - this.messagesListContainer.clientHeight);

        const message = JSON.parse(messageText.body);
        const row = document.createElement("tr");
        const cell = document.createElement("td");
        row.appendChild(cell);
        const color = message.wisp ? "gray" : "white";

        var diF = document.createElement("div");
        diF.setAttribute("class", "userNameLabel");
        if (this.ground) {diF.setAttribute("style", "cursor: default;")}
        diF.innerHTML = message.name + (message.wisp ? " лично Вам " : "") + ": ";
        diF.onclick = this.__addRemoveUserOnWhisperList__.bind(this, message.name);
        cell.appendChild(diF);

        span = document.createElement("span");
        span.setAttribute("style", "float: left; color: " + color + (this.ground ? "; cursor: default;" : ""));
        span.innerHTML = message.message;
        cell.appendChild(span);

        this.messagesList.appendChild(row);

        if (this.ground){
            row.plateDateTime = new Date();
            this.messagePlates.push(row);
        }

        if (mustScrollDown) {
            this.messagesListContainer.scrollTop = this.messagesListContainer.scrollHeight - this.messagesListContainer.clientHeight;
        }

        this.__addOrRemoveUserFromList__(message);
    },
    //=========================================================================================

    /**
     * Добавляет или удаляет пользователя из списка, если в сообщении пришлы события входа или выхода
     * @param messageObject
     * @private
     */
    __addOrRemoveUserFromList__: function(messageObject){
        if (messageObject.connected && !this.ground){

            // Посмотрим нет ли уже готовой плашки
            var plateObj = this.userPlates[messageObject.name];
            if (!plateObj) {
                const plate = document.createElement("tr");
                plate.setAttribute("class", "userPlate");
                plate.onclick = this.__addRemoveUserOnWhisperList__.bind(this, messageObject.name);

                const cell = document.createElement("td");
                cell.setAttribute("class", "fullWidth userPlateCell");
                // cell.onclick = this.__addRemoveUserOnWhisperList__.bind(this, messageObject.name);
                plate.appendChild(cell);

                const diF = document.createElement("div");
                diF.setAttribute("class", "userPlateDiv");
                diF.innerHTML = messageObject.name;
                // diF.onclick = this.__addRemoveUserOnWhisperList__.bind(this, messageObject.name);
                cell.appendChild(diF);

                plateObj = {
                    dom: plate,
                };

                this.userPlates[messageObject.name] = plateObj;
            }
            this.userList.appendChild(plateObj.dom);
        } else if (messageObject.disconnected){
            var plateObj = this.userPlates[messageObject.name];
            if (plateObj) {
                this.userList.removeChild(plateObj.dom);
            } else {
                console.error("Удаление из списка пользователя \"" + messageObject.name + "\" которого там не было.");
            }

        }
    },
    //=========================================================================================

    /**
     *
     * @param object
     * @private
     */
    __addRemoveUserOnWhisperList__: function (userName) {
        var messageText = this.inputMessage.value.trim();

        var users = [];

        if (messageText.startsWith("[") && messageText.indexOf("]:") > 0){
            // есть уже
            users = messageText.substring(1, messageText.indexOf("]:") - 1).trim().split(",");

            // отделим само сообщение от списка пользователей
            messageText = messageText.substring(messageText.indexOf("]:") + 2);

            // проверим есть ли там этот пользователь
            var foundIndex = -1;
            for (var index = 0; index < users.length; index++ ){
                if (users[index].trim() == userName){
                    foundIndex = index;
                    break;
                }
            }

            if (foundIndex >= 0) {
                // пользователь есть. Надо его удалить из списка
                users.splice(foundIndex, 1);
            } else {
                users.push(userName);
            }
        }  else {
            users.push(userName);
        }

        // теперь соберем сначала пользователей, а потом прилепим сообщение
        if (users.length > 0){
            var prefix = "[";
            users.forEach(user => {prefix += user + ","});
            prefix += "]:";
            messageText = prefix + " " + messageText;
        }

        this.inputMessage.value = messageText;
        this.inputMessage.focus();
    }
};