/**
 * Таверна. Место, где игроки могут общаться между собой
 * @param parentDom
 * @param height    - высота в px, % и прочее. строковый параметр
 * @constructor
 */
GAME.Tavern = function (user, publicContextTopic, privateContextTopic, topicPrefix) {

    GAME.tavern = this;

    this.EVENT_FLAGS = {
        ROW: 1,
        EQUIP_SELECTOR: 2,
    };

    this.FIGHT_SECTION_WIDTH = {
        SMALL: "300px",
        LARGE: "700px"
    };

    this.GAME_MODE = {
        CREATE: 1,
        JOIN: 2
    };

    this.user = user;
    this.publicContextTopic = topicPrefix + publicContextTopic;
    this.privateContextTopic = topicPrefix + privateContextTopic;

    this.challengesToFightList = document.getElementById("challengesToFightList");

    // Контейнер, содержащий либо плашки открытых вызовов, либо форму формирования вызова на бой
    this.challengesToFightContainer = document.getElementById("challengesToFightContainer");

    // Форма бросания вызова. На ней набирается отряд
    this.challengeToFightForm = document.getElementById("challengeToFightForm");

    // Форма Ожидания присоединения игрока к игре
    this.waitForJoinForm = document.getElementById("waitForJoinForm");

    // поле для вывода сколько из скольких игроков присоединились
    this.waitForJoinCounterField = document.getElementById("waitForJoinCounterField");

    // Список присоединившихся игроков
    this.joinedPlayersList = document.getElementById("joinedPlayersList");
    this.joinedPlayersList.plates = [];

    // Кнопка отмены ожидания присоединения игроков
    document.getElementById("buttonWaitForJoinCancel").onclick = this.__cancelWaitForJoin__.bind(this);

    // кнопка подтверждения вызова на форме вызова на бой
    this.buttonChallengeToFightOk = document.getElementById("buttonChallengeToFightOk");
    this.buttonChallengeToFightOk.onclick = this.__chellengeToFightOk__.bind(this);

    // кнопка отмены вызова на форме вызова на бой
    this.buttonChallengeToFightCancel = document.getElementById("buttonChallengeToFightCancel");
    this.buttonChallengeToFightCancel.onclick = this.__challengeToFightCancel__.bind(this);

    // кнопка перехода в лагерь отряда на форме вызова на бой
    this.buttonGoToCampaignCamp = document.getElementById("buttonGoToCampaignCamp");
    this.buttonGoToCampaignCamp.onclick = this.__challengeToFightGoToCampaignCamp__.bind(this);

    // таблица, в которую надо помещать плашки открытых вызовов
    this.challengesToFightListForm = document.getElementById("challengesToFightListForm");

    // Кнопка бросить вызов  под плашками с открытими играми
    this.buttonChallengeToFight = document.getElementById("buttonChallengeToFight");
    this.buttonChallengeToFight.onclick = this.__openChallengeToFightForm__.bind(this);

    // Кнопка перехода на арену с формы ожидания присоединения игроков к игре
    this.buttonWaitForJoinGoToArena = document.getElementById("buttonWaitForJoinGoToArena");
    this.buttonWaitForJoinGoToArena.onclick = this.__goToArena__.bind(this);

    // Выпадающий список с картами
    this.mapSelectorField = document.getElementById("mapSelector");
    this.mapSelectorField.onchange = this.__newMapSelected__.bind(this);

    // поля описания карты
    this.descriptionField = document.getElementById("mapInfoDescription");
    this.playersCountField = document.getElementById("mapInfoPlayersCount");
    this.mapSizeField = document.getElementById("mapInfoMapSize");
    this.startArtifactsCountField = document.getElementById("mapInfoArtifactAvailableAtStart");
    this.expandArtifactsAtRoundsField = document.getElementById("mapInfoArtifactExtendsAtRounds");

    // место для расположения плашек воинов
    this.warriorPlatesContainer = document.getElementById("warriorPlatesContainer");

    // Место расположения плашек артефактов
    this.artifactsContainer = document.getElementById("artifactsContainer");

    //Название игры
    this.mapInfoGameName = document.getElementById("mapInfoGameName");

    // тип игры
    this.mapInfoGameType = document.getElementById("mapInfoGameType");

    // кол-во воинов в отряде
    this.mapInfoStartWarriorsCount = document.getElementById("mapInfoStartWarriorsCount");

    // допустимое кол-во призванных воинов
    this.mapInfoSummonedWarriorsCount = document.getElementById("mapInfoSummonedWarriorsCount");

    // кол-во воинов, которых можно передвигать за ход
    this.mapInfoMovesCountPerTurn = document.getElementById("mapInfoMovesCountPerTurn");

    // кол-во манны стартовое
    this.mapInfoStartManna = document.getElementById("mapInfoStartManna");

    // максимальное кол-во манны
    this.mapInfoMaxManna = document.getElementById("mapInfoMaxManna");

    // кол-во манны, восстанавливаемое за ход
    this.mapInfoRestorationManna = document.getElementById("mapInfoRestorationManna");

    // длительность хода
    this.mapInfoMaxPlayerRoundTimeSec = document.getElementById("mapInfoMaxPlayerRoundTimeSec");

    // Размер юнита на карте
    this.mapInfoUnitSizeInCells = document.getElementById("mapInfoUnitSizeMm");

    // кол-во воинов в отряде
    this.mapInfoStartWarriorsCount = document.getElementById("mapInfoStartWarriorsCount");
    this.warriorCounterField = document.getElementById("warriorCounterField");
    this.mapInfoStartWarriorsCount.onchange = function () {
        this.warriorCounterField.innerHTML = this.heroes.filter(hero => hero.checked).length + " из " + this.mapInfoStartWarriorsCount.value;
    }.bind(this);

    // Объект блокировки экрана при запросах на сервер
    this.lockScreenCursor = new GAME.HtmlLockScreenCursor();
    new GAME.ApiRestClient("", this.lockScreenCursor, this.__showMessage__.bind(this));
    this.chatForm = null;

    // Спрячем ненужные пока формы
    this.challengesToFightContainer.removeChild(this.challengeToFightForm);
    this.challengesToFightContainer.removeChild(this.waitForJoinForm);

    // плашки с текущими играми
    this.gamePlates = [];

    this.__tryToFindOldContext__();


};

GAME.Tavern.prototype = {
    constructor: GAME.MainMenu,
    //=========================================================================================

    /**
     * запуск работы таверны
     */
    start: function () {

    },
    //=========================================================================================

    /**
     * Попытка подключения к старому контексту, если таковой есть с загрузкой всех данных игры
     * @private
     */
    __tryToFindOldContext__: function () {
        GAME.apiRestClient.getExistingGameContext(function (result) {
            var existingContext = null;
            if (result.result != "" && result.result != "null") {
                existingContext = JSON.parse(result.result);
                if (!existingContext.technical) {
                    // контекст действующий, игровой.
                    if (existingContext.gameRan) {
                        // TODO предложить перейти в текущую игру
                        document.getElementById("buttonChallengeToFightImg").setAttribute("class", "miniIcon iconArena buttonChallengeToFightImg");
                        document.getElementById("buttonChallengeToFightText").innerHTML = "Продолжить бой";
                        this.buttonChallengeToFight.onclick = this.__goToArena__.bind(this);

                    } else {
                        // игра еще не начата
                        GAME.apiRestClient.gameContextUUID = existingContext.contextId;
                        this.__chainInit__();

                        if (!this.playMetadata) {
                            this.playMetadata = {};
                        }

                        this.playMetadata.gameMode = existingContext.ownedByUser == this.user ? this.GAME_MODE.CREATE : this.GAME_MODE.JOIN;
                        this.playMetadata.contextId = existingContext.contextId;

                        // запросим данные о игре
                        GAME.apiRestClient.getGameFullData(function (result) {
                                const data = JSON.parse(result.result);
                                // найдем своего игрока
                                const playerData = data.players.find(player => player.userName == this.user);

                                // артефакты
                                this.playMetadata.artifacts = playerData.artifacts;

                                // Откроем так же форму ожидания подключения игроков
                                this.__showWaitJoinForm__(this.challengesToFightListForm);

                                this.__recreateJoinedUserPlates__({context: data});
                            }.bind(this)
                            // сбой загрузки данных
                            , function () {

                            }.bind(this));


                    }
                } else {
                    // старый контекст - технический. Просто считаем его действующим и работаем с ним
                    GAME.apiRestClient.gameContextUUID = existingContext.contextId;
                    this.__chainInit__();
                }
            }

            if (!existingContext) {
                GAME.apiRestClient.createTechnicalContext(function (result) {
                    GAME.apiRestClient.gameContextUUID = result.result;
                    this.__chainInit__();
                }.bind(this));
            }
        }.bind(this));
    },
    //=========================================================================================

    /**
     * Метод вывода сообщения
     * @param message
     * @param callback
     * @private
     */
    __showMessage__: function (message, callback) {
        alert(message);
        if (callback) {
            callback(message);
        }
    },
    //=========================================================================================

    /**
     * Переход в лагерь отряда
     * @private
     */
    __challengeToFightGoToCampaignCamp__: function () {
        window.location = "army";
    },
    //=========================================================================================

    /**
     * Продолжение инициализации формы таверны после получения ID технического контекста
     * @private
     */
    __chainInit__: function () {
        new GAME.WebSock();
        GAME.webSock.subscribeToTopic(this.publicContextTopic, this.__incomeGameEventMessage__.bind(this));

        // Подключимся к чатовой форме
        this.chatForm = new GAME.ChatForm("tavernChat", "tavern-sayToAll", this.user, false);

        // Подключим WebSocket
        GAME.webSock.connect("/wargame/tavern-hello", {name: "hello", message: "hello", date: "123456"});

        // загрузим текущие игры
        GAME.apiRestClient.getActiveGameList(this.__processExistingGamesList__.bind(this));
    },
    //=========================================================================================

    /**
     * Обработка полученного списка текущих игр
     * @param result
     * @private
     */
    __processExistingGamesList__: function (result) {
        const gameList = JSON.parse(result.result);
        gameList.forEach(function (gameMetadata) {
            this.__addGamePlate__({context: gameMetadata});
        }.bind(this));
    },
    //=========================================================================================

    /**
     * Функция вызова на бой при нажатии кнопки вызова на бой
     * @private
     */
    __openChallengeToFightForm__: function () {
        GAME.apiRestClient.getPlayerHeroes(
            // Запрос успешен
            function (result) {
                this.heroes = JSON.parse(result.result);
                // Запросим список доступных карт
                GAME.apiRestClient.getMapsList(
                    // Карты успешно загружены
                    function (result) {
                        this.mapsList = JSON.parse(result.result);

                        GAME.apiRestClient.getPlayerArtifacts(
                            // После успешной загрузки артефактов для игрока
                            function (result) {
                                this.artifactsMetadata = JSON.parse(result.result);
                                // это создание игры
                                this.__setGameMode__(this.GAME_MODE.CREATE);
                                this.__fillChallengeForm__();
                            }.bind(this));
                    }.bind(this));
            }.bind(this));

    },
    //=========================================================================================

    /**
     * Функция присоединения, ответа на вызов, после успешного подключения к игре
     * @private
     */
    __openChallengeToFightFormAfterJoin__: function (joinMetadata) {
        // запросим список доступных героев
        GAME.apiRestClient.getPlayerHeroes(
            // Запрос успешен
            function (result) {
                this.heroes = JSON.parse(result.result);
                // this.mapsList = JSON.parse(result.result);

                // загрузим артефакты
                GAME.apiRestClient.getPlayerArtifacts(
                    // После успешной загрузки артефактов для игрока
                    function (result) {
                        this.artifactsMetadata = JSON.parse(result.result);
                        this.__setGameMode__(this.GAME_MODE.JOIN);
                        this.playMetadata.contextId = joinMetadata.context.contextId;
                        this.__fillChallengeForm__(joinMetadata);
                    }.bind(this));
            }.bind(this));
    },
    //=========================================================================================

    /**
     * Выставляет в метаданных сосотояния тип игры: создание или присоединение
     * @param gameMode
     * @private
     */
    __setGameMode__: function (gameMode) {
        if (!this.playMetadata) {
            this.playMetadata = {};
        }
        this.playMetadata.gameMode = gameMode;
    },
    //=========================================================================================

    /**
     * Выставляет возможность изменения параметров игры в окне набора армии
     * @param editable
     * @private
     */
    __setMapInfoEditable__: function (editable) {
        this.mapSelectorField.disabled = !editable;
        this.mapInfoGameName.disabled = !editable;
        this.mapInfoGameType.disabled = !editable;
        this.mapInfoStartWarriorsCount.disabled = !editable;
        this.mapInfoSummonedWarriorsCount.disabled = !editable;
        this.mapInfoMovesCountPerTurn.disabled = !editable;
        this.mapInfoStartManna.disabled = !editable;
        this.mapInfoMaxManna.disabled = !editable;
        this.mapInfoRestorationManna.disabled = !editable;
        this.mapInfoMaxPlayerRoundTimeSec.disabled = !editable;
        this.mapInfoUnitSizeInCells.disabled = !editable;

    },
    //=========================================================================================

    __fillChallengeForm__: function (joinMetadata) {
        this.mapSelectorField.innerHTML = "";

        // заполним список карт
        this.mapsList.forEach(function (mapInfo) {
            const mapPlate = document.createElement("option");
            mapPlate.setAttribute("value", mapInfo.fileName);
            mapPlate.innerHTML = mapInfo.name;
            this.mapSelectorField.appendChild(mapPlate);
        }.bind(this));

        this.__newMapSelected__();

        if (this.playMetadata.gameMode == this.GAME_MODE.CREATE) {
            // режим создания игры
            this.__setMapInfoEditable__(true);

            // покажем параметры первой карты в списке
        } else {
            // Режим подключения к игре
            this.__setMapInfoEditable__(false);

            this.mapInfoGameName.value = joinMetadata.context.gameName;

            this.mapInfoGameType.value = joinMetadata.context.hidden ? "private" : "free";
            this.mapInfoStartWarriorsCount.value = joinMetadata.context.gameRules.maxStartCreaturePerPlayer;
            this.mapInfoSummonedWarriorsCount.value = joinMetadata.context.gameRules.maxSummonedCreaturePerPlayer;
            this.mapInfoMovesCountPerTurn.value = joinMetadata.context.gameRules.movesCountPerTurnForEachPlayer;
            this.mapInfoStartManna.value = joinMetadata.context.gameRules.startMannaPoints;
            this.mapInfoMaxManna.value = joinMetadata.context.gameRules.maxMannaPoints;
            this.mapInfoRestorationManna.value = joinMetadata.context.gameRules.restorationMannaPointsPerTotalRound;
            this.mapInfoMaxPlayerRoundTimeSec.value = joinMetadata.context.gameRules.maxPlayerRoundTime;
            this.mapInfoUnitSizeInCells.value = joinMetadata.context.gameRules.warriorSize;
        }

        // Заполним список воинов
        this.warriorPlatesContainer.innerHTML = "";

        this.heroes.forEach(function (hero) {
            const row = document.createElement("tr");
            row.heroMetadata = hero;

            // имя и класс героя
            var cell = this.___createWarriorRow___(row, 280, "left");
            cell.innerHTML = hero.baseClassName + " \"" + hero.title + "\"";

            // боев. пока не считаем
            cell = this.___createWarriorRow___(row, 40, "right");

            // Смертей
            cell = this.___createWarriorRow___(row, 40, "right");
            cell.innerHTML = hero.deathCount;

            // Экипировки
            cell = this.___createWarriorRow___(row, 250, "right");
            const equipSelector = document.createElement("select");
            equipSelector.setAttribute("class", "blackSelector")
            cell.appendChild(equipSelector);

            hero.equipments.forEach(function (equipment) {
                const opt = document.createElement("option");
                opt.setAttribute("value", equipment.name);
                opt.innerHTML = equipment.name;
                equipSelector.appendChild(opt);
            }.bind(this));

            // ячейка для вывода цены
            var priceCell = this.___createWarriorRow___(row, 40, "right");


            row.onclick = this.__warriorSelectionChanged__.bind(this, hero, equipSelector, row, priceCell, this.EVENT_FLAGS.ROW);
            equipSelector.onchange = this.__warriorSelectionChanged__.bind(this, hero, equipSelector, row, priceCell, this.EVENT_FLAGS.EQUIP_SELECTOR);

            this.warriorPlatesContainer.appendChild(row);
        }.bind(this));

        // атефакты
        this.artifactsContainer.innerHTML = "";
        this.artifactsMetadata.forEach(function (artifactMetadata) {
            const plate = this.__createArtifactPlate__(artifactMetadata);
            this.artifactsContainer.appendChild(plate);
        }.bind(this));

        // Покажем окно
        this.challengesToFightContainer.style.width = this.FIGHT_SECTION_WIDTH.LARGE;
        this.challengesToFightContainer.removeChild(this.challengesToFightListForm);
        this.challengesToFightContainer.appendChild(this.challengeToFightForm);

        this.mapInfoStartWarriorsCount.onchange();
    },
    //=========================================================================================

    __createArtifactPlate__: function (artifactMetadata) {

        const img = document.createElement("img");
        img.setAttribute("src", artifactMetadata.graphicsInfo.largeIcon);
        img.setAttribute("class", "artifactPlateIcon");
        img.setAttribute("title", artifactMetadata.title + "\r\n\r\n" + artifactMetadata.description);

        img.onclick = this.__artifactSelectChanged__.bind(this, artifactMetadata);

        return img;
    },
    //=========================================================================================

    /**
     * Вызывается при установке / снятии отметки на артефакте
     * @param artifactMetadata
     * @private
     */
    __artifactSelectChanged__: function (artifactMetadata, event) {
        const plate = event.target;
        if (!plate.checked) {
            // проверим, что отмечено не более, чем допустимо
            if (this.artifactsMetadata.filter(artifactMetadata => artifactMetadata.checked).length
                >= this.mapsList.find(mapInfo => mapInfo.fileName == this.mapSelectorField.value).artifactRules.maxStartCount) {
                // Превышает допустимое
                return;
            }
        }
        plate.checked = !plate.checked;
        artifactMetadata.checked = plate.checked;

        plate.setAttribute("class", "artifactPlateIcon" + (plate.checked ? " artifactPlateSelected" : ""));
    },
    //=========================================================================================

    /**
     * Создание ячейки в таблице
     * @param row
     * @param align
     * @returns {Element}
     * @private
     */
    ___createWarriorRow___: function (row, width, align) {
        const cell = document.createElement("td");
        cell.setAttribute("class", "selectWarriorTableCell");
        cell.setAttribute("style", "text-align: " + align + "; width: " + width + "px");
        row.appendChild(cell);
        return cell;
    },
    //=========================================================================================

    /**
     * реакция на изменение галочки отметки воина
     * @param element
     * @private
     */
    __warriorSelectionChanged__: function (metadata, equipSelector, row, priceCell, eventFlag, event) {
        // if (eventFlag == this.EVENT_FLAGS.ROW) {

        if (!row.checked) {
            // Добавление воина в отряд. Проверим не слишком ли будет много
            if (this.heroes.filter(hero => hero.checked).length >= this.mapInfoStartWarriorsCount.value) {
                return;
            }
        }

        row.checked = !row.checked;

        metadata.checked = row.checked;

        row.setAttribute("class", row.checked ? "selectWarriorTableRowSelected" : "");

        this.warriorCounterField.innerHTML = this.heroes.filter(hero => hero.checked).length + " из " + this.mapInfoStartWarriorsCount.value;
        // } else {
        //     event.stopImmediatePropagation();
        // }

        if (metadata.checked) {
            metadata.selectedEquip = metadata.equipments.find(equipment => equipment.name == equipSelector.value);
            priceCell.innerHTML = metadata.selectedEquip.cost + metadata.goldPrice;
        } else {
            priceCell.innerHTML = "";
        }

        // event.stopPropagation();
        // event.preventDefault();
        // event.preventBubble()
    },
    //=========================================================================================

    /**
     * Функция подтверждения вызова на бой с формы вызова на бой
     * @private
     */
    __chellengeToFightOk__: function () {
        // Проверка, что выбрано не более допустимого количества воинов
        if (this.heroes.filter(hero => hero.checked).length > this.mapInfoStartWarriorsCount.value) {
            // превышено
            GAME.apiRestClient.externalShowErrorMethod("Превышено допустимое по условиям игры количество воинов в отряде (" + this.mapInfoStartWarriorsCount.value + ")");
            return;
        }

        if (this.heroes.filter(hero => hero.checked).length == 0) {
            // превышено
            GAME.apiRestClient.externalShowErrorMethod("Не выбрано ни одного воина");
            return;
        }

        if (this.mapInfoGameName.value.trim() == "") {
            GAME.apiRestClient.externalShowErrorMethod("Не задано название игры", function () {
                this.mapInfoGameName.focus()
            }.bind(this));
            return;
        }

        if (this.playMetadata.gameMode == this.GAME_MODE.CREATE) {

            // начнем с создания контекста игры
            GAME.apiRestClient.createGame(
                this.mapSelectorField.value
                , this.mapInfoGameName.value
                , true
                , this.mapInfoStartWarriorsCount.value
                , this.mapInfoSummonedWarriorsCount.value
                , this.mapInfoMovesCountPerTurn.value
                , this.mapInfoStartManna.value
                , this.mapInfoMaxManna.value
                , this.mapInfoRestorationManna.value
                , this.mapInfoMaxPlayerRoundTimeSec.value
                , this.mapInfoUnitSizeInCells.value * this.mapInfo.simpleUnitSize
                , function (result) {
                    GAME.apiRestClient.gameContextUUID = result.result;
                    this.__setGameMode__(this.GAME_MODE.CREATE);
                    this.playMetadata.contextId = GAME.apiRestClient.gameContextUUID;

                    this.__preparePlayerDataOnMap__(null);
                }.bind(this)
            );
        } else {
            // присоединение
            this.__doJoinAndPrepareArmy__();
        }
    },
    //=========================================================================================

    /**
     * Создать на форме ожидания плашки подключенных пользователей согласно метеданным игры
     */
    __recreateJoinedUserPlates__: function (metadata) {
        this.joinedPlayersList.innerHTML = "";

        if (metadata) {
            // Создать плашки
            this.__createJoinedPlayerPlate__(metadata.context.ownedByUser);

            metadata.context.players.forEach(function (player) {
                if (player.userName != metadata.context.ownedByUser) {
                    this.__createJoinedPlayerPlate__(player.userName);
                }
            }.bind(this));
        } else {
            // создать плашку себе любимому
            this.__createJoinedPlayerPlate__(this.user);
        }

    },
    //=========================================================================================

    /**
     * Подготовка данных игрока на карте. Сохдание воинов, выдача оружия, артефактов и все такое
     * @private
     */
    __preparePlayerDataOnMap__: function (contextMetadata) {

        var artifacts = this.artifactsMetadata.filter(artifactMetadata => artifactMetadata.checked);

        const resetContextFunction =
            function () {
                GAME.apiRestClient.createTechnicalContext(function (result) {
                    GAME.apiRestClient.gameContextUUID = result.result
                }.bind(this));
            }.bind(this);

        // выдадим артефакты
        var artifactIndex = 0;
        const giveArtifact = function (result) {
            // если это первый вызов, то параметр результата выполнения предыдущей операции будет не определен.
            if (result) {
                // это вызов рекурсивный: сохраним результат
                artifacts[artifactIndex].id = result.result;
                artifactIndex++;
            }
            if (artifactIndex < artifacts.length) {
                // продолжаем запрашивать следующий
                GAME.apiRestClient.giveArtifactToPlayer(artifacts[artifactIndex].title
                    , giveArtifact
                    // в случае сбоя
                    , resetContextFunction);
            } else {
                // загрузка артефактов закончена. Переходим к созданию воинов и их вооружению
                this.playMetadata.artifacts = artifacts;

                const warriors = this.heroes.filter(hero => hero.checked);
                var warriorIndex = 0;

                // Метод создания воинов
                const createWarriorFunction = function (result) {
                    if (result) {
                        // пришел результат создания воина
                        // теперь выдадим оружие воину
                        warriors[warriorIndex].id = result.result;

                        var weaponIndex = 0;

                        const giveWeaponFunction = function (result) {
                            if (result) {
                                // это результат предыдущего вызова
                                weaponIndex++;
                            }

                            if (weaponIndex < warriors[warriorIndex].selectedEquip.stuff.weapons.length) {
                                // следующий выдадим предмет
                                GAME.apiRestClient.giveWeaponToWarrior(warriors[warriorIndex].id
                                    , warriors[warriorIndex].selectedEquip.stuff.weapons[weaponIndex].title
                                    , giveWeaponFunction
                                    , resetContextFunction);
                            } else {
                                // к следующему воину
                                warriorIndex++;

                                if (warriorIndex < warriors.length) {
                                    // создаем следующего воина
                                    GAME.apiRestClient.createWarrior(warriors[warriorIndex].baseClassName
                                        , warriors[warriorIndex].title
                                        , 2, 2
                                        , createWarriorFunction
                                        // В случае сбоя
                                        , resetContextFunction);
                                } else {
                                    // все воины созданы. Откроем игру для других, если она в режиме открытой.

                                    // закрыть форму создания игры и вместо нее отобразить форму ожидания подключения
                                    // других игроков.

                                    const afterAllPrepared = function (contextMetadata) {
                                        this.__showWaitJoinForm__(this.challengeToFightForm);

                                        if (contextMetadata
                                            // проверим все ли игроки подключены
                                            && contextMetadata.context.playersCount == contextMetadata.context.players.length) {

                                            // кол-во игроков набрано
                                            this.__showMessage__("Вперед в бой!", this.__goToArena__.bind(this));

                                        }

                                        this.__recreateJoinedUserPlates__(contextMetadata);

                                    }.bind(this);

                                    if (this.mapInfoGameType.value == "private" || this.playMetadata.gameMode == this.GAME_MODE.JOIN) {
                                        afterAllPrepared(contextMetadata);
                                    } else {
                                        GAME.apiRestClient.setGameVisibility(true, function (result) {
                                                afterAllPrepared(contextMetadata);
                                            }.bind(this)

                                            // В случае сбоя
                                            , resetContextFunction);
                                    }

                                }
                            }
                        }.bind(this);

                        // Вперед - дадим оружие
                        giveWeaponFunction(null);
                    } else {
                        // создаем следующего воина
                        GAME.apiRestClient.createWarrior(warriors[warriorIndex].baseClassName
                            , warriors[warriorIndex].title
                            , 2, 2
                            , createWarriorFunction
                            // В случае сбоя
                            , resetContextFunction);
                    }
                }.bind(this);

                // начнем
                createWarriorFunction(null);
            }
        }.bind(this);

        // Начнем
        giveArtifact(null);

    },
    //=========================================================================================


    /**
     * Функция отмены вызова на бой с формы вызова на бой
     * @private
     */
    __challengeToFightCancel__: function () {

        const closeFormFunc = function () {
            this.playMetadata = null;
            this.challengesToFightContainer.style.width = this.FIGHT_SECTION_WIDTH.SMALL;
            this.challengesToFightContainer.removeChild(this.challengeToFightForm);
            this.challengesToFightContainer.appendChild(this.challengesToFightListForm)
        }.bind(this);

        // if (this.playMetadata.gameMode == this.GAME_MODE.JOIN) {
        //     // Отключиться от контекста игры и создать свой собственный технический контекст
        //     GAME.apiRestClient.createTechnicalContext(function (result) {
        //         GAME.apiRestClient.gameContextUUID = result.result;
        //         closeFormFunc();
        //     }.bind(this));
        // } else {
        // }
        closeFormFunc();
    },
    //=========================================================================================

    /**
     * Действие при выборе новой карты в списке
     * @param element
     * @private
     */
    __newMapSelected__: function (element) {

        const mapInfo = this.mapsList.find(mapInfo => mapInfo.fileName == this.mapSelectorField.value);
        this.mapInfo = mapInfo;

        this.descriptionField.innerHTML = mapInfo.description;
        this.playersCountField.innerHTML = mapInfo.maxPlayersCount;
        this.mapSizeField.innerHTML = mapInfo.width + " x " + mapInfo.width;
        this.mapInfoUnitSizeInCells.value = 3;
        this.startArtifactsCountField.innerHTML = mapInfo.artifactRules.maxStartCount;
        this.expandArtifactsAtRoundsField.innerHTML = mapInfo.artifactRules.additionalArtifactRoundNumbers;

    },
    //=========================================================================================

    /**
     * Переход к ожиданию присоединения других игроков к игре
     * @param formToClose  - форма для закрывания перед открытием формы ожидания
     * @private
     */
    __showWaitJoinForm__: function (formToClose) {
        this.challengesToFightContainer.style.width = this.FIGHT_SECTION_WIDTH.SMALL;
        this.challengesToFightContainer.removeChild(formToClose);
        this.challengesToFightContainer.appendChild(this.waitForJoinForm);

    },
    //=========================================================================================

    /**
     * Реакция на нажатие кнопки омены ожидания присоединения к игре других игроков
     * @private
     */
    __cancelWaitForJoin__: function () {
        GAME.apiRestClient.createTechnicalContext(function (result) {
            GAME.apiRestClient.gameContextUUID = result.result;
            this.playMetadata = null;
            this.__hideWaitForJoinForm__();
        }.bind(this));
    },
    //=========================================================================================

    /**
     * Закрыть форму ожидания подключения всех игроков
     * @private
     */
    __hideWaitForJoinForm__: function () {
        this.challengesToFightContainer.style.width = this.FIGHT_SECTION_WIDTH.SMALL;
        this.challengesToFightContainer.removeChild(this.waitForJoinForm);
        this.challengesToFightContainer.appendChild(this.challengesToFightListForm);
    },
    //=========================================================================================

    /**
     * Выставить плашке игры основной стиль в зависимости от статуса игры
     * @param plate
     * @private
     */
    __setGamePlateContent__: function (plate) {
        plate.setAttribute("class", "challengeToFightGamePlate " + (
            plate.metadata.context.gameRan
                ? "challengeFightingPlate"
                : (
                    plate.metadata.context.playersCount == plate.metadata.context.players.length
                        ? "challengeStagingPlate"
                        : "challengeOpenPlate"
                )
        ));

        var players = plate.metadata.context.ownedByUser;

        plate.metadata.context.players.forEach(function (player) {
            if (player.userName != plate.metadata.context.ownedByUser) {
                players += ", " + player.userName;
            }
        }.bind(this));
        plate.usersField.innerHTML = players;

    },
    //=========================================================================================


    //=========================================================================================

    /**
     * Добавить новую плашку игры в список
     * @private
     */
    __addGamePlate__: function (gameMetadata) {
        const plate = document.createElement("div");
        plate.metadata = gameMetadata;

        var diF = document.createElement("div");
        diF.setAttribute("class", "gameName");
        diF.innerHTML = gameMetadata.context.gameName;
        plate.appendChild(diF);

        diF = document.createElement("div");
        diF.setAttribute("class", "gameCreator");
        plate.usersField = diF;
        plate.appendChild(diF);
        diF.innerHTML = gameMetadata.context.ownedByUser;

        plate.onclick = this.__joinToGame__.bind(this, plate);

        this.__setGamePlateContent__(plate);

        this.gamePlates[gameMetadata.context.contextId] = plate;

        this.challengesToFightList.appendChild(plate);
    },
    //=========================================================================================

    /**
     * Присоединение к игре
     * @param plate
     * @private
     */
    __joinToGame__: function (plate) {
        // TODO Подключение к игре

        // Загоним единственную карту в список
        this.mapsList = [plate.metadata.context.mapMetadata];

        // откроем форму наюра отряда
        this.__openChallengeToFightFormAfterJoin__(plate.metadata);
    },
    //=========================================================================================

    /**
     * Запуск процесса присоединения к игре и создания там всех воинов и предметов в соответствии с
     * выбором пользователя на форме набора отряда
     * @private
     */
    __doJoinAndPrepareArmy__: function () {
        GAME.apiRestClient.connectToGame(this.playMetadata.contextId,
            function (result) {
                //  теперь текущим контекстом является тот, к которому присоединился игрок
                GAME.apiRestClient.gameContextUUID = this.playMetadata.contextId;

                const metadata = JSON.parse(result.result);
                this.__preparePlayerDataOnMap__({context: metadata});
            }.bind(this)
            ,
            // в случае сбоя пересоздадим техконтекст
            function () {
                GAME.apiRestClient.createTechnicalContext(function (result) {
                    GAME.apiRestClient.gameContextUUID = result.result;
                }.bind(this));
            }.bind(this));

    },
    //=========================================================================================

    /**
     * Реакция на закрытие контекста
     * @param gameMetadata
     * @private
     */
    __gameContextCloseEventHandler__: function(gameMetadata){
        this.__removeGamePlate__(gameMetadata);
    },
    //=========================================================================================

    /**
     * Удалить плашку игры из списка, если отключился владелец
     * @private
     */
    __removeGamePlateIfOwnerDisconnected__: function (gameMetadata) {
        if (gameMetadata.context.ownedByUser == gameMetadata.params.player) {
            // Ушел главный игрок
            this.__removeGamePlate__(gameMetadata);

            // если я был к этой игре подключен, то отключиться
            // пересоздать контекст и закрыть окно ожидания игры
            if (this.playMetadata && this.playMetadata.contextId == gameMetadata.context.contextId) {
                // да. Это было оно.
                GAME.apiRestClient.createTechnicalContext(function (result) {
                    GAME.apiRestClient.gameContextUUID = result.result;
                    this.playMetadata = null;
                    if (gameMetadata.params.player != this.user) {
                        // если тот, кто отключился не я сам, то выведем предупреждение об отключении и закроем форму
                        this.__showMessage__("Игра, к которой Вы были подключены внезапно завершена создателем", function () {
                            // Закрыть форму ожидания подключения
                            this.__hideWaitForJoinForm__();
                        }.bind(this))
                    } else {
                        // если это я сам и был источнико сообщения, то тихо закроем форму
                        // this.__hideWaitForJoinForm__();
                    }
                }.bind(this));
            }
        } else {
            // просто в игре стало меньше игроков
            this.__updatePlateINfo__(gameMetadata);

            // если я был подключен к игре из которой вышел игрок, но он не создатель игры, то поправим список подключенных игроков
            this.__recreateJoinedUserPlates__(gameMetadata);
        }
    },
    //=========================================================================================

    /**
     * Удалить плашку игры из списка, если отключился владелец
     * @private
     */
    __removeGamePlate__: function (gameMetadata) {
        const plate = this.gamePlates[gameMetadata.context.contextId];
        if (plate) {
            this.challengesToFightList.removeChild(plate);
            this.gamePlates[gameMetadata.context.contextId] = null;
        }
    },
    //=========================================================================================

    /**
     * обновить информацию на плашке
     * @param metadata
     * @private
     */
    __updatePlateINfo__: function (metadata) {
        const plate = this.gamePlates[metadata.context.contextId];

        if (plate) {
            plate.metadata = metadata;

            // сначала не началась ли игра
            this.__setGamePlateContent__(plate);

        }
    },
    //=========================================================================================

    /**
     * Переход к арене
     * @private
     */
    __goToArena__: function () {
        window.location = "arena";
    },
    //=========================================================================================

    __createJoinedPlayerPlate__: function (userName) {
        const joinedPlayerPlate = document.createElement("div");
        joinedPlayerPlate.setAttribute("class", "joinedPlayerPlate");
        joinedPlayerPlate.innerHTML = userName;

        this.joinedPlayersList.appendChild(joinedPlayerPlate);

        this.joinedPlayersList.plates[userName] = joinedPlayerPlate;
    },
    //=========================================================================================

    /**
     * Проверяет какой тгрок куда подключился
     * @param messageObject
     * @private
     */
    __somePlayerConnectedToAnyContext__: function (metadata) {

        // сначала подправим плашку
        this.__updatePlateINfo__(metadata);

        // если режим ожидания подключения игроков, то смотрим не подключился ли он к нам и не мы же сами являемся источником этого сообщения
        if (this.playMetadata && this.playMetadata.contextId == metadata.context.contextId
            && metadata.params.player != this.user) {

            this.__createJoinedPlayerPlate__(metadata.params.player);

            if (metadata.context.playersCount == metadata.context.players.length) {
                // кол-во игроков набрано
                this.__showMessage__("Вперед в бой!", this.__goToArena__.bind(this));
            }
        }
    },
    //=========================================================================================

    __incomeGameEventMessage__: function (message) {
        const messageObject = JSON.parse(message.body);
        switch (messageObject.eventType) {
            case "GAME_CONTEXT_VISIBILITY_CHANGED" :
                if (!messageObject.context.hidden) {
                    // Новая плашка с игрой
                    this.__addGamePlate__(messageObject);
                } else {
                    this.__removeGamePlate__(messageObject);
                }
                break;

            case "PLAYER_CONNECTED" :
                if (!messageObject.context.hidden) {
                    // проверим кто и куда подключился
                    this.__somePlayerConnectedToAnyContext__(messageObject);
                }
                break;

            case "PLAYER_DISCONNECTED" :
                if (!messageObject.context.hidden) {
                    this.__removeGamePlateIfOwnerDisconnected__(messageObject);
                }
                break;

            case "GAME_CONTEXT_REMOVED":
                if (!messageObject.context.hidden) {
                    this.__gameContextCloseEventHandler__(messageObject);
                }
                break;

        }
    }
    //=========================================================================================


};

