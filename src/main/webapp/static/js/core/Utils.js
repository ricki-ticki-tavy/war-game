GAME.Utils = {
    extractFilePath: function (fullFileName) {
        return fullFileName.split('/').reverse().splice(1).reverse().join('/');
    },

    extractFileName: function (fullFileName) {
        return fullFileName.split('/').pop();
    },

    highLightObject: function (object3D, emissiveR, emissiveG, emissiveB, emissiveIntensity) {
        object3D.traverse(function (child) {
            if (child.material) {
                if (child.material.constructor === Array) {
                    child.material.forEach(child1 => {
                        child1.emissive.r = emissiveR;
                        child1.emissive.g = emissiveG;
                        child1.emissive.b = emissiveB;
                        child1.emissiveIntensity = emissiveIntensity;
                    })
                } else {
                    child.material.emissive.r = emissiveR;
                    child.material.emissive.g = emissiveG;
                    child.material.emissive.b = emissiveB;
                    child.material.emissiveIntensity = emissiveIntensity;
                }
            }
        });
    }

};