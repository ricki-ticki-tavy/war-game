GAME.WebSock = function () {
    GAME.webSock = this;

    this.WS_INIT_URL = "/wsInit";
    this.stompClient = null;
    this.connected = false;

    this.subscribes = [];
};


GAME.WebSock.prototype = {
    constructor: GAME.WebSock,

    subscribeToTopic: function (topicName, callback) {
        var subscription = null;
        const currentSubscription = this.subscribes.find(subscription => subscription.topicName == topicName);
        if (this.connected) {
            if (currentSubscription) {
                currentSubscription.subscription.unsubscribe();
            }
            subscription = this.stompClient.subscribe(topicName, callback);
        }

        if (currentSubscription) {
            currentSubscription.callback = callback;
            currentSubscription.callback = callback;
            currentSubscription.subscription = subscription;
        } else {
            this.subscribes.push({
                callback: callback,
                topicName: topicName,
                subscription: subscription,
            });
        }
    },
    //==============================================================================

    connect: function (greetingEndPoint, greetingObject) {
        var socket = new SockJS(this.WS_INIT_URL);
        this.stompClient = Stomp.over(socket);
        this.stompClient.connect({}, function (frame) {
            // this.setConnected(true);
            console.log('Connected: ' + frame);
            this.stompClient.debug = null;
            this.connected = true;
            this.__reSubscribe__();
            if (greetingObject && greetingEndPoint) {
                this.send(greetingEndPoint, greetingObject);
            }
        }.bind(this));
    },
    //==============================================================================

    disconnect: function () {
        if (this.stompClient !== null) {
            this.stompClient.disconnect();
        }
        this.connected = false;
        console.log("Disconnected");
    },
    //==============================================================================

    /**
     * Переподписаться на все, что было ранее
     * @private
     */
    __reSubscribe__: function (subscribes) {
        if (this.connected) {
            const subscr = subscribes == null ? this.subscribes : subscribes;
            subscr.forEach(function (subscription) {
                subscription.subscription = this.stompClient.subscribe(subscription.topicName, subscription.callback);
            }.bind(this));
        }
        return this.connected;
    },
    //==============================================================================

    send: function (endPoint, messageObject) {
        this.stompClient.send(endPoint, {}, JSON.stringify(messageObject))
    },
    //==============================================================================
    //==============================================================================
};
