(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
        typeof define === 'function' && define.amd ? define(['exports'], factory) :
            (global = global || self, factory(global.GAME = {}));
}(this, function (exports) { 'use strict';
}));

// (function (global, factory) {
//     typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
//         typeof define === 'function' && define.amd ? define(['exports'], factory) :
//             (global = global || self, factory(global.GAME.CORE = {}));
// }(this, function (exports) { 'use strict';
// }));