GAME.ItemsGroup = function () {
    this.modelObjects = [];
    this.selectedIndex = -1;
};

GAME.STUFF_TYPE = {
    WEAPPON: 0,
    ARTIFACT: 1,
};

GAME.STUFF_OWNER = {
    WARRIOR: 0,
    BAG: 1,
};


GAME.ItemsGroup.prototype = {
    constructor: GAME.ItemsGroup,

    STUFF_TYPE: {
        WEAPPON: 0,
        ARTIFACT: 1,
    },

    /**
     * Добавить модель.
     * @param object3D
     * @param metadata
     * @param stuffType    GAME.STUFF_TYPE
     * @returns {{metadata: *, object3D: *, enabled: boolean, stuffType: *}}
     */
    addModelObject: function (object3D, metadata, stuffType) {
        var mObj = {
            metadata: metadata,
            object3D: object3D,
            enabled: true,
            stuffType: stuffType,
            ownerType: GAME.STUFF_OWNER.BAG,
            itemId: null,   // id предмета у воина.
        };
        this.modelObjects.push(mObj);
        return mObj;
    },
    //=====================================================

    setSelectedIndex: function (newSelectedIndex) {
        if (newSelectedIndex >= 0 && !this.modelObjects[newSelectedIndex].enabled) {
            newSelectedIndex = -1;
        }
        if (newSelectedIndex === this.selectedIndex === -1) {
            return false;
        } else {
            var curSelectedIndex = this.selectedIndex;
            this.selectedIndex = newSelectedIndex;

            if (curSelectedIndex !== this.selectedIndex) {
                if (curSelectedIndex !== -1) {
                    this.highLightItem(curSelectedIndex, false);
                }
                if (this.selectedIndex !== -1) {
                    this.highLightItem(this.selectedIndex, true);
                }
            }
            return curSelectedIndex !== this.selectedIndex;
        }
    },
    //=====================================================

    highLightItem: function (itemIndex, highlighted) {
        var emissiveIntensity = 0;
        var emissiveR = 0;
        var emissiveG = 0;
        var emissiveB = 0;

        if (!this.modelObjects[itemIndex].enabled) {
            var emissiveR = 0x07;
            var emissiveG = 0x0;
            var emissiveB = 0x0;
            emissiveIntensity = 0.1;

        } else if (highlighted) {
            var emissiveR = 0x05;
            var emissiveG = 0x05;
            var emissiveB = 0x05;
            emissiveIntensity = 0.1;
        }
        GAME.Utils.highLightObject(this.modelObjects[itemIndex].object3D, emissiveR, emissiveG, emissiveB, emissiveIntensity);
    },
    //===================================================================================

    /**
     * Выставляет разрешение на выбор/подсветку того или иного предмета
     * @param index
     * @param enabled
     */
    setModelenabled: function (index, enabled) {
        this.modelObjects[index].enabled = enabled;
        this.highLightItem(index, false);
    },
    //===================================================================================

    get selectedModelObject(){
        return this.selectedIndex >= 0 ? this.modelObjects[this.selectedIndex] : null;
    }

};