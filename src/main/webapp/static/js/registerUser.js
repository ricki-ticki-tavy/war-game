
var registerResult;

function createOverlay(){
    $('body').append('<div id="requestOverlay" class="overlay"></div>');
    $("#requestOverlay").hide();
}

function showMessage(message, callback){
    window.document.getElementById("message").innerHTML = message;
    $( "#dialog" ).dialog( "open" );
}

function doRegister() {

    $("#requestOverlay").show();

    var params = "username=" + window.document.getElementById("username").value
        +"&password=" + window.document.getElementById("password").value
        +"&retryPassword=" + window.document.getElementById("retryPassword").value
        +"&e-mail=" + window.document.getElementById("e-mail").value
        +"&captcha=" + window.document.getElementById("captcha").value;

    $.ajax({
        type: "POST",
        url: "security/registerUser",
        data: params,
        success: responseRegisterUser.bind(this),
        error: responseRegisterUserFailed.bind(this),
        dataType: "json"
    });
}

function responseRegisterUserFailed(result){
    $("#requestOverlay").hide();
    showMessage(result.responseJSON.error);
}

function responseRegisterUser(result){
    $("#requestOverlay").hide();
    registerResult = result;
    if (result.failed){
        showMessage(result.errorMessage);
    } else {
        showMessage("Регистрация пользователя выполнена. На Вашу почту выслано письмо со ссылкой для подтверждения учетной записи.")
    }
}


$( function() {
    $( "#dialog" ).dialog({
        modal: true,
        autoOpen: false,
        width: "550px",
        buttons: {
            Ok: function() {
                $( this ).dialog( "close" );
                if (!registerResult.failed){
                    // успешно
                    window.location.replace("login");
                }
            }
        }
    });
} );
