/**
 * Класс трехмерной сцены арены
 */

GAME.ArenaScene = function () {
    GAME.arenaScene = this;
    this.scene = null;
    this.camera = null;
    this.renderer = null;
    // this.flowCamera = null;
    this.stats = null;

    this.gameMetadata = null;

    this.preloadDownCounter = 0;

    this.isInitialized = false;
    this.loadGauge = null;
    this.mainMenu = document.getElementById("menuButtonsPlace");

    this.callbackWhenLoaded = null;
    this.stausLine = document.getElementById("statusContainer");

    this.groundPosition = new THREE.Vector2(0, 0);

    // объект кольца вокруг отмеченного юнита
    this.warriorSelectorRing = null;
    this.warriorSelectorRingOwner = null;

    // объект курсора для перемещения
    this.targetMovePointerGreen = null;
    this.targetMovePointerRed = null;
    this.destinationAtackRangedPointer = null;

    // Текущий курсор
    this.currentCursor = null;

    this.selectedWarrior = null;
};

GAME.ArenaScene.prototype = {
    constructor: GAME.ArenaScene,

    //=================================================================================

    /**
     * Устанавливает атрибут отметки воина
     * @param selected
     * @private
     */
    selectWarrior: function(warriorMetadata){
        this.selectedWarrior = warriorMetadata;
        if (!warriorMetadata){
            // снятие выделения
            if (this.warriorSelectorRingOwner){
                this.warriorSelectorRingOwner.remove(this.warriorSelectorRing);
                this.warriorSelectorRingOwner = null;
                if (this.currentCursor){
                    GAME.scene.remove(this.currentCursor);
                    this.currentCursor = null;
                }
            }

        } else {
            // установка выделения
            this.warriorSelectorRingOwner = warriorMetadata.warriorObject.billboardObject3D;
            warriorMetadata.warriorObject.billboardObject3D.add(this.warriorSelectorRing);
            this.__setCursorType__();
        }
    },
    //============================================================================================

    /**
     * Метод решает тип курсора под указателем мыши
     * @private
     */
    __setCursorType__: function(){
        var cursor = null;

        if (GAME.flowCamera.groundCoords) {
            const player = GAME.arena.getThisPlayer();
            const playerOwnsTurn = GAME.arena.getPlayerOwnsThisTurn();
            if (this.selectedWarrior && !player.readyToPlay) {
                if (this.selectedWarrior.warriorObject.playerMetadata.userName != player.userName) {
                    // Это не свой воин. Не может быть никакого курсора
                } else {
                    // Свой воин.
                    if (GAME.arena.hoveredWarrior) {
                        // мышь над воином. Если это тот воин, что отмечен, то кусор движения
                        if (GAME.arena.hoveredWarrior == this.selectedWarrior) {
                            cursor = this.targetMovePointerGreen;
                        } else
                        // Если это вражеский воин, то курсор атаки. Пока одинаковый для всех
                        if (GAME.arena.hoveredWarrior.warriorObject.playerMetadata.userName != player.userName) {
                            // вражеский воин
                        } else {
                            // свой воин. Курсор движения, но заблокированный
                            cursor = this.targetMovePointerRed;
                        }
                    } else {
                        // свободное место. Просчитаем, что можно двигать сюда
                        const gameCoords = GAME.arena.translateMapCoordsToGame(GAME.flowCamera.groundCoords);
                        if (GAME.arena.canMoveTo(gameCoords, this.selectedWarrior)){
                            cursor = this.targetMovePointerGreen;
                        } else {
                            cursor = this.targetMovePointerRed;
                        }
                    }
                }
            } else if (GAME.arena.contextMetadata.context.gameRan){
                // игра уже идет
                if (player.getName == playerOwnsTurn.getName){
                    // я владею ходом

                    if (!this.selectedWarrior || this.selectedWarrior.warriorObject.playerMetadata.userName != player.userName) {
                        // Выделен не свой воин. Не может быть никакого курсора
                    } else {
                        // Свой воин.
                        // если мышь находится над воином, то надо разобраться на чьим и что сейчас выбрано - оружие
                        // или заклинание

                        this.destinationAtackRangedPointer.activeRedRadiusIndicator.scale.set(0.001, 0.001, 0.001);
                        this.destinationAtackRangedPointer.activeGreenRadiusIndicator.scale.set(0.001, 0.001, 0.001);

                        if (GAME.arena.hoveredWarrior) {
                            // мышь над воином. Если это тот воин, что отмечен, то кусор движения
                            if (GAME.arena.hoveredWarrior == this.selectedWarrior) {
                                cursor = this.targetMovePointerGreen;
                            } else
                            // Если это вражеский воин, то курсор атаки. Пока одинаковый для всех
                            if (GAME.arena.hoveredWarrior.warriorObject.playerMetadata.userName != player.userName) {
                                // вражеский воин
                                cursor = this.destinationAtackRangedPointer;
                            } else {
                                // свой воин. Курсор движения, но заблокированный
                                cursor = this.targetMovePointerRed;
                            }
                        } else {
                            // если сейчас выбрано заклинание, еоторое в качастве параметра получает координаты на карте
                            // то разрешаем курсор в любом случае.
                            const spell = GAME.arena.getSelectedWeaponAsSpellRequiresCoords();
                            if (spell){
                                // Это заклиание, которое требует координат на карте
                                const maxQuadDistance = Math.pow(spell.additionalParameters.find(parameter => parameter.type = "MAP_POINT").distanceMax, 2);
                                const cursorGameCoords = GAME.arena.translateMapCoordsToGame(GAME.flowCamera.groundCoords);

                                cursorGameCoords.x -= this.selectedWarrior.coords.x;
                                cursorGameCoords.y -= this.selectedWarrior.coords.y;

                                // преобразуем к 1 / 10 долям клетки, как используется в заклинаниях
                                const cellSize = GAME.arena.contextMetadata.context.mapMetadata.simpleUnitSize;
                                cursorGameCoords.x = (cursorGameCoords.x * 10 / cellSize).toFixed(0);
                                cursorGameCoords.y = (cursorGameCoords.y * 10 / cellSize).toFixed(0);
                                const quadDistance = cursorGameCoords.y * cursorGameCoords.y + cursorGameCoords.x * cursorGameCoords.x;

                                if (quadDistance > maxQuadDistance){
                                    // точка на карте выходит за пределы дальности наложения заклинания
                                    cursor = this.targetMovePointerGreen;
                                } else {
                                    // Не выходит за пределы
                                    cursor = this.destinationAtackRangedPointer;
                                    const scale = spell.influenceRadius / 10  * GAME.userSettingsKeeper.arena.scale;
                                    if (spell.spellSign == "NEGATIVE") {
                                        cursor.activeRedRadiusIndicator.scale.set(scale, scale, scale);
                                        cursor.activeGreenRadiusIndicator.scale.set(0.001, 0.001, 0.001);
                                    } else {
                                        cursor.activeGreenRadiusIndicator.scale.set(scale, scale, scale);
                                        cursor.activeRedRadiusIndicator.scale.set(0.001, 0.001, 0.001);
                                    }
                                }
                            } else {
                                // обычная проверка. Нет никаких заклинаний
                                // свободное место. Просчитаем, что можно двигать сюда
                                const gameCoords = GAME.arena.translateMapCoordsToGame(GAME.flowCamera.groundCoords);
                                if (GAME.arena.canMoveTo(gameCoords, this.selectedWarrior)){
                                    cursor = this.targetMovePointerGreen;
                                } else {
                                    cursor = this.targetMovePointerRed;
                                }
                            }
                        }
                    }

                } else {
                    // нет курсора
                }
            }
        }

        if (this.currentCursor != cursor){
            // тип курсора сменился
            if (this.currentCursor){
                GAME.scene.remove(this.currentCursor);
            }

            this.currentCursor = cursor;

            if (this.currentCursor){
                GAME.scene.add(cursor);
                cursor.position.copy(GAME.flowCamera.groundCoords);
            }
        } else if (this.currentCursor){
            cursor.position.copy(GAME.flowCamera.groundCoords);
        }
    },
    //============================================================================================

    /**
     * Запуск сцены
     * @param contextMetadata - данные игры
     *
     */
    start: function (contextMetadata, afterInitializedCallBack) {

        this.afterInitializedCallBack = afterInitializedCallBack;
        this.contextMetadata = contextMetadata;

        this.scene = new THREE.Scene();
        GAME.scene = this.scene;

        this.camera = new THREE.PerspectiveCamera(30, window.innerWidth / window.innerHeight, 0.1, 1000);
        GAME.camera = this.camera;

        this.renderer = new THREE.WebGLRenderer({
            antialias: GAME.userSettingsKeeper.render.antialias,
        });
        GAME.renderer = this.renderer;

        this.renderer.setSize(window.innerWidth, window.innerHeight);
        this.renderer.shadowMap.enabled = true;
        this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
        this.renderer.physicallyCorrectLights = true;

        document.body.appendChild(this.renderer.domElement);

        window.onresize = this.__onWindowResize__.bind(this);
        this.__onWindowResize__();


        this.stats = new Stats();
        var container = document.createElement('div');
        container.setAttribute("style", "left: 100%; position: fixed; margin-left: -78px; top: 0px");
        container.setAttribute("id", "statContainer");
        container.appendChild(this.stats.dom);
        document.body.appendChild(container);
        container.firstChild.setAttribute("style", "");

        this.loadGauge = new GAME.LoadGauge(this.scene);
        this.loadGauge.total = 23;

        this.render();

        //загрузим объекты
        // объект загрузки метаданных игры
        this.gameMetadata = new GAME.GameMetadata(this.showError, this.apiUrl, !GAME.arena.contextMetadata.context.gameRan);

        new THREE.FlowCamera(this.camera, new THREE.Vector3(0, 8, 14), new THREE.Vector3(0, 0, -10)
            , {
                topLeft: new THREE.Vector2(- this.contextMetadata.context.mapMetadata.width / 2 * GAME.userSettingsKeeper.arena.scale
                    , - this.contextMetadata.context.mapMetadata.height / 2 * GAME.userSettingsKeeper.arena.scale),
                bottomRight: new THREE.Vector2(this.contextMetadata.context.mapMetadata.width / 2 * GAME.userSettingsKeeper.arena.scale
                    , this.contextMetadata.context.mapMetadata.height / 2 * GAME.userSettingsKeeper.arena.scale)
            }, null);
        GAME.flowCamera.heightRestrictions.top = 80;
        GAME.flowCamera.heightRestrictions.bottom= 2;
        GAME.flowCamera.heightRestrictions.wheelCoef = GAME.userSettingsKeeper.arena.wheelCoef;

        GAME.flowCamera.mouseMoveEventListeners.push(this.__mouseMoveEventListener__.bind(this));

        document.getElementById("topDiv").addEventListener('wheel', GAME.flowCamera.__flowCameraWheel__.bind(GAME.flowCamera), true);

        this.loadGauge.incProgress();


        this.gameMetadata.loadAll(
            function () {
                // загрузка моделей
                this.loadGauge.incProgress();

                new GAME.ModelsLibrary(GAME.userSettingsKeeper.render.anisotropy, this.loadGauge, function (object) {
                    this.preloadDownCounter--;
                    this.__checkIfAllPreloadsDone__();
                }.bind(this));

                this.preloadDownCounter += GAME.modelsLibrary.getObjectsCount();
                GAME.modelsLibrary.loadLibrary();

            }.bind(this)
            , this.loadGauge
        );

    },
    //=================================================================================

    /**
     * Создание объекта светящегося кольца для подсветки вокруг отмеченного воина
     * @private
     */
    __createWarriorSelectorRing__: function(){
        const geo = new THREE.PlaneBufferGeometry(1.5, 1.5, 1, 1);
        const mat = GAME.modelsLibrary.materials.guiSelectedWarriorCircleMaterial.clone();
        // mat.emissiveMap = null;
        const obj = new THREE.Mesh(geo, mat);
        obj.rotation.x = - Math.PI / 2;

        return obj;
    },
    //=================================================================================

    /**
     * Создание курсора дистанционной атаки
     * @private
     */
    __createCursorAttackRanged__: function(){
        const cursor = new THREE.Group();

        const geo = new THREE.PlaneBufferGeometry(1.5, 1.5, 1, 1);
        const mat = GAME.modelsLibrary.materials.cursorTargetDistanceMaterial.clone();
        // mat.emissiveMap = null;
        const obj = new THREE.Mesh(geo, mat);
        obj.rotation.x = - Math.PI / 2;

        const activeGreenRadiusIndicator = GAME.WeaponRangeIndicator.__createCircle__(GAME.modelsLibrary.materials.greenBoldLineMaterial);
        const activeRedRadiusIndicator = GAME.WeaponRangeIndicator.__createCircle__(GAME.modelsLibrary.materials.redBoldLineMaterial);

        cursor.add(obj);
        cursor.add(activeGreenRadiusIndicator);
        cursor.add(activeRedRadiusIndicator);

        cursor.activeRedRadiusIndicator = activeRedRadiusIndicator;
        cursor.activeGreenRadiusIndicator = activeGreenRadiusIndicator;

        return cursor;
    },
    //=================================================================================

    /**
     * Создание курсора перемещения
     * @private
     */
    __createCursorTargetMoveGreen__: function(){
        const geo = new THREE.PlaneBufferGeometry(0.6, 0.6, 1, 1);
        const mat = GAME.modelsLibrary.materials.cursorTargetMoveGreenMaterial.clone();
        // mat.emissiveMap = null;
        const obj = new THREE.Mesh(geo, mat);
        obj.rotation.x = - Math.PI / 2;

        return obj;
    },
    //=================================================================================

    /**
     * Создание курсора перемещения
     * @private
     */
    __createCursorTargetMoveRed__: function(){
        const geo = new THREE.PlaneBufferGeometry(0.6, 0.6, 1, 1);
        const mat = GAME.modelsLibrary.materials.cursorTargetMoveRedMaterial.clone();
        // mat.emissiveMap = null;
        const obj = new THREE.Mesh(geo, mat);
        obj.rotation.x = - Math.PI / 2;

        return obj;
    },
    //=================================================================================

    __mouseMoveEventListener__: function (mousePosition) {
        this.stausLine.innerHTML = "Cam: " + GAME.flowCamera.camera.position.x.toFixed(0) + ":"
        + GAME.flowCamera.camera.position.y.toFixed(0) + ":"
        + GAME.flowCamera.camera.position.z.toFixed(0) + "    mouse: " + mousePosition.x.toFixed(5) + ":" + mousePosition.y.toFixed(5);

        this.__setCursorType__();
    },
    //=========================================================================
    /**
     * Проверяет дошел ли до нуля счетчик загрузок и, если да, то вызывает метод дальнейшей инициализации сцены
     */
    __checkIfAllPreloadsDone__: function () {
        if (this.preloadDownCounter == 0) {
            // все предзагружено
            this.__createSceneObjects__();
        }
    },
    //===================================================================================

    /**
     * Создание объектов сцены
     * @private
     */
    __createSceneObjects__: function () {
        new GAME.GuiFormManager(GAME.scene, GAME.flowCamera, GAME.modelsLibrary, GAME.renderer);
        this.loadGauge.incProgress();

        new GAME.ArenaDecorator();
        GAME.arenaDecorator.decorate();
        this.loadGauge.incProgress();

        GAME.flowCamera.ground = GAME.arenaDecorator.ground;
        GAME.flowCamera.speedAcceleration = 40;
        GAME.flowCamera.mouseDownEventListeners.push(this.__mouseDownEventListener__.bind(this));

        // Объект-кольцо подсветка выбранного воина
        this.warriorSelectorRing = this.__createWarriorSelectorRing__();

        this.destinationAtackRangedPointer = this.__createCursorAttackRanged__();
        this.targetMovePointerGreen = this.__createCursorTargetMoveGreen__();
        this.targetMovePointerRed = this.__createCursorTargetMoveRed__();

        // Установим новый курсор запроса к серверу и метод вывода ошибки на экран
        GAME.apiRestClient.lockCursor = GAME.guiFormManager.modalCursor;
        GAME.apiRestClient.externalShowErrorMethod = function (message, callback) {
            GAME.guiFormManager.showMessage("Ошибка обращения к серверу", message, callback)
        }.bind(this);

        this.isInitialized = true;
        this.loadGauge.hide();

        // теперь сцена работает
        this.afterInitializedCallBack();

    },
    //=================================================================================

    /**
     * Реакция на нажатие мыши
     * @param event
     * @private
     */
    __mouseDownEventListener__: function(event){
        if (event.button == 0) {
            return false;
        }
    },
    //=================================================================================

    /**
     * Функция отрисовки сцены, периодически вызывемая браузером
     */
    render: function () {
        requestAnimationFrame(this.render.bind(this));
        this.__animate__();
        if (this.isInitialized) {
            GAME.flowCamera.updateCamera();
            // this.armyEditorDecorator.lightBugsHolder.moveLightBugsCycle();
            GAME.guiFormManager.dispatch();
        }
        this.stats.begin();
        this.renderer.render(this.scene, this.camera);
        this.stats.end();
    },
    //=================================================================================

    /**
     * Различные спецэффекты
     * @private
     */
    __animate__: function(){
        if(GAME.arena) {
            GAME.arena.contextMetadata.context.players.forEach(function (player) {
                player.warriors.forEach(function (warrior) {
                    if (warrior.warriorObject) {
                        warrior.warriorObject.animate();
                    }
                }.bind(this));
            }.bind(this));

            if (this.warriorSelectorRingOwner){
                this.warriorSelectorRing.rotation.z -= 0.02 / GAME.flowCamera.avgRateCoef;
            }
        }
    },
    //=================================================================================

    /**
     * реакция на изменение размеров окна
     * @private
     */
    __onWindowResize__: function () {
        var width = window.innerWidth;
        var height = window.innerHeight;
        this.camera.aspect = width / height;
        var fov = 1920 / window.innerWidth * 30;
        fov *= height / 984;
        this.camera.fov = fov;
        this.camera.updateProjectionMatrix();
        this.renderer.setSize(width, height);
    },
    //===================================================================================


};