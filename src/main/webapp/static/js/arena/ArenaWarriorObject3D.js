/**
 * Объект, имитирующий 3D объекто воина со всеми эффектами и прочим, но реально управляюий разными объектами
 * @param arenaWarriorObject
 * @constructor
 */
GAME.ArenaWarriorObject3D = function (arenaWarriorObject) {
    this.parent = arenaWarriorObject;

    /**
     * Объект для трансляции позиции для фигуры воина и обслуживающих объектов
     * @type {{_x: number, _y: number, _z: number, x, x, y, y, z, z, set: GAME.ArenaWarriorObject3D._position.set}}
     * @private
     */
    this._position = {
        parent: arenaWarriorObject,
        _x: 0, _y: 0, _z: 0, _w: 0,

        set x(value) {
            this._x = value;
            if (this.parent) {
                this.parent.object3D.position.x = value;
                if (this.parent.billboardObject3D) {
                    this.parent.billboardObject3D.position.x = value;
                }
            }
        },

        get x() {
            return this._x;
        },
        // ----------------

        set y(value) {
            this._y = value;
            if (this.parent) {
                this.parent.object3D.position.y = value;
                if (this.parent.billboardObject3D) {
                    this.parent.billboardObject3D.position.y = value;
                }
            }
        },

        get y() {
            return this._y;
        },
        // ----------------

        set z(value) {
            this._z = value;
            if (this.parent) {
                this.parent.object3D.position.z = value;
                if (this.parent.billboardObject3D) {
                    this.parent.billboardObject3D.position.z = value;
                }
            }
        },

        get z() {
            return this._z;
        },
        // ----------------

        set w(value) {
            this._w = value;
        },

        get w() {
            return this._w;
        },
        // ----------------

        set: function (x, y, z) {
            if (x) {
                this.x = x;
            }
            if (y) {
                this.y = y;
            }
            if (z) {
                this.z = z;
            }
        }

    };

    /**
     * Объект для трансляции поворота только на фигурку воина
     * @type {{}}
     */
    this._rotation = {
        parent: arenaWarriorObject,
        _x: 0, _y: 0, _z: 0,

        set x(value) {
            this._x = value;
            if (this.parent) {
                this.parent.object3D.rotation.x = value;
            }
        },

        get x() {
            return this._x;
        },
        // ----------------

        set y(value) {
            this._y = value;
            if (this.parent) {
                this.parent.object3D.rotation.y = value;
            }
        },

        get y() {
            return this._y;
        },
        // ----------------

        set z(value) {
            this._z = value;
            if (this.parent) {
                this.parent.object3D.rotation.z = value;
            }
        },

        get z() {
            return this._z;
        },
        // ----------------

        set: function (x, y, z) {
            if (x) {
                this.x = x;
            }
            if (y) {
                this.y = y;
            }
            if (z) {
                this.z = z;
            }
        }

    };
}
;

GAME.ArenaWarriorObject3D.prototype = {
    constructor: GAME.ArenaWarriorObject3D,

    set position(value) {
        if (this.parent) {
            this.parent.object3D.position.copy(value);

            if (this.parent.billboardObject3D) {
                this.parent.billboardObject3D.position.copy(value);
            }
        }
        this._position.set(value.x, value.y, value.z);
    },
    //==========================================================================

    get position() {
        return this._position;
    },
    //==========================================================================

    set rotation(value) {
        if (this.parent) {
            this.parent.object3D.position.copy(value);
        }
        this._rotation.set(value.x, value.y, value.z);
    },
    //==========================================================================

    get rotation() {
        return this._rotation;
    },
    //==========================================================================
};