/**
 * Панель информации о подсвеченном воине (Который под мышкой)
 * @constructor
 */

GAME.WarriorInfoPanel = function () {
    this.visible = false;
    this.warrior = null;
    this.__createPanelObjects__();
};

GAME.WarriorInfoPanel.prototype = {
    constructor: GAME.WarriorInfoPanel,
    //====================================================

    show: function (warrior) {
        if (!warrior) {
            this.hide();
        } else {
            if (!this.visible) {
                GAME.renderer.domElement.parentElement.appendChild(this.backgroundDomElement);
                GAME.renderer.domElement.parentElement.appendChild(this.innerDomElement);
                this.visible = true;
            }

            if (this.warrior != warrior) {
                this.warrior = warrior;
                this.backgroundDomElement.setAttribute("style", "left: " + GAME.flowCamera.mouseClient.x + "px; top: " + GAME.flowCamera.mouseClient.y + "px");
                this.innerDomElement.setAttribute("style", "left: " + GAME.flowCamera.mouseClient.x + "px; top: " + GAME.flowCamera.mouseClient.y + "px");
                this.warriorName.innerHTML = warrior.title;

                this.health.innerHTML = warrior.health;
                this.manna.innerHTML = warrior.manna;
                this.actionPoints.innerHTML = warrior.actionPoints - warrior.treatedActionPointsForMove;
                this.armor.innerHTML = warrior.armor;
                this.moveCost.innerHTML = warrior.moveCost;

            }
        }
    },
    //====================================================

    /**
     * Скрыть панель
     */
    hide: function () {
        if (this.visible) {
            GAME.renderer.domElement.parentElement.removeChild(this.backgroundDomElement);
            GAME.renderer.domElement.parentElement.removeChild(this.innerDomElement);
            this.visible = false;
            this.warrior = null;
        }
    },
    //====================================================

    /**
     * Создать в таблице одну строку для вывода параметра
     * @param title
     * @private
     */
    __createParamRow__: function (title) {
        const row = document.createElement("tr");
        row.setAttribute("style", "height: 20px");
        this.table.appendChild(row);

        const paramName = document.createElement("td");
        row.appendChild(paramName);
        paramName.setAttribute("class", "warriorInfoPanelParamNameTd");
        paramName.innerHTML = title;

        const fieldTd = document.createElement("td");
        row.appendChild(fieldTd);
        fieldTd.setAttribute("class", "warriorInfoPanelParamValueTd");

        return fieldTd;

    },
    //====================================================

    __createPanelObjects__: function () {
        this.backgroundDomElement = background = document.createElement("div");
        this.backgroundDomElement.setAttribute("class", "warriorInfoPanelBackground");

        this.innerDomElement = document.createElement("div");
        this.innerDomElement.setAttribute("class", "warriorInfoPanelInnerPanel");

        this.table = document.createElement("table");
        this.innerDomElement.appendChild(this.table);
        // table.setAttribute("class", );

        var row = document.createElement("tr");
        row.setAttribute("style", "height: 20px");
        this.table.appendChild(row);

        this.warriorName = document.createElement("td");
        row.appendChild(this.warriorName);
        this.warriorName.setAttribute("colspan", "2");
        this.warriorName.setAttribute("style", "font-size: 14px; text-align: center;");

        this.health = this.__createParamRow__("Здоровье");
        this.manna = this.__createParamRow__("Магия");
        this.actionPoints = this.__createParamRow__("Очки действия");
        this.armor = this.__createParamRow__("Класс брони");
        this.moveCost = this.__createParamRow__("Цена движения");

        row = document.createElement("tr");
        const finalCell = document.createElement("td");
        row.appendChild(finalCell);
        finalCell.setAttribute("colspan", "2");
        this.table.appendChild(row);
    },
    //====================================================

};