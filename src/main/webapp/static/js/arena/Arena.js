/**
 * Главный класс игровой арены
 * @constructor
 */
GAME.Arena = function (user, privateContextTopic, publicContextTopic, tavernPublicChatTopic, arenaPublicChatTopic) {
    GAME.arena = this;

    this.yellow = "#888800";
    this.green = "#008800";
    this.red = "#880000";
    this.gray = "#666666";
    this.maroon = "#332020";

    this.user = user;
    this.privateContextTopic = privateContextTopic;
    this.publicContextTopic = publicContextTopic;
    this.tavernPublicChatTopic = tavernPublicChatTopic;
    this.arenaPublicChatTopic = arenaPublicChatTopic;

    this.contextMetadata = null;

    new GAME.UserSettingsKeeper().load();

    this.chatContainer = document.getElementById("chatFormContainer");
    document.getElementById("messageCell").setAttribute("colspan", 2);
    document.getElementById("userListCell").style.display = "none";
    document.getElementById("inputMessage").style.backgroundColor = "rgba(0,0,0,0)";
    document.getElementById("inputMessage").style.borderColor = "#303030";
    document.getElementById("buttonSendMessage").style.display = "none";
    document.getElementById("sendButtonCell").style.width = "10px";
    // this.chatContainer.style.height = "150px";

    this.menuButtonsPlace = document.getElementById("menuButtonsPlace");

    this.showError = this.__htmlShowMessage__.bind(this);

    // Воин, над которым находится мышь
    this.hoveredWarrior = null;

    // Выранный выоин
    this._selectedWarrior_ = null;

    // игрок, которому принадлежит ход
    this.playerOwnsThisTurn = null;
};

GAME.Arena.prototype = {
    constructor: GAME.Arena,

    /**
     * Запуск инициализации арены
     */
    start: function () {
        // Подключим rest клиент
        new GAME.ApiRestClient(null, new GAME.HtmlLockScreenCursor(), this.__htmlShowMessage__.bind(this));

        // проверим есть ли контекст
        GAME.apiRestClient.getExistingGameContext(function (result) {
            if (result.result == "null" || !result.result) {
                this.__cantLoadArenaError__("У вас нет активной игры");
            } else {
                // контекст есть.
                const contextMetadata = {context: JSON.parse(result.result)};

                // Проверим не технический ли он
                if (contextMetadata.context.technical) {
                    this.__cantLoadArenaError__("У вас нет активной игры");
                    return;
                }

                this.arenaPublicChatTopic += contextMetadata.context.contextId;
                this.privateContextTopic += contextMetadata.context.contextId;

                // Запросим все данные игры
                GAME.apiRestClient.gameContextUUID = contextMetadata.context.contextId;
                GAME.apiRestClient.getGameFullData(function (result) {
                        this.contextMetadata = {context: JSON.parse(result.result)};

                        this.__createTopMenu__();

                        this.__initWebSockAndChat_();

                        // Начнем загрузку сцены
                        new GAME.ArenaScene().start(this.contextMetadata, this.__arenaSceneLoaded__.bind(this));


                    }.bind(this)

                    // что-то пошло не так
                    , this.__goToTavern.bind(this));

            }
        }.bind(this));

    },
    //============================================================================================

    /**
     * Возвращает метаданные этого игрока
     */
    getThisPlayer: function () {
        return this.contextMetadata.context.players.find(player => player.userName == this.user)
    },
    //============================================================================================

    /**
     * Получить игрока, которому сейчас принадлежит ход
     * @returns {null}
     */
    getPlayerOwnsThisTurn: function () {
        return this.playerOwnsThisTurn;
    },
    //============================================================================================

    /**
     * Обработка события установки хода игрока
     * @param msg
     * @private
     */
    __playerOwnedThisTurnEventHandler__: function (msg) {
        this.__setPlayerOwnsThisTurn__(msg);
        this.__updateWarriorButtonsState__();
    },
    //============================================================================================

    /**
     * Установка игрока, владеющего в данный момент ходом. На основе контекста, полученного в сообщении.
     * @param msg
     * @private
     */
    __setPlayerOwnsThisTurn__: function (msg) {
        const userName = msg
            ? msg.context.playerOwnsThisTurn
            : this.contextMetadata.context.playerOwnsThisTurn;

        if (this.playerOwnsThisTurn) {
            // надо скрыть все элементы управления на воинах
            this.playerOwnsThisTurn.warriors.forEach(function (warrior) {
                warrior.warriorObject.hideStartPointer();
                warrior.touchedAtThisTurn = false;
            }.bind(this));
        }

        this.__copyContextFromContext__(msg);

        this.playerOwnsThisTurn = this.contextMetadata.context.players.find(player => player.userName == userName);
        if (this.playerOwnsThisTurn) {
            // показать, где есть линии движения
            this.playerOwnsThisTurn.warriors.forEach(function (warrior) {
                if (warrior.touchedAtThisTurn) {
                    warrior.warriorObject.showStartPointer();
                }
            }.bind(this));
        }

        // внешний вид плашек игроков
        this.__updatePlayerMenuStatus__();

        if (this.user == userName) {
            // мы получили ход . Надо отобразить сообщение
            GAME.guiFormManager.showMessage("Получен ход", "Ваш ход. Маневрируйте, наступайте, атакуйте!");
        }
        // кнопка перехода хода
    },
    //============================================================================================

    /**
     * Преобразовывает размер объекта на карте в размер объекта на клиенте
     * @param x
     * @param y
     * @private
     */
    translateGameSizeToMap: function (size) {
        const cellSize = GAME.arena.contextMetadata.context.mapMetadata.simpleUnitSize;
        return {
            x: size.x / cellSize * GAME.userSettingsKeeper.arena.scale,
            y: size.y / cellSize * GAME.userSettingsKeeper.arena.scale,
        }
    },
    //===============================================================================

    /**
     * Преобразовывает игровые в визуальные на карте
     * @param x
     * @param y
     * @private
     */
    translateGameCoordsToMap: function (coords) {
        const cellSize = GAME.arena.contextMetadata.context.mapMetadata.simpleUnitSize;
        return {
            x: (coords.x / cellSize - this.contextMetadata.context.mapMetadata.width / 2) * GAME.userSettingsKeeper.arena.scale,
            y: 0.01,
            z: (coords.y / cellSize - this.contextMetadata.context.mapMetadata.height / 2) * GAME.userSettingsKeeper.arena.scale,
            w: 0,
        }
    },
    //===============================================================================

    /**
     * Перевод координат карты в координаты игры
     * @param coords
     * @returns {{x: number, y: number}}
     */
    translateMapCoordsToGame: function (coords) {
        const cellSize = GAME.arena.contextMetadata.context.mapMetadata.simpleUnitSize;
        return {
            x: ((coords.x / GAME.userSettingsKeeper.arena.scale + this.contextMetadata.context.mapMetadata.width / 2) * cellSize).toFixed(0),
            y: ((coords.z / GAME.userSettingsKeeper.arena.scale + this.contextMetadata.context.mapMetadata.height / 2) * cellSize).toFixed(0),
        }
    },
    //===============================================================================

    /**
     * Проверка допустимости движения в указанные координаты
     * @param groundCoords
     * @private
     */
    canMoveTo: function (groundCoords, warriorMetadata) {
        if (this.contextMetadata.context.gameRan) {
            if (this.playerOwnsThisTurn.userName != this.user) {
                return false;
            }

            if (!warriorMetadata.touchedAtThisTurn
                && this.playerOwnsThisTurn.warriors.filter(warrior => warrior.touchedAtThisTurn).length >= this.contextMetadata.context.gameRules.movesCountPerTurnForEachPlayer) {
                return false;
            }
        }

        const powOfMinDistance = GAME.arena.contextMetadata.context.gameRules.warriorSize * GAME.arena.contextMetadata.context.gameRules.warriorSize;
        const gameRan = this.contextMetadata.context.gameRan;
        return GAME.arena.contextMetadata.context.players.find(
            player => player.warriors.find(
                warrior =>
                    warrior != warriorMetadata
                    && (((Math.pow(groundCoords.x - warrior.coords.x, 2)
                    + Math.pow(groundCoords.y - warrior.coords.y, 2)) <= powOfMinDistance)
                    || ((gameRan) && (warrior.touchedAtThisTurn) && ((Math.pow(groundCoords.x - warrior.originalCoords.x, 2)
                        + Math.pow(groundCoords.y - warrior.originalCoords.y, 2)) <= powOfMinDistance)))
            )
        )
            ? false : true;
    },
    //============================================================================================

    /**
     * Поиск метаданных пользователя в контексте по имени пользователя
     * @param userName
     */
    findPlayerByName: function (userName) {
        return this.contextMetadata.context.players.find(player => player.userName == userName);
    },
    //============================================================================================

    /**
     * Найти воина по его коду
     * @param warriorId
     * @param player - опционально. Если указан, то поиск идет только среди воинов этого игрока
     */
    findWarriorById: function (warriorId, player) {
        if (player) {
            return player.warriors.find(warrior => warrior.id == warriorId);
        } else {
            for (i = 0; i < this.contextMetadata.context.players; i++) {
                const warrior = this.findWarriorById(warriorId, this.contextMetadata.context.players[i]);
                if (warrior) {
                    return warrior;
                }
            }
            return null;
        }
    },
    //============================================================================================

    /**
     * Проверяет не выбрано ли у отмеченного воина в качестве оружия заклинание, которое требует в
     * качестве парамнтра координаты на карте. Если да, то возвращает метеданные заклинания. В противном
     * случае null
     */
    getSelectedWeaponAsSpellRequiresCoords: function () {
        const spell = this.getSelectedWeaponAsSpell();
        return spell && spell.additionalParameters.find(parameter => parameter.type == "MAP_POINT")
            ? spell
            : null;
    },
    //============================================================================================

    /**
     * Проверяет является ли выбранное текущее оружие выбранного воина заклинаниемБ если да, то возвращает
     * метаданные заклинания. в противном случае null
     */
    getSelectedWeaponAsSpell: function () {
        return (this._selectedWarrior_
            && this._selectedWarrior_.selectedWeapon)
            ? GAME.gameMetadata.findWarriorsSpellByName(this._selectedWarrior_.selectedWeapon)
            : null;
    },
    //============================================================================================

    /**
     * Рассполагает на сцене зоны расстановки войск своих и вражеских
     * @private
     */
    __createPlaceZones__: function (userName) {
        if (!this.contextMetadata.context.gameRan) {

            const otherPlayerPlaceZoneMaterial = new THREE.MeshPhongMaterial({
                color: 0xff0000,
                // transparent: true,
                // opacifty: 1
            });
            const thisPlayerPlaceZoneMaterial = new THREE.MeshPhongMaterial({
                color: 0xffffff,
                // transparent: true,
                // opacity: 1
            });

            for (var index = 0; index < this.contextMetadata.context.players.length; index++) {
                const player = this.contextMetadata.context.players[index];

                if (!userName || userName == player.userName) {
                    var geoSize = {
                        x: (player.startZone.bottomRightConner.x - player.startZone.topLeftConner.x + 1),
                        y: (player.startZone.bottomRightConner.y - player.startZone.topLeftConner.y + 1)
                    };

                    geoSize = this.translateGameSizeToMap(geoSize);

                    const object3D = new THREE.Group();

                    const lineWeight = 0.03;

                    var geo = new THREE.PlaneBufferGeometry(geoSize.x
                        , lineWeight
                        , 1, 1);
                    var mesh = new THREE.Mesh(geo
                        , player.userName == this.user
                            ? thisPlayerPlaceZoneMaterial
                            : otherPlayerPlaceZoneMaterial);
                    mesh.position.set(0, 0, -geoSize.y / 2);
                    mesh.rotation.x = -Math.PI / 2;
                    object3D.add(mesh);

                    geo = new THREE.PlaneBufferGeometry(geoSize.x
                        , lineWeight
                        , 1, 1);
                    mesh = new THREE.Mesh(geo
                        , player.userName == this.user
                            ? thisPlayerPlaceZoneMaterial
                            : otherPlayerPlaceZoneMaterial);
                    mesh.position.set(0, 0, geoSize.y / 2);
                    mesh.rotation.x = -Math.PI / 2;
                    object3D.add(mesh);

                    geo = new THREE.PlaneBufferGeometry(lineWeight
                        , geoSize.y
                        , 1, 1);
                    mesh = new THREE.Mesh(geo
                        , player.userName == this.user
                            ? thisPlayerPlaceZoneMaterial
                            : otherPlayerPlaceZoneMaterial);
                    mesh.position.set(-geoSize.x / 2, 0, 0);
                    mesh.rotation.x = -Math.PI / 2;
                    object3D.add(mesh);

                    geo = new THREE.PlaneBufferGeometry(lineWeight
                        , geoSize.y
                        , 1, 1);
                    mesh = new THREE.Mesh(geo
                        , player.userName == this.user
                            ? thisPlayerPlaceZoneMaterial
                            : otherPlayerPlaceZoneMaterial);
                    mesh.position.set(geoSize.x / 2, 0, 0);
                    mesh.rotation.x = -Math.PI / 2;
                    object3D.add(mesh);


                    geoSize.x = player.startZone.topLeftConner.x + (player.startZone.bottomRightConner.x - player.startZone.topLeftConner.x + 1) / 2;
                    geoSize.y = player.startZone.topLeftConner.y + (player.startZone.bottomRightConner.y - player.startZone.topLeftConner.y + 1) / 2;

                    geoSize = this.translateGameCoordsToMap(geoSize);

                    object3D.position.set(geoSize.x
                        , geoSize.y
                        , geoSize.z);

                    player.startZoneObject3D = object3D;

                    GAME.scene.add(object3D);
                }
            }
        }
    },
    //============================================================================================

    /**
     *
     * Обработка собития движения мыши
     * @param event
     * @private
     */
    __mouseMoveHandler__: function () {
        var warrior = null;
        var player = null;

        // Найдем воина над которым расположена мышь, если такой есть. И игрока, кому он принадлежит
        for (i = 0; i < this.contextMetadata.context.players.length; i++) {
            warrior = this.contextMetadata.context.players[i].warriors.find(warrior__ => GAME.flowCamera.intersectObjects([warrior__.warriorObject.object3D]).length > 0);
            if (warrior) {
                player = this.contextMetadata.context.players[i];
                break;
            }
        }

        // если есть выделенный воин, то проведем черту от него

        if (warrior != this.hoveredWarrior) {
            // что-то изменилось
            if (this.hoveredWarrior) {
                this.hoveredWarrior.warriorObject.highLighted = false;
            }

            if (warrior) {
                warrior.warriorObject.highLighted = true;
            }
            this.hoveredWarrior = warrior;
            this.warriorInfoPanel.show(this.hoveredWarrior);

            return true;
        } else {
            return false;
        }
    },
    //============================================================================================

    /**
     * Обработчик нажатий кнопок мыши
     * @param event
     * @private
     */
    __mouseDownHandler__: function (event) {
        if (event.button == 0) {
            if (GAME.flowCamera.groundCoords
                && this.hoveredWarrior != this.selectedWarrior) {
                GAME.arenaScene.selectWarrior(this.hoveredWarrior);
                this.selectedWarrior = this.hoveredWarrior;
            }

        } else if (event.button == 2) {
            if (this.selectedWarrior && this.selectedWarrior.warriorObject.playerMetadata.userName == this.user) {
                const mapCoords = GAME.flowCamera.unprojectScreePositionToVector3(GAME.flowCamera.mousePosition);
                const coords = this.translateMapCoordsToGame(mapCoords);
                if (GAME.arena.canMoveTo(coords, this.selectedWarrior)) {
                    GAME.apiRestClient.moveWarriorTo(this.selectedWarrior.id, coords.x, coords.y);
                }
            }
        }
    },
    //============================================================================================

    /**
     * Метод вызывается после того, как все объекты для работы ысены хагружены и сцена создана
     * @private
     */
    __arenaSceneLoaded__: function () {
        // this.__createPlaceZones__();
        this.contextMetadata.context.players.forEach(function (player) {
            this.__createPlayerObjects__(player);
        }.bind(this));

        this.playerOwnsThisTurn = this.contextMetadata.context.players.find(player => player.userName == this.contextMetadata.context.playerOwnsThisTurn);
        this.weaponRangeIndicator = new GAME.WeaponRangeIndicator();
        this.warriorInfoPanel = new GAME.WarriorInfoPanel();
        this.__updatePlayerMenuStatus__();

        this.showError = GAME.apiRestClient.externalShowErrorMethod.bind(this);

        // Обработчики мыши
        GAME.flowCamera.mouseMoveEventListeners.push(this.__mouseMoveHandler__.bind(this));
        GAME.flowCamera.mouseDownEventListeners.push(this.__mouseDownHandler__.bind(this));

    },
    //============================================================================================

    /**
     * Инициализация вебсокетов и формы чата
     * @private
     */
    __initWebSockAndChat_: function () {
        // Подключим webSock
        new GAME.WebSock();

        GAME.webSock.subscribeToTopic("/topic/" + this.publicContextTopic, this.__receivePublicEvent__.bind(this));

        GAME.webSock.subscribeToTopic("/topic/" + this.privateContextTopic, this.__receivePrivateEvent__.bind(this));

        // Подключимся к чатовой форме
        this.chatForm = new GAME.ChatForm(this.arenaPublicChatTopic, "arena-say", this.user, true);

        // Подключим WebSocket
        GAME.webSock.connect("/wargame/arena-hello", {name: "hello", message: "hello", date: "123456"});
    },
    //============================================================================================

    __cantLoadArenaError__: function (message) {
        this.__htmlShowMessage__(message, this.__goToTavern.bind(this));
    },
    //============================================================================================

    /**
     * Вывод сообщения на экран в HTML
     * @param message
     * @param callback
     * @private
     */
    __htmlShowMessage__: function (message, callback) {
        alert(message);
        if (callback) {
            callback(message);
        }
    },
    //============================================================================================

    /**
     * Обработка события получения сообщения в приватный канал
     * @param message
     * @private
     */
    __receivePrivateEvent__: function (message) {
        const msg = JSON.parse(message.body);
        this.__consumeGameEvent__(msg);
    },
    //============================================================================================

    /**
     * Обработка события получения сообщения в общий канал
     * @param message
     * @private
     */
    __receivePublicEvent__: function (message) {
        const msg = JSON.parse(message.body);
        if (msg.context.contextId == this.contextMetadata.context.contextId) {
            this.__consumeGameEvent__(msg);
        }
    },
    //============================================================================================

    /**
     * Обработчик игровых событий
     * @param message
     * @private
     */
    __consumeGameEvent__: function (msg) {
        switch (msg.eventType) {
            case "PLAY_PLAYER_ENTERED_TO_MAP":
                // игрок зашел на арену. проверим нет ли еще его данных
                const player = this.contextMetadata.context.players.find(player => player.userName == msg.params.player)
                if (!player) {
                    this.__appendPlayerToArena__(msg);
                }
                break;


            // Добавление воина. Это может быть только ПРИЗВАНИЕ, если игра начата. В остальных
            // случаях - игнорируем событие
            case "WARRIOR_ADDED":
                if (this.contextMetadata.context.gameRan) {
                    this.__appendWarriorToArena__(msg);
                }
                break;

            // Перемещение воина
            case "WARRIOR_MOVED":
                this.__moveWarrior__(msg);
                break;

            // Отмена перемещения воина
            case "WARRIOR_MOVE_ROLLEDBACK":
                this.__moveWarriorRolledBackEventHandler__(msg);
                break;

            // отключение пользователя от игры.
            case "PLAYER_DISCONNECTED":
                // обработка отключения пользователя
                this.__disconnectPlayerFromGame__(msg);
                break;

            // готовность игрока к игре. расстановка окончена
            case "PLAYER_CHANGED_ITS_READY_TO_PLAY_STATUS":
                this.__readyToPlayEventHandler__(msg);
                break;

            // отключение пользователя от игры.
            case "PLAYER_CONNECTED":
                // обработка отключения пользователя
                this.__updatePlayerMenuStatus__(msg.context.players.find(player => player.userName == msg.params.player));
                break;

            // Начало игры
            case "GAME_CONTEXT_GAME_HAS_BEGAN":
                // реагировать только на сообщение с полным контекстом
                if (msg.context.players[0].warriors) {
                    this.__gameHasBeganEventHandler__(msg);
                }
                break;

            // Побег
            case "PLAYER_DECAMPED_FROM_ARENA":
                this.__playerDecampedFromArenaEventHandler__(msg);
                break;

            // Проигрыш
            case "PLAYER_WINS_THE_MATCH":
                this.__playerWinsTheMachEventHandler__(msg);
                break;

            // смена хода
            case "PLAYER_TAKES_TURN":
                this.__playerOwnedThisTurnEventHandler__(msg);
                break;

            case "GAME_CONTEXT_REMOVED":
                // контекста игры более нет
                if (!this.winner) {
                    this.showError("Создатель игры покинул ее навсегда. Бой отменен", this.__goToTavern.bind(this));
                }
                break;
        }
    },
    //============================================================================================

    /**
     * Обработчик события выигрыша в игре
     * @param msg
     * @private
     */
    __playerWinsTheMachEventHandler__: function (msg) {
        this.winner = true;
        this.__copyContextFromContext__(msg);
        if (msg.params.player == this.user) {
            GAME.guiFormManager.showMessage("Бой завершен"
                , "Поздравляем с хорошей победой. Ваши соперники повержены!"
                , this.__goToTavern.bind(this));
        }
    },
    //============================================================================================

    /**
     * Реакция на событие бегства игрока
     * @param msg
     * @private
     */
    __playerDecampedFromArenaEventHandler__: function (msg) {
        if (msg.params.player == this.user) {
            // Вау!!! это сбежал я сам
            this.__goToTavern();
            return;
        } else {
            this.findPlayerByName(msg.params.player).absent = true;
            this.__removePlayerFromArena__(this.findPlayerByName(msg.params.player));
            this.__copyContextFromContext__(msg);
            this.__updatePlayerMenuStatus__();
            this.__updateWarriorButtonsState__();
        }
    },
    //============================================================================================

    /**
     * Переход к таверне
     * @private
     */
    __goToTavern: function () {
        window.location = "tavern";
    },
    //============================================================================================

    /**
     * Обработка события перемещения воина
     * @param msg
     * @private
     */
    __moveWarriorRolledBackEventHandler__: function (msg) {
        const player = this.findPlayerByName(msg.params.player);
        if (!player) {
            console.error("Player " + msg.params.player + " not found in context");
        } else {
            const warrior = this.findWarriorById(msg.params.warrior.id, player);
            if (!warrior) {
                console.error("Player " + msg.params.player + " doesn't have warrior id " + msg.params.warrior.id);
            } else {
                this.__moveWarrior__(msg);
                warrior.warriorObject.hideStartPointer();
            }
        }
    },
    //============================================================================================

    /**
     * Обработка события начала игры
     * @param msg
     * @private
     */
    __gameHasBeganEventHandler__: function (msg) {
        // убрать периметры
        this.contextMetadata.context.players.forEach(function (player) {
            this.__removePlayerStartZone__(player);
        }.bind(this));

        // убрать кнопку готовности
        if (this.topMenu.buttonReadyToPlay) {
            this.menuButtonsPlace.removeChild(this.topMenu.buttonReadyToPlay);
        }

        this.__copyContextFromContext__(msg);
        this.__updateWarriorButtonsState__();
    },
    //============================================================================================

    /**
     * Копирование контекста, пришедшего с сообщением в текущий контекст. Копируются те данные, что удается
     * "вынуть" из пришедшего контекста
     * @param msg
     * @private
     */
    __copyContextFromContext__(msg) {
        if (msg) {
            this.contextMetadata.context.gameRan = msg.context.gameRan;
            if (msg.context.playerOwnsThisTurn) {
                this.contextMetadata.context.playerOwnsThisTurn = msg.context.playerOwnsThisTurn;
            } else {
                this.contextMetadata.context.playerOwnsThisTurn = null;
            }
            if (msg.context.players) {
                msg.context.players.forEach(function (player) {
                    this.__copyPlayerFromContext__(player)
                }.bind(this));
            }
        }
    },
    //============================================================================================

    /**
     * Копирование данных игрока из пришедшего контекста
     * @param msgPlayer
     * @private
     */
    __copyPlayerFromContext__: function (msgPlayer) {

        const player = this.contextMetadata.context.players.find(player => player.userName == msgPlayer.userName);
        if (!player) {
            console.error("player " + msgPlayer.userName + " doesn't exists in context.");
            return;
        }

        player.readyToPlay = msgPlayer.readyToPlay;
        player.manna = msgPlayer.manna;

        if (msgPlayer.warriors) {
            msgPlayer.warriors.forEach(function (warrior) {
                this.__copyWarriorFromContext__(warrior, player);
            }.bind(this));
        }

    },
    //============================================================================================

    /**
     *
     * @param msgWarrior
     * @param destPlayer
     * @private
     */
    __copyWarriorFromContext__: function (msgWarrior, destPlayer) {
        const warrior = destPlayer.warriors.find(warrior => warrior.id == msgWarrior.id);
        if (!warrior) {
            console.error("player " + destPlayer.userName + " doesn't have warrior id " + msgWarrior.id + " in its context.");
            return;
        }

        warrior.abilityActionPoints = msgWarrior.abilityActionPoints;
        warrior.actionPoints = msgWarrior.actionPoints;
        warrior.armor = msgWarrior.armor;
        warrior.contrattackUsedAtThisTurn = msgWarrior.contrattackUsedAtThisTurn;
        warrior.coords = msgWarrior.coords;
        warrior.dead = msgWarrior.dead;
        warrior.health = msgWarrior.health;
        warrior.luckDefense = msgWarrior.luckDefense;
        warrior.luckMeleeAtack = msgWarrior.luckMeleeAtack;
        warrior.luckRangeAtack = msgWarrior.luckRangeAtack;
        warrior.manna = msgWarrior.manna;
        warrior.moveCost = msgWarrior.moveCost;
        warrior.moveLocked = msgWarrior.moveLocked;
        warrior.originalCoords = msgWarrior.originalCoords;
        warrior.rollbackAvailable = msgWarrior.rollbackAvailable;
        warrior.summoned = msgWarrior.summoned;
        warrior.touchedAtThisTurn = msgWarrior.touchedAtThisTurn;
        warrior.treatedActionPointsForMove = msgWarrior.treatedActionPointsForMove;
    },
    //============================================================================================

    /** перемещение воина
     *
     * @param msg
     * @private
     */
    __moveWarrior__: function (msg) {
        const player = this.contextMetadata.context.players.find(player => player.userName == msg.params.player);
        const warrior = player.warriors.find(warrior => warrior.id == msg.params.warrior.id);
        const newCoords = msg.params.warrior.coords;

        if (warrior.warriorObject) {
            const mapOldCoords = this.translateGameCoordsToMap(warrior.coords);
            mapOldCoords.w = warrior.warriorObject.object3DPositionEmulator.rotation.y;
            const mapNewCoords = this.translateGameCoordsToMap(newCoords);

            warrior.warriorObject.wayController.clear();
            warrior.warriorObject.wayController.addWayPointAsIs(mapOldCoords);
            warrior.warriorObject.wayController.addWayPoints([mapNewCoords]);
            warrior.warriorObject.wayController.moveTo(-1, warrior.warriorObject.object3DPositionEmulator);
        }

        this.__copyWarriorFromContext__(msg.params.warrior, player);

        if (this.contextMetadata.context.gameRan) {
            // Надо поставить к началу координат хода воина метку и проложить линию от нее до текущей координаты воина
            warrior.warriorObject.showStartPointer();

            this.__updateWarriorButtonsState__();
        }
    },
    //============================================================================================

    /**
     * Обработка события отключения игрока от игры
     * @private
     */
    __disconnectPlayerFromGame__: function (msg) {
        // если игра еще не начата, то смело забиваем на игрока и удаляем все по нему
        if (!this.contextMetadata.context.gameRan) {

            this.__copyContextFromContext__(msg);

            // проверить не владелец ли игры ее покинул

            var index = -1;
            const players = this.contextMetadata.context.players;
            for (i = 0; i < players.length; i++) {
                if (players[i].userName == msg.params.player) {
                    index = i;
                    break;
                }
            }
            if (index >= 0) {
                // игрок уже есть на арене или был. Надо его удалять и удалять его мусор
                const player = players[index];
                this.contextMetadata.context.players.splice(index, 1);
                this.__removePlayerFromArena__(player);
            }
        } else {
            // выдать на экран уведомление, что пользователь отключился. Владельцу предложить вариант завершить турнир,
            // а остальным покинуть или завершить. При этом идет ожидание возвращения игрока
        }
        this.__updatePlayerMenuStatus__();
    },
    //============================================================================================

    /**
     * Добавить на арену игрока, место его войск и фигурки
     * @param metadata
     * @private
     */
    __appendPlayerToArena__: function (metadata) {
        // запросим данные
        if (this.user != metadata.params.player) {
            // TODO заменить на getPlayerFullData
            GAME.apiRestClient.getGameFullData(function (result) {
                    const newPlayer = JSON.parse(result.result).players.find(player => player.userName == metadata.params.player);
                    this.contextMetadata.context.players.push(newPlayer);
                    this.__createPlayerObjects__(newPlayer);
                }.bind(this)
                ,
                // при сбое
                this.__goToTavern.bind(this));
        }
    },
    //============================================================================================

    /**
     * Выполняет инициализацию дополнителных объектов структуры воина игрока
     * @param player
     * @param warrior
     * @private
     */
    __prepareWarrior__: function (player, warrior) {
        new GAME.ArenaWarriorObject(player, warrior, this.weaponRangeIndicator);
        warrior.baseClassMetadata = GAME.gameMetadata.findWarriorBaseClassByName(warrior.warriorBaseClass);
        warrior.warriorObject.visible = true;
    },
    //============================================================================================

    /**
     * Создает объекты игрока
     * @param player
     * @private
     */
    __createPlayerObjects__: function (player) {
        this.__createPlaceZones__(player.userName);

        player.warriors.forEach(function (warrior) {
            this.__prepareWarrior__(player, warrior);
        }.bind(this));

        // это первое подключение к арене
        if (!this.contextMetadata.context.gameRan) {
            // это режим расстановки воинов. Так как это первый вход на арену, то воины там еще не
            // расставлены и их надо расставить в первичном виде на зоне расстановки ЭТОГО игрока

            // проверим, что воины не расставлены
            if (player.warriors.find(warrior => warrior.coords.x < 10) && player.userName == this.user) {

                var index = 0;
                const zCoord = player.startZone.topLeftConner.y + 1 + 3 * this.contextMetadata.context.gameRules.warriorSize / 2;
                var xCoord = player.startZone.topLeftConner.x + this.contextMetadata.context.gameRules.warriorSize / 2;
                // var xCoord = player.startZone.bottomRightConner.x - this.contextMetadata.context.gameRules.warriorSize / 2 - 2;

                const putNextWarrior = function (result) {
                    if (result) {
                        // пришел ответ от предыдущего вызова
                        // Обработка ответа идет в методе, подлучающем изменения в контексте

                        // const coords = JSON.parse(result.result);
                        //
                        // player.warriors[index].coords.x = coords.x;
                        // player.warriors[index].originalCoords.x = coords.x;
                        // player.warriors[index].coords.y = coords.y;
                        // player.warriors[index].originalCoords.y = coords.y;

                        index++;
                        // xCoord -= (1 + this.contextMetadata.context.gameRules.warriorSize);
                        xCoord += 1 + this.contextMetadata.context.gameRules.warriorSize;
                    }

                    if (index < player.warriors.length) {
                        GAME.apiRestClient.moveWarriorTo(player.warriors[index].id
                            , xCoord
                            , zCoord
                            , putNextWarrior
                            // сбой
                            , this.__goToTavern.bind(this));
                    } else {
                        // все завершено. вызовем расстановку воинов
                        // createWarriors();
                    }
                }.bind(this);

                putNextWarrior();
            }
        }

    },
    //============================================================================================

    __removePlayerStartZone__: function (player) {
        GAME.scene.remove(player.startZoneObject3D);

        player.startZoneObject3D.children.forEach(child => child.geometry.dispose());
        player.startZoneObject3D.children[0].material.dispose();
    },
    //============================================================================================

    /**
     * Удалить воинов игрока, локации, данные и освободить память
     * @param userName
     * @private
     */
    __removePlayerFromArena__: function (player) {
        if (player.startZoneObject3D) {
            this.__removePlayerStartZone__(player);

            // Удаление воинов
            player.warriors.forEach(function (warrior) {
                if (warrior.warriorObject) {
                    warrior.warriorObject.dispose();
                    warrior.warriorObject = null;
                }
            }.bind(this));
        }
    },
    //============================================================================================

    /**
     * Добавление на арену воина, созданного другим игроком
     * @param msg
     * @private
     */
    __appendWarriorToArena__: function (msg) {
        this.__updateWarriorButtonsState__();
    },
    //============================================================================================

    /**
     * Создание плашки на панели меню
     * @param additionalStyles
     * @returns {Element}
     * @private
     */
    __createMenuPlate__: function (additionalStyles) {
        const playerMenuPlate = document.createElement("div");
        playerMenuPlate.setAttribute("class", "arenaMenuPlate" + (additionalStyles ? " " + additionalStyles : ""));
        this.menuButtonsPlace.appendChild(playerMenuPlate);
        return playerMenuPlate;
    },
    //============================================================================================

    /**
     * Создание верхнего меню
     * @private
     */
    __createTopMenu__: function () {
        this.topMenu = {
            playerPlates: [],
            buttonReadyToPlay: null,
            buttonFinishTurn: null,
            buttonDecampment: null,

            buttonRollBackMove: null,

            weaponSelector: null,
        };

        for (i = 0; i < this.contextMetadata.context.mapMetadata.maxPlayersCount; i++) {
            const playerMenuPlate = this.__createMenuPlate__();
            this.topMenu.playerPlates.push(playerMenuPlate);
            // playerMenuPlate.setAttribute("style", "border: solid 3px " + this.topMenu.playerPlates[index].playerMetadata.startZone.color.replace("0x", "#") + ";background-color: " + this.red);
            playerMenuPlate.playerMetadata = null;
        }

        if (!this.contextMetadata.context.gameRan) {
            this.topMenu.buttonReadyToPlay = this.__createMenuPlate__();
            this.topMenu.buttonReadyToPlay.innerHTML = "Готов";

            this.topMenu.buttonReadyToPlay.onclick = this.__readyToPlayButtonPressed__.bind(this);

            const player = this.contextMetadata.context.players.find(player => player.userName == this.user);
            this.topMenu.buttonReadyToPlay.setAttribute("style", "background-color: " + (
                player.readyToPlay
                    ? this.green
                    : this.gray
            ));
        }

        this.topMenu.buttonFinishTurn = this.__createMenuPlate__("buttonFinishTurn");
        this.topMenu.buttonFinishTurn.onclick = this.__endTurnButtonPressed__.bind(this);
        this.topMenu.buttonFinishTurn.innerHTML = "Завершить ход";

        this.topMenu.buttonRollBackMove = this.__createMenuPlate__();
        this.topMenu.buttonRollBackMove.innerHTML = "Откат хода";
        this.topMenu.buttonRollBackMove.onclick = this.__rollbackWarriorMovePressed__.bind(this);


        const weaponSelectorContainer = this.__createMenuPlate__("weaponSelectorContainer");
        this.topMenu.weaponSelector = document.createElement("select");
        this.topMenu.weaponSelector.setAttribute("class", "weaponSelector");
        weaponSelectorContainer.appendChild(this.topMenu.weaponSelector);
        this.topMenu.weaponSelector.onchange = this.__weaponChanged__.bind(this);

        this.topMenu.buttonDecampment = this.__createMenuPlate__("decampingButton");
        this.topMenu.buttonDecampment.innerHTML = "БЕЖАТЬ";
        this.topMenu.buttonDecampment.onclick = this.__decampmentPressed__.bind(this);

        // инитим статусы кнопок воинов
        this.__updateWarriorButtonsState__();

    },
    //============================================================================================

    /**
     * Реакция на смену оружия воина
     * @private
     */
    __weaponChanged__: function () {
        if (this._selectedWarrior_) {
            this._selectedWarrior_.selectedWeapon = this.topMenu.weaponSelector.value;
        }
        this.__updateWeaponRangeIndicator__();
    },
    //============================================================================================

    /**
     * Проапдейтить статусы кнопок действий с воином
     * @private
     */
    __updateWarriorButtonsState__: function () {
        this.topMenu.buttonRollBackMove.enabled = this.contextMetadata
            && this.selectedWarrior
            && this.selectedWarrior.warriorObject
            && this.selectedWarrior.warriorObject.playerMetadata.userName == this.user
            && this.playerOwnsThisTurn
            && this.selectedWarrior.warriorObject.playerMetadata.userName == this.playerOwnsThisTurn.userName
            && this.selectedWarrior.touchedAtThisTurn
            && this.selectedWarrior.rollbackAvailable;
        this.topMenu.buttonRollBackMove.setAttribute("style", "background-color: " + (
            this.topMenu.buttonRollBackMove.enabled
                ? this.green
                : this.gray
        ));

        // выбор оружия
        if (this.topMenu.weaponSelector.oldSelectedWarrior != this.selectedWarrior) {
            this.topMenu.weaponSelector.innerHTML = "";
            this.topMenu.weaponSelector.disabled = !this.selectedWarrior
                // || this.selectedWarrior.warriorObject.playerMetadata.userName != this.user
                || this.selectedWarrior.dead;

            this.topMenu.weaponSelector.setAttribute("style", 'background-color: ' + (
                this.topMenu.weaponSelector.disabled
                    ? this.gray
                    : this.green
            ));

            const createWeaponOption = function (title, id) {
                const option_ = document.createElement("option");
                option_.innerHTML = title;
                option_.setAttribute("value", id);
                this.topMenu.weaponSelector.appendChild(option_);
            }.bind(this);

            // автовыбор оружия
            if (!this.topMenu.weaponSelector.disabled) {
                // заполним список оружием
                this.selectedWarrior.weapons.forEach(function (weapon) {
                    createWeaponOption(weapon.title, weapon.id);
                }.bind(this));

                // заполним список заклинаниями
                this.selectedWarrior.baseClassMetadata.spells.forEach(function (spell) {
                    createWeaponOption(spell.title, spell.title);
                }.bind(this));

                if (!this.selectedWarrior.selectedWeapon
                    && this.selectedWarrior.weapons.length > 0) {
                    this.selectedWarrior.selectedWeapon = this.selectedWarrior.weapons[0].id;
                }

                if (this.selectedWarrior.selectedWeapon) {
                    this.topMenu.weaponSelector.value = this.selectedWarrior.selectedWeapon;
                }
            }

            // запомним текущего воина, чтобы в следующий раз не выполнять эти действия впустую
            this.topMenu.weaponSelector.oldSelectedWarrior = this.selectedWarrior;
        }
    },
    //============================================================================================

    /**
     * Зполняет текст и статус плашек игроков
     * @private
     */
    __updatePlayerMenuStatus__: function (additionalPlayer) {

        const setPlateStatus = function (player, index) {
            this.topMenu.playerPlates[index].playerMetadata = player;
            this.topMenu.playerPlates[index].innerHTML = this.topMenu.playerPlates[index].playerMetadata.userName;

            this.topMenu.playerPlates[index].setAttribute("style", "border: solid 3px " + this.topMenu.playerPlates[index].playerMetadata.startZone.color.replace("0x", "#") + "; background-color: " +
                (this.playerOwnsThisTurn && this.playerOwnsThisTurn.userName == player.userName
                        ? this.green
                        : (this.topMenu.playerPlates[index].playerMetadata.absent
                                ? this.maroon
                                : (this.topMenu.playerPlates[index].playerMetadata.readyToPlay
                                    ? ""
                                    : this.yellow)
                        )
                )
            );
        }.bind(this);

        for (i = 0; i < this.topMenu.playerPlates.length; i++) {
            this.topMenu.playerPlates[i].playerMetadata = null;
            this.topMenu.playerPlates[i].innerHTML = "ОЖИДАНИЕ";
            this.topMenu.playerPlates[i].setAttribute("style", "background-color: " + this.red);
        }

        for (j = 0; j < this.contextMetadata.context.players.length; j++) {
            setPlateStatus(this.contextMetadata.context.players[j], j);
        }

        if (additionalPlayer) {
            setPlateStatus(additionalPlayer, j);
        }

        // кнопка перехода хода
        var style = "background-color: " + (this.contextMetadata.context.gameRan && this.getPlayerOwnsThisTurn().userName == this.user ? this.green : this.gray);
        this.topMenu.buttonFinishTurn.setAttribute("style", style);

        this.topMenu.buttonDecampment.enabled = this.contextMetadata.context.gameRan;
        this.topMenu.buttonDecampment.setAttribute("style", "background-color: " + (
            this.topMenu.buttonDecampment.enabled
                ? this.green
                : this.gray
        ));
    },
    //============================================================================================

    /**
     * Нажатие кнопки ПОБЕГ
     * @private
     */
    __decampmentPressed__: function () {
        if (this.topMenu.buttonDecampment.enabled) {
            GAME.guiFormManager.showQuestion("Побег с поля боя"
                , "При побеге все ставки и выплаты будут потеряны. Вы хотите сбежать с отрядом с поля боя?"
                , null
                , function () {
                    GAME.apiRestClient.decampFromTheArena();
                }.bind(this));
        }
    },
    //============================================================================================

    /**
     * Обработка кнопки отката перемещения воина
     * @private
     */
    __rollbackWarriorMovePressed__: function () {
        if (this.topMenu.buttonRollBackMove.enabled) {
            GAME.apiRestClient.rollbackMove(this.selectedWarrior.id);
        }
    },
    //============================================================================================

    /**
     * Завершить ход и передать следующему игроку
     * @private
     */
    __endTurnButtonPressed__: function () {
        if (this.contextMetadata.context.gameRan && this.playerOwnsThisTurn.userName == this.user) {
            GAME.apiRestClient.endTurn();
        }
    },
    //============================================================================================

    /**
     * Реакция на нажатие кнопки готовности к игре
     * @private
     */
    __readyToPlayButtonPressed__: function () {
        const player = this.contextMetadata.context.players.find(player => player.userName == GAME.arena.user);
        GAME.apiRestClient.playerReadyToPlay(!player.readyToPlay);
    },
    //============================================================================================

    /**
     * Обраьотка события изменения готовности к игре одного из игроков
     * @param msg
     * @private
     */
    __readyToPlayEventHandler__: function (msg) {
        const newPlayer = msg.context.players.find(player => player.userName == msg.params.player);
        const player = this.contextMetadata.context.players.find(player => player.userName == msg.params.player);
        player.readyToPlay = newPlayer.readyToPlay;

        // Если это я сам, то надо еще и кнопку поправить
        if (newPlayer.userName == this.user) {
            this.topMenu.buttonReadyToPlay.setAttribute("style", "background-color: " + (
                player.readyToPlay
                    ? this.green
                    : this.gray
            ));
        }

        this.__copyContextFromContext__(msg);

        this.__updatePlayerMenuStatus__();
    },
    //============================================================================================

    /**
     * Обновляет индикатор дальности оружия
     * @private
     */
    __updateWeaponRangeIndicator__: function () {
        // индикатор дальности действия выбранного оружия или заклинания
        if (this.weaponRangeIndicator.warrior != this._selectedWarrior_
            || (this.weaponRangeIndicator.warrior && this.weaponRangeIndicator.warrior.selectedWeapon != this.weaponRangeIndicator.stuff)) {
            if (!this._selectedWarrior_ || !this._selectedWarrior_.selectedWeapon) {
                // Нет отмеченного воина. Скрыть индикатор
                this.weaponRangeIndicator.hide();
            } else {
                this.weaponRangeIndicator.show(this._selectedWarrior_, this._selectedWarrior_.selectedWeapon);
            }
        }

    },
    //============================================================================================

    set selectedWarrior(value) {
        this._selectedWarrior_ = value;

        this.__updateWarriorButtonsState__();
        this.__updateWeaponRangeIndicator__();
    },

    get selectedWarrior() {
        return this._selectedWarrior_;
    },
    //============================================================================================
};