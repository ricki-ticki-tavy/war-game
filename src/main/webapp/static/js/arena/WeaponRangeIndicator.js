/**
 * Игндикатор дальности действия оружия или заклинания воина
 * @constructor
 */

GAME.WeaponRangeIndicator = function () {
    // На ком установлен индикатор
    this.warrior = null;
    this.stuff = null;

    this.__createCircles__();

};

GAME.WeaponRangeIndicator.__createCircle__ = function (material) {
    const circleGeo = new THREE.Path();
    circleGeo.autoClose = false;

    const circleRadius = 1.0;
    circleGeo.absarc(0, 0, circleRadius, 0, Math.PI * 2, true);
    circleGeo.absarc(0, 0, circleRadius * 0.996, 0, Math.PI * 2, true);
    circleGeo.absarc(0, 0, circleRadius * 1.004, 0, Math.PI * 2, true);

    const geoPoints = new THREE.BufferGeometry().setFromPoints(circleGeo.getPoints());
    const circle = new THREE.Line(geoPoints, material);
    circle.position.set(0, 0.001, 0);
    circle.rotation.x = Math.PI / 2;

    return circle;
};


GAME.WeaponRangeIndicator.prototype = {
    constructor: GAME.WeaponRangeIndicator,
    //==================================================================

    /**
     * скрыть индикатор
     * @param warrior
     * @param staff
     */
    hide: function () {
        this.__show__(null, null);
    },
    //==================================================================

    /**
     * Показать для оружия или заклинания
     * @param warrior
     * @param staff
     */
    show: function (warrior, stuff) {
        this.__show__(warrior, stuff);
    },
    //==================================================================

    /**
     * Показать для оружия или заклинания
     * @param warrior
     * @param staff
     */
    __show__: function (warrior, stuff) {
        if ((this.warrior != warrior || this.stuff != stuff)
            && this.warrior && this.stuff) {
            // скрыть с предыдущего воина и предмета
            this.warrior.warriorObject.billboardObject3D.remove(this.object3D);
        }

        this.warrior = warrior;
        this.stuff = stuff;

        this.__prepareCircles__();

        if (stuff && warrior) {
            this.warrior.warriorObject.billboardObject3D.add(this.object3D);
        }
    },
    //==================================================================

    /**
     * устанавливает  размер кругов
     * @private
     */
    __prepareCircles__: function () {
        if (this.warrior) {

            var weapon = this.warrior.weapons.find(weapon_ => weapon_.id == this.stuff);
            var spell = null;
            if (!weapon) {
                spell = this.warrior.baseClassMetadata.spells.find(spell_ => spell_.title == this.stuff);
            }

            if (weapon) {
                weapon = GAME.gameMetadata.findWeaponByName(weapon.title);
            }

            if (!spell && !weapon) {
                console.error("stuff id " + this.stuff);
                this.circleRed.scale.set(1.1, 1.1, 1.1);
                this.circleBlue.scale.set(1.2, 1.2, 1.2);
                this.circleGreen.scale.set(1.3, 1.3, 1.3);
                this.circleBlack.scale.set(1.4, 1.4, 1.4);
            } else {
                var scale = 0;
                if (weapon) {

                    // прикопаем в оружие его метаданные, чтобы в последствии быстрее их добывать
                    this.stuff.weaponMetadata = weapon;

                    if (weapon.hasMeleeAttack) {
                        // есть ближняя атака
                        scale = weapon.maxMeleeRange * GAME.userSettingsKeeper.arena.scale;
                    } else {
                        scale = 0.001;
                    }
                    this.circleBlack.scale.set(scale, scale, scale);

                    if (weapon.hasRangeAttack) {
                        // дистанционная атака
                        scale = weapon.minRangeDistance * GAME.userSettingsKeeper.arena.scale;
                        this.circleGreen.scale.set(scale, scale, scale);

                        if (weapon.fadeRangeStart == weapon.maxRangeDistance) {
                            scale = 0.001;
                        } else {
                            scale = weapon.fadeRangeStart * GAME.userSettingsKeeper.arena.scale;
                        }
                        this.circleBlue.scale.set(scale, scale, scale);

                        scale = weapon.maxRangeDistance * GAME.userSettingsKeeper.arena.scale;
                        this.circleRed.scale.set(scale, scale, scale);

                    } else {
                        scale = 0.001;
                        this.circleGreen.scale.set(scale, scale, scale);
                        this.circleBlue.scale.set(scale, scale, scale);
                        this.circleRed.scale.set(scale, scale, scale);
                    }


                } else {
                    // это заклинание
                    scale = 0.001;
                    this.circleBlack.scale.set(scale, scale, scale);
                    this.circleGreen.scale.set(scale, scale, scale);
                    this.circleRed.scale.set(scale, scale, scale);

                    // если у заклинания есть дистанция, то очертим ее
                    const distanceParameter = spell.additionalParameters.find(parameter => parameter.distanceMax && !parameter.middleCoords);
                    if (distanceParameter){
                        scale = distanceParameter.distanceMax / 10.0 * GAME.userSettingsKeeper.arena.scale;
                    }

                    this.circleBlue.scale.set(scale, scale, scale);

                }
            }
        }
    },
    //==================================================================

    __createCircles__: function () {

        this.object3D = new THREE.Group();

        this.circleBlack = GAME.WeaponRangeIndicator.__createCircle__(GAME.modelsLibrary.materials.blackBoldLineMaterial);
        this.object3D.add(this.circleBlack);

        this.circleBlue = GAME.WeaponRangeIndicator.__createCircle__(GAME.modelsLibrary.materials.blueBoldLineMaterial);
        this.circleBlue.scale.set(2, 2, 2);
        this.object3D.add(this.circleBlue);

        this.circleGreen = GAME.WeaponRangeIndicator.__createCircle__(GAME.modelsLibrary.materials.greenBoldLineMaterial);
        this.circleGreen.scale.set(3, 3, 3);
        this.object3D.add(this.circleGreen);

        this.circleRed = GAME.WeaponRangeIndicator.__createCircle__(GAME.modelsLibrary.materials.redBoldLineMaterial);
        this.circleRed.scale.set(4, 4, 4);
        this.object3D.add(this.circleRed);
    },
    //==================================================================

};