/**
 * Кдасс флажка над головой воина
 * @param flagMaterial
 * @constructor
 */
GAME.WarriorFlag = function (flagColor) {
    this.flagColor = parseInt(flagColor.replace("#", "0x"));
    this.__createObjects__();
};

GAME.WarriorFlag.prototype = {
    constructor: GAME.WarriorFlag,
    //===========================================================================

    __createObjects__: function(){
        this.object3D = new THREE.Group();
        const armGeo = new THREE.CylinderBufferGeometry(0.02, 0.02, 1.4, 3, 3);
        const armMesh = new THREE.Mesh(armGeo, GAME.modelsLibrary.materials.brownBaseMaterial.clone());
        this.object3D.add(armMesh);
        // armMesh.rotation.x = Math.PI / 2;
        armMesh.position.set(0, 1.3, -0.1);

        // Вымпел
        var triangleShape = new THREE.Shape();
        triangleShape.moveTo( 0, 0 );
        triangleShape.lineTo( 0.8, 0.2 );
        triangleShape.lineTo( 0, 0.4 );
        triangleShape.lineTo( 0, 0 );
        const flagGeo = new THREE.ShapeBufferGeometry(triangleShape);
        const mat = new THREE.MeshPhongMaterial({color: this.flagColor, side: THREE.DoubleSide});

        const flagMesh = new THREE.Mesh(flagGeo, mat);
        this.object3D.add(flagMesh);

        this.flagMesh = flagMesh;

        flagMesh.position.set(0, 1.5, -0.1);
        flagMesh.rotation.y = (Math.random() - 0.5) * 3;

        this.object3D.rotation.z = (Math.random() - 0.5) * 0.5;

    },
    //===========================================================================
};