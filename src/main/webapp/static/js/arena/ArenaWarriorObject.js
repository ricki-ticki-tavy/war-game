/**
 * Объкт воина на карте
 * @constructor
 */

GAME.ArenaWarriorObject = function (playerMetadata, warriorMetadata, weaponRangeIndicator) {
    this.playerMetadata = playerMetadata;
    this.warriorMetadata = warriorMetadata;
    this.weaponRangeIndicator = weaponRangeIndicator;

    warriorMetadata.warriorObject = this;

    this.__visible__ = false;
    this.__disposed__ = false;
    // признак подсвеченности
    this.__highLighted__ = false;

    this.classMetadata = GAME.gameMetadata.findWarriorBaseClassByName(warriorMetadata.warriorBaseClass);
    this.object3D = GAME.gameMetadata.createWarriorObject3DByWarriorClassName(warriorMetadata.warriorBaseClass);

    this.billboardObject3D = new THREE.Group();

    this.wayController = new GAME.WayController();
    this.object3DPositionEmulator = new GAME.ArenaWarriorObject3D(this);

    this.startCoordsPointerVisible = false;
    this.startCoordsPointer = this.__createStartPositionPointer__();
    this.startCoordLine = null;

    this.flag = new GAME.WarriorFlag(playerMetadata.startZone.color);
    this.object3D.add(this.flag.object3D);
};

GAME.ArenaWarriorObject.prototype = {
    constructor: GAME.ArenaWarriorObject,

    /**
     * анимация различных эффектов и передвижение воина
     */
    animate: function () {
        // работа контроллера перемещения
        this.wayController.animate();
    },
    //===============================================================================

    /**
     * Удалить объект воина со сцены
     */
    dispose: function () {
        if (!this.__disposed__) {
            this.__removeFromScene__();

            this.object3D.traverse(function (child) {
                if (child.material) {
                    child.material.dispose();
                }
                if (child.geometry) {
                    child.geometry.dispose();
                }
            }.bind(this));

            this.__disposed__ = true;

            if (this.weaponRangeIndicator.warrior == this.warriorMetadata) {
                // индикатор был на этом воине. Скрыть его
                this.weaponRangeIndicator.hide();
            }

            // удалить объект билборда
            this.billboardObject3D.children.forEach(function (child) {
                child.traverse(function (subChild) {
                    if (subChild.material) {
                        subChild.material.dispose();
                    }
                    if (subChild.geometry) {
                        subChild.geometry.dispose();
                    }
                }.bind(this));
            }.bind(this));

            this.hideStartPointer();
            this.startCoordsPointer.geometry.dispose();
        }
    },
    //===============================================================================

    /**
     * Скрыть указатель координат в начале хода
     * @private
     */
    hideStartPointer: function () {
        if (this.startCoordsPointerVisible) {
            GAME.scene.remove(this.startCoordsPointer);
            this.__hideStartCoordPointerLine__();
            this.startCoordsPointerVisible = false;
        }
    },
    //===============================================================================

    /**
     * Скрыть линию между начальной координатой воина и воином
     * @private
     */
    __hideStartCoordPointerLine__: function () {
        if (this.startCoordLine) {
            GAME.scene.remove(this.startCoordLine);
            this.startCoordLine.geometry.dispose();
            this.startCoordLine = null;
        }
    },
    //===============================================================================

    /**
     * Создать и отобразить линию между начальными координатами воина и текущими
     * @private
     */
    __createStartCoordsLine__: function () {
        const fromCoords = GAME.arena.translateGameCoordsToMap(this.warriorMetadata.originalCoords);
        const toCoords = GAME.arena.translateGameCoordsToMap(this.warriorMetadata.coords);
        const lineGeo = new THREE.Path();
        lineGeo.autoClose = true;
        lineGeo.moveTo(fromCoords.x, fromCoords.z);
        lineGeo.lineTo(toCoords.x, toCoords.z);

        const geoPoints = new THREE.BufferGeometry().setFromPoints(lineGeo.getPoints());
        this.startCoordLine = new THREE.Line(geoPoints, GAME.modelsLibrary.materials.whiteLineMaterial);
        this.startCoordLine.position.set(0, 0.001, 0);
        this.startCoordLine.rotation.x = Math.PI / 2;
        GAME.scene.add(this.startCoordLine);

    },
    //===============================================================================

    /**
     * Показать указатель координат в начале хода
     * @private
     */
    showStartPointer: function () {
        if (!this.startCoordsPointerVisible) {
            GAME.scene.add(this.startCoordsPointer);
            this.startCoordsPointerVisible = true;
        }
        const mapCoords = GAME.arena.translateGameCoordsToMap(this.warriorMetadata.originalCoords);
        this.startCoordsPointer.position.set(mapCoords.x, 0.5, mapCoords.z);

        // Если есть предыдущая линия, то удалить ее
        this.__hideStartCoordPointerLine__();
        this.__createStartCoordsLine__();
    },
    //===============================================================================

    /**
     * Создать объект визуализации начальной координаты воина в данном ходе
     * @private
     */
    __createStartPositionPointer__: function () {
        const geo = new THREE.CylinderBufferGeometry(0.02, 0.02, 1, 3, 1);
        const mesh = new THREE.Mesh(geo, GAME.modelsLibrary.materials.whiteMaterial);
        mesh.castShadow = true;
        mesh.receiveShadow = false;
        // mesh.rotation.x = -Math.PI / 2;
        return mesh;
    },
    //===============================================================================

    /**
     * Удалить объект воина со сцены
     */
    __removeFromScene__: function () {
        if (this.__visible__ && !this.__disposed__) {
            GAME.scene.remove(this.object3D);
            GAME.scene.remove(this.billboardObject3D);
            this.__visible__ = false;
        }
    },
    //===============================================================================

    /**
     * Вставить воина в сцену
     */
    __appendToScene__: function () {
        if (!this.__visible__ && !this.__disposed__) {
            const coords = GAME.arena.translateGameCoordsToMap(this.warriorMetadata.coords);
            this.object3DPositionEmulator.position.set(coords.x, coords.y, coords.z);
            // this.billboardObject3D.position.set(coords.x, coords.y + 0.00001, coords.z);
            GAME.scene.add(this.object3D);
            GAME.scene.add(this.billboardObject3D);
            if (GAME.arena.contextMetadata.context.gameRan && this.warriorMetadata.touchedAtThisTurn) {
                this.showStartPointer();
            }
            this.__visible__ = true;
        }

    },
    //===============================================================================

    set visible(value) {
        if (value) {
            this.__appendToScene__();
        } else {
            this.__removeFromScene__();
        }
    },
    //===============================================================================

    get visible() {
        return this.__visible__;
    },
    //===============================================================================

    set highLighted(value) {
        if (this.__highLighted__ != value) {

            var emissiveIntensity = 1;
            var emissiveR = 0;
            var emissiveG = 0;
            var emissiveB = 0;

            if (value) {
                if (GAME.arena.user == this.playerMetadata.userName) {
                    emissiveG = 0x03;
                    emissiveB = 0x03;
                } else {
                    emissiveR = 0x04;
                }
                emissiveIntensity = 0.1;
            }
            GAME.Utils.highLightObject(this.object3D, emissiveR, emissiveG, emissiveB, emissiveIntensity);

            this.__highLighted__ = value;
        }
    },
    //===============================================================================

    get highLighted() {
        return this.__highLighted__;
    },
    //===============================================================================
};