class Game {

    static apiUrl = "game/api/";

    /**
     * UUID контекста
     * @type {String}
     */
    gameContextUUID = undefined;

    /**
     * Признак,что контекст технический
     * @param isTechical
     */
    constructor (isTechical){
        this.isTechnical = isTechical;

        if (isTechical){
            // контекст технический
            $.ajax({
                url: Game.apiUrl + "createTechnicalContext",
                data: "",
                success: this.receiveCreateContextResult,
                dataType: "json"
            });
        }
    }

    /**
     * реакция на получение ответа на запрос создания контекста
     * @param response
     */
    receiveCreateContextResult(response){
        if (response.failed){
            alert(response);
        } else {
            this.gameContextUUID = response.result;
        }
    }
}
