GAME.DancingFire = function(color, intensity, distance, decay, intensityAmplitude, positionAmplitude, yPositionAmplitude){
    this.intencityChaneSpeed = 0.4;
    this.intensityCangeValue = 0;
    this.intensityChangeStepsCount = 1;

    this.positionChangeStepsCount = 1;
    this.positionAmplitude = positionAmplitude;
    this.yPositionAmplitude = yPositionAmplitude;
    this.positionChangeSpeed = 0.02;
    this.positionChangeXValue = 0;
    this.positionChangeZValue = 0;
    this.positionChangeYValue = 0;

    this.baseIntencity = intensity;
    this.intensityAmplitude = intensityAmplitude;

    this.position = new THREE.Vector3(0, 0, 0);

    this.fireLight = new THREE.PointLight(color, intensity, distance, decay);
    this.fireLight.position.set(0, 1, 0);
    this.fireLight.castShadow = true;

    Object.defineProperties( this, {
        position: {
            configurable: true,
            enumerable: true,
            value: this.position
        },
    });


};

GAME.DancingFire.prototype = {
    constructor: GAME.DancingFire,

    animate: function(){

        this.fireLight.intensity += this.intensityCangeValue;
        if (--this.intensityChangeStepsCount == 0) {
            // пора выбрать новую яркость
            var newIntensity = this.baseIntencity - this.intensityAmplitude / 2 + Math.random() * this.intensityAmplitude;
            var delta = newIntensity - this.fireLight.intensity;
            this.intensityChangeStepsCount = (Math.floor(Math.abs(delta) * GAME.flowCamera.avgRateCoef / (this.intencityChaneSpeed * (0.6 + Math.random() * 0.8)))) + 1;
            this.intensityCangeValue = delta / this.intensityChangeStepsCount;
        }

        this.fireLight.position.x += this.positionChangeXValue;
        this.fireLight.position.z += this.positionChangeZValue;
        this.fireLight.position.y += this.positionChangeYValue;
        // дрожание огня
        if (--this.positionChangeStepsCount == 0){
            var newPlaceX = this.position.x + Math.random() * this.positionAmplitude - this.positionAmplitude / 2;
            var newPlaceZ = this.position.z + Math.random() * this.positionAmplitude - this.positionAmplitude / 2;
            var newPlaceY = this.position.y + Math.random() * this.yPositionAmplitude - this.yPositionAmplitude / 2;
            var deltaX = newPlaceX - this.fireLight.position.x;
            var deltaZ = newPlaceZ - this.fireLight.position.z;
            var deltaY = newPlaceY - this.fireLight.position.y;
            var maxDelta = Math.abs(Math.abs(deltaX) > Math.abs(deltaZ) ? deltaX : deltaZ);
            this.positionChangeStepsCount = Math.floor(maxDelta * GAME.flowCamera.avgRateCoef / ( this.positionChangeSpeed
                + (Math.random() * this.positionChangeSpeed * 0.6
                    - this.positionChangeSpeed * 0.3)));
            this.positionChangeStepsCount++;

            this.positionChangeXValue = deltaX / this.positionChangeStepsCount;
            this.positionChangeZValue = deltaZ / this.positionChangeStepsCount;
            this.positionChangeYValue = deltaY / this.positionChangeStepsCount;
        }

    },

    get light(){
        return this.fireLight;
    }
};