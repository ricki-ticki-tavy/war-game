GAME.GuiImage = function (x, y, width, height, image, iX, iY, iWidth, iHeight, textPaper) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;

    this.iX = iX;
    this.iY = iY;
    this.iWidth = iWidth;
    this.iHeight = iHeight;

    this.image = image;

    this.textPaper = textPaper;
    this.opacity = 1;
    this.isImage = true;

    this.visible = true;
    this.isGeometry = false;
};

GAME.GuiImage.prototype = {
    constructor: GAME.GuiImage,

    draw: function (withClean) {
        if (withClean || false) {
            this.textPaper.clear(this.x, this.y, this.width, this.height);
        }
        if (this.visible) {
            this.textPaper.opacity = this.opacity;
            this.textPaper.drawImage(this.image, this.iX, this.iY, this.iWidth, this.iHeight, this.x, this.y, this.width, this.height);
        }
    },
};