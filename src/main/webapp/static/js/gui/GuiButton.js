GAME.GuiButton = function(x, y, width, height, material, clickEvent){
    this.clickEvent = clickEvent;

    this.geometry = this.__createScroll__(x, y, width, height, material);

    this.isButton = true;
    this.isGeometry = true;
};

GAME.GuiButton.prototype = {
    constructor: GAME.GuiButton,

    // создает плоскость с кнопкой
    __createScroll__: function(x, y, width, height, material){
        var geo = new THREE.PlaneBufferGeometry(width, height, 1, 1);
        var scroll = new THREE.Mesh(geo, material);
        scroll.position.z = 0.0015;
        scroll.position.x = x;
        scroll.position.y = y;

        return scroll;
    },

    set visible(value){
        this.geometry.visible = value;
    },

    get visible(){
        return this.geometry.visible;
    },

    set material(value){
        this.geometry.material = value;
    },



};