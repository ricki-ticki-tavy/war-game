GAME.GuiForm = function (x, y, width, height, modal) {
    this.isForm = true;
    this.visible = false;

    this.formZShift = 0.01;
    this.elementsZShift = 0.001;

    this.x = x / 4;
    this.y = y / 4;
    this.isModal = modal;

    //элементы формы
    this.controls = [];

    // Объединяющий объект
    this.object3D = new THREE.Group();

    // фон формы
    this.papirus = this.__createPapirus__(this.x, this.y, width, height);

    // слой для отображения текста и рисования
    this.textPageMaterial = new GAME.GuiTextPaper(2048, 2048);
    this.textPageMaterial.init();
    this.textPage = this.__createTextPage__(this.x, this.y);

    this.bringToFront();

};

GAME.GuiForm.prototype = {
    constructor: GAME.GuiForm,

    /**
     * Вывести на экран форму
     * @param form
     */
    show: function () {
        if (!this.visible) {
            this.visible = true;
            this.textPageMaterial.clear();
            this.update(false);
            GAME.guiFormManager.__showForm__(this);
        }
    },

    /**
     * скрыть форму. Рекомендуется использовать метод show() у самой формы
     * @param form
     */
    hide: function () {
        if (this.visible) {
            this.visible = false;
            GAME.guiFormManager.__hideForm__(this);
        }
    },

    /**
     * Отрисовать все текстовые элементы на форме
     */
    update: function (withClean) {
        if (this.visible) {
            this.controls.forEach(function (control) {
                if (control.isText || control.isImage) {
                    control.draw(withClean);
                }
            }.bind(this));
        }

    },

    /**
     * стирает изображение всех текстовых данных
     */
    clear: function () {
        this.textPageMaterial.clear();
    },

    createButton: function (x, y, width, height, material, clickEvent) {
        var button = new GAME.GuiButton(x / 4 + this.x + width / 8, y / 4 + this.y - height / 8, width / 4, height / 4, material, clickEvent);
        // button.geometry.geometry.translate(width / 8, y, 0);
        this.object3D.add(button.geometry);
        this.controls.push(button);
        return button;
    },

    createText: function (x, y, width, height, text, fontSize, fontColor) {
        var text = new GAME.GuiText(x, y, width, height, text, fontSize, fontColor, this.textPageMaterial);
        this.controls.push(text);
        return text;
    },

    createImage: function (x, y, width, height, image, imgX, imgY, imgWidth, imgHeight) {
        var img = new GAME.GuiImage(x, y, width, height, image, imgX, imgY, imgWidth, imgHeight, this.textPageMaterial);
        this.controls.push(img);
        return img;
    },

    /**
     * Сдвинуть форму на шаг вниз.
     * @private
     */
    moveToBack: function () {
        this.__moveByDelta__(-this.formZShift);
    },

    /**
     * Сдвинуть форму на шаг вверх.
     * @private
     */
    moveToFront: function () {
        this.__moveByDelta__(this.formZShift);
    },

    /**
     * Сдвинуть на самый вверх.
     * @private
     */
    bringToFront: function () {
        this.object3D.children.forEach(function(child){
            child.position.z = -1 + this.elementsZShift;
        }.bind(this));

        // сдвиг чуть вверх индивидуально дл кнопок
        this.controls.forEach(function (control) {
            if (control.isButton)
                control.geometry.position.z = -1 + this.elementsZShift * 1.01;
        }.bind(this));

        this.textPage.position.z = -1 + this.elementsZShift;
        this.papirus.position.z = -1;
    },

    /**
     * Отображает сообщение
     * @param title
     * @param text
     */
    showMessage: function (title, text) {
        GAME.guiFormManager.showMessage(title, text);
    },

    /**
     * Открывает форму с вопросом
     * @param title
     * @param question
     * @param cancelCallback
     * @param okCallback
     */
    showQuestion: function (title, text, cancelCallback, okCallback) {
        GAME.guiFormManager.showQuestion(title, text, cancelCallback, okCallback);
    },

    lockScreen: function () {
        GAME.guiFormManager.lockScreen();
    },

    unLockScreen: function () {
        GAME.guiFormManager.unLockScreen();
    },

    /**
     * Открывает форму с запросом одного параметра.
     * @param title
     * @param question
     * @param cancelCallback
     * @param okCallback
     */
    prompt: function (title, text, defaultValue, cancelCallback, okCallback) {
        GAME.guiFormManager.prompt(title, text, defaultValue, cancelCallback, okCallback);
    },

    __createPapirus__: function (x, y, width, height) {
        var geo = new THREE.PlaneBufferGeometry(width / 4, height / 4, 1, 1);
        var texture = new THREE.MeshPhongMaterial({
            color: 0xffffff,
            transparent: true,
            map: GAME.modelsLibrary.textures.guiFormTexture
        })
        // var texture = new THREE.MeshPhongMaterial({color: 0xffffff, emissiveIntensity: 1, emissive:0x2a180a, transparent: true,})
        var papirus = new THREE.Mesh(geo, texture);
        papirus.castShadow = false;
        papirus.receiveShadow = false;
        this.object3D.add(papirus);

        papirus.position.x = x + width / 8;
        papirus.position.y = y - height / 8;

        return papirus;

    },

    __createTextPage__: function (x, y) {
        var geo = new THREE.PlaneBufferGeometry(6 / 6, 6 / 6, 1, 1);
        var textPage = new THREE.Mesh(geo, this.textPageMaterial.material);
        this.object3D.add(textPage);
        textPage.position.x = x + 6 / 12;
        textPage.position.y = y - 6 / 12;
        return textPage;
    },

    /**
     * перемещение формы по Z на указанную величину
     * @param delta
     * @private
     */
    __moveByDelta__: function (delta) {

        this.object3D.children.forEach(function(child){
            child.position.z += delta;
        }.bind(this));
    },

    /**
     * Прозрачность фона формы
     * @param val
     */
    set opacity(val) {
        this.papirus.material.opacity = val;
    },

    get opacity() {
        return this.papirus.material.opacity;
    }

};