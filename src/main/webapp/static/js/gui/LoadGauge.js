GAME.LoadGauge = function (scene) {
    this.scene = scene;
    this.currentProgress = 0;
    this.currentTotal = 0;
    this.boxSize = 10;
    this.gaugeSize = 9;

    this.boxMaterial = new THREE.MeshPhongMaterial({color: 0xffff00, emissive: 0xffff00, emissiveIntensity: 1});
    this.gaugefullMaterial = new THREE.MeshPhongMaterial({color: 0xffff00, emissive: 0x2288ff, emissiveIntensity: 1, opacity: 0.2, transparent: true,});
    this.gaugeProgressMaterial = new THREE.MeshPhongMaterial({color: 0xffff00, emissive: 0x2288ff, emissiveIntensity: 1});

    this.baseGeo = new THREE.BoxBufferGeometry(this.boxSize, 0.5, 0.001, 1, 1, 1);
    this.baseMesh = new THREE.Mesh(this.baseGeo, this.boxMaterial);
    // this.currentTotal = new TREE

    this.baseMesh.position.set(0, 0, - 15);
    this.scene.add(this.baseMesh);

    this.totalTubeGeo = new THREE.CylinderBufferGeometry(0.1, 0.1, this.gaugeSize, 16, 1);
    this.totalTubeMesh = new THREE.Mesh(this.totalTubeGeo, this.gaugefullMaterial);
    this.totalTubeMesh.rotation.z = - Math.PI / 2;
    this.totalTubeMesh.position.set(0, 0, -14.95);

    this.scene.add(this.totalTubeMesh);

    this.progressTubeGeo = new THREE.CylinderBufferGeometry(0.1, 0.1, this.gaugeSize, 16, 1);
    // this.progressTubeGeo.center
    this.progressTubeMesh = new THREE.Mesh(this.progressTubeGeo, this.gaugeProgressMaterial);
    this.progressTubeMesh.rotation.z = - Math.PI / 2;
    this.progressTubeMesh.position.set(0, 0, -14.95);

    this.scene.add(this.progressTubeMesh);

    this.total = 25;
    this.progress = 1;
};

GAME.LoadGauge.prototype = {
    constructor: GAME.LoadGauge,

    incProgress: function(){
        this.currentProgress++;
        this.__update__();
    },

    hide: function(){
        this.scene.remove(this.progressTubeMesh);
        this.scene.remove(this.totalTubeMesh);
        this.scene.remove(this.baseMesh);

        this.progressTubeMesh.geometry.dispose();
        this.gaugeProgressMaterial.dispose();

        this.totalTubeMesh.geometry.dispose();
        this.gaugefullMaterial.dispose();

        this.baseMesh.geometry.dispose();
        this.boxMaterial.dispose();
    },

    set progress(value) {
        this.currentProgress = value;
        this.__update__();
    },

    get progress() {
        return this.currentProgress;
    },

    set total(value){
        this.__update__();
        this.currentTotal = value;
    },

    get total(){
        return this.currentTotal;
    },

    __update__: function(){
        this.progressTubeMesh.scale.y = (this.currentProgress / this.currentTotal);
        this.progressTubeMesh.position.x = ( - (this.gaugeSize / 2 - this.gaugeSize * this.progressTubeMesh.scale.y / 2.0));
    }
};