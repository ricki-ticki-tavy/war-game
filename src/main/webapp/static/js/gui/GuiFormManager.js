GAME.GuiFormManager = function (scene, flowCamera, modelsLibrary, renderer, freeMode) {

    GAME.guiFormManager = this;

    this.freeMode = freeMode;

    this.flowCamera = flowCamera;
    this.scene = scene;
    this.modelsLibrary = modelsLibrary;
    this.light = this.__createLight__();
    this.renderer = renderer;

    /**
     * Форма для вывода сообщения
     */
    this.messageBoxForm = this.__createMessageBoxForm__();

    this.modalCursor = this.__createCursorForm__();

    /**
     * регистрация слушателя нажатия кнопок на формах
     */
    this.flowCamera.mouseDownEventListeners.push(this.__mouseDownListener__.bind(this));

    /**
     * Стек активных форм
     * @type {Array}
     */
    this.forms = [];
};

GAME.GuiFormManager.prototype = {
    constructor: GAME.GuiFormManager,

    createForm: function (x, y, width, height, modal) {
        var form = new GAME.GuiForm(x, y, width, height, modal);
        return form;
    },
    //============================================================================

    __hideForm__: function (form) {
        if (this.forms[this.forms.length - 1] !== form) {
            console.error("closing not last opened form. rejected");
        } else {
            this.forms.pop();
            this.scene.remove(form.object3D);
            this.forms.forEach(function (otherForm) {
                otherForm.moveToFront();
            }.bind(this));
        }
    },
    //============================================================================

    __showForm__: function (form) {
        this.scene.add(form.object3D);
        form.object3D.position.set(0, 0, 0);

        form.bringToFront();

        // отодвинем предыдущие формы вниз.
        this.forms.forEach(function (form) {
            form.moveToBack();
        }.bind(this));

        this.forms.push(form);
    },
    //============================================================================

    /**
     * Отображает сообщение
     * @param title
     * @param text
     */
    showMessage: function(title, text, callback){
        this.messageBoxForm.labelTitle.text = title;
        this.messageBoxForm.labelMessage.text = text;
        this.messageBoxForm.buttonOk.visible = false;
        this.messageBoxForm.okCallback = undefined;
        this.messageBoxForm.cancelCallback = callback;
        if (this.forms[this.forms.length - 1] !== this.messageBoxForm){
            this.messageBoxForm.show();
        } else {
            this.messageBoxForm.clear();
            this.messageBoxForm.update(false);
        }

    },
    //============================================================================



    /**
     * Открывает форму с вопросом
     * @param title
     * @param question
     * @param cancelCallback
     * @param okCallback
     */
    showQuestion: function(title, question, cancelCallback, okCallback){
        this.messageBoxForm.labelTitle.text = title;
        this.messageBoxForm.labelMessage.text = question;
        this.messageBoxForm.buttonOk.visible = true;
        this.messageBoxForm.okCallback = okCallback;
        this.messageBoxForm.cancelCallback = cancelCallback;
        if (this.forms[this.forms.length - 1] !== this.messageBoxForm){
            this.messageBoxForm.show();
        } else {
            this.messageBoxForm.clear();
            this.messageBoxForm.update(false);
        }

    },
    //============================================================================

    /**
     * Открывает форму с запросом одного параметра.
     * @param title
     * @param question
     * @param cancelCallback
     * @param okCallback
     */
    prompt: function(title, question, defaultValue, cancelCallback, okCallback){
        this.messageBoxForm.labelTitle.text = title;
        this.messageBoxForm.labelMessage.text = question;
        this.messageBoxForm.buttonOk.visible = true;
        this.messageBoxForm.okCallback = okCallback;
        this.messageBoxForm.cancelCallback = cancelCallback;

        if (this.forms[this.forms.length - 1] !== this.messageBoxForm){
            this.messageBoxForm.show();

            var inputX = 0.5 * this.renderer.context.canvas.width - 100;
            var inputY = 0.5 * this.renderer.context.canvas.height - 12;

            this.messageBoxForm.inputDom.removeAttribute("style");
            this.messageBoxForm.inputDom.setAttribute("style", "position: absolute; opacity: 0.6; font-size: 22px;  width: 200px; top: " + inputY + "px; left: " + inputX + "px");
            document.body.appendChild(this.messageBoxForm.inputDom);
            if (defaultValue) {
                this.messageBoxForm.inputDom.value = defaultValue;
            }

            this.messageBoxForm.inputVisible = true;

        } else {
            this.messageBoxForm.clear();
            this.messageBoxForm.update(false);
        }

    },
    //============================================================================

    lockScreen: function(){
        if (!this.modalCursor.visible) {
            this.modalCursor.show();
        }
    },
    //============================================================================

    unLockScreen: function(){
        if (this.modalCursor.visible) {
            this.modalCursor.hide();
        }
    },
    //============================================================================

    /**
     * Необходимо вызывать перед каждым рендерингом, если камера подвижна. Метод смещает объекты форм под камеру
     * и меняет их поворот, чтобы они были всегда под камерой неподвижно
     */
    dispatch: function () {
        this.forms.forEach(this.__rearrangeForm__.bind(this));
        this.light.position.set(this.flowCamera.camera.position.x, this.flowCamera.camera.position.y, this.flowCamera.camera.position.z);
    },
    //============================================================================
    //============================================================================
    //============================================================================

    __rearrangeForm__: function (form) {
        form.object3D.position.set(this.flowCamera.camera.position.x, this.flowCamera.camera.position.y, this.flowCamera.camera.position.z);
        form.object3D.rotation.set(this.flowCamera.camera.rotation.x, this.flowCamera.camera.rotation.y, this.flowCamera.camera.rotation.z);
    },
    //============================================================================

    __createLight__: function () {
        var light = new THREE.PointLight(0xffffff, 2, 4, 0.1);
        this.scene.add(light);
        light.castShadow = false;
        return light;
    },
    //============================================================================

    __mouseDownListener__: function (mouseEvent) {
        if (this.forms.length > 0) {
            this.flowCamera.rayCaster.setFromCamera(this.flowCamera.mousePosition, this.flowCamera.camera);
            // есть активные формы. ПОищем пересечения с контролами
            var form = this.forms.slice(-1).pop();
            for (var controlIndex = 0; controlIndex < form.controls.length; controlIndex++) {
                if (form.controls[controlIndex].isGeometry) {
                    var intersects = this.flowCamera.rayCaster.intersectObjects([form.controls[controlIndex].geometry], true);
                    if (intersects.length > 0) {
                        if (form.controls[controlIndex].clickEvent) {
                            form.controls[controlIndex].clickEvent(form.controls[controlIndex]);
                        }
                        return true;
                    }
                }
            }

            return form.isModal;
        } else {
            return false;
        }
    },
    //============================================================================

    /**
     * Создает форму, использующуюся для отображения простого уведомления
     * @returns {*}
     * @private
     */
    __createMessageBoxForm__: function () {
        var form = this.createForm(-0.75, 0.3, 1.5, 0.6, true);

        form.labelTitle = form.createText(150, 55, 300, 70, "ЗАГОЛОВОК ОКНА", 45, "#ffffff" );

        form.labelMessage = form.createText(50, 100, 700, 150, "СООБЩЕНИЕ", 35, "#ffff00");
        form.labelMessage.wordWrap = true;

        form.inputDom = document.createElement("input");
        form.inputDom.setAttribute("type", "text");

        form.buttonOk = form.createButton(0.5, -0.43, 0.15, 0.15, this.modelsLibrary.materials.guiOkButtonMaterial,
            this.__standardCloseAction__.bind(this, this.__messageOkCallbackWrapper__.bind(this)));

        form.buttonCancel = form.createButton(0.9, -0.43, 0.15, 0.15, this.modelsLibrary.materials.guiCancelButtonMaterial,
            this.__standardCloseAction__.bind(this, this.__messageCancelCallbackWrapper__.bind(this)));

        form.opacity = 0.8;

        return form;
    },
    //============================================================================

    __messageCancelCallbackWrapper__: function(form){
        if (this.messageBoxForm.cancelCallback) {
            this.messageBoxForm.cancelCallback(form);
        }
    },

    __messageOkCallbackWrapper__: function(form, inputValue){
        if (this.messageBoxForm.okCallback) {
            this.messageBoxForm.okCallback(form, inputValue);
        }
    },

    __standardCloseAction__: function(callback){
        var inputValue;
        if (this.messageBoxForm.inputVisible){
            inputValue = this.messageBoxForm.inputDom.value;
            this.messageBoxForm.inputVisible = false;
            document.body.removeChild(this.messageBoxForm.inputDom);
        }
        this.messageBoxForm.hide();
        if (callback) {
            callback(this.messageBoxForm, inputValue);
        }
    },
    //============================================================================

    __createCursorForm__: function(){
        var form = this.createForm(-0.10, 0.1, 0.2, 0.2, true);
        return form;

    },
    //============================================================================
    //============================================================================

}
;