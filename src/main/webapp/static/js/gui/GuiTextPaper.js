GAME.GuiTextPaper = function (width, height) {
    this.width = width;
    this.height = height;
    this.color = "#000000";
    this.fontSize = 50;
    this.opacity = 1;
    this.fontName = "Caveat";
    // this.fontName = "Amatic SC";
    /**
     * Межстрочный интервал от высоты шрифта
     * @type {number}
     */
    this.interlineSpase = 0.0;

    this.domCanvas = undefined;
    this.canvas2D = undefined;
    this.texture = undefined;
    this.material = undefined;
};

GAME.GuiTextPaper.prototype = {
    constructor: GAME.GuiTextPaper,

    /**
     * Готовит объект к формированию текстуры вывода текста
     */
    init: function () {
        this.domCanvas  = document.createElement("canvas");
        this.domCanvas.setAttribute("width", this.width);
        this.domCanvas.setAttribute("height", this.height);

        this.canvas2D = this.domCanvas.getContext("2d");

        this.texture = new THREE.CanvasTexture(this.domCanvas);
        //this.texture.center.x = -1;
        this.material = new THREE.MeshPhongMaterial({color: 0xffffff, map: this.texture, transparent: true});

        return this.material;

    },
    //=================================================================================================================

    /**
     * Очистить фрагмент или весь лист
     */
    clear: function (x, y, width, height) {
        this.canvas2D.clearRect(x || 0, y || 0, width || this.width, height || this.height);
        this.material.needsUpdate = true;
        this.material.map.needsUpdate = true;
    },
    //=================================================================================================================

    /**
     * освобождает ресурс. вызывать при скрытии формы
     */
    free: function () {
        if(this.material) {
            this.material.map.dispose();
            this.material.dispose();
            this.domCanvas = undefined;
            this.canvas2D = undefined;
            this.texture = undefined;
            this.material = undefined;
        }
    },
    //=================================================================================================================

    drawImage: function(image, imgX, imgY, imgWidth, imgHeight, destX, destY, destWidth, destHeight){
       this.canvas2D.drawImage(image, imgX, imgY, imgWidth, imgHeight, destX, destY, destWidth, destHeight);
    },
    //=================================================================================================================

    fillText: function (text, x, y, width, height, withClean, wrap) {
        if ( wrap || false){
            this.__fillTextWithWraping__(text, x, y, width, height, withClean);
        } else {
            width = width || 0;
            height = height || 0;
            x = x || 0;
            y = y || 0;
            withClean === undefined ? false : withClean;

            if ((width > 0 || height > 0) && withClean ) {
                this.canvas2D.clearRect(x , (y - height ), width, height);
            }
            this.canvas2D.globalAlpha = this.opacity;
            this.canvas2D.font = " " + this.fontSize + "px " + this.fontName;
            this.canvas2D.fillStyle = this.color;
            this.canvas2D.fillText(text, x, y);
            this.material.needsUpdate = true;
            this.texture.needsUpdate = true;
        }
    },
    //=================================================================================================================

    __fillTextWithWraping__: function (text, x, y, width, height, withClean) {
        var startY = y;
        if ((width > 0 || height > 0) && withClean){
            this.canvas2D.clearRect(x, y - height, width, height);
        }
        this.canvas2D.globalAlpha = this.opacity;
        this.canvas2D.font = " " + this.fontSize + "px " + this.fontName;
        this.canvas2D.fillStyle = this.color;

        // вычислим высоту строки
        var lineHeight = this.fontSize * (1 + this.interlineSpase);

        // разобъем на слова
        var words = text.split(" ").reverse();

        var line = "";
        var textWidth;

        while (words.length > 0 && (y + lineHeight - startY) <= height){
            textWidth = this.canvas2D.measureText(line + " " + words.slice(-1)).width;
            if (textWidth > width) {
                // ширина строки + новое слово превысила допустимую ширину.
                // если строка пуста и уже первое слово не влезает, то пишем его, даже не влезающее
                if (line.length === 0){
                    line = words.pop();
                }

                // рисуем текст
                this.canvas2D.fillText(line, x, y);

                // новые координаты и строка
                y += lineHeight;

                line = "";

            } else {
                line += words.pop() + " ";
            }
        }
        // и отрисока последней строки
        if (line.length > 0 && (y + lineHeight - startY) <= height){
            this.canvas2D.fillText(line, x, y);
        }
    },
    //=================================================================================================================

};
