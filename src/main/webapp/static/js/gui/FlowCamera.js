THREE.FlowCamera = function (camera, position, target, sceneSize, ground) {

    this.ground = ground;

    GAME.flowCamera = this;

    this.avgIntervalMs = 0;
    this.avgIntervalMsSMA = 0.07;

    this.camera = camera;

    this.isMoving = false;

    this.enabled = true;

    this.target = target;

    this.rayCaster = new THREE.Raycaster();

    this.wheelCoef = 0.01;

    /**
     * Сглаженный коэффициент реальных FPS от идеальных 60
     * @type {number}
     */
    this.avgRateCoef = 1;

    /**
     * слушатели нажатия кнопки мыши
     * @type {Array}
     */
    this.mouseDownEventListeners = [];

    /**
     * слушатели нажатия кнопки мыши
     * @type {Array}
     */
    this.mouseUpEventListeners = [];

    /**
     * слушатели движения мыши
     * @type {Array}
     */
    this.mouseMoveEventListeners = [];

    this.delta = new THREE.Vector3(position.x - target.x
        , position.y - target.y
        , position.z - target.z);

    if (sceneSize != undefined && sceneSize != null) {
        // Размер карты
        this.sceneSize = {
            topLeft: new THREE.Vector2(sceneSize.topLeft.x, sceneSize.topLeft.y),
            bottomRight: new THREE.Vector2(sceneSize.bottomRight.x, sceneSize.bottomRight.y)
        }
    } else {
        // Размер карты
        this.sceneSize = {
            topLeft: new THREE.Vector2(-7.5, -7.5),
            bottomRight: new THREE.Vector2(7.5, 7.5)
        }
    }

    //дата последнего перемещения
    this.lastTime = 0;

    //координаты мыши при предыдущем расчете
    this.oldMousePosition = new THREE.Vector2();

    this.mousePosition = new THREE.Vector2(0, 0);

    this.mouseClient = new THREE.Vector2(0, 0);

    // Ускорение (коэффициент для SMA)
    this.speedAcceleration = 10.0;

    // куда камера стремится
    this.destination = new THREE.Vector3(target.x, position.y, target.z);

    //минимальная скорость передвижения
    this.minMoveSpeed = 0.001;

    this.heightRestrictions = {
        top: 10,
        bottom: 1
    };

    camera.position.x = position.x;
    camera.position.y = position.y;
    camera.position.z = position.z;
    camera.lookAt(target.x, target.y, target.z);

    this.cameraPosition = new THREE.Vector3(camera.position.x, camera.position.y, camera.position.z);

    document.addEventListener('mousemove', this.__flowCameraMouseMove__.bind(this), false);
    document.addEventListener('mousedown', this.__flowCameraMouseDown__.bind(this), false);
    document.addEventListener('mouseup', this.__flowCameraMouseUp__.bind(this), false);
    document.addEventListener('touchmove', this.__flowCameraMouseMove__.bind(this), false);
    document.addEventListener('contextmenu', function (ev) {
        ev.preventDefault();
    });

    document.getElementsByTagName("canvas").item(0).addEventListener('wheel', this.__flowCameraWheel__.bind(this), true);

    this.tempVector3 = new THREE.Vector3();

    // прокрутка карты
    this.scrollSpeed = new THREE.Vector2(0.005, 0.005);

    // координаты земли, над которыми находится мышь
    this.groundCoords = null;

};
//============================================================================

THREE.FlowCamera.prototype = {

    constructor: THREE.FlowCamera,

    /**
     * Соытие по нажатию кнопки мыши
     * @param event
     * @private
     */
    __flowCameraMouseDown__: function (event) {
        this.mousePosition.x = ( event.clientX / window.innerWidth ) * 2 - 1;
        this.mousePosition.y = -( event.clientY / window.innerHeight ) * 2 + 1;
        var stopChain = false;
        for (var listenerEventIndex = 0; listenerEventIndex < this.mouseDownEventListeners.length; listenerEventIndex++) {
            stopChain = this.mouseDownEventListeners[listenerEventIndex](event);
            if (stopChain) {
                break;
            }

        }
        if (!stopChain && this.ground && event.button == 0) {
            this.groundHookedCoords = this.unprojectScreePositionToVector3(this.mousePosition);
            this.cameraHookedCoords = GAME.flowCamera.destination;
        }
    },
    //============================================================================

    /**
     * обработка позиции камеры
     * @param mousePosition
     */
    updateCamera: function () {

        if (this.lastTime === 0) {
            this.lastTime = new Date();
            return;
        }

        if (!this.ground) {
            // тут расчет новых координат
            if ((this.mousePosition.x !== this.oldMousePosition.x
                    || this.mousePosition.y !== this.oldMousePosition.y) && this.enabled) {
                this.destination.x = (this.sceneSize.bottomRight.x - this.sceneSize.topLeft.x) * (this.mousePosition.x + 1.0) / 2.0
                    + this.sceneSize.topLeft.x;

                this.destination.z = (this.sceneSize.bottomRight.y - this.sceneSize.topLeft.y) * (1.0 - this.mousePosition.y) / 2.0
                    + this.sceneSize.topLeft.y;
            }
        }

        if (this.mousePosition.x !== this.oldMousePosition.x
            || this.mousePosition.y !== this.oldMousePosition.y || this.isMoving) {
            // обработаем слушателей движения мыши
            for (var listenerEventIndex = 0; listenerEventIndex < this.mouseMoveEventListeners.length; listenerEventIndex++) {
                var stopChain = this.mouseMoveEventListeners[listenerEventIndex](this.mousePosition);
                if (stopChain) {
                    break;
                }
            }

        }

        this.oldMousePosition.x = this.mousePosition.x;
        this.oldMousePosition.y = this.mousePosition.y;

        var operDate = new Date();

        var timeMs = operDate - this.lastTime;

        // расчет среднего сглаженного интервала между кадрами
        this.avgIntervalMs = timeMs - (timeMs - this.avgIntervalMs) * (1 - this.avgIntervalMsSMA);
        this.avgRateCoef = 1000 / 60 / this.avgIntervalMs;

// перемещение камеры, если надо перемещать
        var deltaX = this.destination.x - this.cameraPosition.x + this.delta.x;
        var deltaZ = this.destination.z - this.cameraPosition.z + this.delta.z;
        var deltaY = this.destination.y - this.cameraPosition.y;

        if ((Math.abs(deltaX) > 0.0001 || Math.abs(deltaZ) > 0.0001 || Math.abs(deltaY) > 0.0001)) {
            // надо двигать
            var movementX = this.__calcSpeed__(this.cameraPosition.x - this.delta.x
                , this.destination.x);

            var movementZ = this.__calcSpeed__(this.cameraPosition.z - this.delta.z
                , this.destination.z);

            var movementY = this.__calcSpeed__(this.cameraPosition.y
                , this.destination.y);

            var newX = this.cameraPosition.x - this.delta.x + movementX;
            var newZ = this.cameraPosition.z - this.delta.z + movementZ;
            var newY = this.cameraPosition.y + movementY;

            this.cameraPosition.set(newX + this.delta.x, newY, newZ + this.delta.z);
            if (Math.abs(deltaY) > 0.0001) {
                this.camera.lookAt(newX, this.target.y, newZ);
                this.camera.updateProjectionMatrix();
            }


            this.camera.position.x = this.cameraPosition.x;
            this.camera.position.z = this.cameraPosition.z;
            this.camera.position.y = this.cameraPosition.y;


            this.isMoving = true;
        } else {
            this.isMoving = false;
        }

        this.lastTime = operDate;
    },
    //============================================================================

    /**
     * Ищет пересечения с объектами луча зрения , проходящего через позицию мыши на экране
     * @param objects
     * @returns {*}
     */
    intersectObjects: function (objects) {
        this.rayCaster.setFromCamera(this.mousePosition, GAME.camera);
        return this.rayCaster.intersectObjects(objects, true);
    },
    //============================================================================

    /**
     * Событие по отпусканию кнопки мыши
     * @param event
     * @private
     */
    __flowCameraMouseUp__: function (event) {
        this.mousePosition.x = ( event.clientX / window.innerWidth ) * 2 - 1;
        this.mousePosition.y = -( event.clientY / window.innerHeight ) * 2 + 1;
        var stopChain = false;
        for (var listenerEventIndex = 0; listenerEventIndex < this.mouseUpEventListeners.length; listenerEventIndex++) {
            stopChain = this.mouseUpEventListeners[listenerEventIndex](event);
            if (stopChain) {
                break;
            }

        }

        if (this.ground && event.button == 0 && !stopChain) {
            // режим арены. Перемещение по захвату
            this.groundHookedCoords = null;
            this.cameraHookedCoords = null;
        }

    },
    //============================================================================

    /**
     * Преобразует координаты мыши в координаты землм=и
     * @private
     */
    unprojectScreePositionToVector3: function (position) {
        GAME.flowCamera.rayCaster.setFromCamera(position, GAME.camera);
        var intersects = GAME.flowCamera.rayCaster.intersectObjects([this.ground], true);
        if (intersects.length > 0) {
            return intersects[0].point;
        } else {
            return null;
        }
    },
    //============================================================================

    __calcSpeed__: function (sourceCoord, destCoord, timeMs) {
        // Чтобы не было рывков при слабых видюхах. сглаженное время между обсчетами смещения карты
        timeMs = this.avgIntervalMs;

        var delta = (destCoord - sourceCoord) * timeMs * this.speedAcceleration / 1000.0;

        // Контроль на минимальную скорость
        if (Math.abs(delta) < this.minMoveSpeed * timeMs) {
            delta = delta > 0
                ? this.minMoveSpeed * timeMs
                : -this.minMoveSpeed * timeMs;
        }

        // Контроль на не пролететь точку назначения
        if (Math.abs(destCoord - sourceCoord) < Math.abs(delta)) {
            // перелет. подправим, чтобы попасть точно
            delta = destCoord - sourceCoord;
        }

        return delta;
    },
    //============================================================================

    __flowCameraMouseMove__: function (event) {
        event.preventDefault();

        this.mouseClient.x = event.clientX;
        this.mouseClient.y = event.clientY;

        this.mousePosition.x = ( event.clientX / window.innerWidth ) * 2 - 1;
        this.mousePosition.y = -( event.clientY / window.innerHeight ) * 2 + 1;

        if (this.ground) {
            this.groundCoords = this.unprojectScreePositionToVector3(this.mousePosition);
            if (this.groundCoords){
                this.groundCoords.y = 0.001;
            }
            if (this.groundHookedCoords) {
                if (this.groundCoords) {
                    GAME.flowCamera.destination.z = this.groundHookedCoords.z - this.groundCoords.z + this.cameraHookedCoords.z;
                    GAME.flowCamera.destination.x = this.groundHookedCoords.x - this.groundCoords.x + this.cameraHookedCoords.x;

                    if (GAME.flowCamera.destination.z > this.sceneSize.bottomRight.y) {
                        GAME.flowCamera.destination.z = this.sceneSize.bottomRight.y;
                    } else if (GAME.flowCamera.destination.z < this.sceneSize.topLeft.y) {
                        GAME.flowCamera.destination.z = this.sceneSize.topLeft.y;
                    }

                    if (GAME.flowCamera.destination.x > this.sceneSize.bottomRight.x) {
                        GAME.flowCamera.destination.x = this.sceneSize.bottomRight.x;
                    } else if (GAME.flowCamera.destination.x < this.sceneSize.topLeft.x) {
                        GAME.flowCamera.destination.x = this.sceneSize.topLeft.x;
                    }
                }

            } else if (GAME.userSettingsKeeper.arena.activeBorders) {
                if (this.mousePosition.y > 0.95) {
                    window.setTimeout(this.__scrollUp__.bind(this));
                } else if (this.mousePosition.y < -0.95) {
                    window.setTimeout(this.__scrollDown__.bind(this));
                }

                if (this.mousePosition.x < -0.95) {
                    window.setTimeout(this.__scrollLeft__.bind(this));
                } else if (this.mousePosition.x > 0.95) {
                    window.setTimeout(this.__scrollRight__.bind(this));
                }
            }
        }
    },
    //============================================================================

    __scrollUp__: function () {
        if (this.mousePosition.y > 0.95
            && this.destination.z > this.sceneSize.topLeft.y) {
            this.destination.z -= this.scrollSpeed.y;
            if (this.destination.z < this.sceneSize.topLeft.y) {
                this.destination.z = this.sceneSize.topLeft.y;
            }
            window.setTimeout(this.__scrollUp__.bind(this));
        }
    },
    //============================================================================

    __scrollDown__: function () {
        if (this.mousePosition.y < -0.95
            && this.destination.z < this.sceneSize.bottomRight.y) {
            this.destination.z += this.scrollSpeed.y;
            if (this.destination.z > this.sceneSize.bottomRight.y) {
                this.destination.z = this.sceneSize.bottomRight.y;
            }
            window.setTimeout(this.__scrollDown__.bind(this));
        }
    },
    //============================================================================

    __scrollLeft__: function () {
        if (this.mousePosition.x < -0.95
            && this.destination.x > this.sceneSize.topLeft.x) {
            this.destination.x -= this.scrollSpeed.x;
            if (this.destination.x < this.sceneSize.topLeft.x) {
                this.destination.x = this.sceneSize.topLeft.x;
            }
            window.setTimeout(this.__scrollLeft__.bind(this));
        }
    },
    //============================================================================

    __scrollRight__: function () {
        if (this.mousePosition.x > 0.95
            && this.destination.x < this.sceneSize.bottomRight.x) {
            this.destination.x += this.scrollSpeed.x;
            if (this.destination.x > this.sceneSize.bottomRight.x) {
                this.destination.x = this.sceneSize.bottomRight.x;
            }
            window.setTimeout(this.__scrollRight__.bind(this));
        }
    },
    //============================================================================

    __flowCameraWheel__: function (event) {
        event.preventDefault();
        event.stopPropagation();
        if (this.enabled) {
            if (event.ctrlKey) {
                // var deltaY = this.camera.position.y;
                // this.camera.position.y += event.deltaY * this.wheelCoef;
                // if (this.camera.position.y < this.heightRestrictions.bottom) {
                //     this.camera.position.y = this.heightRestrictions.bottom;
                // } else if (this.camera.position.y > this.heightRestrictions.top) {
                //     this.camera.position.y = this.heightRestrictions.top;
                // }
                // deltaY -= this.camera.position.y;
                this.target.y -= event.deltaY * this.wheelCoef;
                GAME.camera.lookAt(this.cameraPosition.x - this.delta.x, this.target.y, this.cameraPosition.z - this.delta.z);
            } else {
                this.destination.y += event.deltaY * this.wheelCoef;
                if (this.destination.y < this.heightRestrictions.bottom) {
                    this.destination.y = this.heightRestrictions.bottom;
                } else if (this.destination.y > this.heightRestrictions.top) {
                    this.destination.y = this.heightRestrictions.top;
                }
            }
        }
    },
//============================================================================
//============================================================================
//============================================================================


}
;
