GAME.GuiText = function (x, y, width, height, text, fontSize, color, textPaper) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.text = text;
    this.fontSize = fontSize;
    this.color = color;
    this.textPaper = textPaper;
    this.opacity = 1;
    this.isText = true;

    this.visible = true;

    this.wordWrap = false;
    this.isGeometry = false;
};

GAME.GuiText.prototype = {
    constructor: GAME.GuiText,

    draw: function (withClean) {
        if (this.visible) {
            this.textPaper.color = this.color;
            this.textPaper.fontSize = this.fontSize;
            this.textPaper.opacity = this.opacity;
            this.textPaper.fillText(this.text, this.x, this.y, this.width, this.height, withClean, this.wordWrap);
        } else {
            if (withClean || false){
                this.textPaper.clear(this.x, this.y - this.height, this.width, this.height);
            }
        }
    },
};