/**
 * Класс, содержащий схему пути из одной точки, в другую
 * @constructor
 */
GAME.WayController = function () {
    /**
     *  скорость поворота. Рад / сек
     * @type {number}
     */
    this.rotationSpeed = Math.PI * 3;

    /**
     * скорость перемещения. ед / сек
     */
    this.moveSpeed = 6;

    /**
     * координаты, по которым идет перемещение.
     * @type {Array}
     */
    this.wayPoints = [];

    /**
     * Текущая позиция. три координаты и угол поворота
     */
    this.position = new THREE.Vector4;

    /**
     * Индекс точки пути в которую сейчас идет движение
     */
    this.wayPointIndexMovingTo = -1;

    /**
     * Конечная точка пути
     * @type {number}
     */
    this.destinationWayPointIndex = -1;

    /**
     * Направление движения. +1 - от начала к концу, -1 от конца к началу
     * @type {number}
     */
    this.direction = +1;

    /**
     * Приращения по координатам и углу поворота для следующего шага
     * @type {number}
     */
    this.deltaX = 0;
    this.deltaY = 0;
    this.deltaZ = 0;
    this.deltaAngle = 0;

    /**
     * Угол поворота, соответствующий повороту лицом на камеру
     * @type {number}
     */
    this.zeroAngle = 0;

    /**
     * Количество шагов, которое нужно еще сделать для завершения перемещения к назначенной точке пути
     * @type {number}
     */
    this.stepsCountToNextWayPoint = 0;

    this.active = false;

    /**
     * метод, который будет вызван, когда движение завершится
     * @type {null}
     */
    this.callbackWhenFinished = null;
    this.object3D = null;

    Object.defineProperties(this,
        {
            position: {
                configurable: true,
                enumerable: true,
                value: this.position
            }
        });
};

GAME.WayController.prototype = {
    constructor: GAME.WayController,
    //=========================================================================================

    animate: function () {
        if (this.active) {
            if (this.stepsCountToNextWayPoint <= 0) {
                // Пора искть новый
                if (this.destinationWayPointIndex == this.wayPointIndexMovingTo) {
                    // движение завершено
                    // развернемся как положено в оригинальной точке
                    if (this.direction < 0) {
                        this.direction = 1;
                        this.__calcDeltas__(new THREE.Vector4(
                            this.wayPoints[this.wayPointIndexMovingTo].x
                            , this.wayPoints[this.wayPointIndexMovingTo].y
                            , this.wayPoints[this.wayPointIndexMovingTo].z
                            , this.wayPoints[this.wayPointIndexMovingTo].w + Math.PI)
                            , this.wayPoints[this.wayPointIndexMovingTo]);
                    } else {
                        this.active = false;
                        if (this.callbackWhenFinished) {
                            this.callbackWhenFinished(this.object3D);
                        }
                    }
                } else {
                    // движение к следующей точке
                    var wayPointFrom = this.wayPoints[this.wayPointIndexMovingTo];
                    this.wayPointIndexMovingTo += this.direction;
                    this.__calcDeltas__(wayPointFrom, this.wayPoints[this.wayPointIndexMovingTo]);
                }
            }

            if (this.active) {
                this.object3D.position.x += this.deltaX;
                this.object3D.position.y += this.deltaY;
                this.object3D.position.z += this.deltaZ;
                this.object3D.rotation.y += this.deltaAngle;
                this.stepsCountToNextWayPoint--;
            }
        }
        return this.active;
    },
    //=========================================================================================

    /**
     * Начать движение объекта
     * @param destinationWayPointIndex 0 - начало, -1 - конец или любое значение куда двигаться
     */
    moveTo: function (destinationWayPointIndex, object3D, callbackWhenFinished) {
        if (!object3D) {
            object3D = this.object3D;
        }
        if (!object3D){
            console.error("object3D can't bee null");
        }
        if (this.wayPoints.length === 0) {
            console.error("there is no one wayPoint is defined");
        }
        if (destinationWayPointIndex === -1) {
            destinationWayPointIndex = this.wayPoints.length - 1;
        }

        var closestIndex = -1;
        var minimalDistance = 9999999;
        // Найти ближайшую точку пути к конечной точке
        for (var index = 0; index < this.wayPoints.length; index++) {
            var wayPoint = this.wayPoints[index];
            var distance = (object3D.position.x - wayPoint.x) * (object3D.position.x - wayPoint.x)
                + (object3D.position.y - wayPoint.y) * (object3D.position.y - wayPoint.y)
                + (object3D.position.z - wayPoint.z) * (object3D.position.z - wayPoint.z);

            if (distance < minimalDistance) {
                minimalDistance = distance;
                closestIndex = index;
            }
        }

        // точка найдена. Теперь определим направление
        if (destinationWayPointIndex > closestIndex) {
            this.direction = 1;
        } else {
            this.direction = -1;
        }

        // Начнем движение к ближайшей точке
        object3D.rotation.y = this.wayPoints[closestIndex].w + (this.direction < 0 ? Math.PI : 0);

        this.__calcDeltas__(new THREE.Vector4(object3D.position.x
            , object3D.position.y
            , object3D.position.z
            , object3D.rotation.y - (this.direction < 0 ? Math.PI : 0)), this.wayPoints[closestIndex]);

        this.active = true;
        this.wayPointIndexMovingTo = closestIndex;
        this.destinationWayPointIndex = destinationWayPointIndex;
        this.callbackWhenFinished = callbackWhenFinished;
        this.object3D = object3D;
    },
    //=========================================================================================

    addWayPoints: function (newWayPoints) {
        newWayPoints.forEach(function(newWayPoint){
            this.__addWayPoint__(newWayPoint, false);
        }.bind(this));
        return this;
    },
    //=========================================================================================

    addWayPointAsIs: function (newWayPoint) {
        this.__addWayPoint__(newWayPoint, true);
        return this;
    },
    //=========================================================================================

    __addWayPoint__: function (wayPoint, addAsIs) {
        if (this.wayPoints.length > 0) {
            var deltaX = wayPoint.x - this.wayPoints[this.wayPoints.length - 1].x;
            var deltaZ = -(wayPoint.z - this.wayPoints[this.wayPoints.length - 1].z);
            if (deltaX !== 0 || deltaZ !== 0) {
                // есть смещение в горизонтальной плоскости
                var sinA = deltaX / Math.sqrt(deltaX * deltaX + deltaZ * deltaZ);
                var angle = Math.asin(sinA);
                if (deltaZ > 0) {
                    if (angle < 0) {
                        angle = -Math.PI - angle;
                    } else {
                        angle = Math.PI - angle;
                    }
                }
                // Добавим еще один поинт пути для поворота
                this.wayPoints.push(new THREE.Vector4(this.wayPoints[this.wayPoints.length - 1].x
                    , this.wayPoints[this.wayPoints.length - 1].y
                    , this.wayPoints[this.wayPoints.length - 1].z
                    , angle));
                // добавим основной вектор перемещения с тем же углом
                this.wayPoints.push(new THREE.Vector4(wayPoint.x, wayPoint.y, wayPoint.z, angle));

                // добавим вектор разворота для обратного пути
                // this.wayPoints.push(new THREE.Vector4(wayPoint.x, wayPoint.y, wayPoint.z, angle));

            } else {
                if (!addAsIs) {
                    wayPoint.w = this.wayPoints[this.wayPoints.length - 1].w;
                }
                this.wayPoints.push(wayPoint);
            }
        } else {
            this.wayPoints.push(wayPoint);
        }
    },
    //=========================================================================================

    /**
     * Расчет дельт для перемещения к слующим координатам
     * @param currentPos
     * @param destPos
     * @private
     */
    __calcDeltas__: function (currentPos, destPos) {
        var deltaX = destPos.x - currentPos.x;
        var deltaY = destPos.y - currentPos.y;
        var deltaZ = destPos.z - currentPos.z;
        var deltaAngle = destPos.w + (this.direction < 0 ? Math.PI : 0) - (currentPos.w + (this.direction < 0 ? Math.PI : 0));
        if (Math.abs(deltaAngle) > 2 * Math.PI) {
            deltaAngle = deltaAngle - 2 * Math.PI * (Math.floor(deltaAngle / 2 / Math.PI));
        }
        if (Math.abs(deltaAngle) > Math.PI) {
            deltaAngle = deltaAngle < 0
                ? Math.PI * 2 + deltaAngle
                : deltaAngle - 2 * Math.PI;
        }

        var maxDelta = Math.max(Math.abs(deltaX), Math.abs(deltaY), Math.abs(deltaZ));
        var curMoveSpeed = this.moveSpeed / GAME.flowCamera.avgRateCoef / 60;
        // var curMoveSpeed = this.moveSpeed / 60;
        var curRotateSpeed = this.rotationSpeed / GAME.flowCamera.avgRateCoef / 60
        // var curRotateSpeed = this.rotationSpeed / 60
        var moveSteps = Math.floor(Math.max(maxDelta / curMoveSpeed, Math.abs(deltaAngle) / curRotateSpeed)) + 1;

        this.stepsCountToNextWayPoint = moveSteps;
        this.deltaX = deltaX / moveSteps;
        this.deltaY = deltaY / moveSteps;
        this.deltaZ = deltaZ / moveSteps;
        this.deltaAngle = deltaAngle / moveSteps;
    }
    ,
    //=========================================================================================

    /**
     * Указывает активно ли сейчас движение по этому пути
     * @returns {boolean}
     */
    get isActive() {
        return this.active;
    }
    ,
    //=========================================================================================

    clear: function () {
        this.wayPoints = [];
    }
    //=========================================================================================
};