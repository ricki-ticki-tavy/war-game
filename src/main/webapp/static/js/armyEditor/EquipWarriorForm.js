/**
 * Форма экипировки героев в отряде
 *
 * @param guiFormManager
 * @param armyModelsLibrary
 * @param apiRestClient
 * @param campaignCamp
 * @constructor
 */
GAME.EquipWarriorForm = function () {
    GAME.equipWarriorForm = this;

    this.onCloseListener = null;

    this.rayCaster = GAME.flowCamera.rayCaster;

    this.warriorMetadata = null;

    this.form = new GAME.GuiForm(-2, 1.0, 1.5, 2, false);

    this.labelWarriorName = null;
    this.buttonEditWarriorName = null;
    this.labelClassName = null;

    /**
     * вооружение на форме выбора
     * @type {{selectedIndex: number, items: Array}}
     */
    this.items = new GAME.ItemsGroup();

    this.itemTransparentLayerMaterial = new THREE.MeshPhongMaterial({color: 0x000000, transparent: true, opacity: 0});

    this.form.opacity = 0.2;


    /**
     * центр для размещения оружия у воина
     */
    this.warriorsWeaponsMiddleCoords = new THREE.Vector3(-2, 2.5, 3);

    /**
     * интервал между предметами по горизонтали над воином. Дополнительно
     * @type {number}
     */
    this.warriorsWeaponsHorizontalInterval = 0.2;


    /**
     * габариты и мето для размещения оружия
     */
    this.weaponPlaceCoords = new THREE.Vector2(-0.48, 0.2);

    /**
     * Ширина места с оружием
     * @type {number}
     */
    this.weaponPlaceWidth = 0.35;

    /**
     * интервал между предметами по горизонтали. Дополнительно
     * @type {number}
     */
    this.weaponsOnFormHorizontalInterval = 0.016;

    /**
     * интервал между предметами по вертикали. Дополнительно
     * @type {number}
     */
    this.weaponsOnFormVerticalInterval = 0.01;

    /**
     * габариты и мето для размещения оружия
     */
    this.artifactsPlaceCoords = new THREE.Vector2(1, -0.5);

    /**
     * Ширина места с оружием
     * @type {number}
     */
    this.artifactsPlaceWidth = 1;

    /**
     * масщтаб предметов для показа на форме
     * @type {number}
     */
    this.formStuffScaleFactor = 0.07;


    this.__prepareForm__();

    GAME.flowCamera.mouseMoveEventListeners.push(this.__mouseMoveEventListener__.bind(this));
    GAME.flowCamera.mouseDownEventListeners.push(this.__mouseDownEventListener__.bind(this));
    GAME.flowCamera.mouseUpEventListeners.push(this.__mouseUpEventListener__.bind(this));

    /**
     * код взятого предмета в группе
     * @type {number}
     */
    this.takenStuffIndex = -1;

    /**
     * Координаты, откуда был взят предмет, чтобы, если что, то быстро его вернуть на место
     * @type {Vector3}
     */
    this.savedWeaponCoords = new THREE.Vector3(0, 0, 0);


    this.tempVector3 = new THREE.Vector3();
    this.tempVector3_2 = new THREE.Vector3();

    /**
     * Варианты экипировки
     * @type {Array}
     */
    this.equipVariantsCount = 5;
    this.equipVariantButtons = [];
    this.__createEquipVariantButtons__();
    this.equipChanged = false;
    this.currentEditingEquipmentIndex = -1;
    this.currentWarriorId = null;

};

GAME.EquipWarriorForm.prototype = {
    constructor: GAME.EquipWarriorForm,

    show: function (warriorMetadata) {
        this.warriorMetadata = warriorMetadata;
        this.equipChanged = false;
        this.__fillForm__(function () {
            this.form.show();
        }.bind(this));
    },
    //=========================================================================

    /**
     * Загрузить новцю экипировку
     * @param newIndex
     * @private
     */
    __openEquip__: function (newIndex, callback) {
        this.equipChanged = false;
        this.currentEditingEquipmentIndex = newIndex;

        const weaponsToLoad = [];

        // очистить ID у всех предметов и переместить их все на форму
        this.__moveAllStuffsToForm__();

        const doLoadStuff = function (result) {
            if (result) {
                var found = false;
                // это ответ на предыдущую команду. Найдем первый не распределенный предмет, кинем его воину и присвоим полученный ID
                for (var weponIndex = 0; weponIndex < this.items.modelObjects.length; weponIndex++) {
                    if (this.items.modelObjects[weponIndex].metadata.title === weaponsToLoad[0].title
                        && this.items.modelObjects[weponIndex].itemId == null) {
                        // нашли такой предмет
                        this.__moveStuffToWarrior__(this.items.modelObjects[weponIndex]);
                        this.items.modelObjects[weponIndex].itemId = result.result;
                        found = true;
                        break;
                    }
                }

                // проверим, что нашли
                if (!found) {
                    this.form.showMessage("Ошибка прменения экипировки", "Среди вооружения не найден элемент экипировки " + weaponsToLoad[0].title);
                }

                // Уберем из массива добавленный воину элемент
                weaponsToLoad.shift();

            }

            if (weaponsToLoad.length > 0) {
                // загрузим очередное оружие воину
                GAME.apiRestClient.giveWeaponToWarrior(this.currentWarriorId, weaponsToLoad[0].title, doLoadStuff);
            } else {
                // загрузка выполнена
                this.__reArrangeItemsOnForm__();
                this.__reArrangeItemsOfWarrior__();
                const newCirclePos = this.equipVariantButtons[newIndex].buttonSelectEquip.geometry.position;
                this.lightCircle.position.set(newCirclePos.x, newCirclePos.y, newCirclePos.z);
                if (callback) {
                    callback();
                }
            }
        }.bind(this);

        const doLoadWarrior = function () {
            // создадим на карте воина
            GAME.apiRestClient.createWarrior(this.warriorMetadata.baseClassName, this.warriorMetadata.title, 1, 1, function (result) {
                // воин создан
                this.currentWarriorId = result.result;

                // теперь выдадим ему все оружие, что записано за ним по конфигурации
                this.warriorMetadata.equipments[this.currentEditingEquipmentIndex].stuff.weapons.forEach(function (weapon) {
                    weaponsToLoad.push(weapon);

                }.bind(this));
                doLoadStuff();

            }.bind(this));
        }.bind(this);

        // Отправим на сервер команду удаления воина, потом создания, потом покидем ему весь шмот
        if (this.currentWarriorId) {
            GAME.apiRestClient.removeWarrior(this.currentWarriorId, doLoadWarrior);
        } else {
            doLoadWarrior();
        }

    },
    //=========================================================================

    /**
     * Сохраниеть текущую экипировку
     * @param newIndex
     * @param callbackAfterSave
     * @private
     */
    __saveEquip__: function (newIndex, callbackAfterSave) {
        if (this.currentEditingEquipmentIndex >= 0) {
            // сохранять надо.
            GAME.apiRestClient.saveWarriorAmmunition(this.currentWarriorId
                , this.warriorMetadata.equipments[this.currentEditingEquipmentIndex].name
                , function () {
                    this.equipChanged = false;

                    // подгрузить сохраненныю экипировку с сервера
                    GAME.apiRestClient.findPlayerHero(this.warriorMetadata.title
                        , function (result) {
                            this.warriorMetadata.equipments[this.currentEditingEquipmentIndex] =
                                JSON.parse(result.result).equipments
                                    .find(equipment => equipment.name == this.warriorMetadata.equipments[this.currentEditingEquipmentIndex].name);
                            // вызов дальнейших действий, после сохранения
                            if (callbackAfterSave) {
                                callbackAfterSave(newIndex);
                            }
                        }.bind(this)
                        , null);
                }.bind(this)
                , null);
        } else {
            // сохранять не требуется. Сразу дернем колбэк
            if (callbackAfterSave) {
                callbackAfterSave(newIndex);
            }
        }
    },
    //=========================================================================

    /**
     * реакция на нажатие кнопки добавления варианта экипировки в этом слоте
     * @param index - индекс слота
     * @private
     */
    __createEquipVariant__: function (index) {
        const loadOldEquip = function () {
            const wId = this.currentWarriorId;
            this.currentWarriorId = -1;
            this.__openEquip__(wId, null);
        }.bind(this);

        this.__saveEquip__(index, function (newIndex) {
            // после сохранения
            this.form.prompt("Создание новой экипировки", "Введите название"
                , ""
                , null
                // после ввода названия
                , function (newIndex, form, equipName) {
                    // проверить на дубликат имени
                    const upperCaseNewName = equipName.toUpperCase();
                    if (this.warriorMetadata.equipments.find(equipment => equipment.name.toUpperCase() == upperCaseNewName)) {
                        this.form.showMessage("Добавление экипировки", "Экипировка с названием \"" + equipName + "\"уже существует.");
                    } else {
                        GAME.apiRestClient.removeWarrior(this.currentWarriorId
                            // после удаления воина с карты
                            , function (newIndex, equipName) {
                                this.currentWarriorId = null;
                                // создадим на карте нового воина
                                GAME.apiRestClient.createWarrior(this.warriorMetadata.baseClassName
                                    , this.warriorMetadata.title
                                    , 1, 1,
                                    // после успешного создания воина
                                    function (newIndex, equipName, result) {
                                        this.currentWarriorId = result.result;
                                        // сохраним пустую экипировку
                                        GAME.apiRestClient.saveWarriorAmmunition(this.currentWarriorId
                                            , equipName
                                            // после успешного сохранения экипировки
                                            , function (newIndex, equipName, result) {
                                                // зачитаем с сервера данные по этому воину
                                                GAME.apiRestClient.findPlayerHero(this.warriorMetadata.title
                                                    // после того, как успешно считана инфа по герою
                                                    , function (newIndex, equipName, result) {
                                                        const metadata = JSON.parse(result.result);
                                                        // найдем там нашу экипировку
                                                        const newEquipment = metadata.equipments.find(equipment => equipment.name == equipName);
                                                        this.warriorMetadata.equipments.push(newEquipment);
                                                        // обновим кнопки
                                                        this.__applyEquipButtonStatus__(newIndex);
                                                        if (index < this.equipVariantsCount - 1) {
                                                            this.__applyEquipButtonStatus__(newIndex + 1);
                                                        }
                                                        this.form.update(true);
                                                        // загрузим ее
                                                        this.__openEquip__(newIndex, null);
                                                    }.bind(this, newIndex, equipName)
                                                    // в случае сбоя загрузки параметров - закрыть форму так как уже
                                                    // сохранена новая экипировка и ее отсутствие на форме может привести
                                                    // к более плохим последствиям, чем просто закрытие формы
                                                    , function () {
                                                        this.__closeForm__();
                                                    }.bind(this));
                                            }.bind(this, newIndex, equipName)
                                            // в случае сбоя сохранения экипировки загрузим предыдущий вариант
                                            , loadOldEquip)
                                    }.bind(this, newIndex, equipName)
                                    // при ошибке добавления воина загрузим обратно предыдущий вариант
                                    , loadOldEquip)

                            }.bind(this, newIndex, equipName)
                            // в случае ошибки удаления воина загрузим обратно предыдущий вариант
                            , loadOldEquip);
                    }
                }.bind(this, newIndex)
            );
        }.bind(this));
    },
    //=========================================================================

    /**
     * Переименование экипировки
     * @param index
     * @private
     */
    __renameEquipment__: function (index) {
        this.form.prompt("Переименование экипировки", "Новое название"
            , this.warriorMetadata.equipments[index].name, null, function (form, newName) {
                if (newName != this.warriorMetadata.equipments[index].name)
                // переименовываем
                    GAME.apiRestClient.renameWarriorAmmunition(this.currentWarriorId
                        , this.warriorMetadata.equipments[index].name
                        , newName
                        , function (index, newName, result) {
                            // успешное переименование
                            this.warriorMetadata.equipments[index].name = newName;
                            this.equipVariantButtons[index].labelName.text = newName;
                            this.form.update(true);
                        }.bind(this, index, newName));
            }.bind(this));
    },
    //=========================================================================

    __removeEquipment__: function (index) {
        const msgTitle = this.warriorMetadata.equipments.length == 1
            ? "УВОЛЬНЕНИЕ воина из отряда"
            : "Удаление экипировки";

        const msgText = this.warriorMetadata.equipments.length == 1
            ? "Это последняя экипировка. ее удаление повлечет за собой увольнение воина из отряда. УВОЛИТЬ воина ?"
            : "Удалить экипировку \"" + this.warriorMetadata.equipments[index].name + "\"";

        this.form.showQuestion(msgTitle
            , msgText
            , null
            , function () {
                GAME.apiRestClient.removeWarriorAmmunition(this.currentWarriorId
                    , this.warriorMetadata.equipments[index].name
                    // если экипировка удалена и / или воин уволен
                    , function (result) {
                        this.warriorMetadata.equipments.splice(index, 1);
                        if (this.warriorMetadata.equipments.length > 0) {
                            // обновим кнопки
                            for (var ebIndex = 0; ebIndex < this.equipVariantsCount; ebIndex++) {
                                this.__applyEquipButtonStatus__(ebIndex);
                            }
                            this.form.update(true);

                            // это была удалена не последняя экипировка
                            if (this.currentEditingEquipmentIndex > index) {
                                // удалена экипировка ДО текущей. Сдвинем текущую
                                this.currentEditingEquipmentIndex--;
                            } else if (this.currentEditingEquipmentIndex == index) {
                                // удалена была текущая. Загузим самую первую
                                this.equipChanged = false;
                                this.__openEquip__(0, null);
                            }
                        } else {
                            // это была последняя экипировка. воина более нет у игрока. Можно закрыть форму, а слот с воином освободить
                            GAME.campaignCamp.__removeHero__(-1);
                           this.__closeForm__();
                        }
                    }.bind(this));
            }.bind(this));
    },
    //=========================================================================

    /**
     * Действие по нажатию кнопки выбора экипировки
     * @param index
     * @param callback
     * @private
     */
    __selectEquipVariant__: function (index, callback) {
        if (this.equipChanged) {
            this.form.showQuestion("Выбор новой экипировки", "Экипировка \""
                + this.warriorMetadata.equipments[index].name
                + "\" изменена. Сохранить?"
                , this.__openEquip__.bind(this, index)
                , this.__saveEquip__.bind(this, index, this.__openEquip__.bind(this)));
        } else {
            this.__openEquip__(index, callback);
        }
    },
    //=========================================================================

    /**
     * Создание кнопок вариантов экипировки
     * @private
     */
    __createEquipVariantButtons__: function () {
        const addEquipButtonWidth = 0.2;
        const addEquipButtonMargin = 0.3;
        const addEquipButtonsStartX = 1.65;
        const addEquipButtonsY = -0.2;

        const startLabelsX = 900;
        const labelWidth = 220;
        const labelMargin = 35;
        const labelY = 270;

        const equipButtonsStartX = 1.7;
        const equipButtonWidth = 0.4;
        const equipButtonMargin = 0.1;
        const equipButtonHeight = 0.17;
        const equipButtonY = -0.485;

        const removeEquipButtonWidth = 0.07;
        const removeEquipButtonY = -0.4;

        const alphaMaterial = new THREE.MeshBasicMaterial({color: 0x0000ff, transparent: true, opacity: 0});

        const geo = new THREE.PlaneBufferGeometry(0.13, 0.13, 1, 1);
        this.lightCircle = new THREE.Mesh(geo, GAME.modelsLibrary.materials.guiLightCircleMaterial);
        this.form.object3D.add(this.lightCircle);
        this.lightCircle.position.set(0, 0, -1);

        for (var index = 0; index < this.equipVariantsCount; index++) {
            // Кнопка добавления варианта экипировки
            const btn = this.form.createButton(addEquipButtonsStartX + (addEquipButtonWidth + addEquipButtonMargin) * index, addEquipButtonsY, addEquipButtonWidth
                , addEquipButtonWidth, GAME.modelsLibrary.materials.guiAddEquipVariantMaterial,
                this.__createEquipVariant__.bind(this, index));

            // название экипировки
            const labelConfigName = this.form.createText(startLabelsX + (labelWidth + labelMargin) * (index - 0.45)
                , labelY
                , labelWidth, 90, ""
                , 30, "#ffff00");
            labelConfigName.wordWrap = true;

            // прозрачная кнопка переименования поверх имени экипировки
            const buttonChangeEquipName = this.form.createButton(equipButtonsStartX + (equipButtonWidth + equipButtonMargin) * (index - 0.33)
                , equipButtonY
                , equipButtonWidth, equipButtonHeight, alphaMaterial,
                this.__renameEquipment__.bind(this, index));

            // кнопка удаления варианта экипировки
            const buttonRemoveEquipment = this.form.createButton(addEquipButtonsStartX + (addEquipButtonWidth + addEquipButtonMargin) * (index + 1) - addEquipButtonMargin
                , removeEquipButtonY
                , removeEquipButtonWidth, removeEquipButtonWidth
                , GAME.modelsLibrary.materials.guiCancelButtonMaterial
                , this.__removeEquipment__.bind(this, index));

            // кнопка выбора экипировки
            const buttonSelectEquip = this.form.createButton(addEquipButtonsStartX + (addEquipButtonWidth + addEquipButtonMargin) * index
                , addEquipButtonsY
                , addEquipButtonWidth, addEquipButtonWidth
                , GAME.modelsLibrary.materials.guiAnonymousIconLargeMaterial,
                this.__selectEquipVariant__.bind(this, index, null));

            // СОбираем все в единый объект
            this.equipVariantButtons.push({
                addEquipButton: btn,
                index: index,
                labelName: labelConfigName,
                buttonChangeEquipName: buttonChangeEquipName,
                buttonRemoveEquipment: buttonRemoveEquipment,
                buttonSelectEquip: buttonSelectEquip,
            });
        }
    },
    //=========================================================================

    /**
     * установить видимость элементов кнопки варианта экипировки в соответствие с ее статусом
     * @param index
     * @private
     */
    __applyEquipButtonStatus__: function (index) {
        const buttonEmpty = index >= this.warriorMetadata.equipments.length;
        const priorButtonEmpty = index - 1 >= this.warriorMetadata.equipments.length;
        this.equipVariantButtons[index].addEquipButton.visible = buttonEmpty && !priorButtonEmpty;
        this.equipVariantButtons[index].labelName.text = buttonEmpty ? "" : this.warriorMetadata.equipments[index].name;
        this.equipVariantButtons[index].labelName.visible = !buttonEmpty && !priorButtonEmpty;
        this.equipVariantButtons[index].buttonRemoveEquipment.visible = !buttonEmpty;
        this.equipVariantButtons[index].buttonSelectEquip.visible = !buttonEmpty;
        this.equipVariantButtons[index].buttonChangeEquipName.visible = !buttonEmpty;
    },
    //=========================================================================

    /**
     * Заполняет форму данными экипировки воина
     * @private
     */
    __fillForm__: function (continueFillCallback) {
        this.labelWarriorName.text = this.warriorMetadata.title;

        const classMetadata = GAME.gameMetadata.findWarriorBaseClassByName(this.warriorMetadata.baseClassName);

        this.equipVariantButtons.forEach(function (button) {
            // Смена иконки вида воина на кнопке выбора варианта экипировки
            button.buttonSelectEquip.material = classMetadata.graphicsInfo.largeIcon
                ? classMetadata.icons.largeIcon
                : GAME.modelsLibrary.materials.guiAnonymousIconLargeMaterial;
            // статус, зависящий от наличия сохраненной экипировки
            this.__applyEquipButtonStatus__(button.index);
        }.bind(this));

        // сразу грузим первый вариант экипировки
        this.__selectEquipVariant__(0, continueFillCallback);
    },
    //=========================================================================

    /**
     * Перенос всех предметов от воина на форму. С изменением масштаба
     * @private
     */
    __moveAllStuffsToForm__: function () {
        this.items.modelObjects.forEach(function (stuff) {
            stuff.itemId = null;
            if (stuff.ownerType === GAME.STUFF_OWNER.WARRIOR) {
                this.__moveStuffToForm__(stuff, true);
            }
        }.bind(this));
        this.__reArrangeItemsOnForm__();
    },
    //=========================================================================

    /**
     * закрывание этой формы
     * @private
     */
    __closeForm__: function () {
        // все предметы на форму
        this.__moveAllStuffsToForm__();

        // закроем
        this.form.hide();
        GAME.flowCamera.enabled = true;
        if (this.onCloseListener) {
            this.onCloseListener();
        }
    },
    //=========================================================================

    /**
     * Сохранения сделанных изменений в экипировках
     * @private
     */
    __saveEquipments__: function () {
        this.__saveEquip__(-1, this.__closeForm__.bind(this));
    },
    //=========================================================================

    /**
     * Нажатие на кнопку закрыть
     * @private
     */
    __createButtonClose__: function () {
        return this.form.createButton(0.9, -1.8, 0.15, 0.15
            , GAME.modelsLibrary.materials.guiCancelButtonMaterial
            , function () {
                if (this.equipChanged) {
                    this.form.showQuestion("Закрытие несохраненной экипировки"
                        , "Экипировка \"" + this.warriorMetadata.equipments[this.currentEditingEquipmentIndex].name + "\" изменена. Закрыть без сохранения?"
                        , null
                        , this.__closeForm__.bind(this));
                } else {
                    this.__closeForm__();
                }
            }.bind(this));
    },
    //==========================================================================

    /**
     * Нажатие на кнопку Сохранить
     * @private
     */
    __createButtonOk__: function () {
        return this.form.createButton(0.5, -1.8, 0.15, 0.15
            , GAME.modelsLibrary.materials.guiOkButtonMaterial
            , this.__saveEquipments__.bind(this));
    },
    //=================================================================================================================

    __createButtonEditName__: function () {
        return this.form.createButton(2.3, -0.04, 0.09, 0.09, GAME.modelsLibrary.materials.guiEditButtonMaterial
            , function () {
                this.form.prompt("Переименование героя", "Введите новое имя героя", this.warriorMetadata.title, null,
                    function (form, newName) {
                        // Проверить на повтор имени
                        GAME.apiRestClient.findPlayerHero(newName, function (newName, response) {
                            if (response.result === "") {
                                this.warriorMetadata.title = newName;
                                this.labelWarriorName.text = newName;
                                this.warriorMetadata.needRename = true;
                                this.form.update(true);
                            } else {
                                // уже есть герой с такимименем
                                this.form.showMessage("Переименование героя", "В вашем отряде уже есть герой с именем " + newName);
                            }
                        }.bind(this, newName));
                    }.bind(this));
            }.bind(this));
    },
    //=================================================================================================================

    /**
     * Заполняет форму элементами для редактирования вариантов экипировки воина
     * @private
     */
    __prepareForm__: function () {
        const geo = new THREE.PlaneBufferGeometry(0.62, 0.165, 1, 1);
        const mesh = new THREE.Mesh(geo, this.form.papirus.material);
        this.form.object3D.add(mesh);
        mesh.name = "addForm";

        mesh.position.set(0.187, 0.1675, -1);

        this.form.createText(350, 40, 180, 40, "СКЛАД", 45, "#ffffff");

        this.form.createText(800, 60, 180, 40, "Воин", 45, "#ffffff");
        this.labelWarriorName = this.form.createText(905, 60, 400, 70, "Имя воина", 45, "#ffff00");

        this.__createButtonEditName__();
        this.__createButtonClose__();
        this.__createButtonOk__();

        this.__prepareWeaponsGrid__();
    },
    //=========================================================================

    /**
     * Содает копию объекта предмета для использования. Так же создает невидимый слой
     * @param metadata
     * @private
     */
    __createStuffCopyFromMetadata__: function (metadata, scaleFactor, z) {
        const obj = new THREE.Group();
        const stuff = metadata.models.baseView.clone();

        // копия материалов
        stuff.traverse(function (child) {
            if (child.material) {
                if (child.material.constructor === Array) {
                    const newMat = [];
                    child.material.forEach(child1 => {
                        newMat.push(child1.clone());
                    });
                    child.material = newMat;
                } else
                    child.material = child.material.clone();

            }
        });

        stuff.scale.set(scaleFactor, scaleFactor, scaleFactor);
        stuff.position.set(metadata.graphicsInfo.baseViewModel.center.x * scaleFactor
            , metadata.graphicsInfo.baseViewModel.center.y * scaleFactor
            , z);
        obj.add(stuff);

        const objSize = new THREE.Box3().setFromObject(obj).getSize();
        const layerGeo = new THREE.PlaneBufferGeometry(objSize.x, objSize.y, 1, 1);
        const layer = new THREE.Mesh(layerGeo, this.itemTransparentLayerMaterial);
        layer.name = "layer";
        layer.objSize = {
            x: objSize.x,
            y: objSize.y
        };
        obj.add(layer);
        layer.position.set(0, 0, z + 0.001);

        obj.rotation.set(-0.2, 0.4, 0.2);
        return obj;

    },
    //=========================================================================

    __reArrangeItemsOfWarrior__: function () {
        var nextWeaponX = this.warriorsWeaponsMiddleCoords.x;
        var weaponsHeight = 0;
        var weapons = [];

        this.items.modelObjects.forEach(function (item) {
            if (item.ownerType === GAME.STUFF_OWNER.WARRIOR) {
                // item.object3D.scale.set(1, 1, 1);
                // предмет принадлежит воину
                switch (item.stuffType) {
                    case GAME.STUFF_TYPE.WEAPPON:
                        // это оружие
                        const objSize = item.object3D.children.find(child => {
                            return child.name === "layer"
                        }).objSize;
                        const normWidth = objSize.x / this.formStuffScaleFactor;
                        const normHeight = objSize.y / this.formStuffScaleFactor;

                        // позиции для предмета. Прикидочные
                        item.object3D.position.x = nextWeaponX + normWidth / 2;

                        nextWeaponX += normWidth + this.warriorsWeaponsHorizontalInterval;
                        if (weaponsHeight < normHeight) {
                            weaponsHeight = normHeight;
                        }
                        weapons.push(item);
                        break;
                }
            }
        }.bind(this));

        // теперь скорректируем координаты
        const middleX = nextWeaponX / 2;
        const middleY = weaponsHeight / 2;
        weapons.forEach(function (weapon) {
            weapon.object3D.position.set(weapon.object3D.position.x - middleX
                , this.warriorsWeaponsMiddleCoords.y - middleY
                , this.warriorsWeaponsMiddleCoords.z);
        }.bind(this));
    },
    //=========================================================================

    /**
     * вычисляет координаты для предметов на форме
     * @param fullReorder
     * @private
     */
    __reArrangeItemsOnForm__: function () {
        var nextWeaponXpos = this.weaponPlaceCoords.x;
        var nextWeaponYpos = this.weaponPlaceCoords.y;
        var maxWeaponXpos = nextWeaponXpos + this.weaponPlaceWidth;
        var currentWeaponLineHeight = 0;
        var weaponsLineObjects = [];

        var nextArtifactXPos = this.artifactsPlaceCoords.x;
        var nextArtifactYpos = this.artifactsPlaceCoords.y;
        var artifactLineObjects = [];


        this.items.modelObjects.forEach(function (item) {
            if (item.ownerType === GAME.STUFF_OWNER.BAG) {
                // этот предмет в багаже. На форму его
                var obj = item.object3D;
                // получим размер объекта
                var objLayer = obj.children.find(subObj => {
                    return subObj.name === "layer"
                });
                var objSize = objLayer.objSize;

                switch (item.stuffType) {
                    case GAME.STUFF_TYPE.WEAPPON:
                        // проверим не вышли ли мы за предельг=ную ширину
                        if (nextWeaponXpos + objSize.x > maxWeaponXpos) {
                            //  превышено. Перейдем на следующую строку
                            // для начала поместим на оодну линию все предметы
                            var middleY = nextWeaponYpos - currentWeaponLineHeight / 2;
                            weaponsLineObjects.forEach(function (stuff) {
                                stuff.position.y = middleY;
                            }.bind(this));

                            // новая строка
                            nextWeaponYpos -= (currentWeaponLineHeight + this.weaponsOnFormVerticalInterval);
                            currentWeaponLineHeight = 0;
                            nextWeaponXpos = this.weaponPlaceCoords.x;
                            weaponsLineObjects = [];
                        }

                        // расположим предмет
                        obj.position.x = nextWeaponXpos + objSize.x / 2;
                        nextWeaponXpos += objSize.x + this.weaponsOnFormHorizontalInterval;
                        if (currentWeaponLineHeight < objSize.y) {
                            currentWeaponLineHeight = objSize.y;
                        }
                        weaponsLineObjects.push(obj);
                        break;
                }


            }
        }.bind(this));

        if (weaponsLineObjects.length > 0) {
            var middleY = nextWeaponYpos - currentWeaponLineHeight / 2;
            weaponsLineObjects.forEach(function (stuff) {
                stuff.position.y = middleY;
            }.bind(this));
        }
    },
    //=========================================================================

    /**
     * Размещает сетку из оружия
     * @private
     */
    __prepareWeaponsGrid__: function () {
        GAME.gameMetadata.weaponClasses.forEach(function (weaponClass) {
            if (weaponClass.models.baseView) {
                for (var copies = 0; copies < weaponClass.graphicsInfo.countOnForm; copies++) {
                    var obj = this.__createStuffCopyFromMetadata__(weaponClass, this.formStuffScaleFactor, 0);
                    this.form.object3D.add(obj);
                    obj.position.set(0, 0, -0.99);
                    this.items.addModelObject(obj, weaponClass, GAME.STUFF_TYPE.WEAPPON);

                    // var line = new THREE.CylinderBufferGeometry(0.001, 0.001, 3, 3, 1);
                    // var mat = new THREE.MeshBasicMaterial({color: 0x000000});
                    // var lineMesh = new THREE.Mesh(line, mat);
                    // lineMesh.rotation.z = Math.PI / 2;
                    // lineMesh.position.set(-0.25, startYPosition, -0.99);
                    // this.form.object3D.add(lineMesh);
                }

            }
        }.bind(this));

        this.__reArrangeItemsOnForm__();
    },
    //=========================================================================

    /**
     * Проверка массива объектов на предмет нахождения одного из них под указателем мыши
     * @param itemsToCheck
     * @private
     */
    __checkArrayForSelections__: function (itemsToCheck, mousePosition) {
        this.rayCaster.setFromCamera(mousePosition, GAME.flowCamera.camera);

        for (var index = 0; index < itemsToCheck.length; index++) {
            var objects = [itemsToCheck[index].object3D];
            var intersects = this.rayCaster.intersectObjects(objects, true);
            if (intersects.length > 0) {
                return index;
            }
        }
        return -1;
    },
    //=========================================================================

    /**
     * Находит предметы, под мышью
     * @private
     */
    __detectSelections__: function (mousePosition) {
        if (this.form.visible) {
            var selectedItemIndex = this.__checkArrayForSelections__(this.items.modelObjects, mousePosition);
            this.items.setSelectedIndex(selectedItemIndex);
        }
    },
    //=========================================================================

    /**
     * Преобразует координату мыши в трехмерную координату мира
     * @param z -- координата глубина для которой подбераются X м Y
     */
    __getXYZFormMouseCoordinates__:
        function (mousePosition, z) {
            this.tempVector3.set(
                mousePosition.x,
                mousePosition.y,
                z);
            this.tempVector3.unproject(GAME.flowCamera.camera);
            this.tempVector3.sub(GAME.flowCamera.camera.position).normalize();

            this.tempVector3.y -= Math.sin(GAME.flowCamera.camera.rotation.x);
            this.tempVector3.x -= Math.sin(GAME.flowCamera.camera.rotation.y);
            this.tempVector3.multiplyScalar(z / this.tempVector3.z);
            return this.tempVector3;
        }

    ,
//============================================================================

    __mouseMoveEventListener__: function (mousePosition) {
        if (this.takenStuffIndex >= 0) {
            // Есть захваченный предмет. Надо его двигать
            var z = this.items.modelObjects[this.takenStuffIndex].object3D.position.z;
            var newCoordinates = this.__getXYZFormMouseCoordinates__(mousePosition, z);
            this.items.modelObjects[this.takenStuffIndex].object3D.position.set(newCoordinates.x, newCoordinates.y, z);
            return true;
        } else {
            this.__detectSelections__(mousePosition);
        }
    },
    //=========================================================================

    __mouseDownEventListener__: function (event) {
        if (event.buttons === 1) {
            // нажали левую кнопку мыши. Без правой и прочей херни
            // Определим какой предмет взят и взят ли
            if (this.items.selectedIndex >= 0) {
                // есть предмет в этой группе
                this.takenStuffIndex = this.items.selectedIndex;
                this.savedWeaponCoords.copy(this.items.selectedModelObject.object3D.position);

                // если объект принадлежит воину, то перенести его на форму
                if (this.items.selectedModelObject.ownerType === GAME.STUFF_OWNER.WARRIOR) {
                    this.__moveStuffToForm__(this.items.selectedModelObject, true);
                    this.__reArrangeItemsOfWarrior__();
                    // передвинем предмет под мышь
                    this.__mouseMoveEventListener__(GAME.flowCamera.mousePosition);
                }
                return true; // ппрервать цепь обработки нажатия кнопки мыши
            }
        }
    },
    //=========================================================================


    __mouseUpEventListener__: function (event) {
        if (event.buttons === 0 && this.takenStuffIndex >= 0) {
            // отпустили левую кнопку мыши. и был ранее захвачен предмет
            this.rayCaster.setFromCamera(GAME.flowCamera.mousePosition, GAME.flowCamera.camera);
            var droppedOnWarrior = this.rayCaster.intersectObjects([GAME.campaignCamp.presentPano], true).length > 0;

            const droppedStufIndex = this.takenStuffIndex;

            const giveWeaponToWarrior = function () {
                this.__moveStuffToWarrior__(this.items.modelObjects[droppedStufIndex]);
                this.__reArrangeItemsOfWarrior__();
                this.__reArrangeItemsOnForm__();
            }.bind(this);

            const justRearrangeItems = function () {
                this.__reArrangeItemsOfWarrior__();
                this.__reArrangeItemsOnForm__();
            }.bind(this);

            if (droppedOnWarrior) {
                // отпустили на герое
                // проверим был ли предмет взят у героя или взт с формы. Если взят с формы, то выполним запрос на выдачу герою предмета
                if (!this.items.modelObjects[droppedStufIndex].itemId) {
                    // был взят с формы
                    GAME.apiRestClient.giveWeaponToWarrior(this.currentWarriorId
                        , this.items.modelObjects[droppedStufIndex].metadata.title
                        , function (result) {
                            this.items.modelObjects[droppedStufIndex].itemId = result.result;
                            giveWeaponToWarrior();
                        }.bind(this)
                        , justRearrangeItems);
                } else {
                    // и так был у героя
                    giveWeaponToWarrior();
                }
            } else {
                // бросили предмет не на герое. перестроим предметы формы
                // проверим был ли предмет у героя. Если был, то заберем предмет и зачистим в нем код
                if (this.items.modelObjects[droppedStufIndex].itemId) {
                    // предмет был у воина
                    GAME.apiRestClient.dropWeaponByWarrior(this.currentWarriorId
                        , this.items.modelObjects[droppedStufIndex].itemId
                        , function (result) {
                            this.items.modelObjects[droppedStufIndex].itemId = null;
                            this.__reArrangeItemsOnForm__();
                        }.bind(this)
                        , giveWeaponToWarrior);
                } else {
                    // предмет был с формы
                    this.__reArrangeItemsOnForm__();
                }
            }
            this.equipChanged = true;
            this.takenStuffIndex = -1;
        }
    },
    //=========================================================================

    __moveStuffToWarrior__: function (stuff) {
        stuff.ownerType = GAME.STUFF_OWNER.WARRIOR;

        // восстановить масштаб
        const newScale = 1 / this.formStuffScaleFactor;
        stuff.object3D.scale.set(newScale, newScale, newScale);

        // перенести с формы к воину
        this.form.object3D.remove(stuff.object3D);
        GAME.campaignCamp.object3D.add(stuff.object3D);
    },
    //=========================================================================

    __moveStuffToForm__: function (stuff, rescale) {
        stuff.ownerType = GAME.STUFF_OWNER.BAG;

        // восстановить масштаб
        if (rescale) {
            const newScale = 1; //this.formStuffScaleFactor;
            stuff.object3D.scale.set(newScale, newScale, newScale);
        }

        // перенести с воина на форму
        GAME.campaignCamp.object3D.remove(stuff.object3D);
        this.form.object3D.add(stuff.object3D);
        stuff.object3D.position.z = -0.99;

    },
    //=========================================================================

    get visible() {
        return this.form.visible;
    }
    ,
//=========================================================================

    set closeListener(value) {
        this.onCloseListener = value;
    }
//=========================================================================

}
;