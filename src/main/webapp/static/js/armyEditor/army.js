GAME.Army = function () {

    this.apiUrl = "game/api/";

    this.gameMetadata;

    this.armyEditor = new GAME.ArmyEditor(16, false);
    this.armyEditor.scene_setup();

    /**
     * UUID контекста
     * @type {String}
     */
    this.gameContextUUID = undefined;

    // объект загрузки метаданных игры
    this.gameMetadata = new GAME.GameMetadata(this.showError, this.apiUrl, true);

    this.apiRestClient = new GAME.ApiRestClient(this.gameContextUUID, new GAME.HtmlLockScreenCursor(), function (message, callback) {
        alert("Ошибка обращения к серверу " + message);
        if (callback) {
            callback();
        }
    }.bind(this));

    this.__tryToFindOldContext__();
};

GAME.Army.prototype = {
    constructor: GAME.Army,

    /**
     * Показать сообщение об ошибке
     * @param errorMessage
     */
    showError: function (errorMessage) {
        alert(errorMessage);
    },


    __tryToFindOldContext__: function () {
        GAME.apiRestClient.getExistingGameContext(function (result) {
            var existingContext = null;
            if (result.result != "" && result.result != "null") {
                existingContext = JSON.parse(result.result);
                if (!existingContext.technical) {
                    // проверим, что этот контекст не имеет других игроков
                    if (existingContext.players.length > 1) {
                        // в игре есть еще игроки. Нельзя уходить из такой игры
                        alert("Негоже покидать свой отряд на арене, оставляя его противнику");
                        window.location = "tavern";
                        return;
                    }
                }
            }
            GAME.apiRestClient.createTechnicalContext(this.__receiveCreateContextResult__.bind(this));
        }.bind(this));

    },
    //=============================================================================


    /**
     * реакция на получение ответа на запрос создания контекста
     * @param response
     */
    __receiveCreateContextResult__: function (response) {
        if (response.failed) {
            alert(response.errorMessage);
        } else {
            this.gameContextUUID = response.result;
            GAME.apiRestClient.gameContextUUID = response.result;
            this.armyEditor.loadGauge.incProgress();
            // и загрузим метеданные
            this.gameMetadata.gameContextUUID = this.gameContextUUID;
            this.gameMetadata.loadAll(this.__metadataLoadedCallback__.bind(this), this.armyEditor.loadGauge);
        }
    },

    __metadataLoadedCallback__: function () {
        this.armyEditor.main(this.gameMetadata, this.gameContextUUID);
    },

};

var armyConfigurator;

function main() {
    armyConfigurator = new GAME.Army();
}
