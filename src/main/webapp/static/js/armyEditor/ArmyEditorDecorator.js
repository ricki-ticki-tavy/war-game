GAME.ArmyEditorDecorator = function (scene, modelsLibrary, flowCamera) {
    this.scene = scene;
    this.modelsLibrary = modelsLibrary;
    this.flowCamera = flowCamera;

    this.mercenaries = []; // наемники у беседки. Базовые классы

    this.mercenaryArborObject = undefined;

    this.moonLight = undefined;
    this.ambientLight = undefined;
    this.lightBugsHolder = undefined;
};

GAME.ArmyEditorDecorator.prototype = {
    constructor: GAME.ArmyEditorDecorator,

    /**
     * наполнить мир редактора объектами
     */
    decorate: function () {
        this.__setSky__();
        this.__setGround__();
        this.__setTrees__();
        this.__setLight__();
        this.__setArbor__();
        this.__setMercenaryLargeArbor__();
        this.__setWell_1__();
        this.__createLightBugHolder__();

        // this.__setStaticSnow__();
    },

    animate: function () {
        return;
        if (Math.random() <= 0.01) {
            this.moonLight.intensity = 6;
            this.modelsLibrary.materials.skyMaterial.color.r = 1;
            this.modelsLibrary.materials.skyMaterial.color.g = 1;
            this.modelsLibrary.materials.skyMaterial.color.b = 1;
            // this.ambientLight.color = 0x4488ff;
        } else if (this.moonLight.intensity == 6) {
            this.moonLight.intensity = 0.7;
            this.modelsLibrary.materials.skyMaterial.color.r = 66 / 256;
            this.modelsLibrary.materials.skyMaterial.color.g = 66 / 256;
            this.modelsLibrary.materials.skyMaterial.color.b = 66 / 256;
            // this.ambientLight.color = 0xffffff;

        }
    },

    __setArbor__: function () {
        var arbor = this.modelsLibrary.arbor.clone();
        arbor.scale.set(2.0, 1.6, 2.0);
        arbor.position.set(14, 0.1, -7);
        arbor.rotation.set(0, -Math.PI / 8, 0);
        this.scene.add(arbor);
    },

    __setMercenaryLargeArbor__: function () {
        var arbor = this.modelsLibrary.largeArbor.clone();
        arbor.position.set(-12, 1.7, 2.5);
        arbor.rotation.y = Math.PI / 8;
        this.scene.add(arbor);
        this.mercenaryArborObject = arbor;
    },

    __setWell_1__: function () {
        var well = this.modelsLibrary.well_1.clone();
        well.scale.set(0.02, 0.02, 0.02);
        well.position.set(-2, 0, 1);
        well.rotation.x = -Math.PI / 2;
        well.rotation.z = Math.PI / 3;
        this.scene.add(well);
    },

    __setSky__: function () {
        var skyGeo = new THREE.SphereBufferGeometry(900, 72, 36, 5 * Math.PI / 4, Math.PI / 2, 0, Math.PI / 3);
        var sky = new THREE.Mesh(skyGeo, this.modelsLibrary.materials.skyMaterial);
        // sky.position.set(0, 3, 0.5);
        sky.position.set(0, -500, 0.5);
        // sky.rotation.x = - Math.PI / 30;
        this.scene.add(sky);
    },

    __setGround__: function () {
        var groundGeo = new THREE.CircleBufferGeometry(80, 120);// PlaneBufferGeometry(150, 50, 6, 6);
        var ground = new THREE.Mesh(groundGeo, this.modelsLibrary.materials.groundMaterial);
        ground.receiveShadow = true;
        ground.position.y = 0;
        ground.rotation.x = -Math.PI / 2;
        this.scene.add(ground);
        this.ground = ground;
    },

    __setTrees__: function () {
        var object = this.modelsLibrary.trees[0].clone();
        object.position.set(-18.5, 0, 5);
        this.scene.add(object);


        object = this.modelsLibrary.trees[2].clone();
        object.position.set(-17, 0, 8);
        this.scene.add(object);

        object = this.modelsLibrary.trees[2].clone();
        object.position.set(-15, 0, 9);
        this.scene.add(object);

        object = this.modelsLibrary.trees[2].clone();
        object.position.set(-7, 0, 6.5);
        this.scene.add(object);

        object = this.modelsLibrary.trees[2].clone();
        object.position.set(-7, 0, -9);
        this.scene.add(object);

        object = this.modelsLibrary.trees[2].clone();
        object.position.set(18.5, 0, -18.5);
        this.scene.add(object);

        object = this.modelsLibrary.trees[2].clone();
        object.position.set(17, 0, -19);
        this.scene.add(object);

        object = this.modelsLibrary.trees[2].clone();
        object.position.set(7, 0, -5);
        this.scene.add(object);


        object = this.modelsLibrary.trees[1].clone();
        object.position.set(-23, 0, -10);
        this.scene.add(object);

        object = this.modelsLibrary.trees[1].clone();
        object.position.set(-7, 0, -3);
        object.rotation.y = 2 * Math.PI / 3;
        this.scene.add(object);

        object = this.modelsLibrary.trees[1].clone();
        object.position.set(18, 0, -20);
        this.scene.add(object);


        object = this.modelsLibrary.trees[3].clone();
        object.position.set(0, 0, -2);
        this.scene.add(object);

        object = this.modelsLibrary.trees[3].clone();
        object.position.set(2, 0, -13);
        this.scene.add(object);

        object = this.modelsLibrary.trees[3].clone();
        object.position.set(-13, 0, -13);
        this.scene.add(object);

        // object = this.modelsLibrary.trees[5].clone();
        // object.position.set(4, 6, 0);
        // this.scene.add(object);

        // object = this.modelsLibrary.trees[6].clone();
        // object.position.set(4, 0, 0);
        // this.scene.add(object);

    },

    __setLight__: function setLight() {
        this.moonLight = new THREE.DirectionalLight(0x7777ff, 0.5);
        this.moonLight.position.set(40, 30, -10);
        this.moonLight.shadow.mapSize.width = 1024;
        this.moonLight.shadow.mapSize.height = 1024;
        this.moonLight.shadow.camera.left = -30;
        this.moonLight.shadow.camera.right = 30;
        this.moonLight.shadow.camera.top = -30;
        this.moonLight.shadow.camera.bottom = 30;
        this.moonLight.shadow.bias = -0.005;
        this.moonLight.shadow.camera.near = 1;
        this.moonLight.shadow.camera.far = 200;
        this.moonLight.castShadow = true;
        this.moonLight.angle = Math.PI / 2;

        this.scene.add(this.moonLight);

        this.sunLight = new THREE.DirectionalLight(0xffffcc, 2.6);
        this.sunLight.position.set(40, 30, -10);
        this.sunLight.shadow.mapSize.width = 2048;
        this.sunLight.shadow.mapSize.height = 2048;
        this.sunLight.shadow.camera.near = 1;
        this.sunLight.shadow.camera.far = 200;
        this.sunLight.shadow.camera.left = -30;
        this.sunLight.shadow.camera.right = 30;
        this.sunLight.shadow.camera.top = -30;
        this.sunLight.shadow.camera.bottom = 30;
        this.sunLight.castShadow = true;
        this.sunLight.shadow.bias = -0.005;
        this.sunLight.angle = Math.PI / 2;

        // this.scene.add(this.sunLight);

        this.ambientLight = new THREE.AmbientLight(0xffffff, 0.25);
        // this.ambientLight = new THREE.AmbientLight(0xffffff, 1.9);
        this.scene.add(this.ambientLight);

    },
//======================================================================================

    __createLightBugHolder__: function () {
        this.lightBugsHolder = new GAME.LightBugsHolder(this.scene, new THREE.Vector2(-28, -20), new THREE.Vector2(22, 20), 6);
    },
//======================================================================================

    __setStaticSnow__: function () {
        var starsGeometry = new THREE.Geometry();

        for (var i = 0; i < 100000; i++) {

            var star = new THREE.Vector3();
            star.x = THREE.Math.randFloatSpread(40);
            star.y = 5 + THREE.Math.randFloatSpread(10);
            star.z = THREE.Math.randFloatSpread(40);

            starsGeometry.vertices.push(star);

        }

        var starsMaterial = new THREE.PointsMaterial({color: 0x888888, size: 0.05});

        var starField = new THREE.Points(starsGeometry, starsMaterial);

        this.scene.add(starField);

    }
//======================================================================================

};