GAME.LightBugsHolder = function (scene, sceneTopLeft, sceneBottomRight, amount) {
    this.scene = scene;
    this.amount = amount;
    this.sceneTopLeft = sceneTopLeft;
    this.sceneBottomRight = sceneBottomRight;

    this.bugSpeed = 0.02;

    this.lightBugs = [];

    this.changeDestenationProbability = 0.01;

    this.lastFlyTime = undefined;
    this.avgSMAFlyInterval = 0;
    this.SMAForFlyInterval = 0.02;

    this.__createBugs__(amount);

};

GAME.LightBugsHolder.prototype = {
    constructor: GAME.LightBugsHolder,

    moveLightBugsCycle: function(){
        if (this.lastFlyTime === undefined){
            this.lastFlyTime = Date.now();
            return;
        }

        var now = Date.now();
        this.avgSMAFlyInterval += (now - this.lastFlyTime - this.avgSMAFlyInterval) * this.SMAForFlyInterval;
        this.lastFlyTime = now;

        this.lightBugs.forEach(function(lightBug){
           this.__moveLightBug__(lightBug);
        }.bind(this));
    },
    //==========================================================================

    __createLightBug__: function (color) {
        var light = new THREE.PointLight(color, 0.6, 4.5, 0.5);
        light.castShadow = false;

        var bugGeo = new THREE.SphereBufferGeometry(0.01, 6, 6);
        var bugMat = new THREE.MeshLambertMaterial({emissive: color, emissiveIntensity: 2});
        var lightMesh = new THREE.Mesh(bugGeo, bugMat);
        lightMesh.receiveShadow = false;
        light.add(lightMesh);
        light.position.set(0, 0.5, -1.7);
        var lightBug = {
            light: light,
            destination: {
                x: 0,
                y: 0,
                z: 0
            },
            speed: {
                x: 0,
                y: 0,
                z: 0
            },
            stepCount: 0,
        };
        this.__generateNewDestination__(lightBug);
        lightBug.light.position.set(lightBug.destination.x, lightBug.destination.y, lightBug.destination.z);
        this.__generateNewDestination__(lightBug);
        return lightBug;
    },
    //==========================================================================

    __createLightBugNoLight__: function (color) {

        var bugGeo = new THREE.SphereBufferGeometry(0.01, 6, 6);
        var bugMat = new THREE.MeshLambertMaterial({emissive: color, emissiveIntensity: 2});
        var lightMesh = new THREE.Mesh(bugGeo, bugMat);
        lightMesh.receiveShadow = false;
        lightMesh.position.set(0, 0.5, -1.7);
        var lightBug = {
            light: lightMesh,
            destination: {
                x: 0,
                y: 0,
                z: 0
            },
            speed: {
                x: 0,
                y: 0,
                z: 0
            },
            stepCount: 0,
        };
        this.__generateNewDestination__(lightBug);
        lightBug.light.position.set(lightBug.destination.x, lightBug.destination.y, lightBug.destination.z);
        this.__generateNewDestination__(lightBug);
        return lightBug;
    },
    //==========================================================================


    __createBugs__: function (amount) {
        for (var index = 0; index < amount; index++) {
            var lightBug = this.__createLightBug__(0xff8833);
            this.lightBugs.push(lightBug);
            this.scene.add(lightBug.light);
        }
    },
    //==========================================================================

    /**
     * генерация новой координаты и скоростей
     * @param lightBug
     * @private
     */
    __generateNewDestination__: function(lightBug){
        lightBug.destination.x = (this.sceneBottomRight.x
            - this.sceneTopLeft.x) * Math.random() + this.sceneTopLeft.x;

        lightBug.destination.z = (this.sceneBottomRight.y
            - this.sceneTopLeft.y) * Math.random() + this.sceneTopLeft.y;

        lightBug.destination.y = 4 * Math.random() + 0.3;

        // расчет скоростей
        var deltaX = lightBug.destination.x - lightBug.light.position.x;
        var deltaY = lightBug.destination.y - lightBug.light.position.y;
        var deltaZ = lightBug.destination.z - lightBug.light.position.z;

        var steps = Math.max(Math.abs(deltaX), Math.abs(deltaY), Math.abs(deltaZ)) / this.bugSpeed * (0.6 + Math.random() * 0.8);

        lightBug.speed.x = deltaX / steps;
        lightBug.speed.y = deltaY / steps;
        lightBug.speed.z = deltaZ / steps;

        lightBug.stepCount = steps;
    },
    //==========================================================================

    __moveLightBug__: function(lightBug){
        // посмотрим не надо ли сменить направление
        if (lightBug.stepCount <= 0
            || Math.random() < this.changeDestenationProbability){
            this.__generateNewDestination__(lightBug);
        }
        lightBug.light.position.x += lightBug.speed.x * this.avgSMAFlyInterval / 17.0;
        lightBug.light.position.y += lightBug.speed.y * this.avgSMAFlyInterval / 17.0;
        lightBug.light.position.z += lightBug.speed.z * this.avgSMAFlyInterval / 17.0;
        lightBug.stepCount--;

    },
    //==========================================================================

    //==========================================================================

};