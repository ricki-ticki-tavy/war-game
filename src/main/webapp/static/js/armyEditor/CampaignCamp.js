/**
 * Класс лагеря отряда
 * @param scene
 * @constructor
 */
GAME.CampaignCamp = function () {
    GAME.campaignCamp = this;

    this.rayCaster = GAME.flowCamera.rayCaster;

    this.object3D = new THREE.Group();

    this.firePane = null;
    this.fireLight = null;
    this.fire = this.__createFire__();

    /**
     * отмеченный в предыдущую проверку герой
     * @type {number}
     */
    this.currentHoveredHeroIndex = -1;
    this.detectSelectionEnabled = true;

    /**
     * Слоты расположения нанятых в отряд воинов вокруг костра
     * @type {Array}
     */
    var center = new THREE.Vector3(0, 0, 0);
    this.warriorSlots = [];
    this.__generateWarriorSlots__(center, 3, -5 * Math.PI / 8, 5 * Math.PI / 8, 6);
    this.__generateWarriorSlots__(center, 4.7, -9 * Math.PI / 16, 9 * Math.PI / 16, 11);
    this.__generateWarriorSlots__(center, 6.4, -9 * Math.PI / 16, 9 * Math.PI / 16, 17);

    // заполним первично загруженные данные
    this.__initialFillWarriorsSlots__();

    this.highLightSpotIntencity = 9;
    this.highLightSpot = this.__createLight__();

    this.presentPano = this.__createPresentPano__();


    GAME.flowCamera.mouseMoveEventListeners.push(this.__detectSelections__.bind(this));
    GAME.flowCamera.mouseDownEventListeners.push(this.__mouseDownEventListener__.bind(this));

    this.savedCamDestination = new THREE.Vector3();
    this.equipWarriorForm = new GAME.EquipWarriorForm();
    this.equipWarriorForm.closeListener = function () {
        this.highLightSpot.intensity = 0.0001;
        this.highLightSpot.distance = 3;
        this.highLightSpot.angle = Math.PI / 6;
        this.backPresentedWarrior();
        this.enableDetectSelection();
        GAME.flowCamera.destination.copy(this.savedCamDestination);

    }.bind(this);
};

GAME.CampaignCamp.prototype = {
    constructor: GAME.CampainCamp,

    /**
     * Создает мерцание огня, смещение теней от огня и пр. Вызывается перед рендерингом сцены
     */
    animate: function () {
        this.firePane.speed = 650 * GAME.flowCamera.avgRateCoef;

        this.fireLight.animate();

        this.__animateSlots__();
    },
    //========================================================================================

    /**
     * Найти свободный слот для размещения нового героя
     */
    findFreeWarriorSlot: function () {
        var index = 0;
        while (index < this.warriorSlots.length) {
            if (!this.warriorSlots[index].isBusy) {
                return index;
            }
            index++;
        }
        return null;
    },
    //====================================================================================

    /**
     * Зарезервировать слот у костра отряда для воина. Воин создастся прозрачным
     */
    reserveWarriorSlot: function (heroMetadata) {
        var index = this.findFreeWarriorSlot();
        if (index) {
            this.warriorSlots[index].reserve(heroMetadata);
        }
        return this.warriorSlots[index];
    },
    //====================================================================================

    /**
     * Вернуть вызванного в данный момент для представления воина назад к месту у костра
     */
    backPresentedWarrior: function () {
        if (this.currentHoveredHeroIndex >= 0) {
            this.warriorSlots[this.currentHoveredHeroIndex].backToPlace();
        }
    },
    //====================================================================================

    /**
     * Освобождает слот от героя
     * @param index
     * @private
     */
    __removeHero__: function(index){
        if (index < 0){
            index = this.currentHoveredHeroIndex;
        }
        this.warriorSlots[index].warriorMetadata = null;
        if (this.currentHoveredHeroIndex == index){
            this.currentHoveredHeroIndex = -1;
        }
    },
    //====================================================================================

    __createPresentPano__: function(){
      var mat = new THREE.MeshPhongMaterial({color: 0, transparent: true, opacity: 0});
      var geo = new THREE.PlaneBufferGeometry(6.4, 3, 1, 1);
      var mesh = new THREE.Mesh(geo, mat);
      mesh.position.set(-0.6, 1.5, 2);
      this.object3D.add(mesh);
      return mesh;
    },
    //====================================================================================



    __mouseDownEventListener__: function () {
        if (!this.equipWarriorForm.visible && this.currentHoveredHeroIndex >= 0) {
            // this.equipWarriorForm.show(this.warriorSlots[this.currentHoveredHeroIndex].warriorMetadata);

            this.warriorSlots[this.currentHoveredHeroIndex].presentToPlayer(function () {
                this.equipWarriorForm.show(this.warriorSlots[this.currentHoveredHeroIndex].warriorMetadata);
                this.savedCamDestination.copy(GAME.flowCamera.destination);

                // GAME.flowCamera.destination.set(10.5, 0.5, -3);
                GAME.flowCamera.destination.set(7.25, 1, 6);

                this.__highLightHero__(this.currentHoveredHeroIndex, false);
                var hero = this.warriorSlots[this.currentHoveredHeroIndex];
                this.highLightSpot.position.set(hero.object3D.position.x
                    , hero.object3D.position.y + 0.5
                    , hero.object3D.position.z + 3.3);
                this.highLightSpot.target = this.presentPano;
                this.highLightSpot.intensity = this.highLightSpotIntencity;
                this.highLightSpot.distance = 5.5;
                this.highLightSpot.angle = 0.7;
            }.bind(this));

            this.__highLightHero__(this.currentHoveredHeroIndex, false);
            GAME.flowCamera.enabled = false;
            this.disableDetectSelection();
            // GAME.flowCamera.destination.set(7.25, 1, 6);
            return true;
        } else {
            return false;
        }
    },
    //====================================================================================

    __animateSlots__: function () {
        this.warriorSlots.forEach(function (warriorSlot) {
            warriorSlot.animate();
        }.bind(this));
    },
    //====================================================================================

    __creteFlameObject__: function(){

    },

    __createFire__: function () {
        var firePlace = new THREE.Group();
        var plane = new THREE.PlaneBufferGeometry(3, 5);
        var fire = new THREE.Fire(plane, {
            textureWidth: 512,
            textureHeight: 1024,
            debug: false
        });
        this.firePane = fire;

        fire.addSource(0.55, 0.12, 0.1, 0.5, 0.0, 1.0);
        fire.drag = 0;
        fire.swirl = 19.16;
        fire.speed = 650;
        fire.burnRate = 1.61;
        fire.colorBias = 0.28;
        fire.color1.set(0xffffff);
        fire.color2.set(0xffa000);
        fire.color3.set(0xcc0000);
        fire.position.set(0, 1.8, 0);
        firePlace.add(fire);
        this.object3D.add(firePlace);

        this.fireLight = new GAME.DancingFire(0xff6600, 7.5, 17, 1.0, 7, 0.35, 0.3);
        this.fireLight.position.set(0, 1, 0);

        firePlace.add(this.fireLight.light);
        this.__createWoods__(firePlace);

        return firePlace;
    },
    //====================================================================================

    __createWoods__: function (object) {

        var createWood = function () {
            var wood = GAME.modelsLibrary.wood.clone();
            wood.castShadow = false;
            wood.receiveShadow = false;
            return wood;
        }.bind(this);

        var wood = createWood();
        wood.position.set(0.2, 0, 0);
        wood.rotation.y = Math.PI / 18;
        object.add(wood);

        wood = createWood();
        wood.rotation.y = -Math.PI / 10;
        wood.position.set(-0.2, 0, 0);
        object.add(wood);

        wood = createWood();
        wood.position.set(0, 0, 0);
        object.add(wood);

        wood = createWood();
        wood.rotation.y = Math.PI / 10;
        wood.position.set(-0.2, 0, -0.4);
        object.add(wood);

        wood = createWood();
        wood.rotation.y = Math.PI / 3;
        wood.position.set(-0.2, 0, -0.25);
        object.add(wood);

        wood = createWood();
        wood.rotation.y = -Math.PI / 3;
        wood.position.set(0.45, 0, -0.2);
        object.add(wood);

        wood = createWood();
        wood.position.set(0.5, 0.2, 0.2);
        // wood.rotation.set(0, - Math.PI / 2.1, 1);
        wood.rotation.x = Math.PI / 6;
        wood.rotation.y = Math.PI / 3;
        wood.rotation.z = Math.PI / 6;
        object.add(wood);

        wood = createWood();
        wood.position.set(-0.3, 0.2, 0.2);
        // wood.rotation.set(0, - Math.PI / 2.1, 1);
        wood.rotation.x = -7 * Math.PI / 4;
        wood.rotation.y = -Math.PI / 3;
        wood.rotation.z = -3 * Math.PI / 2;
        object.add(wood);
    },
    //====================================================================================

    __createLight__: function () {

        var highLightSpot = this.__createSpotLight__();
        highLightSpot.position.set(0, 2, 0);
        highLightSpot.target = this.fire;
        highLightSpot.target.updateMatrixWorld();
        highLightSpot.intensity = 0.001;
        this.object3D.add(highLightSpot);
        return highLightSpot;
    },
    //====================================================================================

    __createSpotLight__: function () {
        var spotLight = new THREE.SpotLight(0xffaa44, 4, 3, Math.PI / 6, 0.65, 0.4);
        spotLight.castShadow = true;
        spotLight.shadow.mapSize.width = 512;
        spotLight.shadow.mapSize.height = 512;
        spotLight.shadow.camera.near = 1;
        spotLight.shadow.camera.far = 5;
        return spotLight;
    },
    //===================================================================================


    /**
     * Стартовое заполнение слотов нанятых героев. Выполняется на основе данных, зачитанных
     * при входе в режим редактирования отряда
     * @private
     */
    __initialFillWarriorsSlots__: function () {
        var slotIndex = 0;
        GAME.gameMetadata.playerHeroes.forEach(function (playerHero) {
            this.warriorSlots[slotIndex++].warriorMetadata = playerHero;
        }.bind(this));
    },
    //====================================================================================

    /**
     * генерирует координаты слота нанятого героя вокруг костра и создает слот. С начального
     * угла до конечного ПО ЧАСОВОЙ СТРЕЛКЕ
     * Нулевой угол - направлен на 12 часов
     * @param center   Vector2
     * @param radius     float
     * @param startAngle  float  rad
     * @param endAngle     float rad
     * @param placesCount  int
     * @private
     */
    __generateWarriorSlots__: function (center, radius, startAngle, endAngle, placesCount) {
        if (Math.abs(startAngle) > Math.PI * 17 / 18) {
            console.error("absolute value of startAngle must be between - 17 Pi /18 and 18 Pi / 18");
            return;
        }
        if (Math.abs(endAngle) > Math.PI * 17 / 18) {
            console.error("absolute value of endAngle must be between - 17 Pi /18 and 18 Pi / 18");
            return;
        }
        if (placesCount < 2) {
            console.error("placesCount must be crater than 2");
            return;
        }
        var arc = endAngle - startAngle;
        var stepAngle = arc / (placesCount - 1);

        while (placesCount-- > 0) {
            var slotPosition = new THREE.Vector3(center.x + radius * Math.sin(startAngle) + Math.random() * 1.2 - 0.6
                , center.y + 0
                , center.z - radius * Math.cos(startAngle) + Math.random() * 1.2 - 0.6);

            var warriorSlot = new GAME.CampWarriorSlot(slotPosition, -startAngle);
            this.warriorSlots.push(warriorSlot);

            startAngle += stepAngle;
        }
    },
    //====================================================================================

    __detectSelections__: function (mousePosition) {
        if (this.detectSelectionEnabled) {
            this.rayCaster.setFromCamera(mousePosition, GAME.flowCamera.camera);

            var newHoveredHeroIndex = -1;
            for (var index = 0; index < this.warriorSlots.length; index++) {
                if (this.warriorSlots[index].isBusy && !this.warriorSlots[index].isReserved) {
                    var objects = [this.warriorSlots[index].object3D];
                    var intersects = this.rayCaster.intersectObjects(objects, true);
                    if (intersects.length > 0) {
                        newHoveredHeroIndex = index;
                        break;
                    }
                }
            }

            // если новый результат не равен предыдущему, то надо менять подсветку
            if (newHoveredHeroIndex != this.currentHoveredHeroIndex) {
                if (this.currentHoveredHeroIndex != -1) {
                    this.__highLightHero__(this.currentHoveredHeroIndex, false);
                }
                if (newHoveredHeroIndex != -1) {
                    this.__highLightHero__(newHoveredHeroIndex, true);
                }
                this.currentHoveredHeroIndex = newHoveredHeroIndex;
            }
        }
    },
    //===================================================================================

    /**
     * Выполняет подсветку для переданного режима посветки
     * @param postamentIndex
     * @param hovered
     * @private
     */
    __highLightHero__: function (heroIndex, highlighted) {
        const hero = this.warriorSlots[heroIndex];

        var emissiveIntensity = 1;
        var emissive = 0;

        if (highlighted) {
            emissive = 0x03;
            emissiveIntensity = 0.1;
            this.highLightSpot.position.set(hero.object3D.position.x
                , hero.object3D.position.y + 2
                , hero.object3D.position.z);
            this.highLightSpot.target = hero.object3D;
            this.highLightSpot.intensity = this.highLightSpotIntencity;
        } else {
            this.highLightSpot.intensity = 0.0001;
        }

        if (hero.object3D) {
            GAME.Utils.highLightObject(hero.object3D, emissive, emissive, emissive, emissiveIntensity);
        }
    },
    //===================================================================================

    /**
     * Разрешить проверку на какого нанятого воина у костра наведена мышь
     */
    enableDetectSelection: function () {
        this.detectSelectionEnabled = true;
        if (this.currentHoveredHeroIndex >= 0) {
            this.__highLightHero__(this.currentHoveredHeroIndex, false);
        }
        this.currentHoveredHeroIndex = -1;
    },
    //====================================================================================

    /**
     * запретить проверку на какого нанятого воина у костра наведена мышь
     */
    disableDetectSelection: function () {
        this.detectSelectionEnabled = false;
    },
    //====================================================================================
};