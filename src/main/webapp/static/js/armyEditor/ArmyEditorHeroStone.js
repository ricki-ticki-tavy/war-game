/**
 * Постамент (место) героя для найма
 * @param position
 * @constructor
 */
GAME.HeroStone = function(x, y, z){

    /**
     * составной объект постамента и света
     * @type {Group}
     */
    this.object3D = new THREE.Group();

    /**
     * Объект самого героя
     * @type {undefined}
     */
    this.heroObject = undefined;

    /**
     * Место , где стоит герой
     * @type {undefined}
     */
    this.stone = this.__createStone__();
    this.object3D.add(this.stone);

    /**
     * маленький свет героя
     * @type {THREE.PointLight}
     */
    this.pointLight = new THREE.PointLight(0xffee88, 3, 1, 0.4);
    this.pointLight.castShadow = false;
    this.pointLight.position.set(0, 1.5, 0.4);
    // this.object3D.add(this.pointLight);


    this.object3D.position.set(x, y, z);

    // ссылка на метаданные класса воина, стоящего на постаменте
    this.warriorsBaseClass = undefined;
};

GAME.HeroStone.prototype = {
    constructor: GAME.HeroStone,

    __createStone__: function(){
        var object = new THREE.Mesh(new THREE.CylinderBufferGeometry(0.4, 0.5, 0.1, 32, 2), GAME.modelsLibrary.materials.stoneMaterial);
        object.rotation.y = Math.PI / 1.8;
        // object.castShadow = true;
        object.receiveShadow = true;
        return object;
    }
};