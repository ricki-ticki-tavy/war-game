/**
 * Класс, обслуживающий таверну наемников
 * @param scene
 * @param flowCamera
 * @param guiFormManager
 * @param armyModelsLibrary
 * @param gameMetadata
 * @param mercenaryArbor
 * @param apiRestClient
 * @constructor
 */
GAME.MercenaryArbor = function (mercenaryArborObject) {
    GAME.mercenaryArborObject = this;

    this.mercenaryArborObject = mercenaryArborObject;

    this.rayCaster = GAME.flowCamera.rayCaster;

    this.priorHoveredStoneIndex = -1;

    this.highLightSpotIntencity = 9;

    this.recruitWarriorForm = this.__createRecruitForm__();

    this.recruitingMode = false;

    // места расположения героев для найма
    this.stones = [];

    this.stoneCoordinates = [
        new THREE.Vector3(-0.7, -1.16, 2.8),
        new THREE.Vector3(0.7, -1.16, 3.1),
        new THREE.Vector3(-3, -1.16, 2),
        new THREE.Vector3(3, -1.16, 2),
        new THREE.Vector3(-2.8, -1.16, -1),
        new THREE.Vector3(3, -1.16, -1),
        new THREE.Vector3(-3, -1.16, -4),
    ];

    this.__setStones__();

    this.highLightSpot = this.__createSpotLight__();
    this.highLightSpot.position.set(this.stones[0].object3D.position.x, 1.35, this.stones[0].object3D.position.z + 0.3);
    this.highLightSpot.target = this.stones[0].object3D;
    this.highLightSpot.target.updateMatrixWorld();
    this.highLightSpot.intensity = 0.001;
    this.mercenaryArborObject.add(this.highLightSpot);

    this.__SetWarriorBaseClassesToStones__();

    GAME.flowCamera.mouseMoveEventListeners.push(this.__detectSelections__.bind(this));
    GAME.flowCamera.mouseDownEventListeners.push(this.__mouseDownHandler__.bind(this));

};

GAME.MercenaryArbor.prototype = {
    constructor: GAME.MercenaryArbor,

    //===================================================================================

    /**
     * Выполняет подсветку для переданного режима посветки
     * @param postamentIndex
     * @param hovered
     * @private
     */
    highLightStone: function (stoneIndex, highlighted) {
        const stone = this.stones[stoneIndex];

        var emissiveIntensity = 1;
        var emissive = 0;

        if (highlighted) {
            emissive = 0x03;
            emissiveIntensity = 0.1;
            this.highLightSpot.position.set(stone.object3D.position.x
                , stone.object3D.position.y + 1.35
                , stone.object3D.position.z);
            this.highLightSpot.target = stone.object3D;
            this.highLightSpot.intensity = this.highLightSpotIntencity;
        } else {
            this.highLightSpot.intensity = 0.0001;
        }

        if (stone.heroObject) {
            GAME.Utils.highLightObject(stone.heroObject, emissive, emissive, emissive, emissiveIntensity);
        }

    },
    //===================================================================================

    __createRecruitForm__: function () {
        var form = new GAME.RecruitHeroForm(this.campaignCamp);
        return form;
    },
    //===================================================================================

    __mouseDownHandler__: function (mouseEvent) {
        this.__detectSelections__(GAME.flowCamera.mousePosition);

        if (this.priorHoveredStoneIndex != -1
            && this.stones[this.priorHoveredStoneIndex].warriorsBaseClass) {
            // есть подсвеченный воин

            // назначим форме метаданные
            this.recruitWarriorForm.warriorsBaseClass = this.stones[this.priorHoveredStoneIndex].warriorsBaseClass;

            // залочим камеру
            if (!this.recruitingMode) {
                GAME.flowCamera.enabled = false;
                GAME.flowCamera.destination.set(-10.7, 4.12, 5.0);
                this.recruitWarriorForm.show();
                this.recruitingMode = true;
                return true;
            }

            this.recruitingMode = true;


        } else {
            // если активен режим наема, то скрыть форму и отпустить камеру
            if (this.recruitingMode) {
                this.recruitingMode = false;
                this.recruitWarriorForm.hide();
                GAME.flowCamera.enabled = true;
                return true;
            } else {
                return false;
            }
        }
        if (!this.recruitingMode) {
        }
    },
    //===================================================================================

    /**
     * метод должен вызываться из рендера. Подсвечивает активные объекты на сцене
     * @param mouseCoords
     */
    __detectSelections__: function (mousePosition) {
        this.rayCaster.setFromCamera(mousePosition, GAME.flowCamera.camera);

        var newHoveredPostamentIndex = -1;
        for (var index = 0; index < this.stones.length; index++) {
            var objects = [this.stones[index].stone];
            if (this.stones[index].heroObject != undefined) {
                objects.push(this.stones[index].heroObject);
            }
            var intersects = this.rayCaster.intersectObjects(objects, true);
            if (intersects.length > 0) {
                newHoveredPostamentIndex = index;
                break;
            }
        }

        // если новый результат не равен предыдущему, то надо менять подсветку
        if (newHoveredPostamentIndex != this.priorHoveredStoneIndex) {
            if (this.priorHoveredStoneIndex != -1) {
                this.highLightStone(this.priorHoveredStoneIndex, false);
            }
            if (newHoveredPostamentIndex != -1) {
                this.highLightStone(newHoveredPostamentIndex, true);
            }
            this.priorHoveredStoneIndex = newHoveredPostamentIndex;
        }
    },
    //===================================================================================
    //===================================================================================

    /**
     * расставляет все постаменты
     * @private
     */
    __setStones__: function () {
        this.stoneCoordinates.forEach(function (position) {
            var stone = new GAME.HeroStone(position.x, position.y, position.z);
            this.stones.push(stone);
            this.mercenaryArborObject.add(stone.object3D);
        }.bind(this))
    },
    //===================================================================================


    __createSpotLight__: function () {
        var spotLight = new THREE.SpotLight(0xffaa44, 4, 3, Math.PI / 4, 0.65, 0.4);
        spotLight.castShadow = true;
        spotLight.shadow.mapSize.width = 512;
        spotLight.shadow.mapSize.height = 512;
        spotLight.shadow.camera.near = 1;
        spotLight.shadow.camera.far = 5;
        return spotLight;
    },
    //===================================================================================

    /**
     * расставляет базовые классы воинов на постаменты в палатке наемников
     * @private
     */
    __SetWarriorBaseClassesToStones__: function () {
        var stoneIndex = 0;
        GAME.gameMetadata.warriorsBaseClasses.forEach(function (warriorBaseClass) {
            if (warriorBaseClass.models.baseView != undefined) {
                // Есть загруженная модель воина
                var object = warriorBaseClass.models.baseView.clone();
                object.traverse(function (child) {
                    if (child.material) {
                        child.material = child.material.clone();
                    }
                }.bind(this));

                this.stones[stoneIndex].warriorsBaseClass = warriorBaseClass;
                this.stones[stoneIndex].heroObject = object;
                this.stones[stoneIndex].object3D.add(object);
                object.position.y = 0;
                stoneIndex++;
            }
        }.bind(this))
    }
    //===================================================================================


};
