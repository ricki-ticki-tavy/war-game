/**
 * Форма наема в армию нового героя
 * @param guiFormManager
 * @constructor
 */
GAME.RecruitHeroForm = function (campaignCamp) {
    this.campaignCamp = campaignCamp;

    this.textColorRegular = "#ffffff";
    this.textColorinfo = "#ffff00";

    this.labelDescription = undefined;
    this.labelWarriorsBaseClass = undefined;
    this.labelAbilities = undefined;
    this.labelSpells = undefined;
    this.labelRecruitCost = undefined;
    this.labelFightCost = undefined;


    this.form = new GAME.GuiForm(0, 1, 1.9, 2, false);
    this.form.opacity = 0.15;

    this.__createLabels__();

    this.buttonOk = this.__createButtonOk__();

    this.warriorsBaseClassMetadata = undefined;
};
//=================================================================================================================
//=================================================================================================================


GAME.RecruitHeroForm.prototype = {
    constructor: GAME.RecruitHeroForm,

    show: function () {
        this.form.show();
    },
    //=================================================================================================================

    hide: function () {
        this.form.hide();
    },

    //=================================================================================================================
    //=================================================================================================================
    //=================================================================================================================


    /**
     * установка метаданных класса, которые надо отобразить на форму
     * @param val
     */
    set warriorsBaseClass(val) {
        this.warriorsBaseClassMetadata = val;
        this.form.textPageMaterial.clear();
        this.__fillForm__();
    },

    get warriorsBaseClass() {
        return this.warriorsBaseClassMetadata;
    },

    //=================================================================================================================
    //=================================================================================================================
    //=================================================================================================================

    /**
     * Заполнить форму из метаданных
     * @private
     */
    __fillForm__: function () {
        if (this.warriorsBaseClassMetadata !== undefined) {
            this.labelWarriorsBaseClass.text = this.warriorsBaseClassMetadata.title;
            this.labelDescription.text = this.warriorsBaseClassMetadata.description;
            this.form.update(false);
        }
    },
    //=================================================================================================================

    /**
     * Нажатие на кнопку НАНЯТЬ
     * @private
     */
    __createButtonOk__: function () {
        var button = this.form.createButton(1, -1.8, 0.15, 0.15
            , GAME.modelsLibrary.materials.guiOkButtonMaterial
            , this.__doRecruitAction__.bind(this));
        return button;
    },
    //=================================================================================================================

    /**
     * Создание всех текстовых данных на форме
     * @private
     */
    __createLabels__: function () {
        this.form.createText(250, 60, 1024, 90, "Нанять в отряд бойца ?", 65, this.textColorRegular);

        this.form.createText(50, 140, 260, 55, "Класс: ", 45, this.textColorRegular);
        this.labelWarriorsBaseClass = this.form.createText(310, 140, 640, 55, "КЛАСС", 45, this.textColorinfo);

        this.form.createText(50, 210, 260, 55, "Способности: ", 45, this.textColorRegular);
        this.labelAbilities = this.form.createText(310, 210, 640, 55, "СПОСОБНОСТИ", 45, this.textColorinfo);

        this.form.createText(50, 280, 260, 55, "Заклинания: ", 45, this.textColorRegular);
        this.labelSpells = this.form.createText(310, 280, 640, 55, "ЗАКЛИНАНИЯ", 45, this.textColorinfo);

        this.form.createText(50, 350, 260, 55, "Цена наема: ", 45, this.textColorRegular);
        this.labelRecruitCost = this.form.createText(310, 350, 640, 55, "0 золотых", 45, this.textColorinfo);

        this.form.createText(50, 420, 260, 55, "Цена боя: ", 45, this.textColorRegular);
        this.labelFightCost = this.form.createText(310, 420, 640, 55, "0 золотых", 45, this.textColorinfo);

        this.form.createText(50, 490, 260, 55, "Описание: ", 45, this.textColorRegular);
        this.labelDescription = this.form.createText(310, 490, 640, 400, "ОПИСАНИЕ", 45, this.textColorinfo);
        this.labelDescription.wordWrap = true;


    },
    //=================================================================================================================

    __createDescription__: function () {

    },
    //=================================================================================================================

    __doRecruitAction__: function () {
        this.form.prompt("Наем в отряд героя " + this.warriorsBaseClassMetadata.title, "Введите имя нового героя:", ""
            , false, function (form, heroName) {

                GAME.apiRestClient.findPlayerHero(heroName, function(heroName, result){
                    if (result.result === ""){
                        // в отряде еще нет воина с таким именем. отправим запрос на создание
                        GAME.apiRestClient.createWarrior(this.warriorsBaseClassMetadata.title, heroName, 1, 1
                            , function(heroName, result){
                                this.currentWarriorId = result.result;
                                // теперь сохраним в БД этого воина, чтобы он стал героем )))
                                GAME.apiRestClient.saveWarriorAmmunition(result.result, "Пустой", function(heroName, result){
                                    // Зачитаем структуры данных по созданному только что герою
                                    GAME.apiRestClient.findPlayerHero(heroName, function(heroName, result) {
                                        // и удалим его с карты, чтобы не мешал
                                        GAME.apiRestClient.removeWarrior(this.currentWarriorId, function(heroName, metadata, result){
                                            GAME.campaignCamp.reserveWarriorSlot(JSON.parse(metadata));
                                            this.form.showMessage("Пополнение отряда", "В вашем отряде появился новый герой " + this.warriorsBaseClassMetadata.title + " " + heroName);
                                        }.bind(this, heroName, result.result));
                                    }.bind(this, heroName))
                                }.bind(this, heroName));
                            }.bind(this, heroName));
                    } else {
                        this.form.showMessage("Пополнение отряда", "В вашем отряде уже есть герой " + " " + " " + heroName);
                    }
                }.bind(this, heroName));

            }.bind(this));
    },
    //=================================================================================================================


};