/**
 * Слот вокруг костра для нанятого героя.
 * @param position
 * @param angle    - Угол поворота от костра
 * @param gameMetadata
 * @param warriorId
 * @constructor
 */
GAME.CampWarriorSlot = function (position, angle, warriorId) {

    /**
     * Данные класса воина
     * @type {undefined}
     */
    this.warriorsBaseClassMetadata = null;

    /**
     * Позиция относительно центра лагеря
     */
    this.position = position;

    /**
     * Угол поворота от костра
     */
    this.angle = angle;

    this.thisWarriorMetadata = null;

    this.warriorId = warriorId;

    /**
     * Занятость слота
     * @type {boolean}
     */
    this.isBusy = false;
    this.isReserved = false;

    /**
     * фигура воина, стоящего на ЭТОМ слоте
     * @type {undefined}
     */
    this.object3D = null;
    this.objectReserved3D = null;

    /**
     * Точка, откуда прибывает нанятый воин к костру
     * @type {THREE.Vector}
     */
    this.arriveFromPoint = new THREE.Vector4(-19.8, 0, 0.4, 0);

    /**
     * Точка, обхода костра
     * @type {THREE.Vector}
     */
    this.arriveAroundFirePoint = new THREE.Vector4(0, 0, -6, 0);

    /**
     * Координата представления воина игроку
     * @type {Vector4}
     */
    this.presentToPlayerPoints = [new THREE.Vector4(-1, 0, 3, 0), new THREE.Vector4(-1, 0, 3.7, 0)];
    // this.presentToPlayerPoints = [new THREE.Vector4(-2.2, 0, 3, 0), new THREE.Vector4(-2.2, 3, 11, 0)];

    /**
     * Путь от места у костра до места представления игроку
     */
    this.presentToPlayerWayPath = this.__createPresentToPlayerWayPath__();

    /**
     * Путь от палатки наемников до места у костра
     */
    this.arriveWayPath = this.__createArriveWatPath__();
};

GAME.CampWarriorSlot.prototype = {
    constructor: GAME.CampWarriorSlot,

    /**
     * Зарезервировать слот для появления на нем нанятого кв отряд героя
     * @param heroMetadata
     */
    reserve: function(heroMetadata){
        this.isBusy = true;
        this.isReserved = true;
        this.thisWarriorMetadata = heroMetadata;

        this.__createWarriorObject3D__();

        // Поставим в исходную точку
        this.object3D.position.set(this.arriveFromPoint.x
            , this.arriveFromPoint.y
            , this.arriveFromPoint.z);


        var geo = new THREE.PlaneBufferGeometry(1, 1, 1, 1);
        var mat = new THREE.MeshPhongMaterial({
            map: GAME.modelsLibrary.textures.portals[1], transparent: true
            , emissiveMap: GAME.modelsLibrary.textures.portals[1].clone()
            , emissive: 0xffffff
            , emissiveIntensity: 1
        });
        this.objectReserved3D = new THREE.Mesh(geo, mat);

        this.objectReserved3D.rotation.set(-Math.PI / 2, 0, 0);
        this.objectReserved3D.position.set(this.position.x
            , this.position.y + 0.01
            , this.position.z);

        GAME.campaignCamp.object3D.add(this.objectReserved3D);

        // запустим перемещение нанятого воина к костру
        this.arriveWayPath.moveTo(-1, this.object3D, function(){
            this.commitReserve();
        }.bind(this))
    },
    //============================================================================

    /**
     * Подтвердить резер. При этом исчезнет знак портала и появится фигура героя
     * @param heroMetadata
     */
    commitReserve: function(){
        if (!this.isReserved){
            console.error("CampWarriorSlot has not been reserved! Committing unsupported!");
            return;
        }
        this.__disposeReservedObject__();
        this.isBusy = true;
        this.isReserved = false;
    },
    //============================================================================

    animate: function () {
        if (this.isReserved){
            this.objectReserved3D.rotation.z -= 0.1 / GAME.flowCamera.avgRateCoef;
            this.arriveWayPath.animate();
        }
        if (this.presentToPlayerWayPath.isActive){
            this.presentToPlayerWayPath.animate();
        }
    },
    //============================================================================

    /**
     * Подвести героя к месту перед костром для представления герою
     */
    presentToPlayer: function(callback){
        this.presentToPlayerWayPath.moveTo(-1, this.object3D, callback);
    },
    //============================================================================

    /**
     * Вернуть георя с любой позиции на место у костра
     */
    backToPlace: function(){
        this.presentToPlayerWayPath.moveTo(0, this.object3D, null);
    },
    //============================================================================

    set warriorMetadata(heroMetadata) {
        this.thisWarriorMetadata = heroMetadata;
        if (heroMetadata == null || !heroMetadata) {
            // удалить героя со слота
            this.warriorsBaseClassMetadata = null;
            this.__disposeHeroObject__();
            this.isBusy = false;
        } else {
            this.__createWarriorObject3D__();
            this.object3D.position.set(this.position.x
                , this.position.y + this.warriorsBaseClassMetadata.graphicsInfo.baseViewModel.floorLevel
                , this.position.z);
        }

    },
    //============================================================================

    /**
     * Создать фишуру фоина и поместить ее на сцену
     * @private
     */
    __createWarriorObject3D__: function(){

        // Найдем базовую модель воина
        this.warriorsBaseClassMetadata = GAME.gameMetadata.findWarriorBaseClassIndexByName(this.thisWarriorMetadata.baseClassName);

        this.object3D = this.warriorsBaseClassMetadata.models.baseView.clone();
        this.object3D.traverse(function (child) {
            if (child.material) {
                child.material = child.material.clone();
            }
        }.bind(this));

        this.object3D.rotation.y = this.angle;

        GAME.campaignCamp.object3D.add(this.object3D);

        this.isBusy = true;
    },
    //============================================================================

    /**
     * Создает путь от места у костра до места представления воина игроку ПЕРЕД костром
     * @private
     */
    __createPresentToPlayerWayPath__: function(){
        var wayController = new GAME.WayController();
        wayController.addWayPointAsIs(new THREE.Vector4(this.position.x
            , this.position.y
            , this.position.z
            , this.angle));
        if (this.position.x < 0) {
            wayController.addWayPoints([new THREE.Vector4(-2, 0, 0.5, 0)
                , new THREE.Vector4(-2, 0, 2, 0)]);
        } else {
            wayController.addWayPoints([new THREE.Vector4(2, 0, 0.5, 0)
                , new THREE.Vector4(2, 0, 2, 0)]);
        }
        wayController.addWayPoints(this.presentToPlayerPoints);
        wayController.addWayPointAsIs(this.presentToPlayerPoints[1]);
        wayController.moveSpeed = 2;
        return wayController;
    },
    //============================================================================

    /**
     * Создает путь перемещения нанятого воина от таверны до места у костра
     * @private
     */
    __createArriveWatPath__: function(){
        var wayController = new GAME.WayController();
        wayController.addWayPoints([this.arriveFromPoint
            , new THREE.Vector4(-6, 0, -2)
        ]);

        if (this.position.x > 0){
            wayController.addWayPoints([this.arriveAroundFirePoint]);
        }
        wayController.addWayPoints([new THREE.Vector4(this.position.x, this.position.y, this.position.z, this.angle)]);
        wayController.addWayPointAsIs(new THREE.Vector4(this.position.x, this.position.y, this.position.z, this.angle));
        wayController.moveSpeed = 2;
        return wayController;
    },
    //============================================================================

    /**
     * Удалить со сцены объект, резервирующий место нанятого героя
     * @private
     */
    __disposeReservedObject__: function(){
        if (this.isReserved){
            this.isReserved = false;
            GAME.campaignCamp.object3D.remove(this.objectReserved3D);
            this.objectReserved3D.material.emissiveMap.dispose();
            this.objectReserved3D.geometry.dispose();
            this.objectReserved3D.material.dispose();
            this.objectReserved3D = null;
        }
    },
    //============================================================================

    /**
     * Удалить со сцены героя
     * @private
     */
    __disposeHeroObject__: function(){
        if (this.isBusy){
            this.isReserved = false;
            GAME.campaignCamp.object3D.remove(this.object3D);
            this.object3D.traverse(function(child){
               if (child.material){
                   child.material.dispose();
               }
               if (child.geometry){
                   child.geometry.dispose();
               }
            });
        }
    },
    //============================================================================

    get warriorMetadata() {
        return this.thisWarriorMetadata;
    },

};
