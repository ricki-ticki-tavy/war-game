GAME.ArmyEditor = function (anisotropy, useSSAO) {
    GAME.armyEditor = this;

    this.scene;
    this.camera;
    this.renderer;
    this.flowCamera;
    this.ssaoPass;
    this.effectComposer;
    this.stats;

    this.gameMetadata;

    this.useSSAO = useSSAO;
    this.anisotropy = anisotropy;

    this.modelsLibrary;
    this.armyEditorDecorator;

    this.mercenaryArborObject;
    this.campaignCamp = null;

    this.preloadDownCounter = 0;

    this.guiFormManager;
    this.gameContextUUID = null;
    this.isInitialized = false;
    this.loadGauge = null;
    this.mainMenu = null;
};

GAME.ArmyEditor.prototype = {
    constructor: GAME.ArmyEditor,

    scene_setup: function () {
        this.scene = new THREE.Scene();
        GAME.scene = this.scene;

        this.camera = new THREE.PerspectiveCamera(30, window.innerWidth / window.innerHeight, 0.1, 1000);
        // camera = new THREE.OrthographicCamera(20, -20, 10, -10, 0.1, 1000);

        this.renderer = new THREE.WebGLRenderer({
            antialias: true,
            // precision: "highp",
            // logarithmicDepthBuffer: true,
        });
        this.renderer.setSize(window.innerWidth, window.innerHeight);
        this.renderer.shadowMap.enabled = true;
        this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
        this.renderer.physicallyCorrectLights = true;
        document.body.appendChild(this.renderer.domElement);

        // window.addEventListener('resize', this.__onWindowResize__.bind(this), false);
        window.onresize = this.__onWindowResize__.bind(this);


        this.stats = new Stats();
        var container = document.createElement('div');
        document.body.appendChild(container);
        container.appendChild(this.stats.dom);

        if (this.useSSAO) {
            var width = window.innerWidth;
            var height = window.innerHeight;
            this.ssaoPass = new THREE.SSAOPass(this.scene, this.camera, width, height);
            this.ssaoPass.kernelRadius = 16;
            this.ssaoPass.output = THREE.SSAOPass.OUTPUT.Default;
            this.effectComposer = new THREE.EffectComposer(this.renderer);
            this.effectComposer.addPass(this.ssaoPass);
        }

        this.loadGauge = new GAME.LoadGauge(this.scene);
        this.loadGauge.total = 23;

        this.render();
    },
    //-------------------------------------------------------------------------------

    render: function () {
        requestAnimationFrame(this.render.bind(this));
        if (this.isInitialized) {
            this.flowCamera.updateCamera();
            this.armyEditorDecorator.lightBugsHolder.moveLightBugsCycle();
            this.guiFormManager.dispatch();
            this.campaignCamp.animate();
            this.armyEditorDecorator.animate();
        }
        this.stats.begin();
        if (this.useSSAO) {
            this.effectComposer.render();
        } else {
            this.renderer.render(this.scene, this.camera);
        }
        this.stats.end();
    },
    //-------------------------------------------------------------------------------

    main: function (gameMetadata, gameContextUUID) {

        this.gameContextUUID = gameContextUUID;
        this.gameMetadata = gameMetadata;

        this.loadGauge.incProgress();

        // this.scene_setup();
        this.flowCamera = new THREE.FlowCamera(this.camera, new THREE.Vector3(0, 5.4, 14), new THREE.Vector3(0, 3, 0)
            , {
                topLeft: new THREE.Vector2(-15.5, -4.5),
                bottomRight: new THREE.Vector2(9.5, 17.5)
            }, false);
        this.flowCamera.heightRestrictions.top = 10;
        this.loadGauge.incProgress();

        // flowCamera.enabled = false;

        // загрузчик объектов сцены
        this.modelsLibrary = new GAME.ModelsLibrary(this.anisotropy, this.loadGauge, function (object) {
            this.preloadDownCounter--;
            this.__checkIfAllPreloadsDone__();
        }.bind(this));
        this.preloadDownCounter += this.modelsLibrary.getObjectsCount();
        this.modelsLibrary.loadLibrary();

    },
    //===================================================================================

    /**
     * Проверяет дошел ли до нуля счетчик загрузок и, если да, то вызывает метод дальнейшей инициализации сцены
     */
    __checkIfAllPreloadsDone__: function () {
        if (this.preloadDownCounter == 0) {
            // все предзагружено
            this.runArmyEditor();
        }
    },
    //===================================================================================

    /**
     * запуск интерфейса набора героев в армию
     */
    runArmyEditor: function () {

        this.guiFormManager = new GAME.GuiFormManager(this.scene, this.flowCamera, this.modelsLibrary, this.renderer);
        this.loadGauge.incProgress();

        this.armyEditorDecorator = new GAME.ArmyEditorDecorator(this.scene, this.modelsLibrary, this.flowCamera);
        this.armyEditorDecorator.decorate();
        this.loadGauge.incProgress();

        // Установим новый курсор запроса к серверу и метод вывода ошибки на экран
        GAME.apiRestClient.lockCursor = this.guiFormManager.modalCursor;
        GAME.apiRestClient.externalShowErrorMethod = function (message, callback) {
            this.guiFormManager.showMessage("Ошибка обращения к серверу", message, callback)
        }.bind(this);

        this.campaignCamp = new GAME.CampaignCamp();
        this.loadGauge.incProgress();

        this.mercenaryArborObject = new GAME.MercenaryArbor(this.armyEditorDecorator.mercenaryArborObject);
        this.loadGauge.incProgress();

        this.campaignCamp.object3D.position.set(10, 0, 8);
        this.scene.add(this.campaignCamp.object3D);

        this.loadGauge.incProgress();
        this.doSomeThing();

        this.loadGauge.incProgress();

        this.isInitialized = true;
        this.loadGauge.hide();

        this.mainMenu = new GAME.MainMenu(document.body);

    },
    //===================================================================================

    __onWindowResize__: function () {
        var width = window.innerWidth;
        var height = window.innerHeight;
        this.camera.aspect = width / height;
        var fov = 1920 / window.innerWidth * 30;
        fov *=  height / 984;
        this.camera.fov = fov;
        this.camera.updateProjectionMatrix();
        this.renderer.setSize(width, height);
        if (this.useSSAO) {
            this.ssaoPass.setSize(width, height);
        }
    },

//===================================================================================

//===================================================================================
//===================================================================================
//===================================================================================

    doSomeThing: function () {

        //
        // const geo = new THREE.PlaneBufferGeometry(2, 2, 1, 1);
        // const mesh = new THREE.Mesh(geo, GAME.modelsLibrary.materials.guiLightCircleMaterial);
        // GAME.scene.add(mesh);
        // mesh.position.set(0, 1, 0);

    },

};
