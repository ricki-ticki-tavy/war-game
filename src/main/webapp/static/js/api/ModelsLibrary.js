GAME.ModelsLibrary = function (anisotropy, loadGauge, loadProgressCallback) {

    GAME.modelsLibrary = this;

    this.loadGauge = loadGauge;
    this.anisotropy = anisotropy;
    this.loadProgressCallback = loadProgressCallback;
    this.loadMethodsList = [];

    this.arbor = undefined;
    this.largeArbor = undefined;
    this.well_1 = undefined;
    this.trees = [];
    this.grasses = [];

    this.wood = undefined;
    this.wallLamp = undefined;
    this.wallMountedTorch = undefined;

    this.buildingsModelFilesPath = "models/buildings/";
    this.interriorModelFilesPath = "models/interior/";
    this.treesModelFilesPath = "models/trees/";
    this.guiCursorsModelFilesPath = "models/gui/cursors/";
    this.weaponsModelFilesPath = "models/weapons/";
    this.effectsModelFilesPath = "models/textures/effects/";

    this.guiTexturesFilesPath = "models/textures/gui/";

    this.commonResourcesPath = "models/textures/forModels/";

    this.cursors = {
        skull: undefined,
    };

    this.images = {
        stones: [],
    };

    this.textures = {
        guiFormTexture: null,
        grass: null,
        guiFormTexture: null,
        portals: [],
    };

    this.materials = {
        skyMaterial: new THREE.MeshBasicMaterial({
            // color: 0xffffff,
            color: 0x666666,
            // emissive: 0x555555,
            // emissiveIntensity: 1,
            side: THREE.BackSide,
        }),
        groundMaterial: new THREE.MeshPhongMaterial({color: 0xffffff}),
        stoneMaterial: new THREE.MeshPhongMaterial({color: 0xffffff}),
        // guiFormMaterial: new THREE.MeshPhongMaterial({color: 0xffffff, emissiveIntensity: 1, emissive:0x2a180a, transparent: true,}),
        guiOkButtonMaterial: new THREE.MeshPhongMaterial({color: 0xffffff, transparent: true,}),
        guiCancelButtonMaterial: new THREE.MeshPhongMaterial({color: 0xffffff, transparent: true,}),
        guiEditButtonMaterial: new THREE.MeshPhongMaterial({color: 0xffffff, transparent: true,}),
        guiAddEquipVariantMaterial: new THREE.MeshPhongMaterial({color: 0xffffff, transparent: true,}),
        guiAnonymousIconLargeMaterial: new THREE.MeshPhongMaterial({color: 0xffffff, transparent: true,}),
        guiLightCircleMaterial: new THREE.MeshPhongMaterial({color: 0xffffff, transparent: true, emissive: 0xffffff, }),
        guiSelectedWarriorCircleMaterial: new THREE.MeshPhongMaterial({color: 0xffffff, transparent: true, emissive: 0xffffff, }),

        cursorTargetDistanceMaterial: new THREE.MeshPhongMaterial({color: 0xffffff, transparent: true, emissive: 0xffffff, }),
        cursorTargetMoveGreenMaterial: new THREE.MeshPhongMaterial({color: 0x88ffff, transparent: true, emissive: 0x55ffff, }),
        cursorTargetMoveRedMaterial: new THREE.MeshPhongMaterial({color: 0xff5555, transparent: true, emissive: 0xff5555}),

        whiteMaterial: new THREE.MeshPhongMaterial({color: 0xffffff, emissive: 0xffffff}),
        whiteLineMaterial: new THREE.LineBasicMaterial({color: 0xffffff, emissive: 0xffffff}),
        blackBoldLineMaterial: new THREE.PointsMaterial({color: 0x000000, size: 3}),
        redBoldLineMaterial: new THREE.LineBasicMaterial({color: 0xff5555, size: 3}),
        greenBoldLineMaterial: new THREE.LineBasicMaterial({color: 0x55ff55, size: 3}),
        blueBoldLineMaterial: new THREE.LineBasicMaterial({color: 0x5555ff, size: 3}),

        brownBaseMaterial: new THREE.MeshPhongMaterial({color: 0x664126}),

    };

    this.weapons = {
        smallVikingShield: null,
    };

    this.loadGauge.incProgress();

    // текстуры
    this.loadMethodsList.push(this.__loadSkyTexture__);
    this.loadMethodsList.push(this.__loadGroundTexture__);
    this.loadMethodsList.push(this.__loadGroundNormalTexture__);
    this.loadMethodsList.push(this.__loadHeroStoneTexture__);
    this.loadMethodsList.push(this.__loadGuiFormTexture__);
    this.loadMethodsList.push(this.__loadEffectsPortal_0__);
    this.loadMethodsList.push(this.__loadEffectsPortal_1__);
    this.loadMethodsList.push(this.__loadEffectsPortal_2__);
    this.loadMethodsList.push(this.__loadEffectsLightCircle__);

    this.loadMethodsList.push(this.__loadCursorTargetDistance__);
    this.loadMethodsList.push(this.__loadCursorTargetMove__);

    this.loadMethodsList.push(this.__loadGuiOkButtonTexture__);
    this.loadMethodsList.push(this.__loadGuiCancelButtonTexture__);
    this.loadMethodsList.push(this.__loadGuiEditButtonTexture__);
    this.loadMethodsList.push(this.__loadGuiAddEquipButtonTexture__);
    this.loadMethodsList.push(this.__loadGuiAnonymousIconLarge__);

    // images DOM
    this.loadMethodsList.push(this.__loadGuiStone_0__);

    // светильники
    this.loadMethodsList.push(this.__loadWallMountedTorch__);

    // растения
    this.loadMethodsList.push(this.__loadTree_0__);
    this.loadMethodsList.push(this.__loadTree_1__);
    this.loadMethodsList.push(this.__loadTree_2__);
    this.loadMethodsList.push(this.__loadTree_3__);
    this.loadMethodsList.push(this.__loadTree_4__);
    this.loadMethodsList.push(this.__loadTree_5__);
    this.loadMethodsList.push(this.__loadTree_6__);

    this.loadMethodsList.push(this.__loadWood__);

    this.loadMethodsList.push(this.__loadArbor__);
    this.loadMethodsList.push(this.__loadLargeArbor__);
    this.loadMethodsList.push(this.__loadWell_1__);

    // курсоры
    // this.loadMethodsList.push(this.__loadCursor_Skull__);

    GAME.gameMetadata.warriorsBaseClasses.forEach(function (warriorsBaseClass) {
        this.loadMethodsList.push(this.__loadAnyBaseClassModel__.bind(this, warriorsBaseClass));
        if (warriorsBaseClass.graphicsInfo.largeIcon){
            this.loadMethodsList.push(this.__loadLargeIconForModel__.bind(this, warriorsBaseClass))
        }
    }.bind(this));

    // вооружения
    GAME.gameMetadata.weaponClasses.forEach(function (weaponClass) {
        this.loadMethodsList.push(this.__loadAnyBaseClassModel__.bind(this, weaponClass));
    }.bind(this));

    this.loadGauge.total += this.getObjectsCount();
};

GAME.ModelsLibrary.prototype = {
    constructor: GAME.ModelsLibrary,

    getObjectsCount: function () {
        return this.loadMethodsList.length;
    },

    loadLibrary: function () {
        this.__nextLoad__();
    },

    __loadCursor_Skull__: function () {
        this.__loadObjectOBJ__(this.guiCursorsModelFilesPath + "/skull/skull.mtl"
            , this.guiCursorsModelFilesPath + "/skull/skull.obj"
            , false, false
            , function (object) {
                object.scale.x = 0.1;
                object.scale.y = 0.1;
                object.scale.z = 0.1;
                this.cursors.skull = object;
            }.bind(this));
    },

    __loadWell_1__: function () {
        this.__loadObject3DS__(this.buildingsModelFilesPath + "well_1/"
            , this.buildingsModelFilesPath + "well_1/Well N070710.3DS"
            , true
            , true
            , function (object) {
                this.well_1 = object;
            }.bind(this));
    },

    __loadTree_0__: function () {
        this.__loadObject3DS__(this.treesModelFilesPath
            , this.treesModelFilesPath + "oak.3ds"
            , true
            , false
            , function (object) {
                object.rotation.x = -Math.PI / 2;
                object.scale.set(1.5, 1.5, 1.5);
                this.trees.push(object);
            }.bind(this));
    },

    __loadTree_1__: function () {
        this.__loadObject3DS__(this.treesModelFilesPath
            , this.treesModelFilesPath + "willow.3ds"
            , true
            , false
            , function (object) {
                var finalObject = new THREE.Group();
                object.scale.set(0.04, 0.04, 0.04);
                object.rotation.x = -Math.PI / 2;
                object.position.y = 6.7;
                finalObject.add(object);
                this.trees.push(finalObject);
            }.bind(this));
    },

    __loadTree_2__: function () {
        this.__loadObject3DS__(this.treesModelFilesPath
            , this.treesModelFilesPath + "tree1.3ds"
            , true
            , false
            , function (object) {
                var finalObject = new THREE.Group();
                object.scale.set(0.01, 0.01, 0.01);
                object.rotation.x = -Math.PI / 2;
                object.position.y = 0.8;
                finalObject.add(object);
                this.trees.push(finalObject);
            }.bind(this));
    },

    __loadTree_3__: function () {
        this.__loadObject3DS__(this.treesModelFilesPath
            , this.treesModelFilesPath + "tree4.3ds"
            , true
            , false
            , function (object) {
                var finalObject = new THREE.Group();
                object.scale.set(0.2, 0.2, 0.2);
                object.rotation.x = -Math.PI / 2;
                // object.position.y = 0.8;
                finalObject.add(object);
                this.trees.push(finalObject);
            }.bind(this));
    },

    __loadTree_4__: function () {
        this.__loadObject3DS__(this.treesModelFilesPath
            , this.treesModelFilesPath + "tree5.3ds"
            , true
            , false
            , function (object) {
                var finalObject = new THREE.Group();
                object.scale.set(0.01, 0.01, 0.01);
                object.rotation.x = -Math.PI / 2;
                // object.position.y = 0.8;
                finalObject.add(object);
                this.trees.push(finalObject);
            }.bind(this));
    },

    __loadTree_5__: function () {
        this.__loadObject3DS__(this.treesModelFilesPath
            , this.treesModelFilesPath + "pine-tree.3ds"
            , true
            , false
            , function (object) {
                var finalObject = new THREE.Group();
                object.scale.set(0.04, 0.04, 0.04);
                object.rotation.x = -Math.PI / 2;
                // object.position.y = 0.8;
                finalObject.add(object);
                this.trees.push(finalObject);
            }.bind(this));
    },

    __loadTree_6__: function () {
        this.__loadObjectOBJ__(this.treesModelFilesPath + "grows1/untitled-scene.mtl"
            , this.treesModelFilesPath + "grows1/untitled-scene.obj"
            , true
            , false
            , function (object) {
                var finalObject = new THREE.Group();
                // object.scale.set(0.4, 0.4, 0.4);
                // object.rotation.x = -Math.PI / 2;
                // object.position.y = 0.8;
                finalObject.add(object);
                this.trees.push(finalObject);
            }.bind(this));
    },

    __loadWood__: function () {
        this.__loadObjectOBJ__(this.interriorModelFilesPath + "wood/wood.mtl"
            , this.interriorModelFilesPath + "wood/wood.obj"
            , true
            , false
            , function (object) {
                var finalObject = new THREE.Group();
                object.scale.set(50, 50, 50);
                // object.rotation.x = -Math.PI / 2;
                object.position.y = 0.07;
                finalObject.add(object);
                this.wood = finalObject;
            }.bind(this));
    },

    __loadArbor__: function () {
        this.__loadObject3DS__(this.buildingsModelFilesPath
            , this.buildingsModelFilesPath + "arbor.3ds"
            , true
            , true
            , function (object) {
                this.arbor = object;
            }.bind(this));
    },

    __loadLargeArbor__: function () {
        this.__loadObject3DS__(this.buildingsModelFilesPath
            , this.buildingsModelFilesPath + "largeArbor.3ds"
            , true
            , true
            , function (object) {
                var finalObject = new THREE.Group();

                object.scale.set(0.02, 0.02, 0.008);
                object.rotation.x = -Math.PI / 2;
                finalObject.add(object);

                var basePointLight = new THREE.PointLight(0xffaa66, 0.8, 6, 1.0);
                basePointLight.castShadow = true;
                basePointLight.position.set(0, 0.8, -0);
                // finalObject.add(basePointLight);


                var torch = this.wallMountedTorch.clone();
                torch.position.set(-1.7, 0.2, 3.1);
                torch.rotation.y = -Math.PI / 2;
                finalObject.add(torch);

                var torch = this.wallMountedTorch.clone();
                torch.position.set(1.7, 0.4, 3.1);
                torch.rotation.y = Math.PI / 2;
                finalObject.add(torch);

                var torch = this.wallMountedTorch.clone();
                torch.position.set(1.7, 0.4, -4);
                torch.rotation.y = Math.PI / 2;
                finalObject.add(torch);

                var torch = this.wallMountedTorch.clone();
                torch.position.set(-1.7, 0.4, -4);
                torch.rotation.y = -Math.PI / 2;
                finalObject.add(torch);

                this.largeArbor = finalObject;
            }.bind(this));
    },


    __loadWallMountedTorch__: function () {
        this.__loadObject3DS__(this.interriorModelFilesPath + "wallMountedTorch/"
            , this.interriorModelFilesPath + "wallMountedTorch/Wall mounted torch.3ds"
            , true
            , false
            , function (object) {
                var finalObject = new THREE.Group();

                object.scale.set(0.03, 0.03, 0.03);
                // object.rotation.z = -Math.PI / 2;
                finalObject.add(object);

                basePointLight = new THREE.PointLight(0xffaa66, 2, 5, 0.4);
                basePointLight.castShadow = true;
                basePointLight.position.set(0.0, 0.15, -0.07);
                finalObject.add(basePointLight);

                var bulbMat = new THREE.MeshStandardMaterial({
                    emissive: 0xffaa66,
                    emissiveIntensity: 4,
                    color: 0x000000
                });
                var bulbGeometry = new THREE.SphereBufferGeometry(0.03, 16, 8);
                basePointLight.add(new THREE.Mesh(bulbGeometry, bulbMat));
                // finalObject.add(basePointLight);

                this.wallMountedTorch = finalObject;
            }.bind(this));
    },

    __loadSkyTexture__: function () {
        this.__LoadTexture__("models/textures/sky/sky4096.jpg", function (texture) {
            this.materials.skyMaterial.map = texture;
        }.bind(this));

    },

    __loadGroundTexture__: function () {
        this.__LoadTexture__("models/textures/grass/grasslight-big.jpg", function (texture) {
            texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
            texture.repeat.set(40, 40);
            texture.center.set(0.5, 0.5);
            this.textures.grass = texture;
            this.materials.groundMaterial.map = texture;
        }.bind(this));

    },

    __loadGroundNormalTexture__: function () {
        this.__LoadTexture__("models/textures/grass/grasslight-big-nm.jpg", function (texture) {
            texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
            texture.repeat.set(10, 10);
            texture.center.set(0.5, 0.5);
            // this.materials.groundMaterial.bumpMap = texture;
            // this.materials.groundMaterial.bumpScale = 12;
        }.bind(this));

    },

    __loadHeroStoneTexture__: function () {
        this.__LoadTexture__("models/textures/Rough-Wooden-Plank-Texture.jpg", function (texture) {
            texture.center.set(0.5, 0.5);
            this.materials.stoneMaterial.map = texture;
        }.bind(this));

    },

    __loadGuiFormTexture__: function () {
        this.__LoadTexture__("models/textures/gui/form/pergament2-1024-2048.png", function (texture) {
            // texture.center.set(0.5, 0.5);
            this.textures.guiFormTexture = texture;
            // this.materials.guiFormMaterial.map = texture;
            // this.materials.guiFormMaterial.opacity = 0.15;
            // this.materials.guiFormMaterial.color = 0x101010;
            // this.materials.guiFormMaterial.reflectivity = 0;
        }.bind(this));

    },

    __loadGuiOkButtonTexture__: function () {
        this.__LoadTexture__("models/textures/gui/buttons/ok.png", function (texture) {
            texture.center.set(0.5, 0.5);
            this.materials.guiOkButtonMaterial.map = texture;
        }.bind(this));

    },

    __loadGuiCancelButtonTexture__: function () {
        this.__LoadTexture__("models/textures/gui/buttons/cancel.png", function (texture) {
            texture.center.set(0.5, 0.5);
            this.materials.guiCancelButtonMaterial.map = texture;
        }.bind(this));

    },

    __loadGuiEditButtonTexture__: function () {
        this.__LoadTexture__("models/textures/gui/buttons/editButton.png", function (texture) {
            texture.center.set(0.5, 0.5);
            this.materials.guiEditButtonMaterial.map = texture;
        }.bind(this));

    },

    __loadGuiAddEquipButtonTexture__: function () {
        this.__LoadTexture__("models/textures/gui/buttons/plus.png", function (texture) {
            texture.center.set(0.5, 0.5);
            this.materials.guiAddEquipVariantMaterial.map = texture;
        }.bind(this));

    },

    __loadGuiAnonymousIconLarge__: function () {
        this.__LoadTexture__("models/textures/gui/icons/anonymous/anonymousLarge.png", function (texture) {
            texture.center.set(0.5, 0.5);
            this.materials.guiAnonymousIconLargeMaterial.map = texture;
        }.bind(this));

    },

    __loadEffectsPortal_0__: function () {
        this.__LoadTexture__(this.effectsModelFilesPath + "portal.png", function (texture) {
            texture.center.set(0.5, 0.5);
            this.textures.portals.push(texture);
        }.bind(this));

    },

    __loadEffectsPortal_1__: function () {
        this.__LoadTexture__(this.effectsModelFilesPath + "portal2.png", function (texture) {
            texture.center.set(0.5, 0.5);
            this.textures.portals.push(texture);
        }.bind(this));

    },

    __loadEffectsPortal_2__: function () {
        this.__LoadTexture__(this.effectsModelFilesPath + "portal3.png", function (texture) {
            texture.center.set(0.5, 0.5);
            this.textures.portals.push(texture);
            this.materials.guiSelectedWarriorCircleMaterial.map = texture;
        }.bind(this));

    },
    //================================================================================================

    __loadCursorTargetDistance__: function () {
        this.__LoadTexture__(this.effectsModelFilesPath + "targetDistance.png", function (texture) {
            texture.center.set(0.5, 0.5);
            this.materials.cursorTargetDistanceMaterial.map = texture;
        }.bind(this));

    },
    //================================================================================================

    __loadCursorTargetMove__: function () {
        this.__LoadTexture__(this.effectsModelFilesPath + "targetMove.png", function (texture) {
            texture.center.set(0.5, 0.5);
            this.materials.cursorTargetMoveGreenMaterial.map = texture;
            this.materials.cursorTargetMoveRedMaterial.map = texture;
        }.bind(this));

    },
    //================================================================================================

    __loadEffectsLightCircle__: function () {
        this.__LoadTexture__(this.effectsModelFilesPath + "lightCircle.png", function (texture) {
            texture.center.set(0.5, 0.5);
            this.materials.guiLightCircleMaterial.map = texture;
            this.materials.guiLightCircleMaterial.emissiveMap = texture.clone();
        }.bind(this));

    },
    //================================================================================================

    __loadGuiStone_0__: function () {
        this.__LoadImage__(this.guiTexturesFilesPath + "form/stone3_2048_1024.jpg", function (image) {
            this.images.stones.push(image);
        }.bind(this));

    },
    //================================================================================================

    /**
     * Загрузка иконок для модельки
     * @param anyBaseClassMetadata
     * @private
     */
    __loadLargeIconForModel__: function(anyBaseClassMetadata){
        const iconLoaded = function(metadata, texture){
            if (!metadata.icons) {
                metadata.icons = {};
            }
            metadata.icons.largeIcon = new THREE.MeshPhongMaterial({color: 0xffffff, transparent: true,});
            metadata.icons.largeIcon.map = texture;
        }.bind(this, anyBaseClassMetadata);

        this.__LoadTexture__(anyBaseClassMetadata.graphicsInfo.largeIcon, iconLoaded);
    },
    //================================================================================================

    /**
     * Загрузка моделей базовых классов воинов
     * @param anyBaseClassMetadata
     * @private
     */
    __loadAnyBaseClassModel__: function (anyBaseClassMetadata) {
        if (anyBaseClassMetadata.graphicsInfo != undefined
            && anyBaseClassMetadata.graphicsInfo.baseViewModel != undefined) {

            var assignObj = function (object) {
                if (Math.abs(1 - anyBaseClassMetadata.graphicsInfo.baseViewModel.scale) > 0.01) {
                    object.scale.set(anyBaseClassMetadata.graphicsInfo.baseViewModel.scale
                        , anyBaseClassMetadata.graphicsInfo.baseViewModel.scale
                        , anyBaseClassMetadata.graphicsInfo.baseViewModel.scale);
                }
                var group = new THREE.Group();
                group.add(object);
                object.position.set(0, anyBaseClassMetadata.graphicsInfo.baseViewModel.floorLevel, 0);
                object.rotation.set(anyBaseClassMetadata.graphicsInfo.baseViewModel.rotationX
                    , anyBaseClassMetadata.graphicsInfo.baseViewModel.rotationY
                    , anyBaseClassMetadata.graphicsInfo.baseViewModel.rotationZ);
                anyBaseClassMetadata.models = {baseView: group};
            }.bind(this);

            switch (anyBaseClassMetadata.graphicsInfo.baseViewModel.modelType) {
                case "OBJ":
                    this.__loadObjectOBJ__(anyBaseClassMetadata.graphicsInfo.baseViewModel.materialsFile
                        , anyBaseClassMetadata.graphicsInfo.baseViewModel.modelFile, true, true, assignObj);
                    break;
                case "3DS":
                    this.__loadObject3DS__(anyBaseClassMetadata.graphicsInfo.baseViewModel.materialsFile
                        , anyBaseClassMetadata.graphicsInfo.baseViewModel.modelFile, true, true, assignObj);
                    break;
                default:

            }
            ;

        } else {
            anyBaseClassMetadata.models = {baseView: undefined};
            this.loadProgressCallback();
            this.__nextLoad__();
        }
    },
    //================================================================================================
    //================================================================================================
    //================================================================================================

    __loadObjectOBJ__: function (materialFile, modelFile, castShadow, receiveShadow, assignModelCallback) {
        var mtlLoader = new THREE.MTLLoader();
        mtlLoader.setResourcePath(GAME.Utils.extractFilePath(materialFile) + "/");
        mtlLoader.setPath(GAME.Utils.extractFilePath(materialFile) + "/");

        mtlLoader.load(GAME.Utils.extractFileName(materialFile), function (materials) {
            // загружен материал
            materials.preload();

            this.loadGauge.incProgress();

            var modelLoader = new THREE.OBJLoader();
            modelLoader.setPath(GAME.Utils.extractFilePath(modelFile) + "/");
            modelLoader.setMaterials(materials);

            // грузим саму модель
            modelLoader.load(GAME.Utils.extractFileName(modelFile), function (object) {
                // объект загружен
                this.__prepareAndPassLoadedOject__(object, castShadow, receiveShadow, assignModelCallback);
            }.bind(this));
        }.bind(this));
    },

    /**
     * Загрузка модели с использованием общего ресурсного пространства текстур
     * @param materialFile
     * @param modelFile
     * @param castShadow
     * @param receiveShadow
     * @param assignModelCallback
     * @private
     */
    __loadObjectOBJCommonResources__: function (materialFile, modelFile, castShadow, receiveShadow, assignModelCallback) {
        var mtlLoader = new THREE.MTLLoader();
        mtlLoader.setResourcePath(this.commonResourcesPath);
        mtlLoader.setPath(GAME.Utils.extractFilePath(materialFile) + "/");

        mtlLoader.load(GAME.Utils.extractFileName(materialFile), function (materials) {
            // загружен материал
            materials.preload();

            this.loadGauge.incProgress();

            var modelLoader = new THREE.OBJLoader();
            modelLoader.setPath(GAME.Utils.extractFilePath(modelFile) + "/");
            modelLoader.setMaterials(materials);

            // грузим саму модель
            modelLoader.load(GAME.Utils.extractFileName(modelFile), function (object) {
                // объект загружен
                this.__prepareAndPassLoadedOject__(object, castShadow, receiveShadow, assignModelCallback);
            }.bind(this));
        }.bind(this));
    },

    __loadObjectFBX__: function (texturesPAth, modelFile, castShadow, receiveShadow, assignModelCallback) {
        var loader = new THREE.FBXLoader();
        loader.setResourcePath(texturesPAth);
        loader.load(modelFile, function (object) {
            this.__prepareAndPassLoadedOject__(object, castShadow, receiveShadow, assignModelCallback);
        }.bind(this));
    },

    __loadObject3DS__: function (resPath, modelFile, castShadow, receiveShadow, assignModelCallback) {
        var loader = new THREE.TDSLoader();
        loader.setResourcePath(resPath);
        loader.load(modelFile, function (object) {
            this.__prepareAndPassLoadedOject__(object, castShadow, receiveShadow, assignModelCallback);
        }.bind(this));

    },

    __LoadTexture__: function (textureFileName, assignTextureCallback) {
        var tex = new THREE.ImageLoader()
            .load(textureFileName, function (image) {
                var texture = new THREE.CanvasTexture(image);
                texture.anisotropy = this.anisotropy;
                assignTextureCallback(texture);
                this.loadProgressCallback(texture);
                this.loadGauge.incProgress();
                this.__nextLoad__();
            }.bind(this));
        // tex.generateMipmaps = false;
    },

    __LoadImage__: function (textureFileName, assignTextureCallback) {
        var img = document.createElement("img");
        var imageLoad = function (event) {
            assignTextureCallback(event.target);
            this.loadProgressCallback(event.target);
            this.loadGauge.incProgress();
            this.__nextLoad__();
        };
        img.onload = imageLoad.bind(this);
        img.setAttribute("src", textureFileName);
    },

    __prepareAndPassLoadedOject__: function (object, castShadow, receiveShadow, assignModelCallback) {
        object.traverse(function (child) {
            child.receiveShadow = receiveShadow;
            child.castShadow = castShadow;
        });
        object.receiveShadow = receiveShadow;
        object.castShadow = castShadow;
        assignModelCallback(object);
        this.loadGauge.incProgress();
        this.loadProgressCallback(object);
        this.__nextLoad__();
    },

    //вызовем следующий метод загрузки, если таковой есть
    __nextLoad__: function () {
        if (this.loadMethodsList.length > 0) {
            var loadMethod = this.loadMethodsList.shift().bind(this);
            loadMethod();
        }
    },

}

