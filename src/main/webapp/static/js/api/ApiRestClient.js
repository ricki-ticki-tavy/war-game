/**
 * Класс, выполняющий запросы к игровому движку
 * @param externalShowErrorMethod
 * @param lockCursor  - форма-блокиратор UI, для показа во время запросов к серверу
 * @param gameApiUrl
 * @constructor
 */
GAME.ApiRestClient = function (contextUUID, lockCursor, externalShowErrorMethod) {
    GAME.apiRestClient = this;
    this.externalShowErrorMethod = externalShowErrorMethod;
    this.gameApiUrl = "game/api/";
    this.lockCursor = lockCursor;

    // this.apiUrl = "game/api/";

    /**
     * UUID текущего контекста
     * @type {String}
     */
    this.gameContextUUID = contextUUID;

};

GAME.ApiRestClient.prototype = {
    constructor: GAME.ApiRestClient,

    /**
     * создать контекст
     * @param isTechical
     */
    createTechnicalContext: function (callback) {
        this.lockCursor.show();
        // контекст технический
        $.ajax({
            url: this.gameApiUrl + "createTechnicalContext",
            data: "",
            success: function (response) {
                this.lockCursor.hide();
                callback(response)
            }.bind(this),
            error: this.__responseFailed__.bind(this, null),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Подключиться к игре
     * @param isTechical
     */
    connectToGame: function (contextId, callback, errorCallback) {
        this.lockCursor.show();
        // контекст технический
        $.ajax({
            url: this.gameApiUrl + "connectToGame",
            data: "contextId=" + contextId,
            success: this.__parseSuccessResponse__.bind(this, callback, errorCallback),
            error: this.__responseFailed__.bind(this, errorCallback),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Подключиться к игре
     * @param isTechical
     */
    decampFromTheArena: function (callback, errorCallback) {
        this.lockCursor.show();
        // контекст технический
        $.ajax({
            url: this.gameApiUrl + "decampFromTheArena",
            data: "contextId=" + this.gameContextUUID,
            success: this.__parseSuccessResponse__.bind(this, callback, errorCallback),
            error: this.__responseFailed__.bind(this, errorCallback),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Загрузить базовые классы воинов
     * @param heroName
     * @param callback
     */
    getBaseWarriorClasses: function (callback, errorCallback) {
        this.lockCursor.show();
        $.ajax({
            url: this.gameApiUrl + "getBaseWarriorClasses",
            data: "contextId=" + this.gameContextUUID,
            success: this.__parseSuccessResponse__.bind(this, callback, errorCallback),
            error: this.__responseFailed__.bind(this, errorCallback),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Загрузка артефактов для игроков
     * @param callback
     * @param errorCallback
     */
    getPlayerArtifacts: function (callback, errorCallback) {
        this.lockCursor.show();
        $.ajax({
            url: this.gameApiUrl + "getPlayerArtifacts",
            data: "contextId=" + this.gameContextUUID,
            success: this.__parseSuccessResponse__.bind(this, callback, errorCallback),
            error: this.__responseFailed__.bind(this, errorCallback),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Загрузка артефактов для воинов
     * @param callback
     * @param errorCallback
     */
    getWarriorArtifacts: function (callback, errorCallback) {
        this.lockCursor.show();
        $.ajax({
            url: this.gameApiUrl + "getWarriorArtifacts",
            data: "contextId=" + this.gameContextUUID,
            success: this.__parseSuccessResponse__.bind(this, callback, errorCallback),
            error: this.__responseFailed__.bind(this, errorCallback),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Загрузка классов оружия
     * @param callback
     * @param errorCallback
     */
    getWeaponClasses: function (callback, errorCallback) {
        this.lockCursor.show();
        $.ajax({
            url: this.gameApiUrl + "getWeaponClasses",
            data: "contextId=" + this.gameContextUUID,
            success: this.__parseSuccessResponse__.bind(this, callback, errorCallback),
            error: this.__responseFailed__.bind(this, errorCallback),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Загрузка заклинаний игрока
     * @param callback
     * @param errorCallback
     */
    getPlayerSpells: function (callback, errorCallback) {
        this.lockCursor.show();
        $.ajax({
            url: this.gameApiUrl + "getPlayerSpells",
            data: "contextId=" + this.gameContextUUID,
            success: this.__parseSuccessResponse__.bind(this, callback, errorCallback),
            error: this.__responseFailed__.bind(this, errorCallback),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Загрузка заклинаний игрока
     * @param callback
     * @param errorCallback
     */
    getWarriorSpells: function (callback, errorCallback) {
        this.lockCursor.show();
        $.ajax({
            url: this.gameApiUrl + "getWarriorSpells",
            data: "contextId=" + this.gameContextUUID,
            success: this.__parseSuccessResponse__.bind(this, callback, errorCallback),
            error: this.__responseFailed__.bind(this, errorCallback),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Загрузить параметры героя и все варианты аммуниции
     * @param heroName
     * @param callback
     */
    findPlayerHero: function (heroName, callback, errorCallback) {
        this.lockCursor.show();
        $.ajax({
            url: this.gameApiUrl + "findPlayerHero",
            data: "contextId=" + this.gameContextUUID + "&heroName=" + heroName,
            success: this.__parseSuccessResponse__.bind(this, callback, errorCallback),
            error: this.__responseFailed__.bind(this, errorCallback),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Загрузить героев и их вариантов аммуниций для игрока
     * @param heroName
     * @param callback
     */
    getPlayerHeroes: function (callback, errorCallback) {
        this.lockCursor.show();
        $.ajax({
            url: this.gameApiUrl + "getPlayerHeroes",
            data: "contextId=" + this.gameContextUUID,
            success: this.__parseSuccessResponse__.bind(this, callback, errorCallback),
            error: this.__responseFailed__.bind(this, errorCallback),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * добавить воина на карту
     * @param warriorBaseClassName
     * @param heroName
     * @param x
     * @param y
     * @param callback
     */
    createWarrior: function (warriorBaseClassName, heroName, x, y, callback, errorCallback) {
        this.lockCursor.show();
        $.ajax({
            url: this.gameApiUrl + "createWarrior",
            data: "contextId=" + this.gameContextUUID
            + "&warriorBaseClassName=" + warriorBaseClassName
            + "&heroName=" + heroName
            + "&x=" + x
            + "&y=" + y,
            success: this.__parseSuccessResponse__.bind(this, callback, errorCallback),
            error: this.__responseFailed__.bind(this, errorCallback),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Удалить воина с карты
     * @param warriorId
     * @param callback
     */
    removeWarrior: function (warriorId, callback, errorCallback) {
        this.lockCursor.show();
        $.ajax({
            url: this.gameApiUrl + "removeWarrior",
            data: "contextId=" + this.gameContextUUID
            + "&warriorId=" + warriorId,
            success: this.__parseSuccessResponse__.bind(this, callback, errorCallback),
            error: this.__responseFailed__.bind(this, errorCallback),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Выдать воину оружие
     * @param warriorId
     * @param weaponName
     * @param callback
     * @param errorCallback
     */
    giveWeaponToWarrior: function (warriorId, weaponName, callback, errorCallback) {
        this.lockCursor.show();
        $.ajax({
            url: this.gameApiUrl + "giveWeaponToWarrior",
            data: "contextId=" + this.gameContextUUID
            + "&warriorId=" + warriorId
            + "&weaponName=" + weaponName,
            success: this.__parseSuccessResponse__.bind(this, callback, errorCallback),
            error: this.__responseFailed__.bind(this, errorCallback),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Выдать артифакт игроку
     * @param warriorId
     * @param weaponName
     * @param callback
     * @param errorCallback
     */
    giveArtifactToPlayer: function (artifactName, callback, errorCallback) {
        this.lockCursor.show();
        $.ajax({
            url: this.gameApiUrl + "giveArtifactToPlayer",
            data: "contextId=" + this.gameContextUUID
            + "&artifactName=" + artifactName,
            success: this.__parseSuccessResponse__.bind(this, callback, errorCallback),
            error: this.__responseFailed__.bind(this, errorCallback),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Забрать у воина оружие
     * @param warriorId
     * @param weaponId
     * @param callback
     * @param errorCallback
     */
    dropWeaponByWarrior: function (warriorId, weaponId, callback, errorCallback) {
        this.lockCursor.show();
        $.ajax({
            url: this.gameApiUrl + "dropWeaponByWarrior",
            data: "contextId=" + this.gameContextUUID
            + "&warriorId=" + warriorId
            + "&weaponId=" + weaponId,
            success: this.__parseSuccessResponse__.bind(this, callback, errorCallback),
            error: this.__responseFailed__.bind(this, errorCallback),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Переместить воина
     * @param warriorId
     * @param x
     * @param y
     * @param callback
     * @param errorCallback
     */
    moveWarriorTo: function (warriorId, x, y, callback, errorCallback) {
        this.lockCursor.show();
        $.ajax({
            url: this.gameApiUrl + "moveWarriorTo",
            data: "contextId=" + this.gameContextUUID
            + "&warriorId=" + warriorId
            + "&x=" + x
            + "&y=" + y
            ,
            success: this.__parseSuccessResponse__.bind(this, callback, errorCallback),
            error: this.__responseFailed__.bind(this, errorCallback),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Отмена перемещения воина
     * @param warriorId
     * @param callback
     * @param errorCallback
     */
    rollbackMove: function (warriorId, callback, errorCallback) {
        this.lockCursor.show();
        $.ajax({
            url: this.gameApiUrl + "rollbackMove",
            data: "contextId=" + this.gameContextUUID
            + "&warriorId=" + warriorId
            ,
            success: this.__parseSuccessResponse__.bind(this, callback, errorCallback),
            error: this.__responseFailed__.bind(this, errorCallback),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * сохранить вариант экипировки воина
     * @param warriorId
     * @param ammunitionName
     * @param callback
     * @param errorCallback
     */
    saveWarriorAmmunition: function (warriorId, ammunitionName, callback, errorCallback) {
        this.lockCursor.show();
        $.ajax({
            url: this.gameApiUrl + "saveWarriorAmmunition",
            data: "contextId=" + this.gameContextUUID
            + "&warriorId=" + warriorId
            + "&ammunitionName=" + ammunitionName,
            success: this.__parseSuccessResponse__.bind(this, callback, errorCallback),
            error: this.__responseFailed__.bind(this, errorCallback),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * переименовать вариант экипировки воина
     * @param warriorId
     * @param oldAmmunitionName
     * @param newAmmunitionName
     * @param callback
     * @param errorCallback
     */
    renameWarriorAmmunition: function (warriorId, oldAmmunitionName, newAmmunitionName, callback, errorCallback) {
        this.lockCursor.show();
        $.ajax({
            url: this.gameApiUrl + "renameWarriorAmmunition",
            data: "contextId=" + this.gameContextUUID
            + "&warriorId=" + warriorId
            + "&oldAmmunitionName=" + oldAmmunitionName
            + "&newAmmunitionName=" + newAmmunitionName,
            success: this.__parseSuccessResponse__.bind(this, callback, errorCallback),
            error: this.__responseFailed__.bind(this, errorCallback),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * удалить вариант экипировки воина
     * @param warriorId
     * @param ammunitionName
     * @param callback
     * @param errorCallback
     */
    removeWarriorAmmunition: function (warriorId, ammunitionName, callback, errorCallback) {
        this.lockCursor.show();
        $.ajax({
            url: this.gameApiUrl + "removeWarriorAmmunition",
            data: "contextId=" + this.gameContextUUID
            + "&warriorId=" + warriorId
            + "&ammunitionName=" + ammunitionName,
            success: this.__parseSuccessResponse__.bind(this, callback, errorCallback),
            error: this.__responseFailed__.bind(this, errorCallback),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Запросить всех воинов на карте по одному или всем игрокам
     * @param heroName
     * @param callback
     */
    getPlayerWarriors: function (ownedByPlayerName, callback, errorCallback) {
        this.lockCursor.show();
        $.ajax({
            url: this.gameApiUrl + "getPlayerWarriors",
            data: "contextId=" + this.gameContextUUID
            + "&ownedByPlayerName=" + warriorId,
            success: this.__parseSuccessResponse__.bind(this, callback, errorCallback),
            error: this.__responseFailed__.bind(this, errorCallback),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Запросить всех воинов на карте по одному или всем игрокам
     * @param heroName
     * @param callback
     */
    createGame: function (mapName
        , gameName
        , hidden
        , maxStartCreaturePerPlayer
        , maxSummonedCreaturePerPlayer
        , movesCountPerTurnForEachPlayer
        , startMannaPoints
        , maxMannaPoints
        , restorationMannaPointsPerTotalRound
        , maxPlayerRoundTime
        , warriorSize
        , callback
        , errorCallback) {
        this.lockCursor.show();
        $.ajax({
            url: this.gameApiUrl + "createGame",
            data: "mapName=" + mapName
            + "&gameName=" + gameName
            + "&hidden=" + hidden
            + "&maxStartCreaturePerPlayer=" + maxStartCreaturePerPlayer
            + "&maxSummonedCreaturePerPlayer=" + maxSummonedCreaturePerPlayer
            + "&movesCountPerTurnForEachPlayer=" + movesCountPerTurnForEachPlayer
            + "&startMannaPoints=" + startMannaPoints
            + "&maxMannaPoints=" + maxMannaPoints
            + "&restorationMannaPointsPerTotalRound=" + restorationMannaPointsPerTotalRound
            + "&maxPlayerRoundTime=" + maxPlayerRoundTime
            + "&warriorSize=" + warriorSize,
            success: this.__parseSuccessResponse__.bind(this, callback, errorCallback),
            error: this.__responseFailed__.bind(this, errorCallback),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Запрос текущего игрового контекста пользователя
     * @param callback
     */
    getExistingGameContext: function (callback, errorCallback) {
        this.lockCursor.show();
        $.ajax({
            url: this.gameApiUrl + "getExistingGameContext",
            data: "",
            success: this.__parseSuccessResponse__.bind(this, callback, errorCallback),
            error: this.__responseFailed__.bind(this, errorCallback),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Запрос текущего игрового контекста и всех игровых данных
     * @param callback
     */
    getGameFullData: function (callback, errorCallback) {
        this.lockCursor.show();
        $.ajax({
            url: this.gameApiUrl + "getGameFullData",
            data: "",
            success: this.__parseSuccessResponse__.bind(this, callback, errorCallback),
            error: this.__responseFailed__.bind(this, errorCallback),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Запрос списка идущих и ожидающих подключения игр
     * @param callback
     */
    getActiveGameList: function (callback, errorCallback) {
        this.lockCursor.show();
        $.ajax({
            url: this.gameApiUrl + "getActiveGameList",
            data: "",
            success: this.__parseSuccessResponse__.bind(this, callback, errorCallback),
            error: this.__responseFailed__.bind(this, errorCallback),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Установить видимость игры для других игроков
     * @param hidden
     * @param callback
     */
    setGameVisibility: function (visibility, callback, errorCallback) {
        this.lockCursor.show();
        $.ajax({
            url: this.gameApiUrl + "setGameVisibility",
            data: "contextId=" + this.gameContextUUID
            + "&hidden=" + !visibility,
            success: this.__parseSuccessResponse__.bind(this, callback, errorCallback),
            error: this.__responseFailed__.bind(this, errorCallback),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * УЗавершить ход и передать следующему игроку
     * @param callback
     */
    endTurn: function (callback, errorCallback) {
        this.lockCursor.show();
        $.ajax({
            url: this.gameApiUrl + "endTurn",
            data: "contextId=" + this.gameContextUUID,
            success: this.__parseSuccessResponse__.bind(this, callback, errorCallback),
            error: this.__responseFailed__.bind(this, errorCallback),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Запросить список активных пользователей чата
     * @param callback
     */
    getActiveUsersList: function (callback, errorCallback) {
        this.lockCursor.show();
        $.ajax({
            url: this.gameApiUrl + "getActiveUsersList",
            data: "",
            success: this.__parseSuccessResponse__.bind(this, callback, errorCallback),
            error: this.__responseFailed__.bind(this, errorCallback),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Запросить список карт
     * @param callback
     */
    getMapsList: function (callback, errorCallback) {
        this.lockCursor.show();
        $.ajax({
            url: this.gameApiUrl + "getMapsList",
            data: "",
            success: this.__parseSuccessResponse__.bind(this, callback, errorCallback),
            error: this.__responseFailed__.bind(this, errorCallback),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Сообщить о готовности или НЕ готовности игрока к сражению
     * @param callback
     */
    playerReadyToPlay: function (readyToPlay, callback, errorCallback) {
        this.lockCursor.show();
        $.ajax({
            url: this.gameApiUrl + "playerReadyToPlay",
            data: "readyToPlay=" + readyToPlay,
            success: this.__parseSuccessResponse__.bind(this, callback, errorCallback),
            error: this.__responseFailed__.bind(this, errorCallback),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Разбор нормального ответа от вызванного сервиса. В нем может быть ошибка
     */
    __parseSuccessResponse__: function (callback, errorCallback, response) {
        this.lockCursor.hide();
        if (response.failed) {
            this.externalShowErrorMethod(response.errorCode + ": " + response.errorMessage, errorCallback);
        } else {
            if (callback){
                callback(response);
            }
        }
    },
    //=============================================================================================

    __responseFailed__: function (callback, result) {
        this.lockCursor.hide();
        if (result.responseText && result.responseText.startsWith("<!DOCTYPE html>")) {
            // это страница логина. Бесполезно париться: переходим на странницу логина
            redirect = function () {
                window.document.location.reload();
            };
            this.externalShowErrorMethod("Сессия потеряна. Необходима повторная авторизация", redirect);
        } else if (result.status == 400) {
            this.externalShowErrorMethod(result.responseText, callback);
        } else if (result.responseJSON.message) {
            this.externalShowErrorMethod(result.responseJSON.message, callback);
        } else {
            this.externalShowErrorMethod(result.responseJSON.error, callback);
        }

    },
    //=============================================================================================

};
