GAME.GameMetadata = function(externalShowErrorMethod, gameApiUrl, loadHeroes){
    GAME.gameMetadata = this;

    this.loadHeroes = loadHeroes;

    /**
     * Методо тображения ошибки
     */
    this.showErrorMethod = externalShowErrorMethod;//.bind(this);

    this.gameApiUrl = gameApiUrl;

    /**
     * Базовые классы воинов
     */
    this.warriorsBaseClasses;

    /**
     * Классы оружия
     */
    this.weaponClasses;

    /**
     * Артефакты для героев
     */
    this.warriorArtifacts;

    /**
     * Заклинания игрока
     */
    this.playerSpells;

    /**
     * Заклинания воинов
     */
    this.warriorSpells;

    /**
     * Артефакты для игроков
     */
    this.playerArtifacts;

    /**
     * Метаданные воинов, нанятых в отряд героя
     */
    this.playerHeroes;

    /**
     * Контекст пользователя
     */
    this.gameContextUUID;

    /**
     * массив методов загрузки данных, выполнеямых последовательно методом LoadAll()
     */
    this.loadMethodsList = [];


};

GAME.GameMetadata.prototype = {
  constructor: GAME.GameMetadata,

    /**
     * Загрузить все метаданные
     */
    loadAll: function(loadMetadataIsDoneCallback, loadGauge){
        this.loadGauge = loadGauge;
        this.loadMethodsList.push(this.__loadWarriorBaseClasses__.bind(this));
        this.loadMethodsList.push(this.__loadWeaponClasses__.bind(this));
        this.loadMethodsList.push(this.__loadPlayerSpells__.bind(this));
        this.loadMethodsList.push(this.__loadWarriorSpells__.bind(this));
        this.loadMethodsList.push(this.__loadWarriorArtifacts__.bind(this));
        this.loadMethodsList.push(this.__loadPlayerArtifacts__.bind(this));

        if (this.loadHeroes) {
            this.loadMethodsList.push(this.__loadHeroes__.bind(this));
        }


        this.loadMethodsList.push(loadMetadataIsDoneCallback);
        this.__resultReceived__(undefined);
    },
    //=========================================================================================

    /**
     * Поиск индекса в метаданных базового класса по его названию
     * @param name
     * @returns {*}
     */
    findWarriorBaseClassIndexByName: function(name){
        for (var index = 0; index < this.warriorsBaseClasses.length; index++){
            if (this.warriorsBaseClasses[index].title == name) {
                return this.warriorsBaseClasses[index];
            }
        }
        return null;
    },
    //=========================================================================================

    /**
     * Создать 3D объект воина по имени класса
     * @param name
     */
    createWarriorObject3DByWarriorClassName: function(warriorBaseClass){
        const metadata = this.findWarriorBaseClassIndexByName(warriorBaseClass);

        const object3D = metadata.models.baseView.clone();
        object3D.traverse(function (child) {
            if (child.material) {
                child.material = child.material.clone();
            }
        }.bind(this));

        return object3D;
    },
    //=========================================================================================

    /**
     * Поиск метаданных в метаданных базового класса по его названию
     * @param name
     * @returns {*}
     */
    findWarriorBaseClassByName: function(name){
        return this.warriorsBaseClasses.find(warriorsBaseClass => {return warriorsBaseClass.title == name});
    },
    //=========================================================================================

    /**
     * Найти оружие по названию
     * @param weaponName
     */
    findWeaponByName: function(weaponName){
          return this.weaponClasses.find(weaponClass => weaponClass.title == weaponName);
    },
    //=========================================================================================

    /**
     * Найти заклинание воина по названию
     * @param weaponName
     */
    findWarriorsSpellByName: function(spellName){
          return this.warriorSpells.find(spellClass => spellClass.title == spellName);
    },
    //=========================================================================================

    /**
     * Загрузка базовых классов воинов
     */
    __loadWarriorBaseClasses__: function(callback){
        if (callback != undefined){
            this.loadMethodsList.push(callback);
        }
        GAME.apiRestClient.getBaseWarriorClasses(this.__resultReceived__.bind(this));
    },
    //=========================================================================================

    /**
     * Загрузка классов оружия
     */
    __loadWeaponClasses__: function(callback){
        if (callback != undefined){
            this.loadMethodsList.push(callback);
        }
        GAME.apiRestClient.getWeaponClasses(this.__resultReceived__.bind(this));
    },
    //=========================================================================================

    /**
     * Загрузка заклинаний игрока
     */
    __loadPlayerSpells__: function(callback){
        if (callback != undefined){
            this.loadMethodsList.push(callback);
        }
        GAME.apiRestClient.getPlayerSpells(this.__resultReceived__.bind(this));
    },
    //=========================================================================================

    /**
     * Загрузка заклинаний игрока
     */
    __loadWarriorSpells__: function(callback){
        if (callback != undefined){
            this.loadMethodsList.push(callback);
        }
        GAME.apiRestClient.getWarriorSpells(this.__resultReceived__.bind(this));
    },
    //=========================================================================================

    /**
     * Загрузка артефактов для воинов
     */
    __loadWarriorArtifacts__: function(callback){
        if (callback != undefined){
            this.loadMethodsList.push(callback);
        }
        GAME.apiRestClient.getWarriorArtifacts(this.__resultReceived__.bind(this));
    },
    //=========================================================================================

    /**
     * Загрузка артефактов для игроков
     */
    __loadPlayerArtifacts__: function(callback){
        if (callback != undefined){
            this.loadMethodsList.push(callback);
        }
        GAME.apiRestClient.getPlayerArtifacts(this.__resultReceived__.bind(this));
    },
    //=========================================================================================

    /**
     * Загрузка списка героев игрока
     */
    __loadHeroes__: function(callback){
        if (callback != undefined){
            this.loadMethodsList.push(callback);
        }
        GAME.apiRestClient.getPlayerHeroes(this.__resultReceived__.bind(this));
    },
    //=========================================================================================

    /**
     * прием всех ответов на запрос загрузки данных
     * @param response
     */
    __resultReceived__: function(response){
        if (response != undefined && response.failed ){
            this.showErrorMethod(response.errorMessage);
        } else {
            // а тут разберем что за ответ и приткнем данные куда надо
            this.loadGauge.incProgress();
            if (response != undefined) {
                switch (response.doneMethod) {
                    case "getBaseWarriorClasses" : {
                        this.warriorsBaseClasses = JSON.parse(response.result);
                        this.warriorsBaseClasses.findWarriorBaseClassByName = this.findWarriorBaseClassByName.bind(this);
                        break;
                    }
                    case "getWeaponClasses" : {
                        this.weaponClasses = JSON.parse(response.result);
                        break;
                    }
                    case "getPlayerSpells" : {
                        this.playerSpells = JSON.parse(response.result);
                        break;
                    }
                    case "getWarriorSpells" : {
                        this.warriorSpells = JSON.parse(response.result);
                        break;
                    }
                    case "getWarriorArtifacts" : {
                        this.warriorArtifacts = JSON.parse(response.result);
                        break;
                    }
                    case "getPlayerArtifacts" : {
                        this.playerArtifacts = JSON.parse(response.result);
                        break;
                    }
                    case "getPlayerHeroes" : {
                        this.playerHeroes = JSON.parse(response.result);
                        break;
                    }
                }
            }

            // а теперь вызовем следующий метод загрузки, если таковой есть
            if (this.loadMethodsList.length > 0){
                var loadMethod = this.loadMethodsList.shift().bind(this);
                loadMethod();
            }
        }
    },
    //=========================================================================================


};
