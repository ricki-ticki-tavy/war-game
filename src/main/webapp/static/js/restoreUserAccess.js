var registerResult;

function showMessage(message) {
    window.document.getElementById("message").innerHTML = message;
    $("#dialog").dialog("open");
}

function createOverlay() {
    $('body').append('<div id="requestOverlay" class="overlay"></div>');
    $("#requestOverlay").hide();
}

function doRequestRestore() {
    $("#requestOverlay").show();
    var params = "userName=" + window.document.getElementById("username").value
        + "&eMail=" + window.document.getElementById("e-mail").value
        + "&captcha=" + window.document.getElementById("captcha").value;

    $.ajax({
        type: "POST",
        url: "security/restoreAccountAccess",
        data: params,
        success: responseSuccess.bind(this),
        error: responseFailed.bind(this),
        dataType: "json"
    });
}

function responseFailed(result) {
    $("#requestOverlay").hide();
    showMessage(result.responseJSON.error);
}

function responseSuccess(result) {
    $("#requestOverlay").hide();
    registerResult = result;
    if (result.failed) {
        showMessage(result.errorMessage);
    } else {
        showMessage("На почтовый адрес пользователя выслано письмо с дальнейшими инструкциями.")
    }
}

$(function () {
    $("#dialog").dialog({
        modal: true,
        autoOpen: false,
        width: "550px",
        buttons: {
            Ok: function () {
                $(this).dialog("close");
                if (!registerResult.failed) {
                    // успешно
                    window.location.replace("login");
                }
            }
        }
    });
});

