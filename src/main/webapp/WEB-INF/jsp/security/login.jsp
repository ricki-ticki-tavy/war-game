<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="css/login.css" />
</head>
<body>

<div class="mainContainer">
    <div class="loginFormContainer anyFormContainer">
        <form method="POST" action="${contextPath}/signin" class="form-signin">
            <table class="loginFormTable">
                <tr>
                    <td colspan="2" class="loginFormHeader">
                        Вход
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <span>${msg}</span>

                    </td>
                </tr>
                <tr>
                    <td class="loginFormFieldTitle">
                        Логин :
                    </td>
                    <td>
                        <input name="username" type="text" class="form-control" placeholder="пользователь"
                               autofocus="true"/>
                    </td>
                </tr>
                <tr>
                    <td class="loginFormFieldTitle">
                        Пароль :
                    </td>
                    <td>
                        <input name="password" type="password" class="form-control" placeholder="пароль"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <span>${errorMsg}</span>

                    </td>
                </tr>
                <tr class="loginFormFieldButtonContainer">
                    <td colspan="2">
                        <button class="resetPasswordButton button" type="button" onclick="window.location='${contextPath}/restoreUserAccess'">забыли пароль?</button>
                        <button class="button registerButton" type="button" onclick="window.location='${contextPath}/register'">Регистрация</button>
                        <button class="button loginButton" type="submit">Войти</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
</body>

</html>


