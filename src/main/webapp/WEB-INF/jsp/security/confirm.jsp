<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="css/login.css" />
    <link rel="stylesheet" href="css/jquery-ui.css">
    <script src="js/jquery-3.3.1.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/confirmUser.js"></script>
</head>
<body onload="doConfirm()">

<div id="dialog" title="Подтверждение регистрации пользователя">
    <p id="message">.</p>
</div>

</body>

</html>


