<%@ page import="web.socket.controller.WebSocketsController" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<%@ include file="mainMenu/imports.jsp" %>
<html lang="en">
<head>
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC|Caveat|Marck+Script" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/menu.css"/>
    <%@ include file="mainMenu/styles.jsp" %>
    <link rel="stylesheet" type="text/css" href="css/game.css"/>
    <link rel="stylesheet" type="text/css" href="css/tavern.css"/>
    <link rel="stylesheet" type="text/css" href="css/iconsMini.css"/>

    <style>
    </style>
</head>
<body onload="new GAME.Tavern('<%=SecurityContextHolder.getContext().getAuthentication().getName()%>'
        , '<%=WebSocketsController.CONTEXT_PUBLIC_MESSAGES_CHANNEL%>'
        , '<%=WebSocketsController.CONTEXT_PRIVATE_MESSAGES_CHANNEL%>'
        , '<%=WebSocketsController.TOPIC_PREFIX%>').start();">
<div class="mainContainer">
    <table class="mainContainer">
        <tr style="height: 30px">
            <td colspan="3">
                <%@ include file="mainMenu/menu.jsp" %>
            </td>
        </tr>
        <tr style="height: 25px" class="blackStyle">
            <td style="width: 200px">
                <div style="padding: 3px 60px">
                    Пользователи
                </div>
            </td>
            <td style="border-right: 2px solid #303030;">
                <div style="padding: 3px 90px">
                    Чат
                </div>
            </td>
            <td class>
                <div style="padding: 3px 60px">
                    Вызовы на бой
                </div>
            </td>
        </tr>
        <tr>
            <td id="containerForTavern" colspan="2" class="chatFormContainer">
                <%@ include file="templates/chat/chatForm.jsp" %>
            </td>
            <td style="width: 300px" id="challengesToFightContainer">
                <%@ include file="templates/tavern/challengeToFightForm.jsp" %>
                <%@ include file="templates/tavern/waitForJoinForm.jsp" %>

                <table class="challengesToFightTable" id="challengesToFightListForm">
                    <tr>
                        <td>
                            <div class="challengeToFight" id="challengesToFightList">

                            </div>
                        </td>
                    </tr>
                    <tr style="height: 30px">
                        <td>
                            <div class="buttonsChallengesToFightContainer">
                                <div id="buttonChallengeToFight" class="buttonChallengeToFight">
                                    <div id="buttonChallengeToFightImg" class="miniIcon iconCrossedOverSword" style="left: 0; float: left; margin-right: 10px; margin-bottom: -4px; margin-bottom: 7px;"></div>
                                    <span id="buttonChallengeToFightText">Бросить вызов</span>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>

<script src="js/jquery-3.3.1.js"></script>
<script src="js/core/defineGameGlobal.js"></script>
<script src="js/core/HtmlLockScreenCursor.js"></script>
<%@ include file="common/wsImport.jsp" %>

<script src="js/core/Utils.js"></script>
<script src="js/api/ApiRestClient.js"></script>
<script src="js/tavern/ChatForm.js"></script>
<script src="js/tavern/Tavern.js"></script>
</body>

</html>
