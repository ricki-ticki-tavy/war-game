<%@ page contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html>
<%@ include file="mainMenu/imports.jsp" %>
<html lang="en">
<head>
    <%@ include file="mainMenu/styles.jsp" %>
</head>
<body>

<%@ include file="mainMenu/menu.jsp" %>

<div class="container">
    <div class="starter-template">
    </div>

</div>

<script src="js/jquery-3.3.1.js"></script>
<script type="text/javascript" src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="js/core/defineGameGlobal.js"></script>
<%@ include file="common/wsImport.jsp" %>

</body>

</html>
