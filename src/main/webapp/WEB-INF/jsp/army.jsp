<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC|Caveat|Marck+Script" rel="stylesheet">
    <style>
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
            overflow: hidden;
        }
    </style>
</head>
<body onload="main();">
    <script src="js/jquery-3.3.1.js"></script>
    <script src="js/three/inflate.min.js"></script>
    <script src="js/three/three.js"></script>
    <script src="js/core/defineGameGlobal.js"></script>
    <%@ include file="common/wsImport.jsp" %>
    <script src="js/core/Utils.js"></script>
    <script src="js/core/HtmlLockScreenCursor.js"></script>
    <script src="js/core/ItemsGroup.js"></script>

    <script src="js/three/shaders/SSAOShader.js"></script>
    <script src="js/three/postprocessing/EffectComposer.js"></script>
    <script src="js/three/postprocessing/ShaderPass.js"></script>
    <script src="js/three/postprocessing/SSAOPass.js"></script>
    <script src="js/three/shaders/CopyShader.js"></script>
    <script src="js/three/SimplexNoise.js"></script>
    <script src="js/three/objects/Fire.js"></script>

    <script src="js/three/loaders/FBXLoader.js"></script>
    <script src="js/three/loaders/MTLLoader.js"></script>
    <script src="js/three/loaders/OBJLoader.js"></script>
    <script src="js/three/loaders/TDSLoader.js"></script>
    <%--<script src="js/three/OrbitControls.js"></script>--%>
    <script src="js/three/stats.min.js"></script>

    <script src="js/gui/LoadGauge.js"></script>
    <script src="js/gui/FlowCamera.js"></script>
    <script src="js/gui/GuiButton.js"></script>
    <script src="js/gui/GuiForm.js"></script>
    <script src="js/gui/GuiTextPaper.js"></script>
    <script src="js/gui/GuiText.js"></script>
    <script src="js/gui/GuiImage.js"></script>
    <script src="js/gui/GuiFormManager.js"></script>
    <script src="js/gui/MainMenu.js"></script>

    <script src="js/api/ApiRestClient.js"></script>
    <script src="js/api/GameMetadata.js"></script>
    <script src="js/api/ModelsLibrary.js"></script>
    <script src="js/objects/DancingFire.js"></script>
    <script src="js/effect/animator/WayController.js"></script>

    <script src="js/armyEditor/ArmyEditorDecorator.js"></script>
    <script src="js/armyEditor/ArmyEditor.js"></script>
    <script src="js/armyEditor/ArmyEditorHeroStone.js"></script>
    <script src="js/armyEditor/MercenaryArbor.js"></script>
    <script src="js/armyEditor/LightsBugHolder.js"></script>
    <script src="js/armyEditor/RecruitHeroForm.js"></script>
    <script src="js/armyEditor/EquipWarriorForm.js"></script>
    <script src="js/armyEditor/CampWarriorSlot.js"></script>
    <script src="js/armyEditor/CampaignCamp.js"></script>


    <script src="js/armyEditor/army.js"></script>
</body>

</html>
