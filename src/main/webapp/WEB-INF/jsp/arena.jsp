<%@ page import="org.springframework.security.core.context.SecurityContextHolder" %>
<%@ page import="web.socket.controller.WebSocketsController" %>
<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--<%--%>
<%--Core core = RequestContextUtils.findWebApplicationContext(request).getBean(Core.class);--%>
<%--String userName = SecurityContextHolder.getContext().getAuthentication().getName();--%>
<%--Context context = core.findUserByName(userName).mapFail(playerResult -> (Result<Player>) success(null)).getResult().getContext();--%>
<%--String contextId = context != null && !context.isTechnical() ? context.getContextId() : "";--%>
<%--%>--%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC|Caveat|Marck+Script" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/arena.css"/>
    <link rel="stylesheet" type="text/css" href="css/iconsMini.css"/>
    <style>
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: black;
        }

    </style>
</head>
<body onload="new GAME.Arena('<%=SecurityContextHolder.getContext().getAuthentication().getName()%>'
        , '<%=WebSocketsController.CONTEXT_PRIVATE_MESSAGES_CHANNEL%>'
        , '<%=WebSocketsController.CONTEXT_PUBLIC_MESSAGES_CHANNEL%>'
        , '<%=WebSocketsController.TAVERN_PRIVATE_MESSAGES_CHANNEL%>'
        , '<%=WebSocketsController.ARENA_PUBLIC_MESSAGES_CHANNEL%>'
        , '<%=WebSocketsController.ARENA_PRIVATE_MESSAGES_CHANNEL%>').start();">
<div class="topMenuDiv" id="topDiv">
    <table class="topMenu">
        <tr>
            <td class="menuButtonsPlace">
                <div class="topMenu" id="menuButtonsPlace"></div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="chatContainer" id="chatFormContainer">
                    <%@ include file="templates/chat/chatForm.jsp" %>
                </div>
            </td>
        </tr>
        <tr style="height: 20px;">
            <td>
                <div id="statusContainer" style="width: 100%; color: white">
                </div>
            </td>
        </tr>
    </table>
</div>
<script src="js/jquery-3.3.1.js"></script>
<script src="js/three/inflate.min.js"></script>
<script src="js/three/three.js"></script>
<script src="js/core/defineGameGlobal.js"></script>
<%@ include file="common/wsImport.jsp" %>
<script src="js/core/Utils.js"></script>
<script src="js/core/HtmlLockScreenCursor.js"></script>

<script src="js/three/shaders/SSAOShader.js"></script>
<script src="js/three/postprocessing/EffectComposer.js"></script>
<script src="js/three/postprocessing/ShaderPass.js"></script>
<script src="js/three/postprocessing/SSAOPass.js"></script>
<script src="js/three/shaders/CopyShader.js"></script>
<script src="js/three/SimplexNoise.js"></script>
<script src="js/three/objects/Fire.js"></script>

<script src="js/three/loaders/FBXLoader.js"></script>
<script src="js/three/loaders/MTLLoader.js"></script>
<script src="js/three/loaders/OBJLoader.js"></script>
<script src="js/three/loaders/TDSLoader.js"></script>
<script src="js/three/stats.min.js"></script>

<script src="js/gui/LoadGauge.js"></script>
<script src="js/gui/GuiButton.js"></script>
<script src="js/gui/GuiForm.js"></script>
<script src="js/gui/GuiTextPaper.js"></script>
<script src="js/gui/GuiText.js"></script>
<script src="js/gui/GuiImage.js"></script>
<script src="js/gui/GuiFormManager.js"></script>
<script src="js/gui/FlowCamera.js"></script>

<script src="js/api/ApiRestClient.js"></script>
<script src="js/api/GameMetadata.js"></script>
<script src="js/api/ModelsLibrary.js"></script>
<script src="js/effect/animator/WayController.js"></script>
<script src="js/tavern/ChatForm.js"></script>

<script src="js/core/UserSettingsKeeper.js"></script>
<script src="js/arena/ArenaWarriorObject3D.js"></script>
<script src="js/arena/ArenaWarriorObject.js"></script>
<script src="js/arena/ArenaScene.js"></script>
<script src="js/arena/ArenaDecorator.js"></script>
<script src="js/arena/WarriorInfoPanel.js"></script>
<script src="js/arena/WarriorFlag.js"></script>
<script src="js/arena/WeaponRangeIndicator.js"></script>
<script src="js/arena/Arena.js"></script>
</body>

</html>
