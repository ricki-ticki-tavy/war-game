<%@ page contentType="text/html; charset=UTF-8" %>
<table id="challengeToFightForm" class="challengeToFight">
    <tr class="challengeToFightFormParamRow">
        <td colspan="2">
            <div class="blockHeader">
                Вызов на бой.
            </div>
            <div class="separator"></div>

        </td>
    </tr>
    <tr class="challengeToFightFormParamRow">
        <td class="paramTitle">
            Карта
        </td>
        <td class="paramValue">
            <select id="mapSelector" class="mapSelector">
            </select>
        </td>
    </tr>
    <tr class="challengeToFightFormParamRow">
        <td class="paramTitle">
            Название игры
        </td>
        <td class="paramValue">
            <input autocomplete="off" type="text" value="" id="mapInfoGameName" class="field" class="field" style="width: 100%">
        </td>
    </tr>

    <tr class="challengeToFightFormParamRow">
        <td colspan="2">
            <div class="separator"></div>
            <div class="blockHeader">
                Параметры карты
            </div>

        </td>
    </tr>
    <tr class="challengeToFightFormParamRow">
        <td class="paramTitle">
            Описание
        </td>
        <td id="mapInfoDescription" class="paramValue">

        </td>
    </tr>
    <tr class="challengeToFightFormParamRow">
        <td class="paramTitle">
            Кол-во игроков
        </td>
        <td id="mapInfoPlayersCount" class="paramValue">

        </td>
    </tr>
    <tr class="challengeToFightFormParamRow">
        <td class="paramTitle">
            Размер карты (см)
        </td>
        <td id="mapInfoMapSize" class="paramValue">

        </td>
    </tr>
    <tr class="challengeToFightFormParamRow">
        <td colspan="2">
            <div class="blockHeader">
                Артефакты
            </div>
        </td>
    </tr>
    <tr class="challengeToFightFormParamRow">
        <td class="paramTitle">
            Доступно сразу
        </td>
        <td id="mapInfoArtifactAvailableAtStart" class="paramValue">

        </td>
    </tr>
    <tr class="challengeToFightFormParamRow">
        <td class="paramTitle">
            Открытие новых после кругов
        </td>
        <td id="mapInfoArtifactExtendsAtRounds" class="paramValue">

        </td>
    </tr>
    <tr class="challengeToFightFormParamRow">
        <td colspan="2">
            <div class="blockHeader">
                Игра
            </div>
        </td>
    </tr>
    <tr class="challengeToFightFormParamRow">
        <td class="paramTitle">
            Тип игры
        </td>
        <td class="paramValue">
            <select id="mapInfoGameType" class="mapSelector">
                <option value="free">Открыта для всех</option>
                <option value="private">Приватная</option>
            </select>
        </td>
    </tr>
    <tr class="challengeToFightFormParamRow">
        <td class="paramTitle">
            Ставка
        </td>
        <td class="paramValue">
            <input autocomplete="off" type="text" id="mapInfoBetOnGame" value="0" class="field">
        </td>
    </tr>

    <tr class="challengeToFightFormParamRow">
        <td colspan="2">
            <div class="blockHeader">
                Расширенные настройки
                <span style="cursor: pointer; margin-left: 30px;"
                      id="buttonExtendedMapSettings"
                      onclick="__hideShowAdditionalSettings__();">v</span>
            </div>
        </td>
    </tr>
    <tr id="addRow1" class="challengeToFightFormParamRow" style="display: none">
        <td class="paramTitle">
            Максимальное кол-во воинов в отряде
        </td>
        <td class="paramValue">
            <select id="mapInfoStartWarriorsCount" class="field">
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option selected value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
                <option value="13">13</option>
                <option value="14">14</option>
            </select>
        </td>
    </tr>
    <tr id="addRow2" class="challengeToFightFormParamRow" style="display: none">
        <td class="paramTitle">
            Можно призвать воинов
        </td>
        <td class="paramValue">
            <select id="mapInfoSummonedWarriorsCount" class="field">
                <option value="1">1</option>
                <option selected value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
            </select>

        </td>
    </tr>
    <tr id="addRow3" class="challengeToFightFormParamRow" style="display: none">
        <td class="paramTitle">
            Кол-во воинов, перемещаемых за ход
        </td>
        <td class="paramValue">
            <select id="mapInfoMovesCountPerTurn" class="field">
                <option value="1">1</option>
                <option value="2">2</option>
                <option selected value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
            </select>
        </td>
    </tr>
    <tr id="addRow4" class="challengeToFightFormParamRow" style="display: none">
        <td class="paramTitle">
            Начальное кол-во манны
        </td>
        <td class="paramValue">
            <input autocomplete="off" type="text" value="50" id="mapInfoStartManna" class="field">
        </td>
    </tr>
    <tr id="addRow5" class="challengeToFightFormParamRow" style="display: none">
        <td class="paramTitle">
            Максимальное кол-во манны
        </td>
        <td class="paramValue">
            <input autocomplete="off" type="text" value="200" id="mapInfoMaxManna" class="field">
        </td>
    </tr>
    <tr id="addRow6" class="challengeToFightFormParamRow" style="display: none">
        <td class="paramTitle">
            Восстановление манны за ход
        </td>
        <td class="paramValue">
            <input autocomplete="off" type="text" value="2" id="mapInfoRestorationManna" class="field">
        </td>
    </tr>
    <tr id="addRow7" class="challengeToFightFormParamRow" style="display: none">
        <td class="paramTitle">
            Максимальная длительность хода (сек)
        </td>
        <td class="paramValue">
            <input autocomplete="off" type="text" value="0" id="mapInfoMaxPlayerRoundTimeSec" class="field">
        </td>
    </tr>
    <tr id="addRow8" class="challengeToFightFormParamRow" style="display: none">
        <td class="paramTitle">
            Размер воина (мм)
        </td>
        <td class="paramValue">
            <input autocomplete="off" type="text" value="30" id="mapInfoUnitSizeMm" class="field">
        </td>
    </tr>

    <tr class="challengeToFightFormParamRow">
        <td colspan="2">
            <div class="separator"></div>
            <div class="blockHeader">
                Набор отряда <span id="warriorCounterField"></span>
            </div>

        </td>
    </tr>

    <tr>
        <td colspan="2">
            <%--<div style="width: 100%; height:100%; overflow-x: hidden; overflow-y: auto; background-color: black">--%>
                <table id="warriorsListTable" class="warriorsListTable">
                    <thead>
                    <tr>
                        <th class="selectWarriorTableHeaders" style="width: 280px">
                            <div title="Имя и класс воина" class="miniIcon iconHelm"></div>
                        </th>
                        <th class="selectWarriorTableHeaders" style="width: 40px">
                            <div title="Количество сражений, к которых участвовал воин" class="miniIcon iconFight"></div>
                        </th>
                        <th class="selectWarriorTableHeaders" style="width: 40px">
                            <div title="Количество смертей фоина" class="miniIcon iconSkull"></div>
                        </th>
                        <th class="selectWarriorTableHeaders" style="width: 250px">
                            <div title="Выбранная экипировка" class="miniIcon iconEquipment"></div>
                        </th>
                        <th class="selectWarriorTableHeaders"  style="width: 40px">
                            <div title="Цена за участие воина в сражении" class="miniIcon iconMoney"></div>
                        </th>
                    </tr>
                    </thead>
                    <tbody id="warriorPlatesContainer">
                    </tbody>
                </table>
            <%--</div>--%>
        </td>
    </tr>

    <tr class="challengeToFightFormParamRow">
        <td colspan="2">
            <div class="separator"></div>
            <div class="blockHeader">
                Набор артефактов
            </div>

        </td>
    </tr>

    <tr class="challengeToFightFormParamRow">
        <td colspan="2">
            <div id="artifactsContainer" style="width: 100%">

            </div>

        </td>
    </tr>

    <script language="JavaScript">
        function __hideShowAdditionalSettings__() {
            if (document.getElementById("buttonExtendedMapSettings").expanded) {
                // надо свернуть
                var displayType = "none";
                var index = 8;
                var step = -1;
            } else {
                var displayType = null;
                var index = 1;
                var step = 1;
            }
            var stepsCount = 8;

            const doNextRow = function () {
                if (stepsCount--) {
                    document.getElementById("addRow" + index).style.display = displayType;
                    index += step;
                    window.setTimeout(doNextRow, 15);
                } else {
                    document.getElementById("buttonExtendedMapSettings").expanded = !document.getElementById("buttonExtendedMapSettings").expanded;
                    document.getElementById("buttonExtendedMapSettings").innerHTML = document.getElementById("buttonExtendedMapSettings").expanded
                        ? "^" : "v";
                }
            }.bind(this);
            window.setTimeout(doNextRow, 30);
        }

        document.getElementById("buttonExtendedMapSettings").expanded = false;

    </script>


    <tr class="challengeToFightFormParamRow">
        <td colspan="2">
            <div class="separator"></div>
        </td>
    </tr>
    <tr style="height: 45px">
        <td colspan="2">
            <div class="buttonsChallengesToFightContainer">
                <div class="buttonChallengeToFight" id="buttonChallengeToFightOk">
                    <img src="models/textures/gui/buttons/ok.png" width="32" height="32"
                         alt="Бросить вызов"/> Бросить вызов
                </div>
                <div class="buttonChallengeToFight" id="buttonChallengeToFightCancel"
                     style="padding-left: 60px">
                    <img src="models/textures/gui/buttons/cancel.png" width="32" height="32"
                         alt="Отмена"/> Отмена
                </div>
                <div class="buttonChallengeToFight" id="buttonGoToCampaignCamp"
                     style="padding-left: 60px">
                    <img src="models/textures/gui/buttons/fortress.png" width="32" height="32"
                         alt="Перейти в лагерь отряда для набора и экипировки бойцов"/> В лагерь отряда
                </div>
            </div>
        </td>
    </tr>
</table>
