<%@ page contentType="text/html; charset=UTF-8" %>
<table id="waitForJoinForm" class="challengeToFight">
    <tr class="challengeToFightFormParamRow">
        <td colspan="2">
            <div class="blockHeader">
                Ожидание игроков <span id="waitForJoinCounterField"></span>.
            </div>
            <div class="separator"></div>

        </td>
    </tr>
    <%--<td class="paramTitle">--%>
        <%--Название игры--%>
    <%--</td>--%>

    <tr class="challengeToFightFormParamRow">
        <td colspan="2">
            <div class="blockHeader">
                Присоединившиеся игроки
            </div>

        </td>
    </tr>

    <tr class="challengeToFightFormParamRow">
        <td colspan="2">
            <div id="joinedPlayersList">

            </div>
        </td>
    </tr>

    <tr class="challengeToFightFormParamRow">
        <td colspan="2">
            <div class="separator"></div>
        </td>
    </tr>

    <tr class="challengeToFightFormParamRow">
        <td colspan="2">
            <div style="width: 100%; cursor: pointer; " id="buttonWaitForJoinGoToArena">
                <div class="miniIcon iconArena" style="cursor: pointer; float: left; left: 0px; margin-left: 61px;" ></div>
                <div class = "blockHeader" style ="cursor: pointer; float: left; width: 180px; margin-top: 6px;" >Перейти на арену</div>
            </div>
        </td>
    </tr>


    <tr>
        <td colspan="2">
        </td>
    </tr>

    <tr style="height: 45px">
        <td colspan="2">
            <div class="buttonsChallengesToFightContainer">
                <div class="buttonChallengeToFight" id="buttonWaitForJoinCancel"
                     style="padding-left: 60px">
                    <img src="models/textures/gui/buttons/cancel.png" width="32" height="32"
                         alt="Отмена"/> Отмена
                </div>
            </div>
        </td>
    </tr>
</table>
