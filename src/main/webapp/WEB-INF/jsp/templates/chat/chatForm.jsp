<%@ page contentType="text/html; charset=UTF-8" %>
<style>
    .fullSize {
        width: 100%;
        height: 100%;
    }

    .userListCell {
        width: 200px;
        border-right: 2px solid #202020;
    }

    .sendMessageRow {
        height: 20px;
    }

    .sendButtonCell {
        width: 100px;
    }

    .messagesList {
        width: 100%;
        height: 100%;
        overflow-x: hidden;
        overflow-y: auto;
        margin-left: 15px;
    }

    .inputText{
        background-color: black;
        border-style: solid;
        border-color: yellow;
        border-width: 1px;
        color: white;
    }

    .fullWidth{
        width: 100%;
    }

    .userPlate{
        width: 100%;
        cursor: pointer;
    }

    .userPlateCell{
        padding-bottom: 5px;
    }

    .userPlateDiv{
        height: 26px;
        background-color: #737373;
        color: white;
        border-radius: 12px;
        padding: 3px 30px;
        cursor: pointer;
    }

    .userNameLabel{
        color: yellow;
        cursor: pointer;
        float: left;
        margin-right: 13px;
    }
</style>
<table class="fullSize">
    <tr>
        <td class="userListCell" id="userListCell">
            <div class="fullSize">
                <table id="userList" class="fullWidth">

                </table>
            </div>
        </td>
        <td id="messageCell">
            <div id="messagesListContainer" class="messagesList">
                <table id="messagesList" style="width: 100%; height: 10px">

                </table>
            </div>
        </td>
    </tr>
    <tr class="sendMessageRow">
        <td colspan="2">
            <form class="fullSize" id="sendMessageForm">
                <table class="fullSize">
                    <tr>
                        <td>
                            <input autocomplete="off" class="fullSize inputText" id="inputMessage" type="text">
                        </td>
                        <td id="sendButtonCell" class="sendButtonCell">
                            <input type="submit" value="Отправить" id="buttonSendMessage">
                        </td>
                    </tr>
                </table>
            </form>
        </td>
    </tr>
</table>