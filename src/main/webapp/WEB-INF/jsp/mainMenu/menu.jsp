<%@ page contentType="text/html; charset=UTF-8" %>
<nav class="navbar navbar-inverse" style="margin-bottom: 0px; !important">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">War games</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="#">Главная</a></li>
                <li><a href="#about">Правила</a></li>
                <% if (SecurityContextHolder.getContext().getAuthentication().getAuthorities().contains(new SimpleGrantedAuthority(ROLE_PLAYER_NAME))) { %>
                <li><a href="army">Моя армия</a></li>
                <li><a href="tavern">Таверна</a></li>
                <%--<li><a href="arena">Арена</a></li>--%>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        <%=SecurityContextHolder.getContext().getAuthentication().getName()%>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="profile">Профиль</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/logout">Выход</a></li>
                    </ul>

                </li>
                <% } else {%>
                <li><a href="login">Войти</a></li>
                <%}%>
            </ul>
        </div>
    </div>
</nav>
